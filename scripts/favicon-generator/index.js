const fs = require('fs');
const { NEXT_PUBLIC_HOME_URL } = process.env;
const configDefaults = require('require-directory')(module, 'config');
const config = configDefaults.default;

function generateManifest() {
  try {
    const { output, base_json } = config.manifest;

    const manifest = {
      ...base_json,
      icons: generateFaviconsData()
    };

    fs.writeFileSync(output, JSON.stringify(manifest, null, 2));
  } catch (error) {
    console.log(error);
  }
}

function generateHtml() {}

function generateFavicons() {}

function generateFaviconsData() {
  const { sizes, prefix } = config.icons;
  const URL = NEXT_PUBLIC_HOME_URL;

  return sizes.map((size) => {
    const faviconObject = {
      type: 'image/svg+xml',
      src: [URL, prefix, `favicon${size.sizes}x${size.sizes}.svg`].join('/'),
      sizes: `${size.sizes}x${size.sizes}`
    };
    for (const [key, value] of Object.entries(size)) {
      if(key !== 'sizes') Object.assign(faviconObject, { [key]: value });
    }

    return faviconObject;
  });
}

module.exports.generateFavicons = generateFavicons;
module.exports.generateManifest = generateManifest;
module.exports.generateHtml = generateHtml;
