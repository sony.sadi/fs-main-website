/**
 * Patch next for fix error "Cannot assign to read only property 'children' of object '#<Object>'"
 * Base on bellow PR
 * https://github.com/vercel/next.js/pull/28498
 */
const fs = require('fs');

const nextDocPath = 'node_modules/next/dist/pages/_document.js';

try {
  const fileContent = fs.readFileSync(nextDocPath).toString();
  const patchFileContent = fileContent.replace(
    `} else if (c.props && c.props['children']) {`,
    `} else if (c.props && c.props['children'] && Object.getOwnPropertyDescriptor(c.props, 'children') && Object.getOwnPropertyDescriptor(c.props, 'children').writable) {`
  );
  fs.writeFileSync(nextDocPath, patchFileContent);
} catch (error) {
  console.log(error);
}
