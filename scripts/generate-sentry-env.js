const childProcess = require('child_process');
const fs = require('fs');

const hostName = childProcess.execSync('hostname').toString().trim();
const commitHash = childProcess.execSync('git rev-parse --short HEAD').toString().trim();
const sentryEnv = `SENTRY_ENV=${hostName}\nSENTRY_COMMIT_HASH=${commitHash}`;

fs.writeFileSync('.env.sentry', sentryEnv);