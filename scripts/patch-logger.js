/**
 * Log detail of error 500 to find problem from rendering image
 */
 const fs = require('fs');

 const nextDocPath = 'node_modules/next/dist/server/next-server.js';
 
 try {
    const fileContent = fs.readFileSync(nextDocPath).toString();
    const patchFileContent = fileContent.replace(
      `const fallbackComponents = await this.getFallbackErrorComponents();`,
       `const fallbackComponents = await this.getFallbackErrorComponents();
        console.error('renderErrorToResponse', isWrappedError ? renderToHtmlError.innerError : renderToHtmlError);
       `
    );
    fs.writeFileSync(nextDocPath, patchFileContent);

//    fs.writeFileSync(nextDocPath, patchFileContent);
 } catch (error) {
   console.log(error);
 }
 