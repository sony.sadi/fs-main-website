const fs = require('fs');

fs.copyFileSync('scripts/next-server-patch/next-server.d.ts', 'node_modules/next/dist/next-server/server/next-server.d.ts');
fs.copyFileSync('scripts/next-server-patch/next-server.js', 'node_modules/next/dist/next-server/server/next-server.js');
fs.copyFileSync('scripts/next-server-patch/next-server.js.map', 'node_modules/next/dist/next-server/server/next-server.js.map');