module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    }
  },
  plugins: ['prettier', 'jest', 'cypress'],
  extends: [
    'airbnb',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react/recommended',

    // Prettier plugin and recommended rules
    'prettier/@typescript-eslint',
    'plugin:prettier/recommended',
    'plugin:cypress/recommended',
    'plugin:jest/recommended'
  ],
  rules: {
    // Include .prettierrc rules
    // Severity should be one of the following: 0 = off, 1 = warn, 2 = error
    'prettier/prettier': [
      'error',
      {
        endOfLine: 'auto'
      },
      {
        usePrettierrc: true
      }
    ],
    'max-len': [1, 300, 2],
    'react/display-name': 'off',
    'no-constant-condition': 'off',
    'no-useless-escape': 'off',
    'no-use-before-define': 'off',
    '@typescript-eslint/no-shadow': 'off',
    'no-shadow': 'off',
    'jsx-a11y/anchor-is-valid': [
      'error',
      {
        components: ['Link'],
        specialLink: ['hrefLeft', 'hrefRight'],
        aspects: ['invalidHref', 'preferButton']
      }
    ],
    'import/no-unresolved': 'off',

    // 'no-plusplus': ['error', { allowForLoopAfterthoughts: true }], // i++ in loop
    'no-plusplus': 'off', // i++ in loop
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
        jsx: 'never',
        ts: 'never',
        tsx: 'never'
      }
    ],

    '@typescript-eslint/no-unused-vars': 'off',
    'react/jsx-filename-extension': ['warn', { extensions: ['.js', '.jsx', '.ts', '.tsx'] }],
    'react/require-default-props': ['off'],
    '@typescript-eslint/no-unused-expressions': [
      2,
      { allowShortCircuit: true, allowTernary: true, allowTaggedTemplates: true }
    ],
    'no-unused-expressions': [2, { allowShortCircuit: true, allowTernary: true, allowTaggedTemplates: true }],
    'react/destructuring-assignment': 'off',
    'react/jsx-indent': [2, 2, { indentLogicalExpressions: true }],
    'react/jsx-one-expression-per-line': 'off',
    'react/button-has-type': 'off',
    'consistent-return': 'off',
    'no-param-reassign': 'off',
    camelcase: 'off',
    // 'jsx-closing-bracket-location': 'off', // must fix
    'react/jsx-closing-bracket-location': ['warn', { selfClosing: 'tag-aligned', nonEmpty: 'after-props' }],
    'react/jsx-props-no-spreading': 'off',
    'react/no-array-index-key': 'off',
    'no-underscore-dangle': ['error', { allow: ['_nextRewroteUrl', '__DEV__'] }],
    'jsx-a11y/label-has-associated-control': 'off',
    'import/no-extraneous-dependencies': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    'import/prefer-default-export': 'off',
    'no-useless-constructor': 'off',
    'class-methods-use-this': 'off',
    'react/react-in-jsx-scope': 'off',
    'no-console': ['warn', { allow: ['info', 'error'] }]
  },
  settings: {
    'import/resolver': {
      node: {
        paths: ['src'],
        extensions: ['.js', '.jsx', '.ts', '.tsx']
      }
    },
    jest: {
      version: 26
    }
  },
  env: {
    browser: true,
    amd: true,
    node: true,
    'jest/globals': true
  }
};
