import mockPublicSearch from './api/public-search.json';
import mockBestDeals from './api/best-deals.json';
import mockPopularAreas from './api/popular-areas.json';

jest.mock('@services/http-client/search', () => {
  return {
    getPublicRentals() {
      return Promise.resolve({
        data: mockPublicSearch
      });
    },

    getBestDeals() {
      return Promise.resolve({
        data: mockBestDeals
      });
    },

    getPopularAreas() {
      return Promise.resolve({
        data: mockPopularAreas
      });
    }
  };
});

export {};
