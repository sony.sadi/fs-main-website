import { TransProps } from 'react-i18next';

jest.mock('next-i18next', () => ({
  useTranslation: () => ({
    t: (key: string) => key,
    i18n: {
      language: 'en'
    }
  }),
  Trans: (props: TransProps<never>) => {
    return props.children;
  }
}));

export {};
