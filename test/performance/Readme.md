We are still looking for the perfect performance testing tool, but here are some scripts that have been helpful so far:

## K6
https://k6.io/docs/getting-started/installation/

`k6 run --vus 20 --duration 30s k6_default_serp.js` 



## Siege

### Install
ubuntu: sudo apt install siege -y

### Create test file:
$ `for ((i=2;i<=1000;i++)); do echo "http://propertyscout.co.th.local/en/bangkok/rentals/page-${i}/" >> siege_test_urls1-1k.txt; done;`

### Run load test
$ `sudo siege -t30s -c20 -b -f siege_test_urls1-1k.txt`