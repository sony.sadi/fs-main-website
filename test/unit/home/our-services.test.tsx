import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import OurServices from '@components/home/our-services';
import ourServices from '@assets/data/our-services.json';
import { Language } from '@constants/enum';
import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';

const mockStore = configureMockStore();

beforeAll(async () => {
  await i18next.use(initReactI18next).init({
    lng: Language.US
  });
});

describe('Home - Our Services', () => {
  beforeEach(() => {
    const store = mockStore();

    render(
      <Provider store={store}>
        <OurServices />
      </Provider>
    );
  });

  test('renders services', () => {
    ourServices.forEach((services) => {
      expect(screen.getByText(services.name)).toBeInTheDocument();
      expect(screen.getByText(services.description)).toBeInTheDocument();
    });
  });
});
