import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import preloadAll from 'jest-next-dynamic';
import configureMockStore from 'redux-mock-store';
import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';
import HomeHero from '@components/home/hero';
import { Language } from '@constants/enum';
import { formatViewNumber } from '@utils/format';
import statistics from '@assets/data/hero-statistics.json';
import { getDefaultParams } from '@flexstay.rentals/fs-url-stringify';

const mockStore = configureMockStore();

beforeAll(async () => {
  await i18next.use(initReactI18next).init({
    lng: Language.US
  });
  await preloadAll();
});

describe('Home - Hero', () => {
  beforeEach(() => {
    const store = mockStore({
      search: {
        params: getDefaultParams()
      }
    });

    render(
      <Provider store={store}>
        <HomeHero statistics={statistics} />
      </Provider>
    );
  });

  test('renders statistics', () => {
    statistics.forEach((stat) => {
      expect(screen.getByText(stat.i18nKey)).toBeInTheDocument();
      expect(screen.getByText(formatViewNumber(stat.total))).toBeInTheDocument();
    });
  });
});
