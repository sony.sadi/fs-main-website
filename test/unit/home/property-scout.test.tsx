import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';
import PropertyScout from '@components/home/property-scout';
import propertyScout from '@assets/data/property-scout.json';
import { Language } from '@constants/enum';

const mockStore = configureMockStore();

beforeAll(async () => {
  await i18next.use(initReactI18next).init({
    lng: Language.US
  });
});

describe('Home - Property Scout', () => {
  beforeEach(() => {
    const store = mockStore({
      common: {
        isMobile: false
      }
    });

    render(
      <Provider store={store}>
        <PropertyScout />
      </Provider>
    );
  });

  test('renders offers', () => {
    propertyScout.desktops.forEach((offer) => {
      expect(screen.getByText(offer.title)).toBeInTheDocument();
      expect(screen.getByText(offer.description)).toBeInTheDocument();
    });
  });
});
