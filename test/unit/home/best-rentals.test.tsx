import React from 'react';
import { act, screen, render, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import startCase from 'lodash/startCase';
import { v4 as uuidv4 } from 'uuid';
import BestRentals from '@components/home/best-rentals';
import { getAddress, getPropertyTitle } from '@utils';
import { Language } from '@constants/enum';
import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';
import mockBestDeals from '../../mocks/services/api/best-deals.json';

const mockStore = configureMockStore();
beforeAll(async () => {
  await i18next.use(initReactI18next).init({
    lng: Language.US
  });
});
describe('Home - Best Rentals', () => {
  beforeEach(async () => {
    await act(async () => {
      const store = mockStore({
        common: {
          isMobile: false
        },
        search: {
          propertypad: {
            favouriteRentals: [],
            identifier: uuidv4()
          }
        }
      });

      render(
        <Provider store={store}>
          <BestRentals deals={mockBestDeals.data as any} />
        </Provider>
      );
    });
  });

  test('renders deals', async () => {
    await waitFor(() => {
      mockBestDeals.data.forEach((deal) => {
        expect(screen.getByText(startCase(getPropertyTitle(deal as any, Language.US)))).toBeInTheDocument();
        screen
          .getAllByText(getAddress(deal as any, Language.US))
          .forEach((element) => expect(element).toBeInTheDocument());
      });
    });
  });
});
