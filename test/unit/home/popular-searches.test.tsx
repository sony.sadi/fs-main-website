import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';
import PopularSearches from '@components/home/popular-searches';
import popularSearches from '@assets/data/popular-searches.json';
import { useTranslation } from 'next-i18next';
import { Language } from '@constants/enum';

const mockStore = configureMockStore();

beforeAll(async () => {
  await i18next.use(initReactI18next).init({
    lng: Language.US
  });
});

describe('Home - Popular Searches', () => {
  beforeEach(() => {
    const store = mockStore();

    render(
      <Provider store={store}>
        <PopularSearches />
      </Provider>
    );
  });

  test('renders services', () => {
    popularSearches.forEach(({ link }) => {
      const textRentElement = screen.getByText(link.rent.name);
      expect(textRentElement).toBeInTheDocument();
      // const textSaleElement = screen.getByText(link.sale.name);
      // expect(textSaleElement).toBeInTheDocument();

      expect(textRentElement.closest('a')).toHaveAttribute('href');
      // expect(textSaleElement.closest('a')).toHaveAttribute('href');
    });
  });

  test('Hyper link', () => {
    const { t } = useTranslation();
    expect(screen.getByText(t('searchesShowAll'))).toBeInTheDocument();
    expect(screen.getByText(t('searchesShowAllSales'))).toBeInTheDocument();
  });
});
