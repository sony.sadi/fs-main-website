import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import OurPartner from '@components/home/our-partners';
import ourPartners from '@assets/data/our-partners.json';

const mockStore = configureMockStore();

describe('Home - Our Partners', () => {
  describe('Desktop', () => {
    beforeEach(() => {
      const store = mockStore({
        common: { isMobile: false }
      });

      render(
        <Provider store={store}>
          <OurPartner />
        </Provider>
      );
    });

    test('renders partners', () => {
      screen.getAllByRole('listitem').forEach((element, index) => {
        expect(element).toHaveStyle(`background-image: url(${ourPartners.desktop[index].imageUrl})`);
      });
    });
  });

  describe('Mobile', () => {
    beforeEach(() => {
      const store = mockStore({
        common: { isMobile: true }
      });

      render(
        <Provider store={store}>
          <OurPartner />
        </Provider>
      );
    });

    test('renders partners', () => {
      screen.getAllByRole('listitem').forEach((element, index) => {
        expect(element).toHaveStyle(`background-image: url(${ourPartners.mobile[index].imageUrl})`);
      });
    });
  });
});
