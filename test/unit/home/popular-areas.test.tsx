import React from 'react';
import { act, screen, render, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import preloadAll from 'jest-next-dynamic';
import configureMockStore from 'redux-mock-store';
import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';
import PopularAreas from '@components/home/popular-areas';
import { Language } from '@constants/enum';
import { useTranslation } from 'next-i18next';
import mockPopularAreas from '../../mocks/services/api/popular-areas.json';

const mockStore = configureMockStore();

beforeAll(async () => {
  await i18next.use(initReactI18next).init({
    lng: Language.US
  });
  await preloadAll();
});

describe('Home - Popular Areas', () => {
  beforeEach(async () => {
    await act(async () => {
      const store = mockStore({
        common: {
          isMobile: false
        }
      });

      render(
        <Provider store={store}>
          <PopularAreas areas={mockPopularAreas.data as any} />
        </Provider>
      );
    });
  });

  test('renders areas', async () => {
    await waitFor(() => {
      mockPopularAreas.data.forEach((area) => {
        expect(screen.getByText(area.name[Language.US], { exact: true })).toBeInTheDocument();
        screen.getAllByText(area.teaser[Language.US]).forEach((element) => expect(element).toBeInTheDocument());
      });
    });
  });

  test('Hyper link', () => {
    const { t } = useTranslation();
    expect(screen.getByText(t('searchesShowAll'))).toBeInTheDocument();
    expect(screen.getByText(t('searchesShowAllSales'))).toBeInTheDocument();
  });
});
