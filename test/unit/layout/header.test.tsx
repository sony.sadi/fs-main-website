import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import preloadAll from 'jest-next-dynamic';
import forEach from 'lodash/forEach';

import Header from '@components/layout/header';
import { LOGO, MOBILE_LANGUAGES } from '@constants';
import { Language } from '@constants/enum';
import mobileMenuLinks from '@assets/data/mobile-menu-links.json';
import { useTranslation } from 'next-i18next';
import { v4 as uuidv4 } from 'uuid';
import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';

const mockStore = configureMockStore();

beforeAll(async () => {
  await i18next.use(initReactI18next).init({
    lng: Language.US
  });
  await preloadAll();
});

describe('Layout - Header', () => {
  describe('HeaderDesktop', () => {
    beforeEach(() => {
      const store = mockStore({
        common: { isMobile: false },
        search: {
          propertypad: {
            favouriteRentals: [],
            identifier: uuidv4()
          }
        }
      });

      render(
        <Provider store={store}>
          <Header />
        </Provider>
      );
    });

    test('renders a Flexstay logo', () => {
      expect(screen.getByTestId(LOGO.DATA_TEST_ID)).toBeInTheDocument();
    });

    test.skip('renders a selected language', () => {
      expect(screen.getByTestId(Language.US)).toBeVisible();
    });

    test.skip('renders selectable language when clicking the selected language', () => {
      const selectedFlag = screen.getByTestId(Language.US);
      fireEvent.click(selectedFlag);

      expect(screen.getByTestId(Language.US)).toBeVisible();
      expect(screen.getByTestId(Language.TH)).toBeVisible();
    });
  });

  describe('HeaderMobile', () => {
    beforeEach(() => {
      const store = mockStore({
        common: { isMobile: true, isMobileOnly: true },
        search: {
          propertypad: {
            favouriteRentals: [],
            identifier: uuidv4()
          }
        }
      });

      render(
        <Provider store={store}>
          <Header />
        </Provider>
      );
    });

    test('renders a Flexstay logo', () => {
      expect(screen.getByTestId(LOGO.DATA_TEST_ID)).toBeInTheDocument();
    });

    test('renders a menu bar', () => {
      expect(screen.getByTestId('red-menu-bar')).toBeVisible();
    });

    test.skip('renders a selected language', () => {
      expect(screen.getByText(MOBILE_LANGUAGES[Language.US].text)).toBeInTheDocument();
    });

    test.skip('renders selectable languages when clicking the selected language', () => {
      fireEvent.click(screen.getByTestId('selected-toggle'));

      forEach(MOBILE_LANGUAGES, (lang) => {
        expect(screen.getByText(lang.text)).toBeInTheDocument();
      });
    });

    test('renders links', () => {
      const { t } = useTranslation();
      mobileMenuLinks.forEach((link) => {
        expect(screen.getByText(t(link.i18nKey))).toBeInTheDocument();
      });
    });
  });
});
