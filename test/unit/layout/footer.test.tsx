import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import Footer from '@components/layout/footer';
import { Property, Basics, Contact } from '@interfaces/home';
import { intros, about, property, basics, contact, generateTitle } from '@assets/data/footer.json';
import { LOGO } from '@constants';
import { Language } from '@constants/enum';
import { useTranslation } from 'next-i18next';
import { LocaleNamespace } from '@flexstay.rentals/fs-url-stringify';
import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';
import footerData from '../../mocks/services/api/footer-data.json';

beforeAll(async () => {
  await i18next.use(initReactI18next).init({
    lng: Language.US
  });
});

describe('Layout - Footer', () => {
  beforeEach(() => {
    render(<Footer footerBuilding={footerData as any} />);
  });

  test('renders a Flexstay logo', () => {
    expect(screen.getByTestId(LOGO.DATA_TEST_ID)).toBeInTheDocument();
  });

  test('renders property scout section', () => {
    intros.forEach((intro) => {
      expect(screen.getByText(intro)).toBeInTheDocument();
    });
  });

  test('renders about property scout section', () => {
    (about as Property).items.forEach(({ label }) => {
      expect(screen.getByText(label)).toBeInTheDocument();
    });
  });

  test('renders properties for rent section', () => {
    (property as Property).items.forEach(({ label }) => {
      expect(screen.getByText(label)).toBeInTheDocument();
    });
  });

  test('renders contact us section', () => {
    (basics as Basics[]).forEach(({ content }) => {
      screen.getAllByText(content).forEach((element) => expect(element).toBeInTheDocument());
    });
  });

  test('renders contact offices section', () => {
    (contact as Contact).offices.forEach(({ name, address }) => {
      expect(screen.getByText(name)).toBeInTheDocument();
      expect(screen.getByText(address)).toBeInTheDocument();
    });
  });

  test.each(['rents', 'sales'])('render building data', (serpType) => {
    const { t: tFooter } = useTranslation(LocaleNamespace.Footer);
    const dataCondo = footerData.indemandBuilding.map((value) => {
      return {
        label: value.name ? value.name[Language.US] : ''
      };
    });

    const dataProperties = footerData.popularAreas.map((value) => {
      return {
        label: value.name ? value.name[Language.US] : ''
      };
    });

    expect(screen.getByText(tFooter(generateTitle.title[serpType]))).toBeInTheDocument();

    dataCondo.forEach((condo) => {
      screen.getAllByText(tFooter(generateTitle.property[serpType], { place: condo.label })).forEach((element) => {
        expect(element).toBeInTheDocument();
      });
    });

    dataProperties.forEach((condo) => {
      screen.getAllByText(tFooter(generateTitle.property[serpType], { place: condo.label })).forEach((element) => {
        expect(element).toBeInTheDocument();
      });
    });
  });
});
