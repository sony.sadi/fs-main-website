import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';
import HowItWorks from '@components/how-it-works/how';
import dataSteps from '@assets/data/how-it-works.json';
import { Language } from '@constants/enum';

const mockStore = configureMockStore();

beforeAll(async () => {
  await i18next.use(initReactI18next).init({
    lng: Language.US
  });
});

describe('How Does It Work - How it works', () => {
  beforeEach(() => {
    const store = mockStore({
      common: { isMobile: false }
    });

    render(
      <Provider store={store}>
        <HowItWorks />
      </Provider>
    );
  });

  test('renders questions and answers correctly', () => {
    dataSteps.forEach(({ numberStep, description }) => {
      expect(screen.getByText(numberStep)).toBeInTheDocument();
      expect(screen.getByText(description)).toBeInTheDocument();
    });
  });
});
