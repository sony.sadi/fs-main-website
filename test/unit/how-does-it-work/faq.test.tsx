import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import FAQ from '@components/how-it-works/faq';
import faqs from '@assets/data/how-it-works-tenant-FAQ.json';

const mockStore = configureMockStore();

describe('How Does It Work - FAQ', () => {
  beforeEach(() => {
    const store = mockStore({
      common: { isMobile: true }
    });

    render(
      <Provider store={store}>
        <FAQ />
      </Provider>
    );
  });

  test('renders steps correctly', () => {
    faqs.forEach(({ question, answers }) => {
      expect(screen.getByText(question)).toBeInTheDocument();

      answers.forEach((answer) => {
        expect(screen.getByText(answer)).toBeInTheDocument();
      });
    });
  });
});
