import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import ContactUsInfo from '@components/contact-us/contact';
import contactsData from '@assets/data/contacts.json';

const mockStore = configureMockStore();

describe('Contact Us - Information', () => {
  beforeEach(() => {
    const store = mockStore({
      common: { isMobile: false }
    });

    render(
      <Provider store={store}>
        <ContactUsInfo />
      </Provider>
    );
  });

  test('renders name and content correctly', () => {
    contactsData.forEach(({ info }) => {
      expect(screen.getByText(info.name)).toBeInTheDocument();
      screen.getAllByText(info.content).forEach((element) => expect(element).toBeInTheDocument());
    });
  });
});
