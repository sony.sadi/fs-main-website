import React from 'react';
import { screen, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';
import Link from '@components/core/link';
import { Language, LocaleNamespace } from '@constants/enum';
import {
  stringifyUrl,
  Amenity,
  PropertyType,
  TransportationSystem,
  getDefaultParams,
  MinLease,
  SortType,
  BuildingType
} from '@flexstay.rentals/fs-url-stringify';
import { SearchRequest } from '@interfaces/search';
import * as commonEn from '../../../public/locales/en/common.json';

describe('Serp URL - noindex serp link', () => {
  const t = i18next.getFixedT(Language.US, LocaleNamespace.Common);
  const defaultParams = getDefaultParams();

  beforeEach(async () => {
    await i18next.use(initReactI18next).init({
      lng: Language.US,
      ns: [LocaleNamespace.Common],
      resources: {
        [Language.US]: {
          [LocaleNamespace.Common]: commonEn
        }
      }
    });
  });

  it('Any Google POI SERPS', () => {
    const params: SearchRequest = {
      ...defaultParams,
      coords: '13.7304651-100.5674563'
    };
    const { rel, path } = stringifyUrl({ request: params, t });
    render(<Link href={path} rel={rel} />);

    expect(screen.getByRole('link')).toHaveProperty('rel', 'nofollow');
  });

  it('SERPS with 2 or more selected property types', () => {
    const params: SearchRequest = {
      ...defaultParams,
      propertyType: [PropertyType.Condo, PropertyType.Apartment]
    };
    const { rel, path } = stringifyUrl({ request: params, t });
    render(<Link href={path} rel={rel} />);

    expect(screen.getByRole('link')).toHaveProperty('rel', 'nofollow');
  });

  it('SERPS with ANY OF THESE SEVEN AMENITY FILTERS `Walk-in-wardrobe` | `Bathtub` | `Refrigerator` | `Microwave` | `Stove` | `TV` | `Jacuzzi`', () => {
    const groups = [
      [Amenity.Tv], // 1 ignore amenity
      [Amenity.Balcony, Amenity.WalkInWardrobe], // 1 ignore amenity, 1 select amenity
      [Amenity.Bathtub, Amenity.Jacuzzi] // 2 ignore amenity
    ];
    render(
      <>
        {groups.map((amenities, index) => {
          const params: SearchRequest = {
            ...defaultParams,
            amenities
          };
          const { rel, path } = stringifyUrl({ request: params, t });
          return <Link key={index} href={path} rel={rel} />;
        })}
      </>
    );

    screen.getAllByRole('link').forEach((element) => expect(element).toHaveProperty('rel', 'nofollow'));
  });

  it('SERPS with 4 or more amenity filters', () => {
    const params: SearchRequest = {
      ...defaultParams,
      amenities: [Amenity.Balcony, Amenity.Gym, Amenity.Garden, Amenity.PetFriendly]
    };
    const { rel, path } = stringifyUrl({ request: params, t });
    render(<Link href={path} rel={rel} />);

    expect(screen.getByRole('link')).toHaveProperty('rel', 'nofollow');
  });

  it('SERPS with floor size filter', () => {
    const params: SearchRequest = {
      ...defaultParams,
      floorSize_gte: 20,
      floorSize_lte: 700
    };
    const { rel, path } = stringifyUrl({ request: params, t });
    render(<Link href={path} rel={rel} />);

    expect(screen.getByRole('link')).toHaveProperty('rel', 'nofollow');
  });

  it('SERPS with price filter', () => {
    const params: SearchRequest = {
      ...defaultParams,
      minPrice: 10,
      maxPrice: 1000000
    };
    const { rel, path } = stringifyUrl({ request: params, t });
    render(<Link href={path} rel={rel} />);

    expect(screen.getByRole('link')).toHaveProperty('rel', 'nofollow');
  });

  it('SERPS with radius filter', () => {
    const params: SearchRequest = {
      ...defaultParams,
      transportationId: 135,
      transportationSystem: TransportationSystem.MRT,
      where: 'Chatuchak Park',
      radius: 14
    };
    const { rel, path } = stringifyUrl({ request: params, t });
    render(<Link href={path} rel={rel} />);

    expect(screen.getByRole('link')).toHaveProperty('rel', 'nofollow');
  });

  it('SERPS pages 2-x', () => {
    const params: SearchRequest = {
      ...defaultParams,
      page: 2
    };
    const { rel, path } = stringifyUrl({ request: params, t });
    render(<Link href={path} rel={rel} />);

    expect(screen.getByRole('link')).toHaveProperty('rel', 'nofollow');
  });

  it('SERPS building with amenity filter', () => {
    const params: SearchRequest = {
      ...defaultParams,
      buildingId: 4475,
      building_type: BuildingType.Condo,
      where: 'Park 24 Phase 1',
      amenities: [Amenity.Balcony]
    };
    const { rel, path } = stringifyUrl({ request: params, t });
    render(<Link href={path} rel={rel} />);

    expect(screen.getByRole('link')).toHaveProperty('rel', 'nofollow');
  });

  it('SERPS building with property type filter', () => {
    const params: SearchRequest = {
      ...defaultParams,
      buildingId: 4475,
      building_type: BuildingType.Condo,
      where: 'Park 24 Phase 1',
      propertyType: [PropertyType.Condo]
    };
    const { rel, path } = stringifyUrl({ request: params, t });
    render(<Link href={path} rel={rel} />);

    expect(screen.getByRole('link')).toHaveProperty('rel', 'nofollow');
  });

  it('SERPS building with non-default min. lease term', () => {
    const params: SearchRequest = {
      ...defaultParams,
      buildingId: 4475,
      building_type: BuildingType.Condo,
      where: 'Park 24 Phase 1',
      minLease: MinLease.SixMonths
    };
    const { rel, path } = stringifyUrl({ request: params, t });
    render(<Link href={path} rel={rel} />);

    expect(screen.getByRole('link')).toHaveProperty('rel', 'nofollow');
  });

  it('SERPS building with non-default sorting', () => {
    const params: SearchRequest = {
      ...defaultParams,
      buildingId: 4475,
      building_type: BuildingType.Condo,
      where: 'Park 24 Phase 1',
      sortBy: SortType.Newest
    };
    const { rel, path } = stringifyUrl({ request: params, t });
    render(<Link href={path} rel={rel} />);

    expect(screen.getByRole('link')).toHaveProperty('rel', 'nofollow');
  });

  it('FS-1463 sales URL', () => {
    const { rel, path } = stringifyUrl({
      request: {
        ...defaultParams,
        buildingId: 4475,
        building_type: BuildingType.Condo,
        where: 'Park 24 Phase 1',
        sortBy: SortType.Recommended,
        serp_type: 'sales',
        index_serp_types: ['rents']
      },
      t
    });
    render(<Link href={path} rel={rel} />);

    expect(screen.getByRole('link')).toHaveProperty('rel', 'nofollow');
  });
});
