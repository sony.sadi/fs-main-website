import React from 'react';
import { screen, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import ListingsPagination from '@components/listings/pagination';
import { FSAppState } from '@stores';
import { getDefaultParams } from '@flexstay.rentals/fs-url-stringify';

const mockStore = configureMockStore();
const moreText = '...';

function makeStoreState({ page, total }: { page?: number; total: number }) {
  const defaultParams = getDefaultParams();

  return {
    search: {
      params: {
        ...defaultParams,
        page: page === undefined ? defaultParams.page : page
      },
      rentals: {
        page: Math.ceil(total / defaultParams.limit),
        perPage: defaultParams.limit,
        total,
        data: []
      }
    }
  };
}

describe('Listings - Pagination', () => {
  describe('renders pagination when the total of pages is greater than 1', () => {
    it(`Do not show more '${moreText}'`, () => {
      const store = mockStore(makeStoreState({ total: 41 }));

      render(
        <Provider store={store}>
          <ListingsPagination onChange={jest.fn} />
        </Provider>
      );

      const state = store.getState() as FSAppState;

      for (let i = 1; i <= state.search.rentals.page; i++) {
        expect(screen.getByText(i)).toBeInTheDocument();
      }
    });

    it(`Do show more '${moreText}'`, () => {
      const store = mockStore(makeStoreState({ total: 73 }));

      render(
        <Provider store={store}>
          <ListingsPagination onChange={jest.fn} />
        </Provider>
      );

      const state = store.getState() as FSAppState;

      for (let i = 1; i <= state.search.rentals.page; i++) {
        expect(screen.getByText(moreText)).toBeInTheDocument();
      }
    });

    it(`Should disable previous button when the current page is 1`, () => {
      const store = mockStore(makeStoreState({ total: 100, page: 1 }));

      render(
        <Provider store={store}>
          <ListingsPagination onChange={jest.fn} />
        </Provider>
      );

      expect(screen.getByTestId('pagination-prev')).toBeDisabled();
      expect(screen.getByTestId('pagination-next')).not.toBeDisabled();
    });

    it(`Should disable nexet button when the current page equals the total`, () => {
      const store = mockStore(makeStoreState({ total: 100, page: 5 }));

      render(
        <Provider store={store}>
          <ListingsPagination onChange={jest.fn} />
        </Provider>
      );

      expect(screen.getByTestId('pagination-prev')).not.toBeDisabled();
      expect(screen.getByTestId('pagination-next')).toBeDisabled();
    });
  });
});
