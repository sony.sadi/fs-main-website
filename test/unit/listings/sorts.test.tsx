import React from 'react';
import { act, screen, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import configureMockStore, { MockStoreEnhanced } from 'redux-mock-store';

import ListingsSort from '@components/listings/sort';
import { getSortingOptions } from '@utils';
import { getDefaultParams } from '@flexstay.rentals/fs-url-stringify';

const mockStore = configureMockStore();
const t = jest.fn((key) => key);

function getInitialState() {
  return {
    search: {
      params: getDefaultParams()
    }
  };
}

let store: MockStoreEnhanced<unknown, unknown>;
let initialState: ReturnType<typeof getInitialState>;

describe('Listings - Sorts', () => {
  beforeEach(async () => {
    await act(async () => {
      initialState = getInitialState();
      store = mockStore(initialState);

      render(
        <Provider store={store}>
          <ListingsSort onSelect={jest.fn} />
        </Provider>
      );
    });
  });

  test('renders options', () => {
    const options = getSortingOptions(t);

    options.forEach(({ label }) => {
      expect(screen.getByText(label)).toBeInTheDocument();
    });
  });
});
