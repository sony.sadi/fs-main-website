import '@testing-library/jest-dom/extend-expect';
import { Language, LocaleNamespace } from '@constants/enum/index';
import { getObjectSchemeSerps, getPaginationText, SchemaSerpsResponse } from '@utils';
import { getDefaultParams } from '@flexstay.rentals/fs-url-stringify';
import i18next from 'i18next';
import { TFunction } from 'next-i18next';
import trimEnd from 'lodash/trimEnd';
import join from 'lodash/join';
import { getListingDetailLink } from '@utils/listing';
import { Listing } from '@interfaces/listing';
import publicSearch from '../../mocks/services/api/public-search.json';
import * as commonEn from '../../../public/locales/en/common.json';

const { NEXT_PUBLIC_HOME_URL } = process.env;

const HOME_URL = trimEnd(NEXT_PUBLIC_HOME_URL, '/');
const originUrl = trimEnd(`${HOME_URL}/${Language.US}`, '/');

describe('List SERP - Schema', () => {
  let t: TFunction;
  let paginationText: string;
  let objectSchema: SchemaSerpsResponse;
  const defaultParams = getDefaultParams();
  beforeEach(async () => {
    await i18next.init({
      lng: Language.US,
      ns: [LocaleNamespace.Common],
      resources: {
        [Language.US]: {
          [LocaleNamespace.Common]: commonEn
        }
      }
    });
    t = i18next.getFixedT(Language.US, LocaleNamespace.Common);
    paginationText = getPaginationText({
      ...defaultParams,
      address: 'Anusawari',
      total: 22,
      pageSize: 20,
      t,
      page: 1
    });
    // objectSchema = getObjectSchemeSerps(publicSearch as any, paginationText, Language.US);
    objectSchema = getObjectSchemeSerps({
      language: Language.US,
      paginationText,
      rentals: {
        data: publicSearch.data as unknown as Listing[],
        page: publicSearch.page,
        perPage: publicSearch.perPage,
        total: publicSearch.data.length,
        aggs: publicSearch.aggs
      },
      descriptionText: ''
    });
  });

  test('H1 title', () => {
    expect(objectSchema.name).toBe('1 - 20 of 22 properties for rent in Anusawari');
  });

  test('High price - Low price', () => {
    expect(objectSchema.offers.lowPrice).toEqual(publicSearch.aggs.lowest_price);
    expect(objectSchema.offers.highPrice).toEqual(publicSearch.aggs.highest_price);
  });

  test('Offer count', () => {
    expect(objectSchema.offers.offerCount).toEqual(publicSearch.data.length);
  });

  test('Offer elements', () => {
    publicSearch.data.forEach((rentalItem: any, index: number) => {
      const { href } = getListingDetailLink(rentalItem, Language.US);
      expect(objectSchema.offers.offers[index].url).toBe(join([originUrl, href.query['listing-title-id'], ''], '/'));
    });
  });
});
