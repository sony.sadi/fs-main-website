import React from 'react';
import { act, screen, render, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import startCase from 'lodash/startCase';
import { v4 as uuidv4 } from 'uuid';
import ListingsCards from '@components/listings/cards';
import { getAddress, getPropertyTitle, getTextPrice } from '@utils';
import { Language, LocaleNamespace, Page } from '@constants/enum';
import { getDefaultParams } from '@flexstay.rentals/fs-url-stringify';
import { TFunction, useTranslation } from 'next-i18next';
import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';
import publicSearch from '../../mocks/services/api/public-search.json';
import * as commonEn from '../../../public/locales/en/common.json';

const mockStore = configureMockStore();

beforeAll(async () => {
  await i18next.use(initReactI18next).init({
    lng: Language.US
  });
});

describe('Listings - Cards', () => {
  beforeEach(async () => {
    // t = i18next.getFixedT(Language.US, LocaleNamespace.Common);
    await act(async () => {
      const defaultParams = getDefaultParams();
      const store = mockStore({
        common: {
          isMobile: false
        },
        search: {
          propertypad: {
            favouriteRentals: [],
            identifier: uuidv4()
          },
          params: {
            ...defaultParams
          }
        }
      });
      render(
        <Provider store={store}>
          <ListingsCards page={Page.Listings} listings={publicSearch.data as any[]} />
        </Provider>
      );
    });
  });

  test('renders listings', () => {
    publicSearch.data.forEach((listing) => {
      const name = startCase(getPropertyTitle(listing as any, Language.US));
      const address = getAddress(listing as any, Language.US);

      expect(screen.getByText(name, { exact: true })).toBeInTheDocument();
      expect(screen.getByText(address, { exact: true })).toBeInTheDocument();
    });
  });

  test('Format price with serpType = rents', () => {
    const defaultParams = getDefaultParams();
    const store = mockStore({
      common: {
        isMobile: false
      },
      search: {
        propertypad: {
          favouriteRentals: [],
          identifier: uuidv4()
        },
        params: {
          ...defaultParams
        }
      }
    });
    render(
      <Provider store={store}>
        <ListingsCards serpType="rents" page={Page.Listings} listings={publicSearch.data as any[]} />
      </Provider>
    );
    const { t } = useTranslation();

    publicSearch.data.forEach((listing) => {
      const listingPrice = getTextPrice(listing.lowestPrice || 0, t);
      screen.getAllByText(listingPrice).forEach((element) => {
        expect(element).toBeInTheDocument();
      });
    });
  });

  test('Format price with serpType = sales', () => {
    const defaultParams = getDefaultParams();
    const store = mockStore({
      common: {
        isMobile: false
      },
      search: {
        propertypad: {
          favouriteRentals: [],
          identifier: uuidv4()
        },
        params: {
          ...defaultParams
        }
      }
    });
    render(
      <Provider store={store}>
        <ListingsCards serpType="sales" page={Page.Listings} listings={publicSearch.data as any[]} />
      </Provider>
    );
    const { t } = useTranslation();

    publicSearch.data.forEach((listing) => {
      const listingPrice = getTextPrice(listing.salePrice || 0, t, 'sales');
      screen.getAllByText(listingPrice).forEach((element) => {
        expect(element).toBeInTheDocument();
      });
    });
  });
});
