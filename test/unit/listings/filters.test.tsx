import React from 'react';
import { act, screen, render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import configureMockStore, { MockStoreEnhanced } from 'redux-mock-store';
import preloadAll from 'jest-next-dynamic';

import ListingsFilterOptions from '@components/listings/filter-options';
import { formatPrice } from '@utils';
import { getDefaultParams } from '@flexstay.rentals/fs-url-stringify';
import { getAmenities, getBedroomOptions, getBudgetTypes, getFilteringTabs, getPropertyTypes } from '@utils/search';
import { ListingTabs, Language } from '@constants/enum';
import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';

const mockStore = configureMockStore();
const t = jest.fn((key) => key);

function getInitialState(isMobile?: boolean) {
  return {
    common: {
      isMobile,
      isMobileOnly: isMobile
    },
    search: {
      filterOption: 'rents',
      params: {
        ...getDefaultParams(),
        minPrice: 1234,
        maxPrice: 56789,
        floorSize_gte: 123,
        floorSize_lte: 456,
        radius: 10
      }
    }
  };
}

let store: MockStoreEnhanced<unknown, unknown>;
let initialState: ReturnType<typeof getInitialState>;

beforeAll(async () => {
  await i18next.use(initReactI18next).init({
    lng: Language.US
  });
  await preloadAll();
});

describe('Listings - Filters', () => {
  describe('Desktop viewport', () => {
    beforeEach(async () => {
      await act(async () => {
        initialState = getInitialState();
        store = mockStore(initialState);

        render(
          <Provider store={store}>
            <ListingsFilterOptions onSearch={jest.fn} onLocationChange={jest.fn} onReset={jest.fn} />
          </Provider>
        );
      });
    });

    test('renders a search button', () => {
      expect(screen.getByTestId('search-now')).toBeInTheDocument();
    });

    test('renders tabs by default', () => {
      const tabs = getFilteringTabs(t);

      tabs.forEach(({ title }) => {
        expect(screen.getByText(title)).toBeInTheDocument();
      });
    });

    test('renders price tab options', () => {
      const priceTabButton = screen.getByTestId(ListingTabs.Price);
      fireEvent.click(priceTabButton);

      expect(screen.getByDisplayValue(formatPrice(initialState.search.params.minPrice))).toBeInTheDocument();
      expect(screen.getByDisplayValue(formatPrice(initialState.search.params.maxPrice))).toBeInTheDocument();
    });

    test('renders beds tab options', () => {
      const bedsTabButton = screen.getByTestId(ListingTabs.Bed);
      fireEvent.click(bedsTabButton);

      const options = getBedroomOptions(t);

      options.forEach(({ label }) => {
        expect(screen.getByText(label)).toBeInTheDocument();
      });
    });

    describe('renders more tab options', () => {
      beforeEach(() => {
        const moreTabButton = screen.getByTestId(ListingTabs.MoreFilter);
        fireEvent.click(moreTabButton);
      });

      test('property types', () => {
        const types = getPropertyTypes(t);

        types.forEach(({ name }) => {
          expect(screen.getByText(name)).toBeInTheDocument();
        });
      });

      test('budget types', () => {
        const types = getBudgetTypes(t);

        types.forEach(({ name }) => {
          expect(screen.getByText(name)).toBeInTheDocument();
        });
      });

      test('floor size', () => {
        expect(screen.getByDisplayValue(formatPrice(initialState.search.params.floorSize_gte))).toBeInTheDocument();
        expect(screen.getByDisplayValue(formatPrice(initialState.search.params.floorSize_lte))).toBeInTheDocument();
      });

      describe('amenities', () => {
        test('renders full of options', () => {
          const amenities = getAmenities(t);

          amenities.forEach(({ name }) => {
            expect(screen.getByText(name)).toBeInTheDocument();
          });
        });
      });

      describe('radius', () => {
        test('renders default value', () => {
          expect(screen.getByDisplayValue(formatPrice(initialState.search.params.radius))).toBeInTheDocument();
        });

        test('renders a correct value when increasing', () => {
          const plusButton = screen.getByTestId('input-number-plus');
          fireEvent.click(plusButton);
          expect(screen.getByDisplayValue(formatPrice(initialState.search.params.radius + 1))).toBeInTheDocument();
        });

        test('renders a correct value when decreasing', () => {
          const plusButton = screen.getByTestId('input-number-minus');
          fireEvent.click(plusButton);
          expect(screen.getByDisplayValue(formatPrice(initialState.search.params.radius - 1))).toBeInTheDocument();
        });
      });
    });
  });

  describe('Mobile viewport', () => {
    beforeEach(async () => {
      await act(async () => {
        initialState = getInitialState(true);
        store = mockStore(initialState);

        render(
          <Provider store={store}>
            <ListingsFilterOptions onSearch={jest.fn} onLocationChange={jest.fn} onReset={jest.fn} />
          </Provider>
        );
      });
    });

    test('renders an apply filter buttons', () => {
      const tabs = getFilteringTabs(t);

      tabs.forEach((tab) => {
        const tabButton = screen.getByTestId(tab.name);
        fireEvent.click(tabButton);

        expect(screen.getByTestId('apply-filter')).toBeInTheDocument();
      });
    });

    test('renders tabs by default', () => {
      const tabs = getFilteringTabs(t);

      tabs.forEach(({ title }) => {
        expect(screen.getByText(title)).toBeInTheDocument();
      });
    });

    test('renders price tab options', () => {
      const priceTabButton = screen.getByTestId(ListingTabs.Price);
      fireEvent.click(priceTabButton);

      expect(screen.getByDisplayValue(formatPrice(initialState.search.params.minPrice))).toBeInTheDocument();
      expect(screen.getByDisplayValue(formatPrice(initialState.search.params.maxPrice))).toBeInTheDocument();
    });

    test('renders beds tab options', () => {
      const bedsTabButton = screen.getByTestId(ListingTabs.Bed);
      fireEvent.click(bedsTabButton);

      const options = getBedroomOptions(t);

      options.forEach(({ label }) => {
        expect(screen.getByText(label)).toBeInTheDocument();
      });
    });

    test('check exist filter search', () => {
      const filter = screen.getByTestId('toggle-serp-type');
      expect(filter).toBeInTheDocument();
    });

    describe('renders more tab options', () => {
      beforeEach(() => {
        const moreTabButton = screen.getByTestId(ListingTabs.MoreFilter);
        fireEvent.click(moreTabButton);
      });

      test('property types', () => {
        const types = getPropertyTypes(t);

        types.forEach(({ name }) => {
          expect(screen.getByText(name)).toBeInTheDocument();
        });
      });

      test('budget types', () => {
        const types = getBudgetTypes(t);

        types.forEach(({ name }) => {
          expect(screen.getByText(name)).toBeInTheDocument();
        });
      });

      test('floor size', () => {
        expect(screen.getByDisplayValue(formatPrice(initialState.search.params.floorSize_gte))).toBeInTheDocument();
        expect(screen.getByDisplayValue(formatPrice(initialState.search.params.floorSize_lte))).toBeInTheDocument();
      });

      test('amenities', () => {
        const amenities = getAmenities(t);

        amenities.forEach(({ name }) => {
          expect(screen.getByText(name)).toBeInTheDocument();
        });
      });

      describe('radius', () => {
        test('renders default value', () => {
          expect(screen.getByDisplayValue(formatPrice(initialState.search.params.radius))).toBeInTheDocument();
        });

        test('renders a correct value when increasing', () => {
          const plusButton = screen.getByTestId('input-number-plus');
          fireEvent.click(plusButton);
          expect(screen.getByDisplayValue(formatPrice(initialState.search.params.radius + 1))).toBeInTheDocument();
        });

        test('renders a correct value when decreasing', () => {
          const plusButton = screen.getByTestId('input-number-minus');
          fireEvent.click(plusButton);
          expect(screen.getByDisplayValue(formatPrice(initialState.search.params.radius - 1))).toBeInTheDocument();
        });
      });
    });
  });
});
