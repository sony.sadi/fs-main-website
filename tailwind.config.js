module.exports = {
  purge: ['./src/containers/**/**.tsx', './src/components/**/**.tsx', './src/pages/**/**.tsx', './src/forms/**/**.tsx'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: {
          black: '#212529',
          gray: '#4f5962',
          blue: '#5bb8ea',
          red: '#f15e75',
          orange: '#E74119',
          lightgray: '#949ca5',
          skyblue: '#00aeef',
          dark: '#002A55',
          pink: '#F7DCD6',
          white: '#FFFFFF'
        },
        secondary: {
          darkwhite: '#F8F8F8',
          darkgray: 'rgba(0,0,0,.5)',
          gray: '#dbdbdb',
          lightgray: '#c5c5c5'
        },
        home: {
          dark: '#050505',
          white: '#FAFAFA',
          gray: '#EBEBEB',
          silver: '#707070'
        },
        whitesmoke: '#f5f5f5',
        footer: {
          black: '#000000'
        },
        'how-it-works': {
          gray: '#E5E5E5',
          step: '#F2F2F2'
        }
      },
      height: {
        menu: '65px',
        'menu-mobile': '75px',
        'home-hero-tablet': '555px',
        'home-choosing-property-scout': '555px',
        'vh-30': '30vh',
        'vh-45': '45vh',
        'vh-60': '60vh',
        'vh-65': '65vh',
        'vh-80': '80vh',
        'vh-12': '12vh',
        'vh-88': '88vh',
        '23rem': '23rem',
        'item-partner': '212px',
        'item-searches': '90px',
        'item-building': '212px',
        'item-searches-mobile': '163px',
        'item-building-mobile': '217px',
        'how-it-works-tablet': '648px',
        'how-it-works-mobile': '738px',
        'list-your-property-mobile': '611px',
        'list-your-property-desktop': '580px',
        inquiry: '578px'
      },
      width: {
        logo: '141.74px',
        50: '12.5rem',
        'logo-mobile': '225px',
        'item-partner': '150px',
        'item-searches': '90px',
        'item-building': '212px',
        'inquiry-default': '404px',
        'inquiry-contact': '392px',
        'how-it-works-step': '618px',
        'list-your-property-mobile': '406px'
      },
      screens: {
        '3xl': '1792px',
        '4xl': '2048px'
      },

      margin: {
        'logo-footer': '76px'
      },

      spacing: {
        '65px': '65px',
        '75px': '75px',
        '3/5': '60%',
        '7/10': '70%'
      },

      minHeight: {
        10: '2.5rem',
        11: '2.75rem'
      },

      minWidth: {
        9: '2.25rem',
        24: '6rem'
      },
      maxWidth: {
        'container-list-your-property': '981px',
        '1/4': '25%',
        '1/2': '50%',
        '3/4': '75%',
        '15.5rem': '15.5rem',
        '17.5rem': '17.5rem',
        '18.75rem': '18.75rem'
      },
      zIndex: {
        header: 60,
        'photos-modal': 100,
        1000: 1000,
        10000: 10000,
        1: 1,
        2: 2
      },

      padding: {
        footer: '110px',
        'about-us-desktop': '46.25%',
        'about-us-laptop': '46.25%',
        'about-us-mobile': '73.958333333%',
        'about-us-team-desktop': '53.469387755%',
        'about-us-team-laptop': '53.469387755%',
        'about-us-team-mobile': '67.487684729%',
        'partner-tablet': '52px'
      },

      backgroundImage: {
        'home-hero-desktop': `url(${process.env.NEXT_PUBLIC_HOME_URL}/images/home-hero-desktop.jpg)`,
        'home-hero-tablet': `url(${process.env.NEXT_PUBLIC_HOME_URL}/images/home-hero-tablet.jpg)`,
        'home-hero-mobile': `url(${process.env.NEXT_PUBLIC_HOME_URL}/images/home-hero-mobile.jpg)`,
        'home-choosing-property-scout': `url(${process.env.NEXT_PUBLIC_HOME_URL}/images/home-choosing-property-scout.jpg)`,
        'home-choosing-property-scout-desktop': `url(${process.env.NEXT_PUBLIC_HOME_URL}/images/home-choosing-property-scout-2400x1350.jpg)`,
        'banner-backdrop': 'linear-gradient(-45deg, rgba(0, 0, 0, 0.37), rgba(0, 0, 0, 0))',
        'card-overlay': 'linear-gradient(rgba(255,255,255,0.6), rgba(255,255,255,0.6))',
        'card-backdrop': 'linear-gradient(180deg,transparent,rgba(0,0,0,.65))',
        'card-half-backdrop': 'linear-gradient(180deg,transparent,transparent 0,transparent 50%,rgba(0,0,0,.75))',
        'how-it-works-tablet': `url(${process.env.NEXT_PUBLIC_HOME_URL}/images/how-it-works-tablet.jpg)`,
        'how-it-works-mobile': `url(${process.env.NEXT_PUBLIC_HOME_URL}/images/how-it-works-mobile.jpg)`,
        'contact-us-mobile': `url(${process.env.NEXT_PUBLIC_HOME_URL}/images/contact-us-mobile.jpg)`,
        'contact-us-desktop': `url(${process.env.NEXT_PUBLIC_HOME_URL}/images/contact-us-desktop.jpg)`,
        'list-your-property-desktop': `url(${process.env.NEXT_PUBLIC_HOME_URL}/images/list-your-property-desktop.jpg)`,
        'list-your-property-mobile': `url(${process.env.NEXT_PUBLIC_HOME_URL}/images/list-your-property-mobile.jpg)`,
        'about-us-desktop': `url(${process.env.NEXT_PUBLIC_HOME_URL}/images/about-us/hero-2400x1110.jpg)`,
        'about-us-laptop': `url(${process.env.NEXT_PUBLIC_HOME_URL}/images/about-us/hero-1200x555.jpg)`,
        'about-us-tablet': `url(${process.env.NEXT_PUBLIC_HOME_URL}/images/about-us/hero-960x710.jpg)`,
        'about-us-mobile': `url(${process.env.NEXT_PUBLIC_HOME_URL}/images/about-us/hero-480x355.jpg)`,
        'about-us-team-desktop': `url(${process.env.NEXT_PUBLIC_HOME_URL}/images/about-us/team-1960x1048.jpg)`,
        'about-us-team-laptop': `url(${process.env.NEXT_PUBLIC_HOME_URL}/images/about-us/team-980x524.jpg)`,
        'about-us-team-mobile': `url(${process.env.NEXT_PUBLIC_HOME_URL}/images/about-us/team-406x274.jpg)`
      },

      backgroundSize: {
        auto: 'auto',
        cover: 'cover',
        contain: 'contain',
        icon: '12px'
      },
      backgroundPosition: {
        'left-center': 'left center'
      },
      transitionProperty: {
        height: 'height',
        rotate: 'rotate'
      },

      borderRadius: {
        10: '0.625rem'
      },
      fontSize: {
        xxs: '.5rem',
        '15px': '15px',
        '13px': '0.8125rem',
        '35px': '35px',
        '45px': '45px'
      }
    },

    screens: {
      // => @media (min-width: 370px) { ... }
      xs: '370px',
      sm: '480px',
      md: '768px',
      lg: '1024px',
      xl: '1200px',
      '2xl': '1400px'
    }
  },
  variants: {
    extend: {
      borderWidth: ['last', 'hover'],
      borderRadius: ['group-hover'],
      fontWeight: ['hover', 'focus'],
      padding: ['last', 'first', 'group-hover', 'hover'],
      margin: ['last', 'first', 'group-hover', 'hover'],
      scale: ['group-hover'],
      opacity: ['disabled']
    }
  },
  plugins: []
};
