const axios = require('axios').default;

module.exports = async () => {
  const localesRes = await Promise.all([
    axios.get(`${process.env.NEXT_PUBLIC_EGSWING_API_BASE_URL}website-locales/en/common`),
    axios.get(`${process.env.NEXT_PUBLIC_EGSWING_API_BASE_URL}website-locales/th/common`)
  ]);
  const [localeEn, localeTh] = localesRes.map((localeRes) => localeRes.data);

  const rentSlug = {
    en: localeEn.rentalsValue,
    th: encodeURI(localeTh.rentalsValue)
  };

  const saleSlug = {
    en: localeEn.salesValue,
    th: encodeURI(localeTh.salesValue)
  };
  const provinceSlug = {
    en: localeEn.bangkokProvince,
    th: encodeURI(localeTh.bangkokProvince)
  };

  const propertyTypes = {
    en: [
      localeEn.propertyTypeCondoValue,
      localeEn.propertyTypeApartmentValue,
      localeEn.propertyTypeHouseValue,
      localeEn.propertyTypeServicedApartmentValue,
      localeEn.propertyTypeTownhouseValue
    ].join('|'),
    th: [
      encodeURI(localeTh.propertyTypeCondoValue),
      encodeURI(localeTh.propertyTypeApartmentValue),
      encodeURI(localeTh.propertyTypeHouseValue),
      encodeURI(localeTh.propertyTypeServicedApartmentValue),
      encodeURI(localeTh.propertyTypeTownhouseValue)
    ].join('|')
  };

  const transportationSystem = ['bts', 'mrt', 'arl'].join('|');

  const renderRedirectArray = (slug) => {
    return [
      {
        source: `/:slug(siam|khao-san-road|asok|ekkamai|phrom-phong|silom|sukhumvit|sathon|thong-lo|suvarnabhumi-airport)/${slug.en}/`,
        destination: `/${provinceSlug.en}/:slug(siam|khao-san-road|asok|ekkamai|phrom-phong|silom|sukhumvit|sathon|thong-lo|suvarnabhumi-airport)/${slug.en}/`,
        statusCode: 301
      },
      {
        source: `/th/${slug.th}/`,
        destination: `/${provinceSlug.th}/${slug.th}/`,
        statusCode: 302,
        locale: false
      },
      {
        source: `/th/${slug.th}/:slug(${transportationSystem})/`, // TH redirect for country element (transportation poi breadcrumbs)
        destination: `/${provinceSlug.th}/${slug.th}/`,
        statusCode: 302,
        locale: false
      },
      {
        source: `/th/${provinceSlug.th}/${slug.th}/:slug(${transportationSystem})/`, // TH redirect for province/city element (transportation poi breadcrumbs)
        destination: `/${provinceSlug.th}/${slug.th}/`,
        statusCode: 302,
        locale: false
      },
      {
        source: `/th/${provinceSlug.th}/:path/${slug.th}/:slug(${transportationSystem})/`, // TH redirect for district/subdistrict element (transportation poi breadcrumbs)
        destination: `/${provinceSlug.th}/:path/${slug.th}/`,
        statusCode: 302,
        locale: false
      },
      {
        source: `/th/:slug(${transportationSystem})/`, // FS-1201 TH redirect for content type element (transportation poi breadcrumbs)
        destination: `/${provinceSlug.th}/:slug(${transportationSystem})/`,
        statusCode: 302,
        locale: false
      },
      {
        source: `/th/:slug(${propertyTypes.th})/`, // TH redirect for content type element (building poi breadcrumbs)
        destination: `/${provinceSlug.th}/${slug.th}/:slug(${propertyTypes.th})/`,
        statusCode: 302,
        locale: false
      },
      {
        source: `/th/${slug.th}/:slug(${propertyTypes.th})/`,
        destination: `/${provinceSlug.th}/${slug.th}/:slug(${propertyTypes.th})/`,
        statusCode: 302,
        locale: false
      },
      {
        source: `/en/${slug.en}/`,
        destination: `/en/${provinceSlug.en}/${slug.en}/`,
        statusCode: 302,
        locale: false
      },
      {
        source: `/en/${slug.en}/:slug(${propertyTypes.en})/`,
        destination: `/en/${provinceSlug.en}/${slug.en}/:slug(${propertyTypes.en})/`,
        statusCode: 302,
        locale: false
      },
      {
        source: `/en/:slug(${transportationSystem})/`, // FS-1201 EN redirect for content type element (transportation poi breadcrumbs)
        destination: `/en/${provinceSlug.en}/:slug(${transportationSystem})/`,
        statusCode: 302,
        locale: false
      },
      {
        source: `/en/${slug.en}/:slug(${transportationSystem})/`, // EN redirect for country element (transportation poi breadcrumbs)
        destination: `/en/${provinceSlug.en}/${slug.en}/`,
        statusCode: 302,
        locale: false
      },
      {
        source: `/en/${provinceSlug.en}/${slug.en}/:slug(${transportationSystem})/`, // EN redirect for province/city element (transportation poi breadcrumbs)
        destination: `/en/${provinceSlug.en}/${slug.en}/`,
        statusCode: 302,
        locale: false
      },
      {
        source: `/en/${provinceSlug.en}/:path/${slug.en}/:slug(${transportationSystem})/`, // EN redirect for district/subdistrict element (transportation poi breadcrumbs)
        destination: `/en/${provinceSlug.en}/:path/${slug.en}/`,
        statusCode: 302,
        locale: false
      },
      {
        source: `/en/:slug(${propertyTypes.en})/`, // EN redirect for content type element (building poi breadcrumbs)
        destination: `/en/${provinceSlug.en}/${slug.en}/:slug(${propertyTypes.en})/`,
        statusCode: 302,
        locale: false
      }
    ];
  };

  const redirectArray = [...renderRedirectArray(rentSlug), ...renderRedirectArray(saleSlug)];

  return redirectArray;
};
