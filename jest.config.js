module.exports = {
  testMatch: ['<rootDir>/test/**/*.test.tsx'],
  setupFiles: ['<rootDir>/test/jest-setup.ts'],
  setupFilesAfterEnv: ['<rootDir>/test/jest-setup-after-env.ts'],
  testPathIgnorePatterns: ['/node_modules/', '/.next/', '/cypress/'],
  transform: {
    '^.+\\.(js|jsx|ts|tsx)$': '<rootDir>/node_modules/babel-jest',
    '^.+\\.css$': '<rootDir>/test/config/css-transform.js'
  },
  transformIgnorePatterns: ['/node_modules/', '^.+\\.module\\.(css|sass|scss)$'],
  moduleNameMapper: {
    '^.+\\.module\\.(css|sass|scss)$': 'identity-obj-proxy',
    '^@components/(.*)': '<rootDir>/src/components/$1',
    '^@utils$': '<rootDir>/src/utils/index.ts',
    '^@utils/(.*)': '<rootDir>/src/utils/$1',
    '^@constants$': '<rootDir>/src/constants/index.ts',
    '^@constants/(.*)': '<rootDir>/src/constants/$1',
    '^@interfaces$': '<rootDir>/src/interfaces/index.ts',
    '^@interfaces/(.*)': '<rootDir>/src/interfaces/$1',
    '^@hooks/(.*)': '<rootDir>/src/hooks/$1',
    '^@stores$': '<rootDir>/src/stores/index.ts',
    '^@stores/(.*)': '<rootDir>/src/stores/$1',
    '^@services/(.*)': '<rootDir>/src/services/$1',
    '^@assets/(.*)': '<rootDir>/src/assets/$1',
    '^@containers/(.*)': '<rootDir>/src/containers/$1',
    '^@forms/(.*)': '<rootDir>/src/forms/$1'
  }
};
