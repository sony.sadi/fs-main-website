const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true'
});
const { i18n } = require('./next-i18next.config');
const withPWA = require('next-pwa');
// const runtimeCaching = require('next-pwa/cache');
const runtimeCaching = require('./next-pwa-cache');
const { withSentryConfig } = require('@sentry/nextjs');
const isDev = process.env.NODE_ENV === 'development';
const isProd = process.env.NODE_ENV === 'production';
const isDisablePwa = process.env.NEXT_DISABLE_PWA === 'true';

// we not use process.env.CI_JOB_STAGE === 'deploy' because it's running on server
// so will not have CI_JOB_STAGE in env
const isDisableSentryWebpack = isDev || process.env.CI_JOB_STAGE === 'build_test';

module.exports = withSentryConfig(
  withBundleAnalyzer(
    withPWA({
      i18n,
      webpack5: true,
      trailingSlash: true,
      images: {
        domains: process.env.NEXT_PUBLIC_OPTIMIZED_IMAGE_DOMAINS.split(','),
        deviceSizes: [400, 600, 768, 1024, 1200, 1400, 1920]
      },
      pwa: {
        disable: isDisablePwa,
        dest: 'public',
        publicExcludes: ['!images/about-us/*', '!images/about-us/*/*', '!svg/SVG/*', '!*.xml'],
        runtimeCaching
      },

      experimental: {
        optimizeCss: true
      },

      webpack(config) {
        config.module.rules.push({
          test: /\.svg$/,
          use: ['@svgr/webpack']
        });

        return config;
      },

      env: {
        SENTRY_DSN:
          process.env.SENTRY_DSN ||
          process.env.NEXT_PUBLIC_SENTRY_DSN ||
          'https://e15531eef62544feb775205e9a293910@o435522.ingest.sentry.io/5732399',
        SENTRY_ENV: process.env.SENTRY_ENV,
        SENTRY_COMMIT_HASH: process.env.SENTRY_COMMIT_HASH
      },

      publicRuntimeConfig: {
        // Will be available on both server and client
        SENTRY_DSN: process.env.SENTRY_DSN || process.env.NEXT_PUBLIC_SENTRY_DSN,
        SENTRY_ENV: process.env.SENTRY_ENV,
        SENTRY_COMMIT_HASH: process.env.SENTRY_COMMIT_HASH
      },

      async headers() {
        const serverHeaders = [];

        if (isProd) {
          serverHeaders.push(
            {
              source: '/((?!_next/data))(.*)',
              headers: [
                {
                  key: 'Strict-Transport-Security',
                  value: 'max-age=31536000; includeSubDomains'
                },
                {
                  key: 'Cache-Control',
                  value: 'public, max-age=86400000, s-maxage=86400000, stale-while-revalidate=86400000'
                }
              ]
            },
            {
              source: '/images/(.*)',
              headers: [
                {
                  key: 'Cache-Control',
                  value: 'public, max-age=86400000, s-maxage=86400000, stale-while-revalidate=86400000'
                }
              ]
            }
          );
        }

        return serverHeaders;
      },
      redirects: require('./next-redirects'),

      rewrites: async () => {
        const transportation_system = ['bts', 'mrt', 'arl'].join('|');

        return [
          {
            source: `/en/bangkok/:transportation_system(${transportation_system})/:slug/`,
            destination: '/landing/:transportation_system/:slug*',
            locale: false
          },
          {
            source: `/th/${encodeURI('กรุงเทพ')}/:transportation_system(${transportation_system})/:slug/`,
            destination: '/landing/:transportation_system/:slug*',
            locale: false
          },
          {
            source: `/en/bangkok/:transportation_system(${transportation_system})/`,
            destination: '/landing/:transportation_system',
            locale: false
          },
          {
            source: `/th/${encodeURI('กรุงเทพ')}/:transportation_system(${transportation_system})/`,
            destination: '/landing/:transportation_system',
            locale: false
          },
          {
            source: '/en/propertypad/:identifier*/',
            destination: '/propertypad/:identifier*',
            locale: false
          },
          {
            source: encodeURI('/th/ติดต่อเรา/'),
            destination: '/contact-us',
            locale: false
          },
          {
            source: encodeURI('/th/ลงประกาศของคุณ/'),
            destination: '/list-your-property',
            locale: false
          },
          {
            source: encodeURI('/th/ทำอย่างไร/'),
            destination: '/how-does-it-work',
            locale: false
          },
          {
            source: encodeURI('/th/เกี่ยวกับเรา/'),
            destination: '/about-us',
            locale: false
          },
          {
            source: encodeURI('/th/ความเป็นส่วนตัว/'),
            destination: '/privacy',
            locale: false
          },
          {
            source: '/en/bangkok/rentals/',
            destination: '/serps',
            locale: false
          },
          {
            source: encodeURI('/th/กรุงเทพ/ให้เช่า/'),
            destination: '/serps',
            locale: false
          },
          {
            source: `/:slugs(bangkok/.*$|${encodeURI('กรุงเทพ')}/.*$)`,
            destination: '/serps/:slugs*'
          },
          {
            source: `/:slugs((?!.*(?:bangkok/|${encodeURI('กรุงเทพ')}/)).*-\\d+/$)`,
            destination: '/listing/:slugs'
          }
          // FS-1114 These rules appear to be internally conflicting
          // {
          //   source: `/:slugs(.*/page-\\d+/$|.*/${encodeURI('หน้า')}-\\d+/$)`,
          //   destination: '/serps/:slugs*'
          // },
          // {
          //   source: '/:slugs(.*-\\d+/$)',
          //   destination: '/listing/:slugs'
          // },
          // {
          //   source: '/:slugs(.*(?<!-\\d+)/$)',
          //   destination: '/serps/:slugs*'
          // }
        ];
      },
      sentry: {
        disableServerWebpackPlugin: isDisableSentryWebpack,
        disableClientWebpackPlugin: isDisableSentryWebpack,
      }
    })
  )
);
