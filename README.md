# Flexstay Rentals website

### The Project Folder Structure

```
│   .env.development // Config of the localhost server
│   .env.production // Config of the production server
│   .env.staging // Config of the staging server
│   .eslintignore
│   .eslintrc.js
│   .gitignore
│   .gitlab-ci.yml
│   .nvmrc
│   .prettierrc
│   commitlint.config.js // Config of linter before your commit
│   cypress.env.json // https://docs.cypress.io/guides/guides/environment-variables#Option-2-cypress-env-json
│   cypress.json // https://docs.cypress.io/guides/guides/environment-variables#Option-1-configuration-file
│   jest.config.js // The entry Jest's config
│   next-env.d.ts
│   next-i18next.config.js // Config that tells next-i18next what project's locales are
│   next.config.js
│   package.json
│   postcss.config.js
│   README.md
│   stylelint.config.js
│   tailwind.config.js // A guide to configuring and customizing your Tailwind installation.
│   tsconfig.json
│   
├───cypress // Including e2e testing implementation, plugins, and so on
├───deployments // Including deploy scripts on servers
├───public // Including static files
│   ├───fonts // Downloaded fonts that aims to do preload (the way to optimize visibility)
│   ├───icons
│   ├───images // Including images on pages such as banners, logo, and so on
│   ├───locales // Including json files those define messages based on specific locale such as EN and TH
│   └───svg // Including SVG files (https://icomoon.io/ as default)
│               
├───src
│   ├───assets // Including static files that will need to use/import in the source project
│   ├───components
│   │   ├───common // Including common components which used through the project widely
│   │   ├───core // It's different from 'common' components, cause this folder includes base components which define the project behavior
│   │   ├───forms // Including form controls such as input, select, textarea, radio, and so on
│   │               
│   ├───constants
│   ├───forms // Including popular forms (containing many controls) such as contact, inquiry, and so on
│   ├───hooks // Defining customizing hooks that are used for convenience
│   ├───interfaces // A unique place to define interfaces in the project
│   ├───pages // NextJS's convention to define page URLs
│   ├───services // Including HTTP request connections
│   ├───stores // Configuring store's dependencies, middleware
│   ├───styles // Global styles, including utilities that used through components
│   └───utils // Defining transformers, formatters and others to help features calculation easily
│           
├───test
│   │   jest-setup-after-env.ts // https://jestjs.io/docs/configuration#setupfilesafterenv-array
│   │   jest-setup.ts // https://jestjs.io/docs/configuration#setupfiles-array
│   │   
│   ├───config
│   ├───mocks // Defining mocking objects to mimic browser's object such as 'window', 'router', and so on 
│   └───unit // Including all unit-testing implementation
└───types
```

### Add more path Aliases into the project

Path aliases have been added to the project to help shorten the path in import statements, the project comes with these aliases

- `@assets` for `src/assets`
- `@utils` for `src/utils`
- `@constants` for `src/constants`
- `@interfaces` for `src/interfaces`
- `@hooks` for `src/hooks`
- `@components` for `src/components`
- `@forms` for `src/forms`
- `@pages` for `src/pages`
- `@stores` for `src/stores`
- `@services` for `src/services`
- `@styles` for `src/styles`
- `@public` for `src/public`

To add more aliases, you need to update the tsconfig.json file

### Component Structure

All shareable UI components must go inside `src/components` folder. According to React standard, all component have to be named by `PascalCase`.
A folder is allowed with `kebab-case` naming, in case of grouping are required for the same type of component.

```
├── about-us
│   ├── hero
│   │   └── index.tsx
│   ├── mission
│   │   └── index.tsx
├── common
│   ├── button
│   │   └── index.tsx
│   ├── cards
│   │   ├── area
│   │   │   ├── dekstop
│   │   │   │   └── index.tsx
│   │   │   └── mobile
│   │   │       └── index.tsx
│   │   ├── building
│   │   │   └── index.tsx
│   ├── google-places-autocomplete
│   │   ├── index.tsx
│   │   └── suggestions
│   │       └── index.tsx
├── core
│   ├── image
│   │   └── index.tsx
│   ├── link
│   │   └── index.tsx
│   └── svg
│       └── index.tsx
```

### Services

`src/services/http-client/connection-instance.ts` is the axios base to request endpoints.
All requests should put in `src/services/http-client/*`, e.g. `src/services/http-client/search.ts`

```js
export function getBuildings(params: BuildingsRequest): Promise<AxiosResponse<Building[]>> {
  return connectionInstance.get('buildings', { params });
}
```

### Utils

When implementing features in an app, there will be bunch of auxiliary functions. Those functions can be fit in different files within the project:

`src/utils/*` - non-specific domain functions, mostly functions that enhance JS capability, e.g. null-check function, formatPrice function

When creating a util function, make sure that your filename related with functions its contains and put the file in `src/utils/*`, for example

```js
// src/utils/search.ts
export function toPoiNameSlug(text?: string) {
  return kebabCase(text);
}

function toPropertyTypeSlug(type: PropertyType) {
  return {
    [PropertyType.AnyType]: '',
    [PropertyType.Loft]: 'loft',
    [PropertyType.Condo]: 'condo',
    [PropertyType.ServicedApartment]: 'serviced-apartment',
    [PropertyType.House]: 'house',
    [PropertyType.Townhouse]: 'townhouse',
    [PropertyType.Apartment]: 'apartment',
    [PropertyType.Commercial]: 'commercial'
  }[type];
}
```

For both, use `kebab-case` if required, e.g. `src/utils/range-slider.ts` or `src/utils/date-time.ts`

### Redux Store

The project uses `@reduxjs/toolkit` to simplify common use cases like store setup, creating reducers, immutable update logic, and more.

To define/add a new redux state, add a new file name in the `src/stores/slices` folder follows required block of code, e.g. a `src/stores/slices/common` state (reducer)

```js
interface CommonState {
  // your state type definition
}

const initialState: CommonState = {
  // your initial state
};

const commonSlice = createSlice({
  name: 'common', // Used to namespace the generated action types
  initialState,
  reducers: { // A mapping from action types to action-type-specific *case reducer* functions
    setYourStateProperty(state, action: PayloadAction<YourType>) {
      state.yourStateProperty = action.payload;
    },
  }
});

export const commonReducer = commonSlice.reducer;

export const commonActions = commonSlice.actions;

export const caseReducers = commonSlice.caseReducers;

// Selectors
export const selectYourStateProperty = (state: FSAppState) => state.common.yourStateProperty;
```

then define your new reducer in `src/stores/reducers/index.ts`

```js
const rootReducer = combineReducers({
  common: commonReducer,
  search: searchReducer,
  map: mapReducer,
  // add your new reducer below
});
```

### Testing

#### Unit test
The project uses `Jest` as a testing framework and `React Testing Library` (https://testing-library.com/docs/react-testing-library/intro) as a light-weight solution for testing web pages by querying and interacting with React components through its APIs

All configurations and testing implementations put in `test` at the root project. There're two important folders that you need to take a look closely

1. `test/mocks`: defines mocking browser objects (router, window) and services (endpoint's responses)
2. `test/unit`: defines page/functionality implementations

It would be wonderful to notice that, for each page/functionality folder (e.g. `test/unit/home`). You should separate component units respectively so that you have as the cleanest/minimal test implementations as possible.

Run the command as below to develop your implementations

```shell
npm run test:watch
```

#### End-to-end test (https://www.cypress.io/)
All configurations and testing implementations put in `cypress` at the root project. There're two important folders that you need to take a look closely

1. `cypress/fixtures`: defines fixed set of data located in a file that is used in your tests. You can stub network requests and have it respond instantly with fixture data
2. `cypress/integration`: defines feature implementations

Other files/dependencies, please follow the instructions https://docs.cypress.io/guides/overview/why-cypress

Run the command as below to develop/verify your implementations

```shell
npm run cypress
```

#### QA Environment
Selecting a branch from https://gitlab.com/flexstay.rentals/fs-main-website/-/pipelines/new and clicking Run Pipeline will deploy the code to qa.propertyscout.co.th
ONLY QA TEAM SHOULD DECIDE WHAT IS DEPLOYED USING THIS FEATURE.

### Sentry
Currently, 6.3.3 is highest version of `@sentry/nextjs` works with this project, higher version cause project cannot build.

Sentry for nextjs cause pwa not works and website not working. Github issues:
- https://github.com/getsentry/sentry-javascript/issues/3538
- https://github.com/shadowwalker/next-pwa/issues/213

So for temporary fix, we have 3 fixs, and should be remove when above issues have been fixed.
1. Patch code of `@sentry/nextjs` to disable automatic inject sentry, using `scripts/patch-sentry.js` file and run it using `postinstall` script in `package.json`.
2. Manually add sentry init in `src/main.ts` for backend side.
3. Manually add sentry init in `src/pages/_app.tsx` for frontend side.


### CriticalCSS
Due to ticket https://flexstay.atlassian.net/browse/FS-689 and solution at https://twitter.com/hdjirdeh/status/1369709676271726599?lang=en. However next version 10.0.7 by some reason have bugs with it. Although next 10.0.9 has fix it but by some reason currently we locked in 10.0.7. So we have some tricky way to fix it to steps
- Clone next 10.0.7 version to local
- Apply fix from https://github.com/vercel/next.js/pull/22513
- Run `yarn`
- Take 3 files `next-server.d.ts`, `next-server.js` and `next-server.js.map` from dist folder to `scripts/next-server-patch`
- Add script to copy 3 files above to `node_modules/next/dist/next-server/server` for replace original files.

This trick should be remove when we upgrade next version