// This file configures the initialization of Sentry on the server.
// The config you add here will be used whenever the server handles a request.
// https://docs.sentry.io/platforms/javascript/guides/nextjs/

import * as Sentry from '@sentry/nextjs';

const SENTRY_DSN = process.env.SENTRY_DSN || process.env.NEXT_PUBLIC_SENTRY_DSN;

Sentry.init({
  dsn: SENTRY_DSN || 'https://e15531eef62544feb775205e9a293910@o435522.ingest.sentry.io/5732399',
  environment: process.env.NODE_ENV || 'development',
  tracesSampleRate: 0.1,
  release: `fs-main-website@${process.env.SENTRY_COMMIT_HASH || 'commit'}`,
  maxValueLength: 2500
});
