import { Language } from '@constants/enum';

function checkNotFound() {
  cy.get('h1').should('contain.text', 'The page you are looking for cannot be found');
}

context('Redirects - Serps page', () => {
  beforeEach(() => {
    cy.viewport('macbook-15');
    cy.setCookie('NEXT_LOCALE', Language.US);
  });

  it('Incorrect url structure', () => {
    cy.visit('/en/NONSENSE/rentals/', { failOnStatusCode: false });
    checkNotFound();
  });

  it('Google POIs: Any lats/long combination outside of Bangkok', () => {
    cy.visit('/en/bangkok/13.7487515-108.5832257/rentals/', { failOnStatusCode: false });
    checkNotFound();
  });

  it('Wrong Filters name', () => {
    cy.visit('/en/bangkok/rentals/bigbalcony/', { failOnStatusCode: false });
    checkNotFound();
  });

  it('Non-existed pagination', () => {
    cy.visit('/en/bangkok/asok/rentals/page-200/', { failOnStatusCode: false });
    checkNotFound();
  });

  it('No ACTIVE listing with ID', () => {
    cy.visit('/en/12345/', { failOnStatusCode: false });
    checkNotFound();
  });

  it('Duplicate property type', () => {
    cy.visit('/en/bangkok/rentals/condo+condo/', { failOnStatusCode: false });
    checkNotFound();
  });

  it('Wrong property type', () => {
    cy.visit('/en/bangkok/rentals/condo+wrong-type/', { failOnStatusCode: false });
    checkNotFound();
  });
});
