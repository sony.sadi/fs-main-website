import { Language } from '@constants/enum';

function checkNotFound() {
  cy.get('h1').should('contain.text', 'The page you are looking for cannot be found');
}

describe('Duplicated filter serp url', () => {
  context('Duplicated 1 param', () => {
    beforeEach(() => {
      cy.viewport('macbook-15');
      cy.setCookie('NEXT_LOCALE', Language.US);
    });

    it('Duplicated Price filter', () => {
      cy.visit('/en/bangkok/arun-ammarin/rentals/0thb-289474thb/0thb-289474thb/', { failOnStatusCode: false });
      checkNotFound();
    });

    it('Duplicated Bedroom filter', () => {
      cy.visit('/en/bangkok/rentals/2-bedrooms-4plus-bedrooms/2-bedrooms-4plus-bedrooms/', { failOnStatusCode: false });
      checkNotFound();
    });

    it('Duplicated Property Type filter', () => {
      cy.visit('/en/bangkok/rentals/apartment+condo/apartment+condo/', { failOnStatusCode: false });
      checkNotFound();
    });

    it('Duplicated Amenity filter', () => {
      cy.visit('/en/bangkok/arun-ammarin/rentals/balcony/balcony/ ', { failOnStatusCode: false });
      checkNotFound();
    });

    it('Duplicated Floor size filter', () => {
      cy.visit('/en/bangkok/rentals/198sqm-1000sqm/198sqm-1000sqm/', { failOnStatusCode: false });
      checkNotFound();
    });

    it('Duplicated Radius filter', () => {
      cy.visit('/en/bangkok/rentals/38000m/38000m/', { failOnStatusCode: false });
      checkNotFound();
    });
  });

  context('A mix of duplicated filters', () => {
    beforeEach(() => {
      cy.viewport('macbook-15');
      cy.setCookie('NEXT_LOCALE', Language.US);
    });

    it('Bedroom + Floor size', () => {
      cy.visit('/en/bangkok/rentals/1-bedroom-4plus-bedrooms/198sqm-1000sqm/1-bedroom-4plus-bedrooms/198sqm-1000sqm/', {
        failOnStatusCode: false
      });
      checkNotFound();
    });

    it('Property Type + Amenity', () => {
      cy.visit('/en/bangkok/arun-ammarin/rentals/condo/balcony/stove-cooktop/apartment', { failOnStatusCode: false });
      checkNotFound();
    });

    it('Radius + Floor size', () => {
      cy.visit('/en/bangkok/rentals/23sqm-919sqm/20000m/23sqm-990sqm/10000m/', { failOnStatusCode: false });
      checkNotFound();
    });

    it('Bedroom + Price', () => {
      cy.visit(
        '/en/bangkok/rentals/1-bedroom-4plus-bedrooms/2-bedrooms-4plus-bedrooms/26233thb-1000000thb/26233thb-862933thb/',
        { failOnStatusCode: false }
      );
      checkNotFound();
    });

    it('Radius + Property Type + Bedroom', () => {
      cy.visit(
        '/en/bangkok/rentals/apartment/2-bedrooms-4plus-bedrooms/condo/10000m/15000m/3-bedrooms-4plus-bedrooms/',
        { failOnStatusCode: false }
      );
      checkNotFound();
    });

    it('Price + Amenity + Bedroom', () => {
      cy.visit(
        '/en/bangkok/rentals/2-bedrooms-4plus-bedrooms/balcony+stove-cooktop/5000thb-999888thb/4plus-bedrooms/10000thb-999888thb/tv',
        {
          failOnStatusCode: false
        }
      );
      checkNotFound();
    });

    it('Price + Floor size + Property Type + Amenity', () => {
      cy.visit(
        '/en/bangkok/rentals/apartment/bathtub/42sqm-931sqm/2700thb-800000thb/balcony+swimming-pool/4800thb-600000thb/condo+townhouse/50sqm-921sqm',
        {
          failOnStatusCode: false
        }
      );
      checkNotFound();
    });
  });
});
