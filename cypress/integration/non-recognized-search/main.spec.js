import { Language } from '@constants/enum';

context('Redirects - Serps page', () => {
  beforeEach(() => {
    cy.viewport('macbook-15');
    cy.setCookie('NEXT_LOCALE', Language.US);
  });

  it('Non-recognized search', () => {
    cy.visit('/en/');
    const input = 'NONSENSE';
    cy.get('input.fs-map-input').click();
    cy.wait(500);
    cy.get('input.fs-map-input').type(input, { delay: 100 });
    cy.scrollTo('top');
    cy.wait(2000);
    cy.get('.fs-home-search-button').click({ force: true });
    cy.wait(2000);

    cy.location('pathname').should('eq', '/en/bangkok/rentals/');
  });

  it('Visit non-recognized url', () => {
    cy.visit('/en/bangkok/NONSENSE/rentals/');
    cy.location('pathname').should('eq', '/en/bangkok/rentals/');
  });
});
