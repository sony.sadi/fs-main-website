import { MAX_FILTERING_FLOOR_SIZE, MAX_FILTERING_PRICE } from '@constants';
import { Language } from '@constants/enum';
import { toPoiNameSlug, PoiType } from '@flexstay.rentals/fs-url-stringify';

function waitForDispatch() {
  cy.wait(2000);
}

function waitForRendering() {
  cy.wait(500);
}

function triggerSearch() {
  waitForDispatch();
  cy.get('.fs-filter-options__search-button').click({ force: true });
  waitForDispatch();
}

function openPriceTab() {
  cy.get('.fs-filter-options .tab-filter__price').click();
  waitForRendering();
}

function openMoreFiltersTab() {
  cy.get('.fs-filter-options .tab-filter__more_filters').click();
  waitForRendering();
}

function waitForApi() {
  cy.wait('@getSuggestion');
  cy.wait(500);
}

function changeDesktopLanguageTo(language: Language) {
  cy.get('#fs-language-toggle').click();
  cy.get(`[data-testid=${language}]`).click();
}

function triggerFilterOption(type) {
  cy.get('#toggle-serp-type').click();
  cy.get(`[data-label=${type}]`).click();
}

describe('SEO - Listings Result page', () => {
  context('Select search parameters	', () => {
    beforeEach(() => {
      cy.setCookie('NEXT_LOCALE', Language.US);
      cy.viewport('macbook-15');
      cy.visit('/en/rentals');
      cy.intercept('GET', '/frontend/suggestions*').as('getSuggestion');
      cy.intercept('GET', '/poi-transportations/search*').as('getTransportationPoi');
    });

    it('Without parameters', () => {
      cy.location('pathname').should('eq', '/en/bangkok/rentals/');
    });

    it('A Google POI', () => {
      const input = 'Bangkok, Thailand';
      cy.get('input.fs-map-input').click();
      cy.wait(500);
      cy.get('input.fs-map-input').type(input);
      // For rendering google results
      waitForApi();
      cy.get('.fs-map-input-suggestion:first-child').click();
      triggerSearch();

      cy.location('pathname').should('eq', '/en/bangkok/13.7563309-100.5017651/rentals/');
    });

    it('A free text', () => {
      const input = 'Phra Khanong Bangkok Thailand';
      cy.get('input.fs-map-input').click();
      cy.wait(500);
      cy.get('input.fs-map-input').as('input').type(input);
      triggerSearch();

      cy.location('pathname').should('eq', `/en/bangkok/rentals/`);
    });

    it('Check default Filter option', () => {
      const activeLabel = '2+';

      cy.get('.fs-filter-options .tab-filter__bed').click();
      waitForRendering();
      cy.get(`.fs-filter-options .listings-bed__option[data-label="${activeLabel}"]`).click();
      triggerSearch();

      cy.get(`[data-label=sell]`).click({force: true});
      waitForDispatch()
      triggerSearch();

      cy.location('pathname').should('eq', `/en/bangkok/sales/`);
    });

    it(`Buildings`, () => {
      cy.fixture('buildings').each((building: any) => {
        cy.intercept(
          {
            method: 'GET',
            url: `/frontend/suggestions?_q=${building.name.en.replace(/\s/g, '+')}&limit=100`
          },
          {
            data: [
              {
                id: building.id,
                name: { ...building.name, id: building.id },
                lat: 13.729432731070167,
                lng: 100.58040590195748,
                poi_type: PoiType.BUILDING,
                building_type: building.type
              }
            ]
          }
        );

        cy.visit('/en/rentals');

        cy.get('input.fs-map-input').type(building.name.en);
        waitForApi();

        // For rendering buildings and google results
        cy.get('.fs-map-input-suggestion:first-child').click();
        triggerSearch();
        cy.fixture('static-language/building-type').then((buildingType) => {
          cy.location('pathname').should(
            'eq',
            `/en/bangkok/${buildingType[building.type].en}/${toPoiNameSlug(building.name.en, true)}/rentals/`
          );
        });
      });
    });

    it('An area name', () => {
      const input = 'Phrom Phong';
      cy.get('input.fs-map-input').click();
      cy.wait(500);
      cy.get('input.fs-map-input').type(input);
      // For rendering areas and google results
      waitForApi();
      cy.get('.fs-map-input-suggestion:first-child').click();
      triggerSearch();

      cy.location('pathname').should('eq', '/en/bangkok/phrom-phong/rentals/');
    });

    it('A transportation name', () => {
      const input = 'BTS Phrom Phong';
      cy.get('input.fs-map-input').click();
      cy.wait(500);
      cy.get('input.fs-map-input').type(input);
      // For rendering transportation and google results
      waitForApi();
      cy.get('.fs-map-input-suggestion:first-child').click();
      triggerSearch();

      cy.location('pathname').should('eq', '/en/bangkok/bts/phrom-phong/rentals/');
    });

    it('Transportation URL change to TH', () => {
      cy.visit('/en/bangkok/bts/phrom-phong/rentals/');
      cy.wait(500);

      changeDesktopLanguageTo(Language.TH)
      cy.wait(500);

      cy.wait('@getTransportationPoi')
      cy.wait(500);


      cy.location('pathname').should('eq', encodeURI('/กรุงเทพ/bts/พร้อมพงษ์/ให้เช่า/'));
    });

    it('Transportation URL change to EN', () => {
      cy.visit(encodeURI('/กรุงเทพ/bts/พร้อมพงษ์/ให้เช่า/'));
      cy.wait(500);

      changeDesktopLanguageTo(Language.US)
      cy.wait(500);

      cy.wait('@getTransportationPoi')
      cy.wait(500);


      cy.location('pathname').should('eq', '/en/bangkok/bts/phrom-phong/rentals/');
    });

    it('Min price', () => {
      const input = '1000';

      openPriceTab();
      cy.get('.listings-price-range .range-input__min').type(input);
      triggerSearch();

      cy.location('pathname').should('eq', `/en/bangkok/rentals/${input}thb-${MAX_FILTERING_PRICE}thb/`);
    });

    it('Max price', () => {
      const input = '20000';

      openPriceTab();
      cy.get('.listings-price-range .range-input__max').clear().type(input);
      triggerSearch();

      cy.location('pathname').should('eq', `/en/bangkok/rentals/0thb-${input}thb/`);
    });

    it('Min/Max Price - For Rent', () => {
      triggerFilterOption('rentFilter')
      const inputMin = '1000';
      const inputMax = '900000';

      openPriceTab();
      cy.get('.listings-price-range .range-input__min').clear().type(inputMin);
      cy.wait(500)
      cy.get('.listings-price-range .range-input__max').clear().type(inputMax);
      triggerSearch();

      cy.location('pathname').should('eq', `/en/bangkok/rentals/${inputMin}thb-${inputMax}thb/`);
    });

    it('Min/Max Price - For Sale', () => {
      triggerFilterOption('buyFilter')
      const inputMin = '20000';
      const inputMax = '900000000';

      openPriceTab();
      cy.get('.listings-price-range .range-input__min').clear().type(inputMin);
      cy.wait(500)
      cy.get('.listings-price-range .range-input__max').clear().type(inputMax);
      triggerSearch();

      cy.location('pathname').should('eq', `/en/bangkok/sales/${inputMin}thb-${inputMax}thb/`);
    });

    it('Reset price - For Sale', () => {
      cy.visit('/en/bangkok/rentals/1000thb-900000thb/');
      triggerFilterOption('buyFilter')
      cy.wait(500);
      triggerSearch();

      cy.location('pathname').should('eq', `/en/bangkok/sales/`);
    });

    it('Reset price - For Rent', () => {
      cy.visit('/en/bangkok/sales/20000thb-900000000thb/');
      triggerFilterOption('rentFilter')
      cy.wait(500);
      triggerSearch();

      cy.location('pathname').should('eq', `/en/bangkok/rentals/`);
    });




    it('Unit type', () => {
      const activeLabel = '2+';
      const activeSlug = '2-bedrooms-4plus-bedrooms';

      cy.get('.fs-filter-options .tab-filter__bed').click();
      waitForRendering();
      cy.get(`.fs-filter-options .listings-bed__option[data-label="${activeLabel}"]`).click();
      triggerSearch();

      cy.location('pathname').should('eq', `/en/bangkok/rentals/${activeSlug}/`);
    });

    it('Property type', () => {
      const activeLabel = 'Apartment';
      const activeSlug = 'apartment';

      openMoreFiltersTab();
      cy.get(`.fs-filter-options .select-button[data-label=${activeLabel}]`).click();
      triggerSearch();

      cy.location('pathname').should('eq', `/en/bangkok/rentals/${activeSlug}/`);
    });

    it('Min lease', () => {
      const activeLabel = 'Up to 2 mo.';
      const activeSlug = 'two-months';

      openMoreFiltersTab();
      cy.get(`.fs-filter-options .select-button[data-label="${activeLabel}"]`).click();
      triggerSearch();

      cy.location('pathname').should('eq', `/en/bangkok/rentals/${activeSlug}/`);
    });

    it('Property amenities', () => {
      const activeLabels = ['Balcony', 'Walk-in-wardrobe', 'TV'];
      const activeSlug = 'balcony+tv+walk-in-wardrobe';
      const selector = activeLabels.map((label) => `.select-button[data-label="${label}"]`).join(',');

      openMoreFiltersTab();

      cy.get('.fs-filter-options').find(selector).click({ multiple: true });
      triggerSearch();

      cy.location('pathname').should('eq', `/en/bangkok/rentals/${activeSlug}/`);
    });

    it('Min Floor Size', () => {
      const input = '100';

      openMoreFiltersTab();
      cy.get('.listings-floor-size .range-input__min').type(input);
      triggerSearch();

      cy.location('pathname').should('eq', `/en/bangkok/rentals/${input}sqm-${MAX_FILTERING_FLOOR_SIZE}sqm/`);
    });

    it('Max Floor Size', () => {
      const input = '300';

      openMoreFiltersTab();

      cy.get('.listings-floor-size .range-input__max').clear().type(input);
      triggerSearch();

      cy.location('pathname').should('eq', `/en/bangkok/rentals/0sqm-${input}sqm/`);
    });

    it('Min and Max Floor Size', () => {
      const min = '300';
      const max = '600';

      openMoreFiltersTab();
      cy.get('.listings-floor-size .range-input__min').clear().type(min);
      cy.get('.listings-floor-size .range-input__max').clear().type(max);
      triggerSearch();

      cy.location('pathname').should('eq', `/en/bangkok/rentals/${min}sqm-${max}sqm/`);
    });

    it('Radius', () => {
      const input = '10';

      openMoreFiltersTab();
      cy.get('.fs-filter-options__radius .input-number__input').clear({ force: true }).type(input);
      triggerSearch();

      cy.location('pathname').should('eq', `/en/bangkok/rentals/${Number(input) * 1000}m/`);
    });

    it('Radius with 1Km', () => {
      const input = '1';

      openMoreFiltersTab();
      cy.get('.fs-filter-options__radius .input-number__input').clear({ force: true }).type(input);
      triggerSearch();

      cy.location('pathname').should('eq', `/en/bangkok/rentals/${Number(input) * 1000}m/`);
    });

    it('Sorting', () => {
      const label = 'Newest listing first';

      cy.get('.fs-filter-options__sorting-dropdown').as('dropdown').click();
      cy.get('@dropdown').find(`.fs-select__option[data-label="${label}"]`).click({ force: true });

      cy.location('pathname').should('eq', `/en/bangkok/rentals/latest/`);
    });

    context('Combine different parameters', () => {
      it('A Building Name + Min Lease + Amenities', () => {
        const buildingName = 'Parkway Chalet Ramkhamhaeng';
        const activeMinLeaseLabel = 'Up to 3 mo.';
        const activeMinLeaseSlug = 'three-months';
        const activeAmenitiesLabels = ['Balcony', 'Pet-Friendly', 'Pool'];
        const activeAmenitiesSlug = 'balcony+pet-friendly+swimming-pool';
        const activeAmenitiesSelector = activeAmenitiesLabels
          .map((label) => `.select-button[data-label="${label}"]`)
          .join(',');

        // Building name
        cy.get('input.fs-map-input').click();
        cy.wait(500);
        cy.get('input.fs-map-input').type(buildingName);
        waitForApi();
        cy.get('.fs-map-input-suggestion:first-child').click();

        // More filters
        openMoreFiltersTab();
        // Min Lease
        cy.get(`.fs-filter-options .select-button[data-label="${activeMinLeaseLabel}"]`).click();

        // Amenities
        cy.get('.fs-filter-options').find(activeAmenitiesSelector).click({ multiple: true });
        triggerSearch();

        cy.location('pathname').should(
          'eq',
          `/en/bangkok/house/parkway-chalet-ramkhamhaeng/rentals/${activeMinLeaseSlug}/${activeAmenitiesSlug}/`
        );
      });

      it('A Transportation Name + Property Type + Floor Size + Radius', () => {
        const transportationName = 'Thong Lor';
        const activePropertyTypeLabel = 'House';
        const activePropertyTypeSlug = 'house';
        const minFloorSize = '100';
        const maxFloorSize = '400';
        const floorSizeSlug = `${minFloorSize}sqm-${maxFloorSize}sqm`;
        const radiusInput = '18';
        const radiusSlug = '18000m';

        // Building name
        cy.get('input.fs-map-input').click();
        cy.wait(500);
        cy.get('input.fs-map-input').type(transportationName);
        waitForApi();
        cy.get('.fs-map-input-suggestion:first-child').click();

        // More filters
        openMoreFiltersTab();

        // Property type
        cy.get(`.fs-filter-options .select-button[data-label=${activePropertyTypeLabel}]`).click();

        // Floor Size
        cy.get('.listings-floor-size .range-input__min').clear().type(minFloorSize);
        cy.get('.listings-floor-size .range-input__max').clear().type(maxFloorSize);
        cy.get('.fs-filter-options__radius .input-number__input').click({ force: true });
        cy.wait(500);
        cy.get('.fs-filter-options__radius .input-number__input').clear({ force: true }).type(radiusInput);

        triggerSearch();

        cy.location('pathname').should(
          'eq',
          `/en/bangkok/bts/thong-lor/rentals/${activePropertyTypeSlug}/${floorSizeSlug}/${radiusSlug}/`
        );
      });
    });
  });
});
