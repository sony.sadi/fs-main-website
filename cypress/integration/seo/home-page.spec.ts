import { MAX_FILTERING_PRICE } from '@constants';
import { Language } from '@constants/enum';
import { toPoiNameSlug, PoiType } from '@flexstay.rentals/fs-url-stringify';

function waitForRedirection() {
  cy.wait(1500);
}

function waitForDispatch() {
  cy.wait(2000);
}

function triggerSearch() {
  cy.get('.fs-home-search-button').click({ force: true });
}

function openPriceSelection() {
  cy.get('.fs-home-search__any-price').click();
  cy.wait(500);
}

function triggerFilterOption(type) {
  cy.get('#toggle-serp-type').click();
  cy.get(`[data-label=${type}]`).click();
}

describe('SEO - Home page', () => {
  context('Select search parameters', () => {
    beforeEach(() => {
      cy.setCookie('NEXT_LOCALE', Language.US);
      cy.viewport('macbook-15');
      cy.visit('/');
    });

    it('Without parameters', () => {
      triggerSearch();
      cy.location('pathname').should('eq', '/en/bangkok/rentals/');
    });

    it('Without parameters - Filter by Rent', () => {
      triggerFilterOption('rentFilter')
      triggerSearch();
      cy.location('pathname').should('eq', '/en/bangkok/rentals/');
    });

    it('Without parameters - Filter by Sale', () => {
      triggerFilterOption('buyFilter')
      triggerSearch();
      cy.location('pathname').should('eq', '/en/bangkok/sales/');
    });

    it('Filter price for Sale', () => {
      const input = '900000000';
      triggerFilterOption('buyFilter')

      openPriceSelection();
      cy.get('input.range-input__max').clear().type(input);
      waitForDispatch();
      cy.get('.hero-prices__apply-filter').click();

      waitForDispatch();
      triggerSearch();
      waitForRedirection();

      cy.location('pathname').should('eq', `/en/bangkok/sales/0thb-${input}thb/`);
    });

    it('Filter price for Rent', () => {
      const input = '4000';
      triggerFilterOption('rentFilter')

      openPriceSelection();
      cy.get('input.range-input__max').clear().type(input);
      waitForDispatch();
      cy.get('.hero-prices__apply-filter').click();

      waitForDispatch();
      triggerSearch();
      waitForRedirection();

      cy.location('pathname').should('eq', `/en/bangkok/rentals/0thb-${input}thb/`);
    });

    it('A Google POI', () => {
      const input = 'Bangkok, Thailand';
      cy.get('input.fs-map-input').click();
      cy.wait(500);
      cy.get('input.fs-map-input').type(input, { delay: 100 });
      cy.scrollTo('top');
      // For rendering google results
      waitForDispatch();
      cy.get('.fs-map-input-suggestion:first-child').click();
      waitForDispatch();
      triggerSearch();
      waitForRedirection();

      cy.location('pathname').should('eq', '/en/bangkok/13.7563309-100.5017651/rentals/');
    });

    it('A free text', () => {
      const input = 'Phra Khanong Bangkok Thailand';
      cy.get('input.fs-map-input').click();
      cy.wait(500);
      cy.get('input.fs-map-input').as('input').type(input);
      cy.scrollTo('top');
      waitForDispatch();
      cy.get('@input').type('{enter}');
      waitForRedirection();

      cy.location('pathname').should('eq', '/en/bangkok/rentals/');
    });

    it(`Buildings`, () => {
      cy.fixture('buildings').each((building: any) => {
        cy.intercept(
          {
            method: 'GET',
            url: `/frontend/suggestions?_q=${building.name.en.replace(/\s/g, '+')}&limit=100`
          },
          {
            data: [
              {
                id: building.id,
                name: { ...building.name, id: building.id },
                lat: 13.729432731070167,
                lng: 100.58040590195748,
                poi_type: PoiType.BUILDING,
                building_type: building.type
              }
            ]
          }
        );

        cy.visit('/');
        cy.get('input.fs-map-input').click();
        cy.wait(500);
        cy.get('input.fs-map-input').type(building.name.en, { delay: 100 });
        cy.scrollTo('top');
        waitForDispatch();

        // For rendering buildings and google results
        cy.get('.fs-map-input-suggestion:first-child').click();
        waitForDispatch();
        triggerSearch();
        waitForRedirection();
        cy.fixture('static-language/building-type').then((buildingType) => {
          cy.location('pathname').should(
            'eq',
            `/en/bangkok/${buildingType[building.type].en}/${toPoiNameSlug(building.name.en, true)}/rentals/`
          );
        });
      });
    });

    it('An area name', () => {
      const input = 'Phrom Phong';
      cy.get('input.fs-map-input').click();
      cy.wait(500);
      cy.get('input.fs-map-input').type(input, { delay: 100 });
      cy.scrollTo('top');
      // For rendering areas and google results
      waitForDispatch();
      cy.get('.fs-map-input-suggestion:first-child').click();
      waitForDispatch();
      triggerSearch();
      waitForRedirection();

      cy.location('pathname').should('eq', '/en/bangkok/phrom-phong/rentals/');
    });

    it('A transportation name', () => {
      const input = 'BTS Phrom Phong';
      cy.get('input.fs-map-input').click();
      cy.wait(500);
      cy.get('input.fs-map-input').type(input, { delay: 100 });
      cy.scrollTo('top');
      // For rendering transportation and google results
      waitForDispatch();
      cy.get('.fs-map-input-suggestion:first-child').click();
      waitForDispatch();
      triggerSearch();
      waitForRedirection();

      cy.location('pathname').should('eq', '/en/bangkok/bts/phrom-phong/rentals/');
    });

    it('Default price', () => {
      openPriceSelection();
      cy.get('.hero-prices__apply-filter').click();

      waitForDispatch();
      triggerSearch();
      waitForRedirection();

      cy.location('pathname').should('eq', `/en/bangkok/rentals/0thb-${MAX_FILTERING_PRICE}thb/`);
    });

    it('Min price', () => {
      const input = '1000';

      openPriceSelection();
      cy.get('input.range-input__min').type(input);
      waitForDispatch();
      cy.get('.hero-prices__apply-filter').click();

      waitForDispatch();
      triggerSearch();
      waitForRedirection();

      cy.location('pathname').should('eq', `/en/bangkok/rentals/${input}thb-${MAX_FILTERING_PRICE}thb/`);
    });

    it('Max price', () => {
      const input = '40000';

      openPriceSelection();
      cy.get('input.range-input__max').clear().type(input);
      waitForDispatch();
      cy.get('.hero-prices__apply-filter').click();

      waitForDispatch();
      triggerSearch();
      waitForRedirection();

      cy.location('pathname').should('eq', `/en/bangkok/rentals/0thb-${input}thb/`);
    });

    it('Unit type', () => {
      cy.get('.fs-home-search__unit-type').click();

      // For rendering popup
      cy.wait(500);
      cy.get('.hero-beds__option').last().click();
      cy.get('.hero-beds__apply-filter').click();

      triggerSearch();
      waitForRedirection();

      cy.location('pathname').should('eq', '/en/bangkok/rentals/4plus-bedrooms/');
    });

    it('Combine all parameters', () => {
      const buildingName = 'Sribumpen';
      const minPrice = '1000';
      const maxPrice = '30000';
      const unitType = '2+';

      // Search a building name
      cy.get('input.fs-map-input').click();
      cy.wait(500);
      cy.get('input.fs-map-input').type(buildingName, { delay: 100 });
      cy.scrollTo('top');
      waitForDispatch();
      cy.get('.fs-map-input-suggestion:first-child').click();

      // Fill prices
      openPriceSelection();
      cy.get('input.range-input__min').clear().type(minPrice);
      waitForDispatch();
      cy.get('input.range-input__max').clear().type(maxPrice);
      waitForDispatch();
      cy.get('.hero-prices__apply-filter').click();

      // Select a unit type
      cy.get('.fs-home-search__unit-type').click({ force: true });
      cy.wait(500);
      cy.get(`.hero-beds__option[data-label="${unitType}"]`).click({ force: true });
      cy.get('.hero-beds__apply-filter').click({ force: true });

      waitForDispatch();
      triggerSearch();
      waitForRedirection();
      cy.location('pathname').should(
        'eq',
        '/en/bangkok/condo/sribumpen-condo-home/rentals/2-bedrooms-4plus-bedrooms/1000thb-30000thb/'
      );
    });

    it('POI building slug', () => {
      const input = 'The ACE Ekamai';
      const slug = toPoiNameSlug(input, true);

      cy.get('input.fs-map-input').as('input').click();
      waitForDispatch();

      cy.get('@input').type(input, { delay: 100 });
      cy.scrollTo('top');
      waitForDispatch();

      cy.get('body')
        .find('.fs-map-input-suggestion > span')
        .contains(new RegExp(`^${input}$`))
        .click();

      cy.get('#search-target').click();

      waitForRedirection();

      cy.location('pathname').should('eq', `/en/bangkok/condo/${encodeURIComponent(slug)}/rentals/`);

      expect(slug).to.equal('ace-ekamai');
    });
  });
});
