import { Language } from '@constants/enum';

context('Redirects - Serps page', () => {
  beforeEach(() => {
    cy.viewport('macbook-15');
  });

  it('TH: from `/rent` to `/province/rent`', () => {
    cy.setCookie('NEXT_LOCALE', Language.TH);
    cy.visit(encodeURI('/ให้เช่า/'));
    cy.location('pathname').should('eq', encodeURI('/กรุงเทพ/ให้เช่า/'));
  });

  it('EN: from `/rent` to `/province/rent`', () => {
    cy.setCookie('NEXT_LOCALE', Language.US);
    cy.visit('/en/rentals/');
    cy.location('pathname').should('eq', '/en/bangkok/rentals/');
  });

  it('TH: from `/rent/transportationSystem` to `/province/rent`', () => {
    cy.setCookie('NEXT_LOCALE', Language.TH);
    cy.visit(encodeURI('/ให้เช่า/bts/'));
    cy.location('pathname').should('eq', encodeURI('/กรุงเทพ/ให้เช่า/'));
  });

  it('EN: from `/rent/transportationSystem` to `/province/rent`', () => {
    cy.setCookie('NEXT_LOCALE', Language.US);
    cy.visit('/en/rentals/bts/');
    cy.location('pathname').should('eq', '/en/bangkok/rentals/');
  });

  it('TH: from `/province/rent/transportationSystem` to `/province/rent`', () => {
    cy.setCookie('NEXT_LOCALE', Language.TH);
    cy.visit(encodeURI('/กรุงเทพ/ให้เช่า/bts/'));
    cy.location('pathname').should('eq', encodeURI('/กรุงเทพ/ให้เช่า/'));
  });

  it('EN: from `/province/rent/transportationSystem` to `/province/rent`', () => {
    cy.setCookie('NEXT_LOCALE', Language.US);
    cy.visit('/en/bangkok/rentals/bts/');
    cy.location('pathname').should('eq', '/en/bangkok/rentals/');
  });

  it('TH: from `/province/name/rent/transportationSystem` to `/province/name/rent`', () => {
    cy.setCookie('NEXT_LOCALE', Language.TH);
    cy.visit(encodeURI('/กรุงเทพ/อโศก/ให้เช่า/bts/'));
    cy.location('pathname').should('eq', encodeURI('/กรุงเทพ/อโศก/ให้เช่า/'));
  });

  it('EN: from `/province/name/rent/transportationSystem` to `/province/name/rent`', () => {
    cy.setCookie('NEXT_LOCALE', Language.US);
    cy.visit('/en/bangkok/asok/rentals/bts/');
    cy.location('pathname').should('eq', '/en/bangkok/asok/rentals/');
  });

  it('TH: from `/transportationSystem` to `/province/transportationSystem`', () => {
    cy.setCookie('NEXT_LOCALE', Language.TH);
    cy.visit(encodeURI('/bts/'));
    cy.location('pathname').should('eq', encodeURI('/กรุงเทพ/bts/'));
  });

  it('EN: from `/transportationSystem` to `/province/transportationSystem`', () => {
    cy.setCookie('NEXT_LOCALE', Language.US);
    cy.visit(encodeURI('/en/bts/'));
    cy.location('pathname').should('eq', '/en/bangkok/bts/');
  });

  it('TH: from `/propertyType` to `/province/rent/propertyType`', () => {
    cy.setCookie('NEXT_LOCALE', Language.TH);
    cy.visit(encodeURI('/คอนโด/'));
    cy.location('pathname').should('eq', encodeURI('/กรุงเทพ/ให้เช่า/คอนโด/'));
  });

  it('EN: from `/propertyType` to `/province/rent/propertyType`', () => {
    cy.setCookie('NEXT_LOCALE', Language.US);
    cy.visit(encodeURI('/en/condo/'));
    cy.location('pathname').should('eq', '/en/bangkok/rentals/condo/');
  });

  it('TH: from `/rent/propertyType` to `/province/rent/propertyType`', () => {
    cy.setCookie('NEXT_LOCALE', Language.TH);
    cy.visit(encodeURI('/ให้เช่า/คอนโด/'));
    cy.location('pathname').should('eq', encodeURI('/กรุงเทพ/ให้เช่า/คอนโด/'));
  });

  it('EN: from `/rent/propertyType` to `/province/rent/propertyType`', () => {
    cy.setCookie('NEXT_LOCALE', Language.US);
    cy.visit('/en/rentals/condo/');
    cy.location('pathname').should('eq', '/en/bangkok/rentals/condo/');
  });
});
