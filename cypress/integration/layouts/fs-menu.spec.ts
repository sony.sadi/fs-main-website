import { LanguageContent, LanguageMobileContent } from '@interfaces/home';
import { Language } from '@constants/enum';

function changeDesktopLanguageTo(language: Language) {
  cy.get('#fs-language-toggle').click();
  cy.get(`[data-testid=${language}]`).click();
}

function changeMobileLanguageTo(language: Language) {
  cy.get('.h-menu-mobile [data-testid=selected-toggle]').click();
  cy.get(`[data-testid=${language}]`).click();
}


describe('Layout FsMenu', () => {
  context('with desktop viewport', () => {
    beforeEach(() => {
      cy.viewport('macbook-13');
      cy.visit('/en');
    });

    it('should show navigation links with default language', () => {
      cy.fixture('layouts/desktop-links').each((link: LanguageContent) => {
        cy.get('.h-menu').should('contain', link.en);
      });
    });

    // context('with multiple languages', () => {
    //   it("displays 'th' language messages", () => {
    //     changeDesktopLanguageTo(Language.TH);

    //     cy.fixture('layouts/desktop-links').each((link: LanguageContent) => {
    //       cy.get('.h-menu').should('contain', link.th);
    //     });
    //   });
    // });
  });

  context('with mobile viewport', () => {
    beforeEach(() => {
      cy.viewport('ipad-mini');
      cy.wait(200);
      cy.visit('/en');
    });

    it('should display a menu bar', () => {
      cy.get('.h-menu-mobile [data-testid=red-menu-bar]').should('exist');
    });

    it('should show navigation links with default language', () => {
      cy.get('.h-menu-mobile [data-testid=red-menu-bar]').click();

      cy.fixture('layouts/mobile-links').each((link: LanguageMobileContent, index: number) => {
        if(link.subItem){
          cy.get(`.h-menu-mobile #fs-mobile-menu [data-testid=mobile-arrow-${index}]`).click();
          link.subItem.forEach((value) =>{
            cy.get('.h-menu-mobile #fs-mobile-menu ul').should('contain', value.en);
          })
        }else{
          cy.get('.h-menu-mobile #fs-mobile-menu').should('contain', link.name.en);
        }

      });
    });

    // context('with multiple languages', () => {
    //   it("displays 'th' language messages", () => {
    //     changeMobileLanguageTo(Language.TH);


    //     cy.fixture('layouts/mobile-links').each((link: LanguageContent) => {
    //       cy.get('.fs-menu .fs-mobile-menu').should('contain', link.th);
    //     });
    //   });
    // });
  });
});
