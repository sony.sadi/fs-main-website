import { SearchRequest, PoiSuggestion } from './search';

export interface CoordinateParams {
  minMapLat: number;
  maxMapLat: number;
  minMapLng: number;
  maxMapLng: number;
  limit: number;
}

export interface RentalsCoordinateParams extends SearchRequest, CoordinateParams {}

export interface Coordinate {
  id: number;
  gpsLat: number;
  gpsLong: number;
}

export interface RentalsCoordinateResponse {
  total: number;
  page: number;
  perPage: number;
  data: Coordinate[];
}

export interface MapInputCoordinates {
  lat: number;
  lng: number;
  address: string;
  poi?: PoiSuggestion;
}
