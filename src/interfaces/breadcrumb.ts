import { BreadcrumbElement } from '@constants/enum/breadcrumb';
import { SearchRequest } from './search';

export interface BreadcrumbParams extends SearchRequest {
  id?: number;
}

export interface GeneratedBreadcrumb {
  label: string;
  path: string;
  rel?: string;
}

export interface BreadcrumbItem {
  label?: string;
  params: BreadcrumbParams;
  element: BreadcrumbElement;
}
