import { Language } from '@constants/enum';
import { Transportation } from './search';

export interface LandingTransportRequest {
  locale: Language;
  poi: Transportation;
}

type I18nRecord = Record<Language, string>;

export interface LandingTransportContent {
  station_title: I18nRecord;
  location_title: I18nRecord;
  site_title: I18nRecord;
  faq_title: I18nRecord;
  location_map_title: I18nRecord;
  recent_rent_title: I18nRecord;
  recent_sale_title: I18nRecord;
  top_five_title: I18nRecord;
  station_content: I18nRecord;
  location_content: I18nRecord;
  site_content: I18nRecord;
  top_five_content: I18nRecord;
  faq_ask: I18nRecord;
  hyperlink_rent: I18nRecord;
  hyperlink_sale: I18nRecord;
  meta_title: I18nRecord;
  meta_description: I18nRecord;
  faqs: Array<{
    question: I18nRecord;
    answer: I18nRecord;
  }>;
}
