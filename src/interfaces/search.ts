import { Language } from '@constants/enum';
import {
  PropertyType,
  MinLease,
  Amenity,
  SortType,
  NumberOfBed,
  PoiType,
  TransportationSystem,
  BuildingType,
  SerpType
} from '@flexstay.rentals/fs-url-stringify';
import { Listing } from '@interfaces/listing';

export interface BuildingsRequest {
  name_en_contains?: string;
  name_th_contains?: string;
  _limit?: number;
}

export interface Building {
  id: number;
  name: string;
  lng: number;
  lat: number;
  name_en: string;
  name_th: string;
}

export interface SearchRequest {
  where?: string;
  buildingId?: number;
  building_type?: BuildingType;
  areaId?: number;
  transportationId?: number;
  transportationSystem?: TransportationSystem;
  keyword?: string;
  coords: string;
  minPrice: number;
  maxPrice: number;
  floorSize_lte: number;
  floorSize_gte: number;
  numberBedrooms: NumberOfBed;
  propertyType: PropertyType[];
  minLease: MinLease;
  amenities: Amenity[];
  radius: number;
  page: number;
  limit: number;
  sortBy?: SortType;
  serp_type?: SerpType;
}

export interface SearchResponse {
  total: number;
  page: number;
  perPage: number;
  aggs?: {
    lowest_price: number;
    highest_price: number;
  };
  data: Listing[];
}

export interface PoisSuggestionRequest {
  _q: string;
  limit: number;
  locale?: Language;
}

export interface GenericPoisSuggestionRequest {
  _q: string;
  _limit?: number;
  transportation_system?: TransportationSystem;
}

export interface PoisSuggestionResponse {
  total: number;
  page: number;
  per_page: number;
  data: PoiSuggestion[];
}

export interface GenericPoisSuggestionResponse {
  total: number;
  data: PoiSuggestion[];
}

export interface GenericPoiSuggestion {
  id: number;
}

export interface PoiSuggestion {
  id: number;
  name: {
    th: string;
    en: string;
    id: number;
  };
  lat: number;
  lng: number;
  poi_type: PoiType;
  building_type?: BuildingType;
  transportation_system?: TransportationSystem;
  place_id?: string;
}

export interface Transportation {
  id: number;
  station: {
    th: string;
    en: string;
    id: number;
  };
  gps_lat: number;
  gps_long: number;
  transportation_system: TransportationSystem;
  postal_code: string;
  address_street: {
    en: string;
    th: string;
  };
  opening_hours: string;
  stop_id: string;
  province_ps: {
    en: string;
    th: string;
  };
  medias: string[];
}

export interface TransportationResponse {
  total: number;
  page: number;
  perPage: number;
  data: Transportation[];
}

export interface Area {
  images: {
    name: string;
    id: number;
    url: string;
  }[];
  postcode: string;
  name: {
    th: string;
    en: string;
    id: number;
  };
  id: number;
  priority: string;
  population: number;
  teaser: {
    th: string;
    en: string;
    id: number;
  };
  short_teaser: {
    th: string;
    en: string;
    id: number;
  };
  center_lat: number;
  center_lng: number;
}

export interface AreaResponse {
  total: number;
  page: number;
  perPage: number;
  data: Area[];
}

export interface InDemandBuilding {
  short_description: {
    th: string;
    en: string;
    id: number;
  };
  images: {
    name: string;
    id: number;
    url: string;
  }[];
  name: {
    th: string;
    en: string;
    id: number;
  };
  slug: {
    th: string;
    en: string;
  };
  id: number;
  priority: string;
  lat: number;
  lng: number;
  building_type?: BuildingType;
}

export interface InDemandBuildingResponse {
  total: number;
  page: number;
  perPage: number;
  data: InDemandBuilding[];
}

export interface SimilarListing {
  price: number;
  lat: number;
  long: number;
  serp_type: SerpType;
}

export interface FavouriteRental {
  user_id: string | number;
  added_at: string;
  rental_id: number;
  source: string;
}

export interface PropertyPadBase {
  id: number;
  user_id: number | string;
  hubspotdealurl: string;
  identifier: string;
  name: string;
  isServerIdentifier: boolean;
}
export interface PropertyPad extends PropertyPadBase {
  favouriteRentals: FavouriteRental[];
}
export interface PropertyPadResponse extends PropertyPadBase {
  rental_ids: FavouriteRental[];
}
export interface FetchRentalRequest {
  rentals: FavouriteRental[];
  page: number;
  totalAddress: number;
  sort?: SortType;
}

export interface FetchRentalResponse {
  data: Listing[];
  page: number;
}
