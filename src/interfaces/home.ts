import { Area, InDemandBuilding } from './search';

export interface LanguageMobileContent {
  name: LanguageContent;
  subItem: LanguageContent[];
}
export interface LanguageContent {
  en: string;
  th: string;
}

export interface OurOffering {
  icon: string;
  title: LanguageContent;
  description: LanguageContent;
  details: LanguageContent[];
}

export interface PopularNeighborhood {
  name: LanguageContent;
  cover: string;
  link: string;
}

export interface OurPartner {
  name: string;
  image: string;
}

export interface Property {
  title: string;
  items: {
    label: string;
    path: string;
    rel?: string;
  }[];
}

export interface Basics {
  icon: string;
  content: string;
  href: string;
}

export interface Contact {
  offices: Offices[];
}

export interface Offices {
  name: string;
  address: string;
  building: string;
}

export interface AllRightReserved {
  year: string;
  terms: {
    label: string;
    path: string;
  }[];
  socials: { icon: string; href: string }[];
}

export interface FooterInterface {
  intros: LanguageContent[];
  about: Property;
  property: Property;
  contact: Contact;
  basics: Basics[];
  allRightsReserved: AllRightReserved;
}

export interface HomeBuildings {
  popularAreas: Area[];
  inDemandBuildings: InDemandBuilding[];
}
