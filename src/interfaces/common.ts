export interface EmailJSInformation {
  page: string;
  url: string;
}

export interface BaseApiResponse<T> {
  data: T[];
  total: number;
  page: number;
  perPage: number;
}
