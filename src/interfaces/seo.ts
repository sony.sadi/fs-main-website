export interface SeoResponse {
  description: string;
  singular_description?: string;
  empty_result_description?: string;
  meta_title: string;
  singular_meta_title?: string;
  meta_description: string;
  singular_meta_description?: string;
  title: string;
  singular_title?: string;
  title_counter: string;
}

export interface AssembleSeoResponse {
  description: string;
  meta_title: string;
  meta_description: string;
  title: string;
  title_counter: string;
  schema_title: string;
  schema_description: string;
}
