import React, { useState, useEffect, useRef } from 'react';

/**
 * Hook return a value that check an element is viewed on screen or not
 * @param ref // Reference element which we want to observe
 * @param disableRecall // Disable to invoke setIntersecting value each time IntersectionObserver is called
 * @returns
 */
export const useOnScreen = (ref: React.RefObject<Element>, disableRecall: boolean | undefined) => {
  const [isIntersecting, setIntersecting] = useState<boolean>(false);

  const avoidCall = useRef<boolean>(false);

  const observer = new IntersectionObserver(([entry]) => {
    if (!avoidCall.current) {
      // set intersecting value when element is viewed
      setIntersecting(entry.isIntersecting);
    }

    // Passing disableRecall = true, and at the first time element is viewed,
    // this will be invoked to avoid set intersecting value each time the element is viewed.
    if (disableRecall && !avoidCall.current) {
      avoidCall.current = entry.isIntersecting;
    }
  });

  useEffect(() => {
    if (ref.current) observer.observe(ref.current);
    return () => {
      observer.disconnect();
    };
  }, []);

  return isIntersecting;
};
