import { useRouter } from 'next/router';
import { useCallback } from 'react';
import cookie from 'js-cookie';
import { useTranslation } from 'next-i18next';

const useChangeLanguage = () => {
  const { query, locale: currentLocale } = useRouter();
  const { i18n } = useTranslation();

  const changeLanguage = useCallback(
    async (locale: string, options?: { cookie?: boolean }) => {
      if (currentLocale === locale) return;
      const { cookie: saveCookie = true } = options || {};
      const tLocale = await i18n.changeLanguage(locale);
      saveCookie && cookie.set('NEXT_LOCALE', locale, { expires: 365 });
      const event = new CustomEvent('locale_changed', { detail: { locale, tLocale } });
      document.dispatchEvent(event);
    },
    [query, currentLocale]
  );

  return changeLanguage;
};

export default useChangeLanguage;
