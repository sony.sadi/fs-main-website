import { useEffect, useRef, useState } from 'react';

type Callback<S> = (state: S) => void | (() => void | undefined);

function useStateWithCallbackLazy<T>(initialValue: T) {
  const callbackRef = useRef<Callback<T>>();

  const [value, setValue] = useState(initialValue);

  useEffect(() => {
    if (callbackRef.current) {
      callbackRef.current(value);
    }
  }, [value]);

  const setValueWithCallback = (newValue: T, callback: Callback<T>) => {
    callbackRef.current = callback;

    return setValue(newValue);
  };

  return [value, setValueWithCallback];
}

export default useStateWithCallbackLazy;
