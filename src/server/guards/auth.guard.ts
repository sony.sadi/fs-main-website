import { ExecutionContext } from '@nestjs/common';
import { IncomingMessage } from 'http';

export class AuthGuard {
  async canActivate(context: ExecutionContext) {
    const req: IncomingMessage = context.getArgs()[0];
    return req.headers['ps-api-key'] === process.env.NEXT_WEBSITE_API_KEY;
  }
}
