/* eslint no-await-in-loop: "off" */
// need await in the loop for background job
import axios, { AxiosResponse } from 'axios';
import fs from 'fs-extra';
import { SitemapStream, streamToPromise, SitemapIndexStream, SitemapItem, EnumChangefreq, LinkItem } from 'sitemap';
import { Readable } from 'stream';
import times from 'lodash/times';
import trimEnd from 'lodash/trimEnd';
import { SearchRequest } from '@interfaces/search';
import { MinLease, NumberOfBed } from '@flexstay.rentals/fs-url-stringify';
import { join } from 'path';
import { Language } from '@constants/enum';
import { SITEMAP_LANGUAGES } from '@server/common';

interface SerpSitemapResponse {
  perPage: number;
  page: number;
  data: Array<{ priority: number; en_url: string; th_url: string }>;
}

class SerpXml {
  async build() {
    if (!SITEMAP_LANGUAGES.length) return;

    const shouldAddAlternateLink = SITEMAP_LANGUAGES.length > 1;

    const axiosBaseUrl = trimEnd(process.env.NEXT_PUBLIC_EGSWING_API_BASE_URL, '/');
    const hostname = trimEnd(process.env.NEXT_PUBLIC_HOME_URL, '/');
    let page = 1;
    const limit = Number(process.env.NEXT_SITEMAP_PAGE_SIZE) || 50000;
    const requestUrl = `${axiosBaseUrl}/serp-combinations/sitemap`;
    let errorTimes = 0;

    while (true) {
      try {
        if (errorTimes > 50) break;
        const result: AxiosResponse<SerpSitemapResponse> = await axios.get(requestUrl, {
          params: {
            limit,
            page
          }
        });
        const list = result.data.data;
        if (!list.length) break;

        const sitemapItems = Object.fromEntries(SITEMAP_LANGUAGES.map((lang) => [lang, [] as SitemapItem[]]));

        list.forEach((serp) => {
          const serpLink = {
            [Language.US]: join('/en', serp.en_url, '/'),
            [Language.TH]: join('/', serp.th_url, '/')
          };
          let links: LinkItem[] = [];
          if (shouldAddAlternateLink) {
            links = SITEMAP_LANGUAGES.map((lang) => ({
              url: serpLink[lang],
              lang,
              hreflang: lang
            }));
          }
          SITEMAP_LANGUAGES.forEach((lang) => {
            sitemapItems[lang].push({
              url: serpLink[lang],
              changefreq: EnumChangefreq.DAILY,
              priority: serp.priority,
              fullPrecisionPriority: true,
              links,
              video: [],
              img: []
            });
          });
        });

        for (let index = 0; index < SITEMAP_LANGUAGES.length; index++) {
          const language = SITEMAP_LANGUAGES[index];
          const pageStream = new SitemapStream({
            hostname,
            xmlns: {
              xhtml: true,
              video: false,
              image: false,
              news: false
            }
          });
          const pageContent = await streamToPromise(Readable.from(sitemapItems[language]).pipe(pageStream));
          await fs.writeFile(`./public/serps_sitemap_${language}_${page}.xml`, pageContent);
        }
        page++;
      } catch (e) {
        console.error(e);
        errorTimes++;
      }
    }

    const childrenItems = Object.fromEntries(
      SITEMAP_LANGUAGES.map((lang) => [
        lang,
        times(page - 1, (num) => ({
          url: `${hostname}/serps_sitemap_${lang}_${num + 1}.xml`
        }))
      ])
    );

    for (let index = 0; index < SITEMAP_LANGUAGES.length; index++) {
      const language = SITEMAP_LANGUAGES[index];
      const childrenUrls = childrenItems[language];
      if (!childrenUrls.length) return;
      const serpStreamIndex = new SitemapIndexStream();
      const serpContent = await streamToPromise(Readable.from(childrenUrls).pipe(serpStreamIndex));
      await fs.writeFile(`./public/serps_sitemap_${language}.xml`, serpContent);
      serpStreamIndex.end();
    }

    console.info('success generate serp sitemap!!');
    return this.cleanOldSitemap(page);
  }

  async cleanOldSitemap(fromPage: number) {
    let start = fromPage;
    const languages = Object.values(Language);
    while (true) {
      try {
        const existLanguages: Language[] = [];
        for (let idx = 0; idx < languages.length; idx++) {
          const lang = languages[idx];
          const path = `./public/serps_sitemap_${lang}_${start}.xml`;
          const exist = fs.existsSync(path);
          if (exist) {
            fs.unlinkSync(path);
            existLanguages.push(lang);
          }
        }
        if (existLanguages.length === 0) break;
        start++;
      } catch (error) {
        console.error(error);
      }
    }
  }

  getPriority(params: SearchRequest) {
    const reducePriority = 0.05;
    let level = 0;

    if (params.propertyType?.length) {
      level++;
    }
    if (params.amenities?.length) {
      level++;
    }
    if (params.minLease && params.minLease !== MinLease.TwelveMonths) {
      level++;
    }
    if (params.numberBedrooms && params.numberBedrooms !== NumberOfBed.AnyType) {
      level++;
    }
    if (params.sortBy) {
      level++;
    }

    // DOCME: no filter:  floor size, radius and price in sitemap serp;

    return 1 - reducePriority * level;
  }
}

const instance = new SerpXml();
instance.build();

export {};
