import { Language } from '@constants/enum';
import { join } from 'path';

export const STATIC_PATH = join(__dirname, '../static');

export const SCRIPT_PATH = join(__dirname, '../scripts');

export const SITEMAP_LANGUAGES = (process.env.NEXT_SITEMAP_LANGUAGES?.split(',') || []) as Language[];
