import { Language, LocaleNamespace } from '@constants/enum';
import { OnModuleInit } from '@nestjs/common';
import axios from 'axios';
import i18next from 'i18next';

export class I18nService implements OnModuleInit {
  async onModuleInit() {
    if (i18next.isInitialized) return;
    const localesRes = await Promise.all([
      axios.get(`${process.env.NEXT_PUBLIC_EGSWING_API_BASE_URL}website-locales/en/common`),
      axios.get(`${process.env.NEXT_PUBLIC_EGSWING_API_BASE_URL}website-locales/th/common`)
    ]);
    const [commonEn, commonTh] = localesRes.map((localeRes) => localeRes.data);

    await i18next.init({
      ns: [LocaleNamespace.Common],
      resources: {
        [Language.US]: {
          [LocaleNamespace.Common]: commonEn
        },
        [Language.TH]: {
          [LocaleNamespace.Common]: commonTh
        }
      }
    });
  }
}
