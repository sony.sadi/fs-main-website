import { Injectable, OnModuleInit } from '@nestjs/common';
import { IncomingMessage, ServerResponse } from 'http';
import next from 'next';
import axios from 'axios';
import trimStart from 'lodash/trimStart';
import { Listing } from '@interfaces/listing';
import { Language, LocaleNamespace } from '@constants/enum';
import i18next from 'i18next';
import { getListingDetailLink } from '@utils/listing';
import { toAmenitiesSlug, toPropertyTypeSlug } from '@flexstay.rentals/fs-url-stringify';
import { parseAmenitiesSlug, parsePropertyTypeSlug } from '@utils/search';
import { TFunction } from 'react-i18next';

@Injectable()
export class NextService implements OnModuleInit {
  private readonly server = next({ dev: false });

  private readonly handler = this.server.getRequestHandler();

  private readonly translate = {
    [Language.US]: i18next.getFixedT(Language.US, LocaleNamespace.Common),
    [Language.TH]: i18next.getFixedT(Language.TH, LocaleNamespace.Common)
  };

  private readonly connectionInstance = axios.create({
    timeout: 20000,
    baseURL: process.env.NEXT_PUBLIC_EGSWING_API_BASE_URL
  });

  async onModuleInit() {
    try {
      await this.server.prepare();
    } catch (error) {
      console.error(error);
    }
  }

  getNextServer() {
    return this.server.getRequestHandler();
  }

  toSortedAmenitiesUrl(url: string, t: TFunction) {
    const slugs = url.split('/').map((slug) => decodeURI(slug));
    const amenitySlug = slugs.find((slug) => !!parseAmenitiesSlug(slug, t).length);
    if (!amenitySlug) return url;
    const amenities = parseAmenitiesSlug(amenitySlug, t);
    const correctAmenitySlug = toAmenitiesSlug(amenities, t);
    return url.replace(encodeURI(amenitySlug), encodeURI(correctAmenitySlug));
  }

  toSortedPropertyTypesUrl(url: string, t: TFunction, language: Language) {
    const serpBasePath =
      language === Language.TH
        ? encodeURI(`/${this.translate[language]('bangkokProvince')}`)
        : encodeURI(`/${language}/${this.translate[language]('bangkokProvince')}`);
    const slugs = url.split('/').map((slug) => decodeURI(slug));
    const propertyTypeSlug = slugs.find((slug) => {
      const parsedResult = parsePropertyTypeSlug(slug, t);
      if (!parsedResult.length) return false;
      // check slug is building type (building type has slug same as property type slug)
      const regex = new RegExp(`^${serpBasePath}/${encodeURI(slug)}`);
      return !regex.test(url);
    });
    if (!propertyTypeSlug) return url;
    const propertyTypes = parsePropertyTypeSlug(propertyTypeSlug, t);
    const correctPropertyTypeSlug = toPropertyTypeSlug(propertyTypes, t);
    return url.replace(encodeURI(propertyTypeSlug), encodeURI(correctPropertyTypeSlug));
  }

  async serpHandler(req: IncomingMessage, res: ServerResponse) {
    const requestUrl = req.url;

    if (!requestUrl) return this.handler(req, res);

    const language = Object.values(Language).find((lang) => requestUrl.startsWith(`/${lang}/`)) || Language.TH;

    const t = this.translate[language];

    let sortedUrl = this.toSortedAmenitiesUrl(requestUrl, t);
    sortedUrl = this.toSortedPropertyTypesUrl(sortedUrl, t, language);

    if (sortedUrl === requestUrl) {
      return this.handler(req, res);
    }

    res.writeHead(302, {
      Location: sortedUrl
    });
    res.end();
  }

  async nextHandler(req: IncomingMessage, res: ServerResponse) {
    const requestUrl = req.url;

    if (!requestUrl) return this.handler(req, res);

    const serpBasePath = Object.fromEntries(
      Object.values(Language).map((lang) => {
        return [
          lang,
          lang === Language.TH
            ? encodeURI(`/${this.translate[lang]('bangkokProvince')}`)
            : encodeURI(`/${lang}/${this.translate[lang]('bangkokProvince')}`)
        ];
      })
    );

    if (Object.values(serpBasePath).some((path) => requestUrl.startsWith(path))) {
      return this.serpHandler(req, res);
    }

    return this.handler(req, res);
  }

  async listingDetails(req: IncomingMessage, res: ServerResponse, identifier: string) {
    let id: number | null = null;
    if (/^\d+$/.test(identifier)) {
      id = Number(identifier);
    }
    if (/-\d+$/.test(identifier)) {
      const parts = identifier.split('-');
      id = Number(parts[parts.length - 1]);
    }
    if (!id) return this.nextHandler(req, res);

    try {
      const { data }: { data: Listing } = await this.connectionInstance.get(`rentals/frontend/${id}`, {
        params: { showLoading: false }
      });
      const language = trimStart(req.url, '/').startsWith(Language.US) ? Language.US : Language.TH;
      const localePath = language === Language.TH ? '' : `/${language}`;
      const listingidentifier = encodeURIComponent(getListingDetailLink(data, language).as.replace(/\//g, ''));
      if (listingidentifier === encodeURIComponent(identifier)) {
        return this.nextHandler(req, res);
      }
      res.writeHead(301, {
        Location: `${localePath}/${listingidentifier}/`
      });
      res.end();
    } catch (error) {
      return this.nextHandler(req, res);
    }
  }
}
