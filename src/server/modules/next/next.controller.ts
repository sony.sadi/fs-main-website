import { Controller, Get, Param, Req, Res } from '@nestjs/common';
import { IncomingMessage, ServerResponse } from 'http';
import { NextService } from './next.service';

@Controller()
export class NextController {
  constructor(private readonly nextService: NextService) {}

  @Get(['/en/:identifier', '/th/:identifier', '/:identifier'])
  async listingDetails(
    @Req() req: IncomingMessage,
    @Res() res: ServerResponse,
    @Param('identifier') identifier: string
  ) {
    return this.nextService.listingDetails(req, res, identifier);
  }

  @Get('*')
  async nextHandler(@Req() req: IncomingMessage, @Res() res: ServerResponse) {
    return this.nextService.nextHandler(req, res);
  }
}
