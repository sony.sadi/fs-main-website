import { Controller, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@server/guards/auth.guard';
import { SerpService } from './serp.service';

@Controller('serps')
export class SerpController {
  constructor(private readonly serpService: SerpService) {}

  @Post('generate')
  @UseGuards(AuthGuard)
  async generateSerps() {
    return this.serpService.generate();
  }
}
