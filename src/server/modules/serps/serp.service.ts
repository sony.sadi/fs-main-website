import { Injectable, NotFoundException } from '@nestjs/common';
import { SCRIPT_PATH } from '@server/common';
import { Worker } from 'worker_threads';
import { join } from 'path';
import lowerCase from 'lodash/lowerCase';

@Injectable()
export class SerpService {
  async generate() {
    if (lowerCase(process.env.NEXT_SITEMAP_GENERATOR_ENABLED) !== 'true') {
      throw new NotFoundException();
    }
    // eslint-disable-next-line no-new
    new Worker(join(SCRIPT_PATH, 'build-serp-xml.js'));
  }
}
