import { Module } from '@nestjs/common';
import { I18nService } from '@server/providers/i18n.service';
import { NextModule } from './next/next.module';
import { SerpModule } from './serps/serp.module';
import { StaticModule } from './static/static.module';

/*
  - NextModule must be added at the end to handle request from NextJS
*/
@Module({
  imports: [SerpModule, StaticModule, NextModule],
  controllers: [],
  providers: [I18nService],
  exports: []
})
export class AppModule {}
