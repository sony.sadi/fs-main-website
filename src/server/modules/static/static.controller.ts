import { Language } from '@constants/enum';
import { Controller, Get, Req, Res } from '@nestjs/common';
import { IncomingMessage, ServerResponse } from 'http';
import { StaticService } from './static.service';

@Controller('/')
export class StaticController {
  constructor(private readonly staticService: StaticService) {}

  @Get('robots.txt')
  async robotsTxt(@Req() req: IncomingMessage, @Res() res: ServerResponse) {
    return this.staticService.robotsTxt(req, res);
  }

  @Get(`pages_sitemap_(${Object.values(Language).join('|')}).xml`)
  async pageSitemapIndex(@Req() req: IncomingMessage, @Res() res: ServerResponse) {
    return this.staticService.pageSitemapIndex(req, res);
  }

  @Get(`rentals_sitemap_(${Object.values(Language).join('|')}).xml`)
  async rentalSitemapIndex(@Req() req: IncomingMessage, @Res() res: ServerResponse) {
    return this.staticService.rentalSitemapIndex(req, res);
  }

  @Get(`rentals_sitemap_(${Object.values(Language).join('|')})_*.xml`)
  async rentalSitemapPage(@Req() req: IncomingMessage, @Res() res: ServerResponse) {
    return this.staticService.rentalSitemapPage(req, res);
  }

  @Get(`serps_sitemap_(${Object.values(Language).join('|')}).xml`)
  async serpSitemapIndex(@Req() req: IncomingMessage, @Res() res: ServerResponse) {
    return this.staticService.serpSitemapIndex(req, res);
  }

  @Get(`serps_sitemap_(${Object.values(Language).join('|')})_*.xml`)
  async serpSitemapPage(@Req() req: IncomingMessage, @Res() res: ServerResponse) {
    return this.staticService.serpSitemapPage(req, res);
  }
}
