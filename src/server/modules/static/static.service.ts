import { Injectable, NotFoundException } from '@nestjs/common';
import { IncomingMessage, ServerResponse } from 'http';
import trimEnd from 'lodash/trimEnd';
import toNumber from 'lodash/toNumber';
import fs from 'fs-extra';
import { join } from 'path';
import axios from 'axios';
import ceil from 'lodash/ceil';
import times from 'lodash/times';
import { Readable } from 'stream';
import { EnumChangefreq, LinkItem, SitemapIndexStream, SitemapItem, SitemapStream, streamToPromise } from 'sitemap';
import isNaN from 'lodash/isNaN';
import { getListingDetailLink } from '@utils/listing';
import { Language } from '@constants/enum';
import { SITEMAP_LANGUAGES, STATIC_PATH } from '@server/common';
import { Listing } from '@interfaces/listing';

@Injectable()
export class StaticService {
  private readonly CMS_API_URL = trimEnd(process.env.NEXT_PUBLIC_EGSWING_API_BASE_URL, '/');

  private readonly HOME_URL = trimEnd(process.env.NEXT_PUBLIC_HOME_URL, '/');

  private readonly SITEMAP_PAGE_SIZE = toNumber(process.env.NEXT_SITEMAP_PAGE_SIZE) || 50000;

  private readonly DEFAULT_LANGUAGE = Language.TH;

  private readonly SHOULD_ADD_ALTERNATE_LINK = SITEMAP_LANGUAGES.length > 1;

  getRouteLanguage(url?: string) {
    const parts = (url || '').split('.')[0].split('_');
    const languages = Object.values(Language).map((lang) => lang.toString());
    const language = parts.find((part) => languages.includes(part));
    return (language || Language.US) as Language;
  }

  notFound(req: IncomingMessage, res: ServerResponse) {
    throw new NotFoundException('This file could not be found.');
  }

  robotsTxt(req: IncomingMessage, res: ServerResponse) {
    const buffer = fs.readFileSync(join(STATIC_PATH, 'robots.txt'));
    const content = buffer.toString().replace('HOME_URL', this.HOME_URL);
    res.setHeader('Content-Type', 'text/plain');
    res.write(content);
    res.end();
  }

  async rentalSitemapIndex(req: IncomingMessage, res: ServerResponse) {
    const language = this.getRouteLanguage(req.url);
    if (!SITEMAP_LANGUAGES.includes(language)) {
      return this.notFound(req, res);
    }
    const { data } = await axios.get(`${this.CMS_API_URL}/rentals/sitemap/count`);
    const totalPage = ceil(Number(data) / this.SITEMAP_PAGE_SIZE);
    const stream = new SitemapIndexStream();
    const urls = times(totalPage).map((num) => ({
      url: `${this.HOME_URL}/rentals_sitemap_${language}_${num + 1}.xml`
    }));
    const buffer = await streamToPromise(Readable.from(urls).pipe(stream));
    res.setHeader('Content-Type', 'text/xml');
    res.write(buffer.toString());
    res.end();
  }

  async rentalSitemapPage(req: IncomingMessage, res: ServerResponse) {
    const match = (req.url || '').match(/\d+/);
    if (!match) return this.notFound(req, res);
    const language = this.getRouteLanguage(req.url);
    if (!SITEMAP_LANGUAGES.includes(language)) {
      return this.notFound(req, res);
    }
    const pageReg = new RegExp(`rentals_sitemap_(${Object.values(Language).join('|')})_|.xml|\/|\/`, 'g');
    const page = Number((req.url || '').replace(pageReg, ''));
    if (isNaN(page) || page < 1) return this.notFound(req, res);
    const offset = (page - 1) * this.SITEMAP_PAGE_SIZE;
    const { data }: { data: Listing[] } = await axios.get(`${this.CMS_API_URL}/rentals/sitemap`, {
      params: {
        _start: offset,
        _limit: this.SITEMAP_PAGE_SIZE
      }
    });
    if (!data.length) return this.notFound(req, res);

    const stream = new SitemapStream({
      hostname: this.HOME_URL,
      xmlns: {
        xhtml: true,
        video: false,
        image: false,
        news: false
      }
    });

    const sitemapItems = data.map((rental) => {
      const urlByLanguage = Object.fromEntries(
        SITEMAP_LANGUAGES.map((lang) => {
          const rentUrl = join(trimEnd(getListingDetailLink(rental, lang).as), '/');
          return [lang, lang === this.DEFAULT_LANGUAGE ? rentUrl : join(language, rentUrl)];
        })
      );
      let links: LinkItem[] = [];
      if (this.SHOULD_ADD_ALTERNATE_LINK) {
        links = SITEMAP_LANGUAGES.map((lang) => ({
          url: urlByLanguage[lang],
          lang,
          hreflang: lang
        }));
      }
      const item: SitemapItem = {
        url: urlByLanguage[language],
        changefreq: EnumChangefreq.WEEKLY,
        priority: 0.3,
        links,
        img: [],
        video: []
      };
      return item;
    });
    const buffer = await streamToPromise(Readable.from(sitemapItems).pipe(stream));
    res.setHeader('Content-Type', 'text/xml');
    res.write(buffer.toString());
    res.end();
  }

  async pageSitemapIndex(req: IncomingMessage, res: ServerResponse) {
    const language = this.getRouteLanguage(req.url);
    if (!SITEMAP_LANGUAGES.includes(language)) {
      return this.notFound(req, res);
    }
    const paths = {
      [Language.US]: [
        '/en/',
        '/en/about-us/',
        '/en/contact-us/',
        '/en/how-does-it-work/',
        '/en/list-your-property/',
        '/en/privacy/'
      ],
      [Language.TH]: ['/', '/เกี่ยวกับเรา/', '/ติดต่อเรา/', '/ทำอย่างไร/', '/ลงประกาศของคุณ/', '/ความเป็นส่วนตัว/']
    };

    const urls = paths[language].map((path, index) => {
      let links: LinkItem[] = [];
      if (this.SHOULD_ADD_ALTERNATE_LINK) {
        links = SITEMAP_LANGUAGES.map((lang) => ({
          url: paths[lang][index],
          lang,
          hreflang: lang
        }));
      }
      const item: SitemapItem = {
        url: path,
        changefreq: EnumChangefreq.WEEKLY,
        priority: 1,
        video: [],
        img: [],
        links
      };
      return item;
    });
    const stream = new SitemapStream({
      hostname: this.HOME_URL,
      xmlns: {
        xhtml: true,
        video: false,
        image: false,
        news: false
      }
    });
    const buffer = await streamToPromise(Readable.from(urls).pipe(stream));
    res.setHeader('Content-Type', 'text/xml');
    res.write(buffer.toString());
    res.end();
  }

  async serpSitemapIndex(req: IncomingMessage, res: ServerResponse) {
    const language = this.getRouteLanguage(req.url);
    if (!SITEMAP_LANGUAGES.includes(language)) {
      return this.notFound(req, res);
    }
    const path = `./public/serps_sitemap_${language}.xml`;
    const exist = fs.existsSync(path);
    if (!exist) return this.notFound(req, res);
    const buffer = fs.readFileSync(path);
    res.setHeader('Content-Type', 'text/xml');
    res.write(buffer.toString());
    res.end();
  }

  async serpSitemapPage(req: IncomingMessage, res: ServerResponse) {
    const match = (req.url || '').match(/\d+/);
    if (!match) return this.notFound(req, res);
    const language = this.getRouteLanguage(req.url);
    if (!SITEMAP_LANGUAGES.includes(language)) {
      return this.notFound(req, res);
    }
    const pageReg = new RegExp(`serps_sitemap_(${Object.values(Language).join('|')})_|.xml|\/|\/`, 'g');
    const page = Number((req.url || '').replace(pageReg, ''));
    if (isNaN(page) || page < 1) return this.notFound(req, res);
    const path = `./public/serps_sitemap_${language}_${page}.xml`;
    const exist = fs.existsSync(path);
    if (!exist) return this.notFound(req, res);
    const buffer = fs.readFileSync(path);
    res.setHeader('Content-Type', 'text/xml');
    res.write(buffer.toString());
    res.end();
  }
}
