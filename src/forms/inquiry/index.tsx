import { useCallback, useEffect, useMemo, useRef, useState, FocusEvent } from 'react';
import { Formik, Form, Field, ErrorMessage, FormikProps, FormikValues } from 'formik';
import cntl from 'cntl';
import { useTranslation } from 'next-i18next';
import * as Yup from 'yup';
import emailjs from 'emailjs-com';

import { Language, LocaleNamespace, Page } from '@constants/enum';
import { EMAILJS } from '@constants';
import { Listing } from '@interfaces/listing';
import { updateGTMDataLayer } from '@utils/gtm';
import { property } from 'lodash';
import { getListingDetailLink } from '@utils/listing';
import { generateHrefLang } from '@utils/filter-language';
import { Transportation } from '@interfaces/search';
import { toLandingTransportUrl } from '@utils/landing-transport';

interface Props {
  page: Page;
  title?: JSX.Element | string;
  property?: Listing;
  transportation?: Transportation;
}

function Inquiry(props: Props) {
  const { t, i18n } = useTranslation(LocaleNamespace.Listing);
  const { t: tContactForm } = useTranslation(LocaleNamespace.ContactForm);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);
  const [alert, setAlert] = useState<{ message: string; isError: boolean }>({
    message: '',
    isError: false
  });
  const [focusField, setFocusField] = useState<string>('');
  const formikEl = useRef<FormikProps<FormikValues>>(null);

  useEffect(() => {
    emailjs.init(process.env.NEXT_PUBLIC_EMAILJS_USER_ID as string);
  }, []);

  useEffect(() => {
    formikEl.current?.setFieldValue('note', placeholderNote);
  }, [i18n.language]);

  const placeholderNote = useMemo(() => {
    switch (props.page) {
      case Page.Contact:
        return t('inquiryNoteContact');

      case Page.HowItWorks:
        return t('inquiryNoteHowItWorks');

      case Page.Listing:
        return t('inquiryNoteListingDetail');

      case Page.LandingTransportDetail:
        return tContactForm('inquiryNoteTranspo');

      default:
        return t('inquiryNoteDefault');
    }
  }, [props.page]);

  /**
   * Generate Url of current page
   */
  const generateUrlPage = useMemo(() => {
    switch (props.page) {
      case Page.Listing: {
        if (!props.property) {
          return '';
        }
        const { href } = getListingDetailLink(props.property, i18n.language as Language);
        return `${generateHrefLang(`/${href.query['listing-title-id']}`, i18n.language)}`;
      }
      case Page.Contact:
        return `${generateHrefLang(tCommon('contactUsPath'), i18n.language)}`;
      case Page.ListYourProperty:
        return `${generateHrefLang(tCommon('listYourPropertyPath'), i18n.language)}`;
      case Page.HowItWorks:
        return `${generateHrefLang(tCommon('howDoesItWorkPath'), i18n.language)}`;
      case Page.LandingTransportDetail: {
        if (!props.transportation) return '';
        const { station, transportation_system } = props.transportation;
        return toLandingTransportUrl({
          t: tCommon,
          name: station[i18n.language as Language],
          transportation_system,
          locale: i18n.language as Language,
          isFullRoute: true,
          isAllowTail: true
        });
      }

      default:
        return '';
    }
  }, [props.page]);

  /**
   * Check if allow tracking event GTM
   * @returns
   */
  const isAllowTracking = () => {
    if (
      props.page === Page.Listing ||
      props.page === Page.Contact ||
      props.page === Page.ListYourProperty ||
      props.page === Page.HowItWorks ||
      props.page === Page.LandingTransportDetail
    )
      return true;
    return false;
  };

  const formClasses = useMemo(() => {
    switch (props.page) {
      case Page.Contact:
      case Page.HowItWorks:
        return 'bg-white xl:bg-how-it-works-gray sm:w-inquiry-default';

      default:
        return 'bg-white lg:bg-how-it-works-gray sm:w-inquiry-contact';
    }
  }, [props.page]);

  const renderTextButton = useMemo(() => {
    switch (props.page) {
      case Page.HowItWorks:
      case Page.ListYourProperty:
        return t('submit');
      case Page.LandingTransportDetail:
        return tContactForm('contactFormTranspoButton');
      default:
        return t('inquireNow');
    }
  }, [props.page]);

  const handleSubmit = useCallback((values, { setSubmitting, resetForm }) => {
    setSubmitting(true);

    const data = {
      name: values.name,
      phone: values.phone,
      email: values.email,
      whats_app: values.whatsApp,
      line_id: values.lineId,
      message: values.note,
      information: { page: props.page, url: window.location.href }
    };

    emailjs
      .send(EMAILJS.EMAIL_SERVICE_ID, EMAILJS.EMAIL_TEMPLATE, data)
      .then(() => {
        onUpdateGTMBySubmitForm('form_completed');
        setAlert({ message: t('inquireSuccess'), isError: false });
        resetForm();
        setTimeout(() => {
          setAlert({ message: '', isError: false });
        }, 3000);
      })
      .catch(() => setAlert({ message: t('inquireError'), isError: true }))
      .finally(() => setSubmitting(false));
  }, []);

  /**
   * Function on focus form field
   */
  const onFocusFormField = (event: FocusEvent<HTMLInputElement>) => {
    if (focusField === event.currentTarget.name) return;
    setFocusField(event.currentTarget.name);
    const eventObject: Record<string, any> = {
      event: 'select_content',
      content_type: 'form_field',
      url: generateUrlPage
    };
    handleUpdateGTMEvent(eventObject);
  };

  /**
   * Reset focus field
   */
  const resetFocusField = () => {
    setFocusField('');
  };

  /**
   * Update GTM event
   * @param eventObject
   */
  const handleUpdateGTMEvent = useCallback((eventObject: Record<string, any>) => {
    if (props.property) {
      const { property } = props;
      eventObject.item_id = property.id;
      if (property.tenure === 'rent' || property.tenure === 'rentsell') eventObject.lowestprice = property.lowestPrice;
      if (property.tenure === 'sell' || property.tenure === 'rentsell') eventObject.saleprice = property.salePrice;
      eventObject.building_id = property.buildingId?.id;
      eventObject.province_ps = i18n.language === Language.TH ? property.province_ps_th : property.province_ps_en;
      eventObject.district_ps = i18n.language === Language.TH ? property.district_ps_th : property.district_ps_en;
      eventObject.subdistrict_ps =
        i18n.language === Language.TH ? property.subdistrict_ps_th : property.subdistrict_ps_en;
    }
    if (isAllowTracking()) {
      updateGTMDataLayer(null, eventObject);
    }
  }, []);

  /**
   * Update GTM event when click submit form
   * @param eventType
   */
  const onUpdateGTMBySubmitForm = (eventType: string) => {
    const errorFields = formikEl.current?.errors || {};
    const eventObject: Record<string, any> = {
      event: eventType,
      content_type: 'contact_form',
      url: generateUrlPage
    };
    const errorArrays: string[] = [];
    Object.keys(errorFields).forEach((value) => {
      errorArrays.push(`${value} field`);
    });

    if (errorArrays.length) {
      const errors = errorArrays.join(', ');
      eventObject.error_fields = errors;
    }
    handleUpdateGTMEvent(eventObject);
  };

  const onClickSubmitButton = () => {
    onUpdateGTMBySubmitForm('form_submit');
  };

  return (
    <Formik
      innerRef={formikEl}
      initialValues={{
        name: '',
        phone: '',
        email: '',
        whatsApp: '',
        lineId: '',
        note: ''
        // note: t('inquiryDefaultNote')
      }}
      validationSchema={Yup.object().shape({
        name: Yup.string().required(t('inqueryNameRequired')),
        phone: Yup.number().typeError(t('inquiryPhoneInvalidType')).required(t('inqueryPhoneRequired')),
        email: Yup.string().email(t('inquiryEmailInvalid')).required(t('inquiryEmailRequired'))
      })}
      onSubmit={handleSubmit}>
      {({ isSubmitting }) => (
        <Form>
          <div className={classes.form({ classes: formClasses })}>
            <p className={classes.title}>{props.title}</p>
            <div className="pb-4 sm:pb-6">
              {alert.message && <div className={classes.alert(alert.isError)}>{alert.message}</div>}
              <div className="grid gap-6">
                <div>
                  <Field
                    onClick={onFocusFormField}
                    onBlur={resetFocusField}
                    type="text"
                    name="name"
                    className={classes.field}
                    placeholder={`${t('name')}*`}
                  />
                  <ErrorMessage className="text-primary-red text-xs" name="name" component="div" />
                </div>

                <div>
                  <Field
                    onClick={onFocusFormField}
                    onBlur={resetFocusField}
                    name="phone"
                    className={classes.field}
                    placeholder={`${t('phone')}*`}
                  />
                  <ErrorMessage className="text-primary-red text-xs" name="phone" component="div" />
                </div>

                <div>
                  <Field
                    onClick={onFocusFormField}
                    onBlur={resetFocusField}
                    type="text"
                    name="email"
                    className={classes.field}
                    placeholder={`${t('email')}*`}
                  />
                  <ErrorMessage className="text-primary-red text-xs" name="email" component="div" />
                </div>

                <div>
                  <Field
                    onClick={onFocusFormField}
                    onBlur={resetFocusField}
                    type="text"
                    name="whatsApp"
                    className={classes.field}
                    placeholder={t('whatsApp')}
                  />
                </div>

                <div>
                  <Field
                    onClick={onFocusFormField}
                    onBlur={resetFocusField}
                    type="text"
                    name="lineId"
                    className={classes.field}
                    placeholder={t('lineID')}
                  />
                </div>

                <div>
                  <Field
                    onClick={onFocusFormField}
                    onBlur={resetFocusField}
                    as="textarea"
                    name="note"
                    rows={3}
                    className={classes.field}
                    placeholder={placeholderNote}
                  />
                </div>
              </div>
            </div>
            <div className="pb-3 text-right">
              <button
                type="submit"
                className={classes.submitButton}
                disabled={isSubmitting}
                onClick={onClickSubmitButton}>
                {renderTextButton}
              </button>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
}

const classes = {
  form: ({ classes }: { classes: string }) => cntl`
  overflow-hidden border p-4 sm:p-5 xl:px-10 rounded-3xl
  w-80
  ${classes}
  `,
  title: cntl`
    text-2xl text-primary-orange font-bold text-left  
  `,
  alert: (isError: boolean) => cntl`
    mb-4 text-center ${isError ? 'text-primary-red' : 'text-green-400'}
  `,
  field: cntl`
    block w-full p-2 
    text-sm
    placeholder-primary-lightgray
    placeholder-font-light
    border rounded-md
    focus:border-primary-blue
    focus:outline-none      
  `,
  submitButton: cntl`
    w-full py-2 px-4 
    h-12
    text-sm text-white font-medium font-semibold uppercase tracking-widest
    rounded-md    
    focus:outline-none focus:border-transparent bg-primary-orange
    group hover:bg-primary-dark
  `
};

export default Inquiry;
