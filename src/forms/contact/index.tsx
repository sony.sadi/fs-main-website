import { useMemo } from 'react';
import { useTranslation, Trans } from 'next-i18next';

import Inquiry from '@forms/inquiry';
import { Page, LocaleNamespace } from '@constants/enum';
import { Listing } from '@interfaces/listing';
import { Transportation } from '@interfaces/search';

interface Props {
  page: Page;
  title?: string | JSX.Element;
  property?: Listing;
  transportation?: Transportation;
}

const ContactForm = (props: Props) => {
  const { t } = useTranslation(LocaleNamespace.ContactForm);

  const title = useMemo(() => {
    if (props.title) return props.title;
    switch (props.page) {
      case Page.HowItWorks:
        return (
          <>
            {t('contactFormHowHead')}
            <br />
            {t('contactFormHowDesc')}
          </>
        );

      case Page.ListYourProperty:
        return t('contactFormListHead');
      case Page.Contact:
        return (
          <>
            {t('contactFormContactHead')}
            <br />
            {t('contactFormContactDesc')}
          </>
        );

      case Page.Listing:
        return (
          <>
            {t('contactFormListingHead')}
            <br />
            {t('contactFormListingDesc')}
          </>
        );

      default:
        return '';
    }
  }, [props.page, props.title]);
  return <Inquiry transportation={props.transportation} property={props.property} page={props.page} title={title} />;
};

export default ContactForm;
