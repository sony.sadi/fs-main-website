import { NestFactory } from '@nestjs/core';
import { AppModule } from './server/modules/app.module';

async function bootstrap() {
  const PORT = process.env.PORT || 3000;
  const app = await NestFactory.create(AppModule, {
    logger: process.env.NEXT_NEST_LOGGER_ENABLED === 'true'
  });
  await app.listen(PORT);
  console.info(`>>>> Ready on localhost:${PORT}`);
}
bootstrap();
