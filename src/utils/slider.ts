/**
 * Style center mode - single view
 * @param shrink
 * @param slidePerView
 * @returns
 */
export const getStyleSliderSingleView = (shrink: number, slidePerView: number) => {
  const containerWidth = slidePerView * 100 - shrink * slidePerView;
  const margin = (100 - shrink * slidePerView) / slidePerView;

  return {
    width: `${containerWidth}%`,
    marginLeft: `${-1 * margin}%`
  };
};

/**
 * Style center mode - multiple view
 * @param growPercent
 * @param slidePerView
 * @returns
 */
export const getStyleSliderMultipleView = (growPercent: number, slidePerView: number) => {
  const currentGrowPercent = 100 + growPercent * slidePerView;
  const centerViewPort = (currentGrowPercent / slidePerView) * (slidePerView - 1);

  const overlayInView = (100 - centerViewPort) / 2;

  const marginPercent = (currentGrowPercent - (overlayInView + centerViewPort)) / (currentGrowPercent / 100);

  return {
    width: `${currentGrowPercent}%`,
    marginLeft: `${-1 * marginPercent}%`
  };
};

/**
 * Get dots class for paging
 * @param itemIndex
 * @param currentDot
 * @returns
 */
export const getDotsClass = (itemIndex: number, currentDot: number) => {
  return itemIndex === currentDot ? 'slider-active' : 'slider-inactive';
};
