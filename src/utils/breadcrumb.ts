/* eslint-disable no-case-declarations */
import numeral from 'numeral';
import startCase from 'lodash/startCase';
import trimEnd from 'lodash/trimEnd';
import { BreadcrumbElement } from '@constants/enum/breadcrumb';
import { BreadcrumbItem, GeneratedBreadcrumb } from '@interfaces/breadcrumb';
import { TFunction } from 'next-i18next';

import { getSortingFullName, getBudgetTypesFullName, getBedroomFullNames } from '@utils';
import {
  BuildingType,
  SortType,
  TransportationSystem,
  stringifyUrl,
  getDefaultParams,
  SerpType
} from '@flexstay.rentals/fs-url-stringify';
import { getAmenities, getBuildingTypes, getPropertyTypes, getTransportationSystem } from './search';

interface GenerateUrlFromBreadcrumbProps {
  breadcrumbs: BreadcrumbItem[];
  t: TFunction;
  currentPath: {
    href: string;
    rel?: string;
  };
  searchTerm?: string;
  serpType: SerpType | undefined;
}

interface GeneratedBreadcrumbItemProps {
  t: TFunction;
  breadcrumb: BreadcrumbItem;
  currentPath: {
    href: string;
    rel?: string;
  };
  serpType: SerpType | undefined;
  searchTerm?: string;
  extraParams?: {
    transportationSystem?: TransportationSystem;
  };
}

function getBreadcrumbLabelFromElement({ t, breadcrumb, searchTerm, serpType }: GeneratedBreadcrumbItemProps): string {
  const { element, label, params } = breadcrumb;
  const {
    id,
    minPrice,
    maxPrice,
    minLease,
    floorSize_gte,
    floorSize_lte,
    page,
    amenities = [],
    numberBedrooms,
    propertyType = [],
    sortBy,
    radius,
    transportationSystem,
    building_type = BuildingType.Condo
  } = params;
  switch (element) {
    case BreadcrumbElement.Base:
      return t('home');
    case BreadcrumbElement.Type:
      return serpType === 'sales' ? t('sales') : t('rentals');
    case BreadcrumbElement.ContentType:
      return transportationSystem ? getTransportationSystem(transportationSystem) : getBuildingTypes(building_type, t);
    case BreadcrumbElement.PropertyType:
      return getPropertyTypes(t)
        .filter(({ id }) => propertyType.includes(id))
        .map((p) => p.name)
        .join(', ');
    case BreadcrumbElement.Country:
      return t('thailand');
    case BreadcrumbElement.ProvinceState:
      return t('bangkok');
    case BreadcrumbElement.City:
      return t('bangkok');
    case BreadcrumbElement.District:
    case BreadcrumbElement.Subdistrict:
    case BreadcrumbElement.ContentTypeName:
      return startCase(label || searchTerm);
    case BreadcrumbElement.Amenities:
      return getAmenities(t)
        .filter(({ value }) => amenities.includes(value))
        .map((a) => a.name)
        .join(', ');
    case BreadcrumbElement.UnitType:
      return getBedroomFullNames({ t, type: numberBedrooms });
    case BreadcrumbElement.MinleasePeriod:
      return getBudgetTypesFullName({ t, type: minLease });
    case BreadcrumbElement.FloorSize:
      return floorSize_gte === floorSize_lte
        ? t('floorSizeExactly', {
            value: floorSize_gte
          })
        : t('floorSizeFromTo', {
            from: floorSize_gte,
            to: floorSize_lte
          });
    case BreadcrumbElement.PriceRange:
      return minPrice === maxPrice
        ? t(serpType === 'sales' ? 'priceMonthlySaleExactly' : 'priceMonthlyExactly', {
            price: numeral(minPrice).format('0,0')
          })
        : t(serpType === 'sales' ? 'priceMonthlySaleFromTo' : 'priceMonthlyFromTo', {
            from: numeral(minPrice).format('0,0'),
            to: numeral(maxPrice).format('0,0')
          });
    case BreadcrumbElement.Sorting:
      return getSortingFullName({ t, type: sortBy || SortType.Recommended, serpType });
    case BreadcrumbElement.Radius:
      return t('withinRadiusInKilometer', {
        radius
      });
    case BreadcrumbElement.Pagination:
      return t('pageNumber', {
        page
      });
    case BreadcrumbElement.ListingId:
      return t('indexNumber', {
        id
      });
    default:
      return '';
  }
}

function getBreadcrumbFromElement({
  t,
  breadcrumb,
  currentPath,
  searchTerm,
  extraParams = {},
  serpType
}: GeneratedBreadcrumbItemProps): GeneratedBreadcrumb {
  const { element, params } = breadcrumb;
  const label = getBreadcrumbLabelFromElement({ t, breadcrumb, currentPath, searchTerm, serpType });
  const breadcrumbLink: { path: string; rel?: string } = {
    path: ''
  };
  const defaultSearchParams = getDefaultParams();
  switch (element) {
    case BreadcrumbElement.Base:
      breadcrumbLink.path = '/';
      break;
    case BreadcrumbElement.ListingId:
      breadcrumbLink.path = currentPath.href;
      breadcrumbLink.rel = currentPath.rel;
      break;
    default:
      const { path, rel } = stringifyUrl({
        request: { ...defaultSearchParams, ...params, serp_type: serpType, index_serp_types: ['rents'] },
        t
      });
      breadcrumbLink.path = path;
      breadcrumbLink.rel = rel;
      break;
  }

  /**
   * FS-1100 uses default url (redirect) with some specials elements
   */
  if ([BreadcrumbElement.Type, BreadcrumbElement.Country, BreadcrumbElement.ContentType].includes(element)) {
    let slugs = breadcrumbLink.path.split('/');
    const ignoreSlugs = [t('bangkokProvince')];
    if (element === BreadcrumbElement.ContentType) {
      const typeValue = serpType === 'sales' ? t('salesValue') : t('rentalsValue');
      // ignoreSlugs.push(typeValue);
      if (params.transportationSystem) {
        slugs = [`/${params.transportationSystem.toLowerCase()}`];
      }
    }

    breadcrumbLink.path = slugs.filter((slug) => !ignoreSlugs.includes(slug)).join('/');
  }

  /**
   * FS-1113
   * For the breadcrumb elements between BTS/MRT/ARL and the currently selected station name
   * All hyperlinks should link to SERP URLs filtered for /bts (/arl, /mrt accordingly)
   */

  if (
    extraParams.transportationSystem &&
    [
      BreadcrumbElement.Country,
      BreadcrumbElement.ProvinceState,
      BreadcrumbElement.City,
      BreadcrumbElement.District,
      BreadcrumbElement.Subdistrict
    ].includes(element)
  ) {
    breadcrumbLink.path = [trimEnd(breadcrumbLink.path, '/'), extraParams.transportationSystem.toLowerCase()].join('/');
  }

  return {
    label,
    path: [trimEnd(breadcrumbLink.path, '/'), '/'].join(''),
    rel: breadcrumbLink.rel
  };
}

export function generateUrlFromBreadcrumb({
  breadcrumbs,
  t,
  currentPath,
  searchTerm,
  serpType
}: GenerateUrlFromBreadcrumbProps) {
  const contentTypeNameBreadcrumb = breadcrumbs.find((item) => item.element === BreadcrumbElement.ContentTypeName);

  const extraParams = {
    transportationSystem: contentTypeNameBreadcrumb?.params.transportationSystem
  };
  return breadcrumbs
    .map((breadcrumb) => {
      return getBreadcrumbFromElement({ t, breadcrumb, currentPath, searchTerm, extraParams, serpType });
    })
    .filter((i) => i.label);
}
