import TagManager from 'react-gtm-module';

export interface DataLayerObject {
  type: string;
  locale: string;
}

/**
 * FS-1145
 * Handle send data layer to GTM
 * @param object
 */
export const updateGTMDataLayer = (object: DataLayerObject | null, customEventObject?: Record<string, any>) => {
  TagManager.dataLayer({
    dataLayer: object || customEventObject
  });
};
