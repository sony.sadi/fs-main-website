import { ParsedUrlQuery } from 'querystring';
import { GetStaticPropsContext } from 'next';
import { Language } from '@constants/enum';
import { TransportationSystem } from '@constants/enum/search';
import { getTransportationsPoisSuggestion } from '@services/http-client/search';
import { TFunction } from 'next-i18next';
import { LandingTransportRequest } from '@interfaces/landing-transport';
import { toPoiNameSlug } from '@flexstay.rentals/fs-url-stringify';
import { defaultLanguage } from '@constants';

export async function parseLandingTransportRequest(
  context: GetStaticPropsContext<ParsedUrlQuery>
): Promise<LandingTransportRequest> {
  const locale = (context.locale as Language) || Language.TH;
  const transportationSystem = (context.params?.transportation_system as string).toUpperCase() as TransportationSystem;
  const nameSlug = context.params?.slug as string;
  const transportName = nameSlug.replace(/-/g, ' ');
  const transportRes = await getTransportationsPoisSuggestion({
    _q: transportName,
    _limit: 1,
    transportation_system: transportationSystem
  });
  const poi = transportRes.data.data[0];

  if (
    !poi ||
    ![poi.transportation_system].includes(transportationSystem) ||
    nameSlug !== toPoiNameSlug(poi.station[locale])
  ) {
    throw new Error('Invalid transportation');
  }
  return {
    poi,
    locale
  };
}

interface ToLandingTransportUrlProps {
  name: string;
  transportation_system: TransportationSystem;
  t: TFunction;
  locale?: Language;
  isFullRoute?: boolean;
  isAllowTail?: boolean;
}

export function toLandingTransportUrl({
  name,
  t,
  transportation_system,
  locale,
  isFullRoute,
  isAllowTail
}: ToLandingTransportUrlProps) {
  const currentLocale = locale === defaultLanguage ? '' : `/${locale}`;
  const nameSlug = toPoiNameSlug(name);
  let path = `${isFullRoute ? process.env.NEXT_PUBLIC_HOME_URL + currentLocale : ''}/${t(
    'bangkokProvince'
  )}/${transportation_system.toLocaleLowerCase()}/${nameSlug}`;
  if (isAllowTail) {
    path += '/';
  }
  return path;
}
