import bangkokPolygon from '@assets/data/bangkok-polygon.json';
import { pointInPolygon } from './point-polygon';

// eslint-disable-next-line import/prefer-default-export
export function isValidLatLngSlug(text: string) {
  const pattern = /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)-\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/;

  return pattern.test(text);
}

export function isPointInBangkok({ lat, lng }: { lat: number; lng: number }) {
  return pointInPolygon([lat, lng], bangkokPolygon);
}
