import { Language } from '@constants/enum';
import { Listing } from '@interfaces/listing';
import { removeFillerWords, toSlug } from './format';

export function getListingDetailLink(
  { id, listingTitleEnGenStandard, listingTitleThGenStandard, tenure }: Listing,
  language: Language
) {
  const title =
    {
      [Language.US]: listingTitleEnGenStandard,
      [Language.TH]: listingTitleThGenStandard
    }[language] ||
    listingTitleEnGenStandard ||
    'listing';

  const listingTitleId = `${toSlug(removeFillerWords(title))}-${id}`;
  const href = { pathname: '/[listing-title-id]', query: { 'listing-title-id': listingTitleId } };
  const as = `/${listingTitleId}`;

  /* FS-1463: noindex all listings link with tenure = sell or rentsell */
  const rel = tenure && ['sell', 'rentsell'].includes(tenure) ? 'nofollow' : undefined;

  return { href, as, rel };
}

export function getListingAdress(rental: Listing, locale: Language) {
  let fields: Array<string | undefined> = [];
  switch (locale) {
    case Language.US:
      fields = [
        rental.neighborhood_ps_en,
        rental.subdistrict_ps_en,
        rental.district_ps_en,
        rental.province_ps_en,
        rental.postcode
      ];
      break;
    case Language.TH:
      fields = [
        rental.neighborhood_ps_th,
        rental.subdistrict_ps_th,
        rental.district_ps_th,
        rental.province_ps_th,
        rental.postcode
      ];
      break;
    default:
      break;
  }

  return fields.filter(Boolean).join(', ');
}

export function getNumberFromValue(value: string) {
  const numbers = [
    { start: 'one', value: 1 },
    { start: 'two', value: 2 },
    { start: 'three', value: 3 },
    { start: 'four', value: 4 },
    { start: 'five', value: 5 },
    { start: 'six', value: 6 },
    { start: 'seven', value: 7 },
    { start: 'eight', value: 8 },
    { start: 'nine', value: 9 },
    { start: 'ten', value: 10 }
  ];

  return numbers.find((item) => value.startsWith(item.start))?.value;
}
