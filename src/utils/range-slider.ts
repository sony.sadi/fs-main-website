// https://www.w3.org/TR/wai-aria-practices/examples/slider/multithumb-slider.html
interface SliderChanges {
  value: number;
  left: number;
}

interface SliderOptions {
  onChange: (changes: SliderChanges) => void;
}

export default class Slider {
  domNode: HTMLElement;

  options: SliderOptions;

  railDomNode: HTMLElement;

  minDomNode: Element | null;

  maxDomNode: Element | null;

  valueNow: number;

  railMin: number;

  railMax: number;

  railWidth: number;

  railBorderWidth: number;

  thumbWidth: number;

  thumbHeight: number;

  constructor(domNode: HTMLElement, options: SliderOptions) {
    this.domNode = domNode;
    this.options = options;
    this.railDomNode = domNode.parentElement as HTMLElement;

    this.minDomNode = null;
    this.maxDomNode = null;

    this.valueNow = 50;

    this.railMin = 0;
    this.railMax = 100;
    this.railWidth = 0;
    this.railBorderWidth = 1;

    this.thumbWidth = 20;
    this.thumbHeight = 20;
  }

  init() {
    if (this.domNode.previousElementSibling) {
      this.minDomNode = this.domNode.previousElementSibling;
      this.railMin = Number(this.minDomNode.getAttribute('aria-valuemin'));
    } else {
      this.railMin = Number(this.domNode.getAttribute('aria-valuemin'));
    }

    if (this.domNode.nextElementSibling) {
      this.maxDomNode = this.domNode.nextElementSibling;
      this.railMax = Number(this.maxDomNode.getAttribute('aria-valuemax'));
    } else {
      this.railMax = Number(this.domNode.getAttribute('aria-valuemax'));
    }

    this.valueNow = Number(this.domNode.getAttribute('aria-valuenow'));

    this.railWidth = Number(this.railDomNode.clientWidth);

    this.domNode.addEventListener('touchstart', this.handleMouseDown.bind(this));
    this.domNode.addEventListener('mousedown', this.handleMouseDown.bind(this));

    this.moveSliderTo(this.valueNow);
  }

  moveSliderTo(value: number) {
    const valueMax = Number(this.domNode.getAttribute('aria-valuemax'));
    const valueMin = Number(this.domNode.getAttribute('aria-valuemin'));
    const isMaxSide = value > valueMax / 2;

    if (value > valueMax) {
      value = valueMax;
    }

    if (value < valueMin) {
      value = valueMin;
    }

    this.valueNow = value;

    const pos = Math.round(
      ((this.valueNow - this.railMin) * (this.railWidth - 2 * (this.thumbWidth - this.railBorderWidth))) /
        (this.railMax - this.railMin)
    );

    const left = pos - this.railBorderWidth + (isMaxSide ? this.thumbWidth : 0);
    this.domNode.style.left = `${left}px`;

    this.options.onChange({
      value: this.valueNow,
      left
    });
  }

  /**
   * This method validates price range (min <= max)
   * @param value
   */
  // moveSliderTo(value: number) {
  //   const valueMax = Number(this.domNode.getAttribute('aria-valuemax'));
  //   const valueMin = Number(this.domNode.getAttribute('aria-valuemin'));

  //   if (value > valueMax) {
  //     value = valueMax;
  //   }

  //   if (value < valueMin) {
  //     value = valueMin;
  //   }

  //   this.valueNow = value;

  //   this.domNode.setAttribute('aria-valuenow', String(this.valueNow));

  //   if (this.minDomNode) {
  //     this.minDomNode.setAttribute('aria-valuemax', String(this.valueNow));
  //   }

  //   if (this.maxDomNode) {
  //     this.maxDomNode.setAttribute('aria-valuemin', String(this.valueNow));
  //   }

  //   const pos = Math.round(
  //     ((this.valueNow - this.railMin) * (this.railWidth - 2 * (this.thumbWidth - this.railBorderWidth))) /
  //       (this.railMax - this.railMin)
  //   );

  //   const left = pos - this.railBorderWidth + (this.minDomNode ? this.thumbWidth : 0);
  //   this.domNode.style.left = left + 'px';

  //   this.options.onChange({
  //     value: this.valueNow,
  //     left
  //   });
  // }

  handleMouseDown(e: MouseEvent | TouchEvent) {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const self = this;

    const handleMouseMove = (event: MouseEvent | TouchEvent) => {
      let pageX = 0;
      if (event instanceof MouseEvent) {
        pageX = event.pageX;
      } else if (event instanceof TouchEvent) {
        pageX = Number(event.changedTouches.item(0)?.pageX);
      }
      const diffX = pageX - self.railDomNode.getBoundingClientRect().left;
      self.valueNow = self.railMin + Number(((self.railMax - self.railMin) * diffX) / self.railWidth);

      self.moveSliderTo(self.valueNow);

      event.preventDefault();
      event.stopPropagation();
    };

    const handleMouseUp = () => {
      document.removeEventListener('touchmove', handleMouseMove);
      document.removeEventListener('mousemove', handleMouseMove);
      document.removeEventListener('mouseup', handleMouseUp);
      document.removeEventListener('touchend', handleMouseUp);
    };

    document.addEventListener('touchmove', handleMouseMove);
    document.addEventListener('mousemove', handleMouseMove);

    document.addEventListener('touchend', handleMouseUp);
    document.addEventListener('mouseup', handleMouseUp);

    e.preventDefault();
    e.stopPropagation();
  }
}
