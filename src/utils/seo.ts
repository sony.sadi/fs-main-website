import { AssembleSeoResponse, SeoResponse } from '@interfaces/seo';
import { formatThousands, recursiveFormat, striptags } from '@utils';
import { TFunction } from 'next-i18next';
import trimEnd from 'lodash/trimEnd';

const HOME_URL = trimEnd(process.env.NEXT_PUBLIC_HOME_URL, '/');

interface IAssembleSeoProps {
  seo: SeoResponse;
  total: number;
  page: number;
  pageSize: number;
  lowestPrice: number;
  address: string;
  coords: string;
  t: TFunction;
}
interface IReplacePlaceholderProps {
  value: string;
  total: string;
  to: string;
  from: string;
  lowestPrice: string;
  address: string;
  coords: string;
}

export function replaceSeoPlaceholder({
  value,
  total,
  to,
  from,
  lowestPrice,
  address,
  coords
}: IReplacePlaceholderProps) {
  return recursiveFormat(
    value
      .replace(/\[SERP_TOTAL_RESULT\]/g, total.toString())
      .replace(/\[WEB_DOMAIN_URL\]/g, HOME_URL)
      .replace(/\[SERP_FROM\]/g, from.toString())
      .replace(/\[SERP_TO\]/g, to.toString())
      .replace(/\[LOWEST_PRICE\]/g, lowestPrice.toString())
      .replace(/\[GOOGLE_POI\]/g, address || `(${coords.replace('-', ',')})`)
      .trim()
  );
}

export function assembleSeo({
  seo,
  total,
  page,
  pageSize,
  lowestPrice,
  address,
  coords,
  t
}: IAssembleSeoProps): AssembleSeoResponse {
  const offset = (page - 1) * pageSize;
  const from = offset + 1;
  const to = Math.min(offset + pageSize, total);
  const isEmpty = total === 0;
  const isSingular = total === 1;

  // TODO: handle i18n when total = 0

  const params: IReplacePlaceholderProps = {
    total: isEmpty ? t('emptyResultLabel') : formatThousands(total),
    to: formatThousands(to),
    from: formatThousands(from),
    lowestPrice: formatThousands(lowestPrice),
    address,
    coords,
    value: ''
  };
  const {
    meta_title,
    meta_description,
    title_counter,
    title,
    singular_title,
    description,
    singular_description,
    empty_result_description,
    singular_meta_description,
    singular_meta_title
  } = seo;

  const seoTitle = (isSingular && singular_title ? singular_title : title).trim();

  const getTitleCounter = () => {
    return isEmpty || isSingular ? '' : title_counter;
  };

  const getDescription = () => {
    if (isEmpty) return empty_result_description || '';
    if (isSingular && singular_description) return singular_description;
    return description;
  };

  const getTitle = () => seoTitle;

  const getMetaDescription = () => {
    return isSingular && singular_meta_description ? singular_meta_description : meta_description;
  };

  const getMetaTitle = () => {
    return isSingular && singular_meta_title ? singular_meta_title : meta_title;
  };

  const getMetaDescriptionTotal = () => {
    return isEmpty || isSingular ? '' : formatThousands(total);
  };

  const getMetaTitleTotal = () => {
    return formatThousands(total);
  };

  const getSchemaTitle = () => {
    return seoTitle.replace(/^\[SERP_TOTAL_RESULT\]/, '');
  };

  const getSchemaDescription = () => {
    return striptags(getDescription().replace(/<br>|<br\/>/g, ' '));
  };

  return {
    meta_title: replaceSeoPlaceholder({
      ...params,
      value: getMetaTitle(),
      total: getMetaTitleTotal()
    }),
    meta_description: replaceSeoPlaceholder({
      ...params,
      value: getMetaDescription(),
      total: getMetaDescriptionTotal()
    }),
    title_counter: replaceSeoPlaceholder({
      ...params,
      value: getTitleCounter()
    }),
    title: replaceSeoPlaceholder({
      ...params,
      value: getTitle()
    }),
    description: replaceSeoPlaceholder({
      ...params,
      value: getDescription()
    }),
    schema_title: replaceSeoPlaceholder({
      ...params,
      value: getSchemaTitle()
    }),
    schema_description: replaceSeoPlaceholder({
      ...params,
      value: getSchemaDescription()
    })
  };
}
