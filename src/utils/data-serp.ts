/* eslint-disable no-case-declarations */
import { BANGKOK_PROVINCE, DEFAULT_SEARCH_ADDRESS } from '@constants';
import { Language } from '@constants/enum';
import { PoiType, toTransportationName, getDefaultRadius } from '@flexstay.rentals/fs-url-stringify';
import { BreadcrumbItem } from '@interfaces/breadcrumb';
import {
  AreaResponse,
  InDemandBuildingResponse,
  PoiSuggestion,
  SearchRequest,
  Transportation,
  TransportationResponse
} from '@interfaces/search';
import { SeoResponse } from '@interfaces/seo';
import { CombineSearchParam, getDataBuildSerp } from '@services/http-client/frontend';
import { GetSerpSeoMetaProps } from '@services/http-client/seo';
import { getSerpRequestRadius } from '@utils';
import { lowerCase } from 'lodash';
import { TFunction } from 'next-i18next';
import { QueryResponse } from './search';

interface BreadcrumbObject {
  radiusBreadcrumb: number;
  coordsBreadcrumb: string;
  lang: Language;
}

/**
 * Generate breamcrum object for combine
 * @param params
 * @param lang
 * @returns
 */
export function generateBreadcrumbObject(params: SearchRequest, lang: Language): BreadcrumbObject {
  const requestParams = {
    ...params,
    lang
  };
  let poiType;
  let where = params.where || '';

  const poiId = requestParams.buildingId || requestParams.areaId || requestParams.transportationId;

  if (poiId) {
    switch (poiId) {
      case requestParams.buildingId:
        poiType = PoiType.BUILDING;
        break;
      case requestParams.areaId:
        poiType = PoiType.AREA;
        break;
      case requestParams.transportationId:
        poiType = PoiType.TRANSPORTATION;
        break;
      default:
        break;
    }
  } else {
    requestParams.coords = params.coords.replace('-', ',');
    where = params.coords ? params.where || '' : DEFAULT_SEARCH_ADDRESS;
  }
  const isDefaultRadius = getDefaultRadius({ address: where, type: poiType }) === params.radius;

  requestParams.radius = isDefaultRadius ? 0 : params.radius;

  return {
    radiusBreadcrumb: requestParams.radius,
    coordsBreadcrumb: requestParams.coords,
    lang: requestParams.lang
  };
}

interface SeoObject {
  radiusSeo: number;
  coordsSeo: string;
  province: string;
  url: string;
  locale: string;
}

/**
 * Generate Seo Object for combine
 * @param params
 * @returns
 */
export function generateSeoObject(params: GetSerpSeoMetaProps): SeoObject {
  const defaultRadius = getSerpRequestRadius({ ...params });
  const requestParams = {
    ...params,
    radius: params.radius === defaultRadius ? 0 : params.radius,
    coords: params.coords.replace('-', ','),
    province: BANGKOK_PROVINCE
  };

  return {
    radiusSeo: requestParams.radius,
    coordsSeo: requestParams.coords,
    province: requestParams.province,
    url: requestParams.url,
    locale: params.locale
  };
}

export interface CombineDataSerp {
  serp_seo: SeoResponse;
  breadcrumbs: BreadcrumbItem[];
  queryObject: QueryResponse;
}

/**
 * Return combine serp data include:
 * queryObject
 * serp_seo
 * breadcrumb
 * @param locale
 * @param tLocale
 * @param params
 * @param url
 * @returns
 */
export const generateCombineDataSerps: (
  locale: Language,
  tLocale: TFunction,
  params: SearchRequest,
  url: string
) => Promise<CombineDataSerp> = async (locale, tLocale, params, url) => {
  const { where, areaId, buildingId, transportationId, transportationSystem } = params;
  let whereQuery = where;
  const poi = null;
  let poiType: PoiType | undefined;
  if (whereQuery) {
    const poiId = areaId || buildingId || transportationId;
    switch (poiId) {
      case buildingId:
        poiType = PoiType.BUILDING;
        break;
      case transportationId:
        if (transportationSystem) {
          whereQuery = toTransportationName(whereQuery, transportationSystem);
        }
        poiType = PoiType.TRANSPORTATION;
        break;
      case areaId:
        poiType = PoiType.AREA;
        break;
      default:
        break;
    }
  }

  const breadcrumbObject = generateBreadcrumbObject(params, locale);
  const seoObject = generateSeoObject({ ...params, locale, url });

  const paramObject = {
    ...params,
    ...breadcrumbObject,
    ...seoObject
  };

  const result = await parseCombineDataFromPoi(paramObject, whereQuery, tLocale, poiType);

  if (result.poiObject) {
    whereQuery = result.poiObject.name[locale];
  }
  return {
    breadcrumbs: result.breadcrumbs,
    serp_seo: result.serp_seo,
    queryObject: {
      queryValue: whereQuery,
      queryDatas: result.poiObject?.name || null
    }
  };
};

interface CombineDataPoi {
  serp_seo: SeoResponse;
  breadcrumbs: BreadcrumbItem[];
  poiObject: PoiSuggestion | null;
}

/**
 * Get data combine data and poiObject from poi_type provided
 * @param params
 * @param slug
 * @param t
 * @param poiType
 * @returns
 */
export async function parseCombineDataFromPoi(
  params: CombineSearchParam,
  slug: string | undefined,
  t: TFunction,
  poiType?: PoiType
): Promise<CombineDataPoi> {
  if (!slug || slug === t('rentalsValue') || !slug.replace(/-/g, ' ') || !poiType) {
    const responseData = await getDataBuildSerp<undefined>(params);

    return {
      serp_seo: responseData.data.serp_seo,
      breadcrumbs: responseData.data.breadcrumbs,
      poiObject: null
    };
  }

  const poiName = slug.replace(/-/g, ' ');

  const lowerCaseName = lowerCase(poiName);

  switch (poiType) {
    case PoiType.TRANSPORTATION:
      const transportationRes = await getDataBuildSerp<TransportationResponse>(params, poiName, poiType);
      const transportation = transportationRes.data.poi_search?.data.find((i: Transportation) =>
        [lowerCase(i.station.en), lowerCase(i.station.th)].includes(lowerCaseName)
      );

      return {
        serp_seo: transportationRes.data.serp_seo,
        breadcrumbs: transportationRes.data.breadcrumbs,
        poiObject: !transportation
          ? null
          : {
              ...transportation,
              name: transportation.station,
              lat: transportation.gps_lat,
              lng: transportation.gps_long,
              poi_type: PoiType.TRANSPORTATION
            }
      };

    case PoiType.BUILDING:
      const resBuilding = await getDataBuildSerp<InDemandBuildingResponse>(params, poiName, poiType);
      const building = resBuilding.data.poi_search?.data.find(
        (i) =>
          [lowerCase(i.name.en), lowerCase(i.name.th)].includes(lowerCaseName) || [i.slug.en, i.slug.th].includes(slug)
      );

      return {
        serp_seo: resBuilding.data.serp_seo,
        breadcrumbs: resBuilding.data.breadcrumbs,
        poiObject: !building
          ? null
          : {
              ...building,
              poi_type: PoiType.BUILDING
            }
      };

    default:
      const resArea = await getDataBuildSerp<AreaResponse>(params, poiName, poiType);
      const area = resArea.data.poi_search?.data.find((i) =>
        [lowerCase(i.name.en), lowerCase(i.name.th)].includes(lowerCaseName)
      );
      return {
        serp_seo: resArea.data.serp_seo,
        breadcrumbs: resArea.data.breadcrumbs,
        poiObject: !area
          ? null
          : {
              ...area,
              lat: area.center_lat,
              lng: area.center_lng,
              poi_type: PoiType.AREA
            }
      };
  }
}
