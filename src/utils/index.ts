import { SSRConfig, TFunction } from 'next-i18next';
import range from 'lodash/range';
import reduce from 'lodash/reduce';
import startCase from 'lodash/startCase';
import isNumber from 'lodash/isNumber';
import round from 'lodash/round';
import numeral from 'numeral';
import lowerCase from 'lodash/lowerCase';
import 'numeral/locales/en-au';
import 'numeral/locales/th';
import { Listing } from '@interfaces/listing';
import { Language, LocaleNamespace } from '@constants/enum';
import {
  MinLease,
  NumberOfBed,
  Params,
  PoiType,
  PropertyType,
  SortType,
  TransportationSystem,
  stringifyUrl,
  getDefaultRadius,
  getDefaultParams,
  SerpType
} from '@flexstay.rentals/fs-url-stringify';
import { SearchRequest, SearchResponse } from '@interfaces/search';
import { DEFAULT_SEARCH_ADDRESS, LANGUAGES } from '@constants';
import trimEnd from 'lodash/trimEnd';

import { getListingDetailLink } from './listing';

interface PaginationTextProps {
  total: number;
  page: number;
  pageSize: number;
  address?: string;
  t: TFunction;
  areaId?: number;
  buildingId?: number;
  transportationId?: number;
  transportationSystem?: TransportationSystem;
  coords?: string;
  defaultPagingText?: string;
}

interface GetObjectSchemeSerpsProps {
  rentals: SearchResponse;
  paginationText: string;
  language: Language;
  descriptionText?: string;
}

interface GetTransportationSystemProps {
  closestBts: string;
  closestMrt: string;
  closestArl: string;
  closestTransport: string;
}

interface GetTextDistanceProps extends GetTransportationSystemProps {
  closestTransport: string;
  closestTransportKm: number;
  t: TFunction;
  radius?: number;
}

interface AggregateOffer {
  '@type': string;
  lowPrice: number;
  highPrice: number;
  offerCount: number;
  priceCurrency: string;
  offers: Array<any>;
}
export interface SchemaSerpsResponse {
  '@context': string;
  '@type': string;
  name: string;
  description: string;
  offers: AggregateOffer;
}
interface ICounterTextProps {
  page: number;
  pageSize: number;
  total: number;
  t: TFunction;
}

export function getImageUrl(path: string, hostname?: string) {
  if (!path) return '/default.gif';

  if (/^https?:\/\//i.test(path)) return path;

  return (hostname || process.env.NEXT_PUBLIC_IMAGE_URL) + path.replace(/^\//, '');
}

export function useUnoptimizedImage(url: string) {
  const domains = String(process.env.NEXT_PUBLIC_OPTIMIZED_IMAGE_DOMAINS).split(',');

  return /^https?:\/\//i.test(url) && !domains.some((domain) => url.includes(domain));
}

export function getBedroomNumber(value: string) {
  const beds = [
    'one_bedroom',
    'two_bedrooms',
    'three_bedrooms',
    'four_bedrooms',
    'five_bedrooms',
    'six_bedrooms',
    'seven_bedrooms',
    'eight_bedrooms',
    'nine_bedrooms',
    'ten_bedrooms'
  ];
  return beds.indexOf(value) === -1 ? 0 : beds.indexOf(value) + 1;
}

export function getBathroomNumber(value: string) {
  const baths = [
    'one_bathroom',
    'two_bathrooms',
    'three_bathrooms',
    'four_bathrooms',
    'five_bathrooms',
    'six_bathrooms',
    'seven_bathrooms',
    'eight_bathrooms',
    'nine_bathrooms',
    'ten_bathrooms'
  ];
  return baths.indexOf(value) === -1 ? 0 : baths.indexOf(value) + 1;
}

export function getPropertyTitle(item: Listing, language: Language) {
  const {
    buildingId,
    listingTitleEn,
    listingTitleTh,
    listingTitleEnGenStandard,
    listingTitleThGenStandard,
    propertyType
  } = item;

  const buildingName = language === Language.US ? buildingId?.name_en : buildingId?.name_th;

  const title = language === Language.US ? listingTitleEn : listingTitleTh;
  const titleGenStandard = language === Language.US ? listingTitleEnGenStandard : listingTitleThGenStandard;

  if ([PropertyType.Apartment, PropertyType.ServicedApartment].includes(propertyType as PropertyType)) {
    return titleGenStandard || title;
  }
  if (buildingName) return buildingName;
  if (title) return title;
  if (titleGenStandard) return titleGenStandard;

  return '';
}

export function getTextBedroomByNumber(value: string, t: TFunction) {
  const beds = [
    'one_bedroom',
    'two_bedrooms',
    'three_bedrooms',
    'four_bedrooms',
    'five_bedrooms',
    'six_bedrooms',
    'seven_bedrooms',
    'eight_bedrooms',
    'nine_bedrooms',
    'ten_bedrooms'
  ];
  const numberRoom = beds.indexOf(value) === -1 ? 0 : beds.indexOf(value) + 1;
  return numberRoom > 1 ? t('bedRooms', { numberRoom }) : t('bedRoom', { numberRoom });
}

export function getTextBathroomByNumber(value: string, t: TFunction) {
  const baths = [
    'one_bathroom',
    'two_bathrooms',
    'three_bathrooms',
    'four_bathrooms',
    'five_bathrooms',
    'six_bathrooms',
    'seven_bathrooms',
    'eight_bathrooms',
    'nine_bathrooms',
    'ten_bathrooms'
  ];
  const numberRoom = baths.indexOf(value) === -1 ? 0 : baths.indexOf(value) + 1;
  return numberRoom > 1 ? t('bathRooms', { numberRoom }) : t('bathRoom', { numberRoom });
}

export function getAddress(rent: Listing, locale: Language) {
  let area = '';
  let province = '';
  switch (locale) {
    case Language.US:
      area = rent.neighborhood_ps_en || rent.subdistrict_ps_en || rent.district_ps_en || '';
      province = rent.province_ps_en || '';
      break;
    case Language.TH:
      area = rent.neighborhood_ps_th || rent.subdistrict_ps_th || rent.district_ps_th || '';
      province = rent.province_ps_th || '';
      break;
    default:
      return '';
  }
  return [area, province].filter(Boolean).join(', ');
}

export function getClosestTransportSystem({
  closestTransport,
  closestArl,
  closestBts,
  closestMrt
}: GetTransportationSystemProps) {
  switch (lowerCase(closestTransport)) {
    case lowerCase(closestArl):
      return TransportationSystem.ARL;
    case lowerCase(closestBts):
      return TransportationSystem.BTS;
    case lowerCase(closestMrt):
      return TransportationSystem.MRT;
    default:
      return null;
  }
}

export function getTextDistance({
  closestTransport,
  closestTransportKm,
  closestArl,
  closestBts,
  closestMrt,
  t,
  radius
}: GetTextDistanceProps) {
  if (!closestTransport || !closestTransportKm) return '';

  const numberMeter = round(closestTransportKm * 1000, -1);
  if (isNumber(radius) && numberMeter > radius) {
    return '';
  }

  const transportationSystem = getClosestTransportSystem({
    closestArl,
    closestBts,
    closestMrt,
    closestTransport
  });

  if (!transportationSystem) {
    return t('distance', { numberMeter, address: startCase(closestTransport) });
  }

  const i18nKey = `station_${transportationSystem.toLowerCase()}_${closestTransport}`;
  return t('distance', { numberMeter, address: t(i18nKey) });
}

export function formatPrice(value: number) {
  return new Intl.NumberFormat().format(value);
}

export function getSerpRequestRadius(request: SearchRequest) {
  const { buildingId, areaId, transportationId } = request;
  let where = request.where || '';
  const poiId = buildingId || areaId || transportationId;
  let poiType: PoiType | undefined;
  if (poiId) {
    switch (poiId) {
      case buildingId:
        poiType = PoiType.BUILDING;
        break;
      case areaId:
        poiType = PoiType.AREA;
        break;
      default:
        poiType = PoiType.TRANSPORTATION;
        break;
    }
  } else {
    where = request.coords ? where : DEFAULT_SEARCH_ADDRESS;
  }
  return getDefaultRadius({ address: where, type: poiType });
}

export function getPaginationText({
  total,
  page,
  pageSize,
  address,
  areaId,
  transportationId,
  transportationSystem,
  buildingId,
  coords,
  defaultPagingText = 'availablePropertyAddress',
  t
}: PaginationTextProps) {
  if (!total) return t('noProperty');

  const from = (page - 1) * pageSize;
  const to = Math.min(from + pageSize, total);
  const unit = total > 1 ? t('properties') : t('property');
  const totalFormatted = numeral(total).format('0,0');

  if (areaId) {
    return t('availablePropertyArea', { from: from + 1, to, total: totalFormatted, unit, area: address });
  }

  if (transportationId) {
    return t('availablePropertyTranpo', {
      from: from + 1,
      to,
      total: totalFormatted,
      unit,
      system: transportationSystem,
      transportation: address
    });
  }

  if (buildingId) {
    return t('availablePropertyBuilding', { from: from + 1, to, total: totalFormatted, unit, building: address });
  }

  if (coords) {
    if (address) {
      return t('availablePropertyGooglePoi', { from: from + 1, to, total: totalFormatted, unit, address });
    }
    return t('availableProperty', { from: from + 1, to, total: totalFormatted, unit });
  }

  return t(defaultPagingText, {
    from: from + 1,
    to,
    total: totalFormatted,
    unit,
    address: address || 'Bangkok'
  });
}

export function getCounterText({ page, pageSize, total, t }: ICounterTextProps) {
  if (total === 0) return '';
  const start = (page - 1) * pageSize;
  const to = Math.min(start + pageSize, total);
  return t('counterText', {
    from: formatThousands(start + 1),
    to: formatThousands(to)
  });
}

export function formatThousands(value: number) {
  return numeral(value).format('0,0');
}

/**
 * Function return schema serps object
 * @param rentals
 * @param rentalsParams
 * @param paginationText
 */
export const getObjectSchemeSerps: ({
  rentals,
  paginationText,
  language,
  descriptionText
}: GetObjectSchemeSerpsProps) => SchemaSerpsResponse = ({ rentals, paginationText, language, descriptionText }) => {
  const { NEXT_PUBLIC_HOME_URL } = process.env;

  const HOME_URL = trimEnd(NEXT_PUBLIC_HOME_URL, '/');
  const { data, aggs, total } = rentals;

  const localePrefix = language === Language.US ? Language.US : '';
  const originUrl = trimEnd(`${HOME_URL}/${localePrefix}`, '/');

  const arrayOffer = data.map((value) => {
    const { href } = getListingDetailLink(value, language);
    return {
      '@type': 'Offer',
      url: `${originUrl}/${href.query['listing-title-id']}/`
    };
  });

  const aggregateOffer = {
    '@type': 'AggregateOffer',
    lowPrice: aggs?.lowest_price || 0,
    highPrice: aggs?.highest_price || 0,
    offerCount: total,
    priceCurrency: 'THB',
    offers: arrayOffer
  };

  return {
    '@context': 'https://schema.org',
    '@type': 'Product',
    name: paginationText,
    description: descriptionText || '',
    offers: aggregateOffer
  };
};

/**
 * Handling how many markers should be visible on specific room level
 * @param zoom a zoom level
 */
export function getNumberOfMarker(zoom: number) {
  if (zoom < 10) return 20;
  if (zoom < 13) return 100;
  return 600;
}

export function getSortingOptions(t: TFunction, isSale?: boolean | undefined) {
  return [
    { label: t('recommended'), value: SortType.Recommended },
    { label: t('newestListingsFirst'), value: SortType.Newest },
    { label: t(isSale ? 'saleFromLowestToHighest' : 'rentFromLowestToHighest'), value: SortType.PriceLow },
    { label: t(isSale ? 'saleFromHighestToLowest' : 'rentFromHighestToLowest'), value: SortType.PriceHigh }
  ];
}

export function getSortingFullName({
  t,
  type,
  serpType
}: {
  t: TFunction;
  type: SortType;
  serpType: SerpType | undefined;
}) {
  switch (type) {
    case SortType.Newest:
      return t('sortByNewestListingFirst');
    case SortType.PriceLow:
      return t(serpType === 'sales' ? 'sortBySaleFromLowestToHighest' : 'sortByRentFromLowestToHighest');
    case SortType.PriceHigh:
      return t(serpType === 'sales' ? 'sortBySaleFromHighestToLowest' : 'sortByRentFromHighestToLowest');
    default:
      return '';
  }
}

export function getBudgetTypesFullName({ t, type }: { t: TFunction; type: MinLease }) {
  switch (type) {
    case MinLease.Monthly:
      return t('monthlyFullName');
    case MinLease.TwoMonths:
      return t('2+monthFullName');
    case MinLease.ThreeMonths:
      return t('3+monthFullName');
    case MinLease.SixMonths:
      return t('6+monthFullName');
    default:
      return '';
  }
}

export function getBedroomFullNames({ t, type }: { t: TFunction; type: NumberOfBed }) {
  switch (type) {
    case NumberOfBed.AnyType:
      return t('bedStudioPlus');
    case NumberOfBed.OnePlus:
      return t('bedOnePlus');
    case NumberOfBed.TwoPlus:
      return t('bedTwoPlus');
    case NumberOfBed.ThreePlus:
      return t('bedThreePlus');
    case NumberOfBed.FourPlus:
      return t('bedFourPlus');
    default:
      return '';
  }
}

export function getPriceOptions() {
  return range(1e3, 1e6 + 1000, 1e3).map((num) => ({
    label: `${formatPrice(num)} THB`,
    value: num
  }));
}

export function decamelizeProperty(property: Listing, searchString: string) {
  return reduce(
    property,
    (list, value, key) => {
      if (key.startsWith(searchString) && value === true) {
        list.push(key.slice(searchString.length).replace(/(.)([A-Z])/g, '$1 $2'));
      }

      return list;
    },
    new Array<string>()
  );
}

export function echoHTML(content: string) {
  return content.replace(/(?:\r\n|\r|\n)/g, '<br/>');
}

export function getTextPrice(price: number, t: TFunction, serpType?: SerpType) {
  if (serpType === 'sales') return t('salePrice', { price: formatPrice(price) });
  return t('lowestPrice', { price: formatPrice(price) });
}

export function getOrdinals(number: number, language: Language = Language.US) {
  numeral.locale(language);
  return numeral(number).format('0o');
}

export function isJsonString(str: string) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

export function isUrl(url: string) {
  return /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/.test(url);
}

export function useSSRTranslations(ssrConfig: SSRConfig, ns: string = LocaleNamespace.Common) {
  // eslint-disable-next-line no-underscore-dangle
  const messages = ssrConfig._nextI18Next.initialI18nStore[ssrConfig._nextI18Next.initialLocale][ns];
  return {
    t(key: string) {
      return messages[key];
    }
  };
}

export function getSerpBaseUrl(tCommon: TFunction, serpType?: SerpType) {
  const serp_type = serpType || 'rents';
  return stringifyUrl({ request: { ...getDefaultParams(serp_type), index_serp_types: ['rents'] }, t: tCommon });
}

export function getAllowedLocales() {
  const appLocales = Object.values(Language);
  const localesSetting = process.env.NEXT_PUBLIC_ALLOWED_LOCALES?.split(',') || [];
  const locales = appLocales.filter((locale) => localesSetting.includes(locale));
  return locales.length ? locales : appLocales;
}

export function getLanguageSelectOptions() {
  const allowedLocales = getAllowedLocales();
  return allowedLocales.map((lang) => ({
    name: LANGUAGES[lang].name,
    icon: LANGUAGES[lang].icon,
    text: LANGUAGES[lang].text
  }));
}
export async function sleep(duration: number) {
  await new Promise((resolve) => {
    setTimeout(() => {
      resolve(null);
    }, duration);
  });
}

export function startCasePoiName(name: string) {
  return name
    .split(' ')
    .map((part) =>
      part
        .split('-')
        .map((word) => (word ? word.replace(word[0], word[0].toUpperCase()) : word))
        .join('-')
    )
    .join(' ');
}

export const recursiveFormat = (text: string, count = 0): string => {
  if (count > 15) return text;
  const regexRemoveDoubleSpace = / {2,}/g;
  const regexRemoveSpaceBeforeDot = / +\./g;
  const regexRemoveSpaceBeforeComma = / +,/g;
  const regexDoubleDot = /[\.,]{2,}/g;
  const regexDoubleEnter = /^[\n,.\s]{2,}/gm;
  if (
    new RegExp(regexDoubleDot).test(text) ||
    new RegExp(regexDoubleEnter).test(text) ||
    new RegExp(regexRemoveSpaceBeforeDot).test(text) ||
    new RegExp(regexRemoveSpaceBeforeComma).test(text) ||
    new RegExp(regexRemoveDoubleSpace).test(text)
  ) {
    const replaceText = text
      .replace(regexRemoveSpaceBeforeDot, '.')
      .replace(regexRemoveSpaceBeforeComma, ',')
      .replace(regexDoubleDot, '.')
      .replace(regexDoubleEnter, '\n')
      .replace(regexRemoveDoubleSpace, ' ');

    return recursiveFormat(replaceText, ++count);
  }
  return text;
};

export const striptags = (text?: string) => {
  if (!text) return '';
  return recursiveFormat(text.replace(/(<([^>]+)>)/gi, ''));
};
