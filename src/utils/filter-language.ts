import { defaultLanguage } from '@constants';
import { getAllowedLocales } from '@utils';
import { Area } from '@interfaces/search';

export function filterLanguage(arrs: Area[], locale = 'th') {
  const localeDelete = locale === 'th' ? 'en' : 'th';
  // eslint-disable-next-line no-restricted-syntax
  for (const arr of arrs) {
    delete arr.name[localeDelete];
    delete arr.short_teaser[localeDelete];
    delete arr.teaser[localeDelete];
  }
  return arrs;
}

/**
 * Return alternate language array
 */
export const getAlternateLanguage: (language: string) => Array<any> = (language) => {
  const allowLocale = getAllowedLocales();
  if (allowLocale && allowLocale.length > 1) {
    const alternateArray = allowLocale.filter((locale) => !locale.includes(language));
    return alternateArray;
  }
  return [];
};

/**
 * Return hrefLang
 * @param urlPath
 */
export const generateHrefLang = (urlPath: string, locale: string, isNotAllowTail?: boolean) => {
  const currentLocale = locale === defaultLanguage ? '' : `/${locale}`;
  let path = `${process.env.NEXT_PUBLIC_HOME_URL}${currentLocale}${urlPath}`;
  if (!isNotAllowTail) {
    path += `/`;
  }
  return path;
};
