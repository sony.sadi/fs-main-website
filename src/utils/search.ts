/* eslint-disable no-nested-ternary */
/* eslint-disable no-continue */
/* eslint-disable no-case-declarations */
import { GetStaticPropsContext } from 'next';
import { TFunction } from 'next-i18next';
import lowerCase from 'lodash/lowerCase';
import uniq from 'lodash/uniq';

import {
  getAreasPoisSuggestion,
  getBuildingPoisSuggestion,
  getTransportationsPoisSuggestion
} from '@services/http-client/search';
import { PoiSuggestion, SearchRequest } from '@interfaces/search';
import {
  Params,
  PoiType,
  TransportationSystem,
  MinLease,
  NumberOfBed,
  PropertyType,
  SortType,
  Amenity,
  BuildingType,
  toTransportationName,
  getDefaultRadius,
  SerpType
} from '@flexstay.rentals/fs-url-stringify';
import { Language, ListingTabs } from '@constants/enum';

import { DEFAULT_SEARCH_RADIUS } from '@constants';
import { startCasePoiName } from '@utils';
import { isPointInBangkok, isValidLatLngSlug } from '@utils/map';

export function getFilteringTabs(t: TFunction) {
  return [
    {
      title: t('price'),
      name: ListingTabs.Price
    },
    {
      title: t('beds'),
      name: ListingTabs.Bed
    },
    {
      title: t('more'),
      name: ListingTabs.MoreFilter
    }
  ];
}

export function getBedroomOptions(t: TFunction) {
  return [
    { label: t('studio+'), value: NumberOfBed.AnyType },
    { label: t('1+'), value: NumberOfBed.OnePlus },
    { label: t('2+'), value: NumberOfBed.TwoPlus },
    { label: t('3+'), value: NumberOfBed.ThreePlus },
    { label: t('4+'), value: NumberOfBed.FourPlus }
  ];
}

export function getPropertyTypes(t: TFunction) {
  return [
    {
      name: t('condo'),
      id: PropertyType.Condo
    },
    {
      name: t('apartment'),
      id: PropertyType.Apartment
    },
    {
      name: t('house'),
      id: PropertyType.House
    },
    {
      name: t('servicedApartment'),
      id: PropertyType.ServicedApartment
    },
    {
      name: t('townhouse'),
      id: PropertyType.Townhouse
    }
  ];
}

export function getBuildingTypes(type: BuildingType, t: TFunction) {
  switch (type) {
    case BuildingType.Condo:
      return t('buildingTypeCondo');
    case BuildingType.Apartment:
      return t('buildingTypeApartment');
    case BuildingType.House:
      return t('buildingTypeHouse');
    case BuildingType.ServicedApartment:
      return t('buildingTypeServicedApartment');
    case BuildingType.Townhouse:
      return t('buildingTypeTownhouse');
    case BuildingType.Commercial:
      return t('buildingTypeCommercial');
    default:
      return '';
  }
}

export function getTransportationSystem(type: TransportationSystem) {
  switch (type) {
    case TransportationSystem.MRT:
      return TransportationSystem.MRT;
    case TransportationSystem.BTS:
      return TransportationSystem.BTS;
    case TransportationSystem.ARL:
      return TransportationSystem.ARL;
    default:
      return '';
  }
}

export function getBudgetTypes(t: TFunction) {
  return [
    {
      name: t('monthly'),
      id: MinLease.Monthly
    },
    {
      name: t('2+month'),
      id: MinLease.TwoMonths
    },
    {
      name: t('3+month'),
      id: MinLease.ThreeMonths
    },
    {
      name: t('6+month'),
      id: MinLease.SixMonths
    },
    {
      name: t('12+month'),
      id: MinLease.TwelveMonths
    }
  ];
}

export function getAmenities(t: TFunction) {
  return [
    {
      name: t('balcony'),
      value: Amenity.Balcony
    },
    {
      name: t('walkInWardrobe'),
      value: Amenity.WalkInWardrobe
    },
    {
      name: t('stove'),
      value: Amenity.StoveCooktop
    },
    {
      name: t('refrigerator'),
      value: Amenity.Refrigerator
    },
    {
      name: t('tv'),
      value: Amenity.Tv
    },
    {
      name: t('microwave'),
      value: Amenity.Microwave
    },
    {
      name: t('oven'),
      value: Amenity.Oven
    },
    {
      name: t('playground'),
      value: Amenity.Playground
    },
    {
      name: t('petFriendly'),
      value: Amenity.PetFriendly
    },
    {
      name: t('gym'),
      value: Amenity.Gym
    },
    {
      name: t('garden'),
      value: Amenity.Garden
    },
    {
      name: t('parking'),
      value: Amenity.Parking
    },
    {
      name: t('pool'),
      value: Amenity.Pool
    },
    {
      name: t('jacuzzi'),
      value: Amenity.Jacuzzi
    },
    {
      name: t('bathtub'),
      value: Amenity.Bathtub
    }
  ];
}

export function parseFloorSizeSlug(slug: string, t: TFunction) {
  const unit = t('floorSizeUnitValue');
  const regex = new RegExp(`\\d{1,}${unit}$`);

  if (!regex.test(slug)) return [];

  const [gte = '', lte = ''] = slug.split('-');
  return [Number(gte.replace(unit, '')), Number(lte.replace(unit, ''))];
}

export function parsePriceRangeSlug(slug: string, t: TFunction) {
  const unit = t('priceUnitValue');
  const regex = new RegExp(`\\d+${unit}$`);

  if (!regex.test(slug)) return [];

  const [min = '', max = ''] = slug.split('-');
  return [Number(min.replace(unit, '')), Number(max.replace(unit, ''))];
}

export function parsePropertyTypeSlug(slug: string, t: TFunction) {
  const propertyTypeMap = {
    [t('propertyTypeCondoValue')]: PropertyType.Condo,
    [t('propertyTypeServicedApartmentValue')]: PropertyType.ServicedApartment,
    [t('propertyTypeHouseValue')]: PropertyType.House,
    [t('propertyTypeTownhouseValue')]: PropertyType.Townhouse,
    [t('propertyTypeApartmentValue')]: PropertyType.Apartment
  };
  const propertyTypes = slug.split('+').map((key) => propertyTypeMap[key]);
  /**
   * FS-1193 return 404 for any URLs that have duplicated propertyType or wrong property type
   */
  return propertyTypes.length === uniq(propertyTypes.filter(Boolean)).length ? propertyTypes : [];
}

function parseBuildingTypeSlug(slug: string, t: TFunction) {
  return {
    [t('buildingTypeCondoValue')]: BuildingType.Condo,
    [t('buildingTypeServicedApartmentValue')]: BuildingType.ServicedApartment,
    [t('buildingTypeHouseValue')]: BuildingType.House,
    [t('buildingTypeTownhouseValue')]: BuildingType.Townhouse,
    [t('buildingTypeApartmentValue')]: BuildingType.Apartment
  }[slug];
}

export function parseUnitTypeSlug(slug: string, t: TFunction) {
  return {
    [t('unitTypeOnePlusValue')]: NumberOfBed.OnePlus,
    [t('unitTypeTwoPlusValue')]: NumberOfBed.TwoPlus,
    [t('unitTypeThreePlusValue')]: NumberOfBed.ThreePlus,
    [t('unitTypeFourPlusValue')]: NumberOfBed.FourPlus
  }[slug];
}

export function parseMinLeaseSlug(slug: string, t: TFunction) {
  return {
    [t('minLeaseOneMonthValue')]: MinLease.Monthly,
    [t('minLeaseTwoMonthsValue')]: MinLease.TwoMonths,
    [t('minLeaseThreeMonthsValue')]: MinLease.ThreeMonths,
    [t('minLeaseSixMonthsValue')]: MinLease.SixMonths,
    [t('minLeaseTwelveMonthsValue')]: MinLease.TwelveMonths
  }[slug];
}

export function parseAmenitiesSlug(slug: string, t: TFunction) {
  const amenity = {
    [t('amenityBalconyValue')]: Amenity.Balcony,
    [t('amenityWalkInWardrobeValue')]: Amenity.WalkInWardrobe,
    [t('amenityStoveCooktopValue')]: Amenity.StoveCooktop,
    [t('amenityRefrigeratorValue')]: Amenity.Refrigerator,
    [t('amenityTvValue')]: Amenity.Tv,
    [t('amenityMicrowaveValue')]: Amenity.Microwave,
    [t('amenityOvenValue')]: Amenity.Oven,
    [t('amenityPlaygroundValue')]: Amenity.Playground,
    [t('amenityPetFriendlyValue')]: Amenity.PetFriendly,
    [t('amenityGymValue')]: Amenity.Gym,
    [t('amenityGardenValue')]: Amenity.Garden,
    [t('amenityParkingValue')]: Amenity.Parking,
    [t('amenitySwimmingPoolValue')]: Amenity.Pool,
    [t('amenityJacuzziValue')]: Amenity.Jacuzzi,
    [t('amenityBathtubValue')]: Amenity.Bathtub
  };

  return uniq(
    slug
      .split('+')
      .map((key) => amenity[key])
      .filter(Boolean)
  );
}

export function parseSortingSlug(slug: string, t: TFunction) {
  return {
    [t('sortingRecommendedValue')]: SortType.Recommended,
    [t('sortingLatestValue')]: SortType.Newest,
    [t('sortingCheapestValue')]: SortType.PriceLow,
    [t('sortingPriceDescValue')]: SortType.PriceHigh
  }[slug] as SortType;
}

export function parseRadiusSlug(slug: string, t: TFunction) {
  const unit = t('radiusUnitValue');
  const regex = new RegExp(`\\d{3,}${unit}$`);

  if (regex.test(slug)) {
    return Number(slug.replace(',', '.').replace(unit, '')) / 1000;
  }
}

export function parseCoords(slug: string) {
  if (!isValidLatLngSlug(slug)) return;
  const [lat, lng] = slug.split('-').map(Number);
  if (!isPointInBangkok({ lat, lng })) return;
  return slug;
}

export function parsePageSlug(slug: string, t: TFunction) {
  const unit = t('pageUnitValue');
  const regex = new RegExp(`^${unit}-\\d+$`);
  if (!regex.test(slug)) return;
  return Number(slug.replace(`${unit}-`, '')) || 1;
}

interface ParsePoiSlugProps {
  slug: string;
  t?: TFunction;
  poiType?: PoiType;
  generalTypeObject?: GeneralTypeObject;
}

export async function parsePoiSlug({
  slug,
  t,
  poiType,
  generalTypeObject
}: ParsePoiSlugProps): Promise<PoiSuggestion | undefined> {
  if (slug === t?.('rentalsValue')) return;

  const poiName = slug.replace(/-/g, ' ');
  if (!poiName || !poiType) return;

  const lowerCaseName = lowerCase(poiName);

  switch (poiType) {
    case PoiType.TRANSPORTATION:
      const transportationRes = await getTransportationsPoisSuggestion({ _q: poiName });
      // Passing transportation_system if Poitype = transportation, find transportation if conditional have both match station name and transportation_system
      const transportationSystem = generalTypeObject?.transportationType || '';
      const transportation = transportationRes.data.data.find(
        (i) =>
          [lowerCase(i.station.en), lowerCase(i.station.th)].includes(lowerCaseName) &&
          lowerCase(i.transportation_system) === transportationSystem
      );
      if (!transportation) return;
      return {
        ...transportation,
        name: transportation.station,
        lat: transportation.gps_lat,
        lng: transportation.gps_long,
        poi_type: PoiType.TRANSPORTATION
      };

    case PoiType.BUILDING:
      const resBuilding = await getBuildingPoisSuggestion({ _q: poiName });
      const building = resBuilding.data.data.find(
        (i) =>
          [lowerCase(i.name.en), lowerCase(i.name.th)].includes(lowerCaseName) || [i.slug.en, i.slug.th].includes(slug)
      );
      if (!building) return;
      return {
        ...building,
        poi_type: PoiType.BUILDING
      };

    default:
      const resArea = await getAreasPoisSuggestion({ _q: poiName });
      const area = resArea.data.data.find((i) => [lowerCase(i.name.en), lowerCase(i.name.th)].includes(lowerCaseName));
      if (!area) return;
      return {
        ...area,
        lat: area.center_lat,
        lng: area.center_lng,
        poi_type: PoiType.AREA
      };
  }
}

interface GeneralTypeObject {
  transportationType?: string;
  buildingType?: string;
  propertyType?: string;
}

export async function getSearchRequest(context: GetStaticPropsContext, t: TFunction): Promise<SearchRequest> {
  const slugs = context.params?.slugs as string[];
  const language = context.locale as Language;
  const rentalsString = t('rentalsValue');
  const salesString = t('salesValue');

  let serpType: SerpType = 'rents';
  // https://docstore.mik.ua/orelly/java/langref/appa_01.htm
  // \u0E00 - \u0E7F Thai
  const regexRental = new RegExp(`([\u0E00-\u0E7Fa-zA-Z0-9-\s,]+)\/${rentalsString}`);
  const regexSale = new RegExp(`([\u0E00-\u0E7Fa-zA-Z0-9-\s,]+)\/${salesString}`);

  if (slugs) {
    const isRentSlug = regexRental.test(slugs.join('/'));
    const isSaleSlug = regexSale.test(slugs.join('/'));

    serpType = isSaleSlug ? 'sales' : 'rents';
  }
  const params: SearchRequest = {
    [Params.Where]: '',
    [Params.Coords]: '',
    [Params.MinPrice]: 0,
    [Params.MaxPrice]: 0,
    [Params.FloorSizeGte]: 0,
    [Params.FloorSizeLte]: 0,
    [Params.NumberBedrooms]: NumberOfBed.AnyType,
    [Params.PropertyType]: [],
    [Params.MinLease]: MinLease.TwelveMonths,
    [Params.Amenities]: [],
    [Params.Radius]: DEFAULT_SEARCH_RADIUS,
    [Params.Page]: 1,
    [Params.Limit]: 20,
    [Params.SortBy]: SortType.Recommended,
    [Params.BuildingType]: BuildingType.Condo,
    [Params.SerpType]: serpType
  };

  const totalMatched = Object.fromEntries(Object.values(Params).map((param) => [param, 0]));

  let validParamsPattern = false;
  function createParamsProxy<K extends keyof SearchRequest>() {
    return new Proxy(params, {
      set(target: SearchRequest, p: K, value: SearchRequest[K]) {
        target[p] = value;
        totalMatched[p]++;
        validParamsPattern = true;
        return true;
      }
    });
  }

  const paramsProxy = createParamsProxy();
  const invalidSlugs: string[] = [];

  let isRadiusExist = false;
  let poiType = PoiType.AREA;
  const generalTypeObject: GeneralTypeObject = {}; // include {property-type}, {transportation-type}, {building-type}...

  if (slugs?.length) {
    for (let index = 0; index < slugs.length; index++) {
      const slug = slugs[index];
      switch (index) {
        case 0:
          if (slug === t('bangkokProvince')) continue;
          throw new Error('Invalid parameter values');
        case 1:
          const buildingType = parseBuildingTypeSlug(slug, t);
          if (!buildingType) break;
          paramsProxy.building_type = buildingType;
          poiType = PoiType.BUILDING;
          continue;
        default:
          break;
      }

      const [gte, lte] = parseFloorSizeSlug(slug, t);
      if (gte) paramsProxy[Params.FloorSizeGte] = gte;
      if (lte) paramsProxy[Params.FloorSizeLte] = lte;

      const [min, max] = parsePriceRangeSlug(slug, t);
      if (min) paramsProxy[Params.MinPrice] = min;
      if (max) paramsProxy[Params.MaxPrice] = max;

      /*
        - now, url slug of building type is same as property type
        - so we must add condition by check if the index of slug equal to one
      */
      const propertyTypes = parsePropertyTypeSlug(slug, t);
      if (propertyTypes.length) paramsProxy[Params.PropertyType] = propertyTypes;

      const unitType = parseUnitTypeSlug(slug, t);
      if (unitType) paramsProxy[Params.NumberBedrooms] = unitType;

      const minLease = parseMinLeaseSlug(slug, t);
      if (minLease && serpType === 'sales') {
        throw new Error('No Min Lease param on sales');
      }
      if (minLease) paramsProxy[Params.MinLease] = minLease;

      const amenities = parseAmenitiesSlug(slug, t);
      if (amenities.length) paramsProxy[Params.Amenities] = amenities;

      const sorting = parseSortingSlug(slug, t);
      if (sorting) {
        paramsProxy[Params.SortBy] = sorting;
      }

      const radius = parseRadiusSlug(slug, t);
      if (radius) {
        paramsProxy[Params.Radius] = radius;
        isRadiusExist = true;
      }

      const coords = parseCoords(slug);
      if (coords) {
        paramsProxy.coords = coords;
      }

      const page = parsePageSlug(slug, t);
      if (page) paramsProxy.page = page;

      if (validParamsPattern) {
        validParamsPattern = false;
      } else {
        invalidSlugs.push(slug);
      }
    }

    if (Object.values(totalMatched).some((total) => total > 1)) {
      throw new Error('Invalid parameter values');
    }

    if (Object.values(TransportationSystem).some((value) => value.toLowerCase() === slugs[1].toLocaleLowerCase())) {
      generalTypeObject.transportationType = slugs[1].toLocaleLowerCase();
      poiType = PoiType.TRANSPORTATION;
    }

    // must be ignore slug at index 0 because it is province
    const matchesRental = slugs.slice(1).join('/').match(regexRental);

    const matchesSale = slugs.slice(1).join('/').match(regexSale);

    const isRentalSerp = !!(matchesRental && matchesRental.length > 1);
    const isSaleSerp = !!(matchesSale && matchesSale.length > 1);

    if ((isRentalSerp || isSaleSerp) && !paramsProxy.coords) {
      const [_, searchTerm] = (isRentalSerp && matchesRental) || (isSaleSerp && matchesSale) || [];
      paramsProxy.where = searchTerm;
      const poi = await parsePoiSlug({ slug: searchTerm, t, poiType, generalTypeObject });
      if (poi) {
        switch (poi.poi_type) {
          case PoiType.BUILDING:
            paramsProxy.buildingId = poi.id;
            if (paramsProxy.building_type !== (poi.building_type || BuildingType.Condo)) {
              invalidSlugs.push(slugs[1]);
            }
            break;
          case PoiType.AREA:
            paramsProxy.areaId = poi.id;
            break;
          case PoiType.TRANSPORTATION:
            paramsProxy.transportationId = poi.id;
            paramsProxy.transportationSystem = poi.transportation_system;
            break;
          default:
            break;
        }

        const name = poi.name ? poi.name[language] : searchTerm.replace(/-/g, ' ');

        paramsProxy.where = startCasePoiName(name);

        // Prefix transportationSystem before search keyword
        if (paramsProxy.transportationSystem) {
          paramsProxy.where = `${paramsProxy.transportationSystem} ${paramsProxy.where}`;
        }

        if (!isRadiusExist) {
          paramsProxy.radius = getDefaultRadius({ address: paramsProxy.where, type: poi.poi_type });
        }
      }
    }
  }

  if (paramsProxy.coords && !isRadiusExist) {
    paramsProxy.radius = getDefaultRadius({ address: paramsProxy.where });
  }

  if (invalidSlugs.length) {
    const path = invalidSlugs.join('/');
    const transportationSystemsPattern = Object.values(TransportationSystem).join('|');

    // possible buildings, transportation, areas, Google POIs, and free text
    const wherePatternRental = new RegExp(
      `^((${transportationSystemsPattern})/)?([\u0E00-\u0E7Fa-zA-Z0-9-\\s,]+)/${rentalsString}$`,
      'i'
    );
    const wherePatternSales = new RegExp(
      `^((${transportationSystemsPattern})/)?([\u0E00-\u0E7Fa-zA-Z0-9-\\s,]+)/${salesString}$`,
      'i'
    );

    if (
      !wherePatternRental.test(path) &&
      path !== rentalsString &&
      !wherePatternSales.test(path) &&
      path !== salesString
    ) {
      throw new Error('Invalid parameter values');
    }
  }

  return params;
}

export interface QueryResponse {
  queryValue: string | undefined;
  queryDatas: Record<string, any> | null;
}

/**
 * Get where query
 */
export const getWhereQuery: (locale: Language, tLocale: TFunction, params: SearchRequest) => Promise<QueryResponse> =
  async (locale, tLocale, params) => {
    const { where, areaId, buildingId, transportationId, transportationSystem } = params;
    let whereQuery = where;
    let poi = null;
    const generalTypeObject: GeneralTypeObject = {};
    if (whereQuery) {
      let poiType: PoiType | undefined;
      const poiId = areaId || buildingId || transportationId;
      switch (poiId) {
        case buildingId:
          poiType = PoiType.BUILDING;
          break;
        case transportationId:
          if (transportationSystem) {
            generalTypeObject.transportationType = lowerCase(transportationSystem);
            whereQuery = toTransportationName(whereQuery, transportationSystem);
          }

          poiType = PoiType.TRANSPORTATION;
          break;
        case areaId:
          poiType = PoiType.AREA;
          break;
        default:
          break;
      }
      poi = await parsePoiSlug({ slug: whereQuery, t: tLocale, poiType, generalTypeObject });

      if (poi) {
        whereQuery = poi.name[locale];
      }
    }

    return {
      queryValue: whereQuery,
      queryDatas: poi?.name || null
    };
  };
