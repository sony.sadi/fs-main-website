import { Language } from '@constants/enum';
import { Area, InDemandBuilding } from '@interfaces/search';
import { getInDemandBuildings, getPopularAreas } from '@services/http-client/search';
import { filterLanguage } from './filter-language';

export interface BuildingObject {
  popularAreas: Area[];
  indemandBuilding: InDemandBuilding[];
}

export interface LayoutParamObject {
  buildingData: BuildingObject;
}

/**
 * Get building data (Popular area, Indemand Building)
 * @param locale
 * @returns
 */
export async function getBuildingDatas(locale: Language): Promise<BuildingObject | undefined> {
  try {
    const [popularAreasResponse, inDemandBuildingsResponse] = await Promise.all([
      getPopularAreas(),
      getInDemandBuildings()
    ]);

    return {
      popularAreas: filterLanguage(popularAreasResponse.data.data, locale),
      indemandBuilding: inDemandBuildingsResponse.data.data
    };
  } catch (error) {
    console.info(error);
  }
}

/**
 * Get layout data
 * @param param0
 * @returns
 */
export function getLayoutData({ buildingData }: LayoutParamObject) {
  return {
    footerBuilding: {
      popularAreas: buildingData.popularAreas,
      indemandBuilding: buildingData.indemandBuilding
    }
  };
}
