import {
  PropertyType,
  Amenity,
  SortType,
  stringifyUrl,
  getDefaultParams,
  SearchRequest,
  SerpType
} from '@flexstay.rentals/fs-url-stringify';
import { TFunction } from 'next-i18next';

export function getFooterAssets({ tFooter, tCommon }: { tFooter: TFunction; tCommon: TFunction }) {
  const baseRentalsParams: SearchRequest & { index_serp_types: SerpType[] } = {
    ...getDefaultParams(),
    index_serp_types: ['rents']
  };
  const baseSalesParams: SearchRequest & { index_serp_types: SerpType[] } = {
    ...getDefaultParams('sales'),
    index_serp_types: ['rents']
  };

  return {
    intros: [tFooter('footerIntroOurMission'), tFooter('footerIntroFounded')],
    about: {
      title: tFooter('footerAboutTitle'),
      items: [
        {
          label: tFooter('footerAboutLabelAboutUs'),
          path: tCommon('aboutUsPath')
        },
        {
          label: tFooter('footerAboutLabelPropertyScout'),
          path: tCommon('howDoesItWorkPath')
        },
        {
          label: tFooter('footerAboutLabelList'),
          path: tCommon('listYourPropertyPath')
        },
        {
          label: tFooter('footerAboutLabelContactUs'),
          path: tCommon('contactUsPath')
        }
      ]
    },
    property: {
      title: tFooter('footerPropertyTitle'),
      items: [
        {
          label: tFooter('footerPropertyCondos'),
          ...stringifyUrl({ request: { ...baseRentalsParams, propertyType: [PropertyType.Condo] }, t: tCommon })
        },
        {
          label: tFooter('footerPropertyApartments'),
          ...stringifyUrl({ request: { ...baseRentalsParams, propertyType: [PropertyType.Apartment] }, t: tCommon })
        },
        {
          label: tFooter('footerPropertyServicedApartments'),
          ...stringifyUrl({
            request: { ...baseRentalsParams, propertyType: [PropertyType.ServicedApartment] },
            t: tCommon
          })
        },
        {
          label: tFooter('footerPropertyHouses'),
          ...stringifyUrl({ request: { ...baseRentalsParams, propertyType: [PropertyType.House] }, t: tCommon })
        },
        {
          label: tFooter('footerPropertyTownhouses'),
          ...stringifyUrl({ request: { ...baseRentalsParams, propertyType: [PropertyType.Townhouse] }, t: tCommon })
        }
      ]
    },
    propertySale: {
      title: tFooter('footerSalePropertyTitle'),
      items: [
        {
          label: tFooter('footerSalePropertyCondos'),
          ...stringifyUrl({ request: { ...baseSalesParams, propertyType: [PropertyType.Condo] }, t: tCommon })
        },
        {
          label: tFooter('footerSalePropertyApartments'),
          ...stringifyUrl({ request: { ...baseSalesParams, propertyType: [PropertyType.Apartment] }, t: tCommon })
        },
        {
          label: tFooter('footerSalePropertyServicedApartments'),
          ...stringifyUrl({
            request: { ...baseSalesParams, propertyType: [PropertyType.ServicedApartment] },
            t: tCommon
          })
        },
        {
          label: tFooter('footerSalePropertyHouses'),
          ...stringifyUrl({ request: { ...baseSalesParams, propertyType: [PropertyType.House] }, t: tCommon })
        },
        {
          label: tFooter('footerSalePropertyTownhouses'),
          ...stringifyUrl({ request: { ...baseSalesParams, propertyType: [PropertyType.Townhouse] }, t: tCommon })
        }
      ]
    },
    contact: {
      offices: [
        {
          name: tFooter('footerContactCompanyName'),
          building: tFooter('footerContactCompanyBuilding'),
          address: tFooter('footerContactCompanyAddress')
        }
      ]
    },
    basics: [
      {
        icon: 'FACEBOOK',
        content: 'fb.com/propertyscout.co.th',
        href: 'https://facebook.com/propertyscout.co.th'
      },
      {
        icon: 'LINE',
        content: '@propertyscout',
        href: 'https://line.me/R/ti/p/@propertyscout'
      },
      {
        icon: 'PHONE',
        content: '+66 92 264 3444',
        href: 'tel:+66 92 264 3444'
      },
      {
        icon: 'PHONE_OUTLINE',
        content: '+66 92 264 3444',
        href: 'https://wa.me/+66922643444'
      },
      {
        icon: 'MAIL_BOX',
        content: 'contact@propertyscout.co.th',
        href: 'mailto:contact@propertyscout.co.th'
      }
    ],
    allRightsReserved: {
      year: tFooter('footerYear'),
      terms: [
        {
          label: tFooter('footerPrivacy'),
          path: tCommon('privacyPath')
        }
      ],
      socials: [
        {
          icon: 'INSTAGRAM',
          href: 'https://instagram.com/propertyscout.co.th'
        },
        {
          icon: 'TWITTER',
          href: 'https://twitter.com/propertyscoutth'
        },
        {
          icon: 'LINKEDIN',
          href: 'https://www.linkedin.com/company/propertyscout/'
        }
      ]
    }
  };
}

export function getPopularSearches({ tCommon, tHome }: { tCommon: TFunction; tHome: TFunction }) {
  const baseRentalsParams: SearchRequest & { index_serp_types: SerpType[] } = {
    ...getDefaultParams(),
    index_serp_types: ['rents']
  };

  return [
    {
      icon: 'pet-friendly-condos',
      link: {
        rent: {
          ...stringifyUrl({
            request: { ...baseRentalsParams, propertyType: [PropertyType.Condo], amenities: [Amenity.PetFriendly] },
            t: tCommon
          }),
          name: tHome('searchesPetFriendlyCondos')
        },
        sale: {
          ...stringifyUrl({
            request: {
              ...baseRentalsParams,
              propertyType: [PropertyType.Condo],
              amenities: [Amenity.PetFriendly],
              serp_type: 'sales'
            },
            t: tCommon
          }),
          name: tHome('searchesPetFriendlyCondosSale')
        }
      }
    },
    {
      icon: 'modern-gym',
      link: {
        rent: {
          ...stringifyUrl({ request: { ...baseRentalsParams, amenities: [Amenity.Gym] }, t: tCommon }),
          name: tHome('searchesModernGym')
        },
        sale: {
          ...stringifyUrl({
            request: { ...baseRentalsParams, amenities: [Amenity.Gym], serp_type: 'sales' },
            t: tCommon
          }),
          name: tHome('searchesModernGymSale')
        }
      }
    },
    {
      icon: 'with-balcony',
      link: {
        rent: {
          ...stringifyUrl({
            request: { ...baseRentalsParams, propertyType: [PropertyType.Condo], amenities: [Amenity.Balcony] },
            t: tCommon
          }),
          name: tHome('searchesWithBalcony')
        },
        sale: {
          ...stringifyUrl({
            request: {
              ...baseRentalsParams,
              propertyType: [PropertyType.Condo],
              amenities: [Amenity.Balcony],
              serp_type: 'sales'
            },
            t: tCommon
          }),
          name: tHome('searchesWithBalconySale')
        }
      }
    },
    {
      icon: 'great-value-condos',
      link: {
        rent: {
          ...stringifyUrl({
            request: { ...baseRentalsParams, propertyType: [PropertyType.Condo], sortBy: SortType.PriceLow },
            t: tCommon
          }),
          name: tHome('searchesGreatValueCondos')
        },
        sale: {
          ...stringifyUrl({
            request: {
              ...baseRentalsParams,
              propertyType: [PropertyType.Condo],
              sortBy: SortType.PriceLow,
              serp_type: 'sales'
            },
            t: tCommon
          }),
          name: tHome('searchesGreatValueCondosSale')
        }
      }
    }
  ];
}

export function getMobileMenuLinks({ tCommon }: { tCommon: TFunction }) {
  const baseRentalsParams: SearchRequest & { index_serp_types: SerpType[] } = {
    ...getDefaultParams(),
    index_serp_types: ['rents']
  };

  const rentLink = stringifyUrl({ request: baseRentalsParams, t: tCommon });
  const saleLink = stringifyUrl({ request: { ...getDefaultParams('sales'), index_serp_types: ['rents'] }, t: tCommon });
  const condoLink = stringifyUrl({ request: { ...baseRentalsParams, propertyType: [PropertyType.Condo] }, t: tCommon });
  const apartmentLink = stringifyUrl({
    request: { ...baseRentalsParams, propertyType: [PropertyType.Apartment] },
    t: tCommon
  });
  const servicedApartmentLink = stringifyUrl({
    request: { ...baseRentalsParams, propertyType: [PropertyType.ServicedApartment] },
    t: tCommon
  });
  const houseLink = stringifyUrl({ request: { ...baseRentalsParams, propertyType: [PropertyType.House] }, t: tCommon });
  const townhouseLink = stringifyUrl({
    request: { ...baseRentalsParams, propertyType: [PropertyType.Townhouse] },
    t: tCommon
  });

  return [
    {
      href: rentLink.path,
      rel: rentLink.rel,
      i18nKey: tCommon('rent'),
      children: [
        {
          i18nKey: tCommon('condo'),
          href: condoLink.path,
          rel: condoLink.rel
        },
        {
          i18nKey: tCommon('apartment'),
          href: apartmentLink.path,
          rel: apartmentLink.rel
        },
        {
          i18nKey: tCommon('servicedApartment'),
          href: servicedApartmentLink.path,
          rel: servicedApartmentLink.rel
        },
        {
          i18nKey: tCommon('house'),
          href: houseLink.path,
          rel: houseLink.rel
        },
        {
          i18nKey: tCommon('townhouse'),
          href: townhouseLink.path,
          rel: townhouseLink.rel
        }
      ]
    },
    {
      href: saleLink.path,
      i18nKey: tCommon('buyFilter'),
      rel: 'nofollow' // FS-1463 remove this when release homesale
    },
    { href: tCommon('listYourPropertyPath'), i18nKey: tCommon('saleHeader') },
    {
      href: tCommon('howDoesItWorkPath'),
      i18nKey: tCommon('howDoesItWork')
    }
  ];
}
