import { Listing } from '@interfaces/listing';
import { createSlice, current, PayloadAction } from '@reduxjs/toolkit';
import storage from 'redux-persist/lib/storage';
import { PersistConfig, persistReducer } from 'redux-persist';
import { AxiosResponse } from 'axios';
import { v4 as uuidv4 } from 'uuid';
import { SortType, getDefaultParams, SerpType } from '@flexstay.rentals/fs-url-stringify';
import get from 'lodash/get';
import head from 'lodash/head';
import orderBy from 'lodash/orderBy';

import {
  FavouriteRental,
  FetchRentalRequest,
  FetchRentalResponse,
  PropertyPad,
  PropertyPadResponse,
  SearchRequest,
  SearchResponse
} from '@interfaces/search';
import { FSAppState, FSAppThunk } from '@stores';
import { Coordinate, RentalsCoordinateResponse } from '@interfaces/map';
import {
  getPropertyPad,
  getPropertyPadRentals,
  sendPropertyPadToServer,
  syncPropertyPadWithBackend
} from '@services/http-client/search';
import { mapActions } from '../map';

export interface SearchState {
  searchTerm: string;
  filterOption: SerpType;
  params: SearchRequest;
  rentals: SearchResponse;
  activeRental?: Listing;
  address?: string;
  propertypad: PropertyPad;
}

const initialState: SearchState = {
  searchTerm: '',
  filterOption: 'rents',
  params: getDefaultParams(),
  rentals: {
    page: 1,
    perPage: 20,
    total: 0,
    data: []
  },
  propertypad: {
    id: 0,
    favouriteRentals: [],
    name: 'Website User',
    identifier: uuidv4(),
    hubspotdealurl: '',
    user_id: `WEBSITE_USER_${new Date().getTime()}`,
    isServerIdentifier: false
  }
};

const searchSlice = createSlice({
  name: 'search',
  initialState,
  reducers: {
    setSearchTerm(state: SearchState, action: PayloadAction<string>) {
      state.searchTerm = action.payload;
    },
    setFilterOption(state: SearchState, action: PayloadAction<SerpType>) {
      state.filterOption = action.payload;
    },
    setSearchAddress(state: SearchState, { payload }: PayloadAction<string>) {
      state.address = payload;
    },
    setRentalsParam<K extends keyof SearchRequest>(
      state: SearchState,
      action: PayloadAction<{ key: K; value: SearchRequest[K] }>
    ) {
      state.params[action.payload.key] = action.payload.value;
    },

    setRentalsParams(state: SearchState, action: PayloadAction<SearchRequest>) {
      state.params = {
        ...action.payload,
        minPrice: Number(action.payload.minPrice),
        maxPrice: Number(action.payload.maxPrice),
        floorSize_gte: Number(action.payload.floorSize_gte),
        floorSize_lte: Number(action.payload.floorSize_lte),
        radius: Number(action.payload.radius),
        page: Number(action.payload.page),
        limit: Number(action.payload.limit),
        amenities: action.payload.amenities || []
      };
    },

    resetRentalsParams(state) {
      state.params = getDefaultParams();
    },

    setRentals(state, action: PayloadAction<SearchResponse>) {
      state.rentals = action.payload;
    },

    setActiveRental(state, action: PayloadAction<Listing | undefined>) {
      state.activeRental = action.payload;
    },

    setPropertyPad(state, { payload }: PayloadAction<PropertyPadResponse>) {
      const { propertypad } = current(state);
      state.propertypad = { ...payload, favouriteRentals: payload.rental_ids || [] };
      /*
      if (payload.identifier === browserUserId) {
        const stateRentalIds = propertypad.favouriteRentals;
        const propertyPadRentalIds = payload.rental_ids;
        const rentalsMerged = [...stateRentalIds, ...propertyPadRentalIds];
        const rentalsUnique = rentalsMerged.filter((item, index, self) => {
          return self.findIndex((r) => r.rental_id === item.rental_id) === index;
        });
        state.propertypad = { ...payload, favouriteRentals: rentalsUnique || [] };
      } else {
        // const stateRentalIds = propertypad.favouriteRentals;
        const propertyPadRentalIds = payload.rental_ids;
        // const rentalsMerged = [...stateRentalIds, ...propertyPadRentalIds];
        // const rentalsUnique = rentalsMerged.filter((item, index, self) => {
        //   return self.findIndex((r) => r.rental_id === item.rental_id) === index;
        // });
      }
      */
    },

    setPropertyPadRental(state, { payload }: PayloadAction<FetchRentalResponse>) {
      const { data, page } = payload;
      const perPage = 20;
      const minIndex = (page - 1) * perPage;
      const maxIndex = minIndex + perPage - 1;
      const dataPerPage = data.filter((i, idx) => idx >= minIndex && idx <= maxIndex);
      const rentals: SearchResponse = { data: dataPerPage, page, total: data.length, perPage };

      state.rentals = rentals;
      state.params = { ...state.params, page };
    },
    /*
    storePropertypad(state, { payload }: PayloadAction<FavouriteRental>) {
      const favouriteRentals = state.propertypad.favouriteRentals || [];

      if (!state.propertypad.id) {
        state.propertypad.identifier = state.browserUserId;
      }

      let action = 'add';
      if (!favouriteRentals.length) {
        state.propertypad.favouriteRentals = [payload];
      } else {
        const isExist = favouriteRentals.find((x) => x?.rental_id === payload?.rental_id);
        if (isExist) {
          action = 'remove';
        }
        const maxFavoriteStoreAble = 100;
        const isAbleStore = favouriteRentals.length < maxFavoriteStoreAble;
        let newFavoriteRentals = [];
        if (!isExist && isAbleStore) {
          newFavoriteRentals = [...favouriteRentals, payload];
        } else {
          newFavoriteRentals = favouriteRentals.filter((x) => x?.rental_id !== payload?.rental_id);
        }
        // filter duplicate rental_ids
        newFavoriteRentals = newFavoriteRentals.filter((item, idx, arr) => arr.indexOf(item) === idx);
        state.propertypad.favouriteRentals = newFavoriteRentals;
      }
      if (!state.propertypad.id) {
        action = 'create';
      }
    },
    */
    updatePadRentals(state, { payload }: PayloadAction<FavouriteRental[]>) {
      state.propertypad.favouriteRentals = payload;
    }
  }
});

export const persistConfig: PersistConfig<SearchState> = {
  key: 'propertypad',
  version: 1,
  storage,
  whitelist: ['propertypad']
};

export const fetchPropertyPad =
  (identifier: string): FSAppThunk =>
  async (dispatch) => {
    try {
      if (!identifier) return;
      const res: AxiosResponse<PropertyPadResponse[]> = await getPropertyPad(identifier);
      if (!res.data.length) return;
      const properypad = { ...res.data[0], isServerIdentifier: !!res.data.length };
      dispatch(searchActions.setPropertyPad(properypad));
    } catch (error) {
      console.error(error);
    }
  };

export const fetchRentals =
  ({ rentals, page, totalAddress, sort }: FetchRentalRequest): FSAppThunk =>
  async (dispatch) => {
    try {
      const ids = rentals.map((rent) => rent.rental_id);
      const res: AxiosResponse<Listing[]> = await getPropertyPadRentals(ids);
      let propertyPadRentals = res.data.map((rental) => {
        let propertyPrice = 0;
        switch (rental.tenure) {
          case 'sell':
            propertyPrice = rental.salePrice || 0;
            break;
          case 'rent':
            propertyPrice = rental.lowestPrice;
            break;
          case 'rentsell':
            propertyPrice = rental.salePrice || 0;
            break;

          default:
            break;
        }
        return {
          ...rental,
          disabled: rental.status !== 'ACTIVE',
          propertyPrice,
          featuredImageUrl: get(head(rental.cdnImages), 'url', '')
        };
      });

      switch (sort) {
        case SortType.Newest:
          propertyPadRentals = orderBy(propertyPadRentals, (rent) => rent.id, 'desc');
          break;
        case SortType.PriceLow:
          propertyPadRentals = orderBy(propertyPadRentals, (rent) => rent.propertyPrice, 'asc');
          break;
        case SortType.PriceHigh:
          propertyPadRentals = orderBy(propertyPadRentals, (rent) => rent.propertyPrice, 'desc');
          break;
        default:
          // eslint-disable-next-line no-case-declarations
          const addDateMap = new Map(rentals.map((rent) => [rent.rental_id, new Date(rent.added_at)]));
          propertyPadRentals = orderBy(propertyPadRentals, (rent) => addDateMap.get(rent.id), 'desc');
          break;
      }

      dispatch(searchActions.setPropertyPadRental({ data: propertyPadRentals, page }));

      const coordinates = res.data.map(
        (item) => ({ id: item.id, gpsLat: item.gpsLat, gpsLong: item.gpsLong } as Coordinate)
      );
      const perPage = 20;
      const minIndex = (page - 1) * perPage;
      const maxIndex = minIndex + perPage - 1;
      const coorsShow = coordinates.filter((i, idx) => idx >= minIndex && idx <= maxIndex);
      const coorResponse: RentalsCoordinateResponse = { data: coorsShow, page, perPage, total: totalAddress };
      dispatch(mapActions.setCoords(coorResponse));
    } catch (error) {
      console.error(error);
    }
  };

export const resetPropertyPad = (): FSAppThunk => (dispatch) => {
  dispatch(searchActions.setPropertyPadRental({ data: [], page: 1 }));
  dispatch(mapActions.setCoords({ data: [], page: 1, perPage: 20, total: 0 }));
};

/*
export const sendRentalToServer = (): FSAppThunk => async (dispatch, getState) => {
  const currentState = getState();
  const propertyPad = selectPropertyPad(currentState);
  const { id } = propertyPad;
  const updateRst = await sendPropertyPadToServer(propertyPad);
  if (!id) {
    const updatedPad = updateRst.data;
    dispatch(searchActions.setPropertyPad(updatedPad));
  }
};
*/

export const storePadRental =
  (rental: FavouriteRental): FSAppThunk =>
  async (dispatch, getState) => {
    const currentState = getState();
    const propertyPad = { ...selectPropertyPad(currentState) };
    let favouriteRentals = propertyPad.favouriteRentals || [];

    let action = 'add';
    if (!favouriteRentals.length) {
      favouriteRentals = [rental];
    } else {
      const isExist = favouriteRentals.find((x) => x?.rental_id === rental?.rental_id);
      if (isExist) {
        action = 'remove';
      }
      const maxFavoriteStoreAble = 100;
      const isAbleStore = favouriteRentals.length < maxFavoriteStoreAble;
      let newFavoriteRentals = [];
      if (!isExist && isAbleStore) {
        newFavoriteRentals = [...favouriteRentals, rental];
      } else {
        newFavoriteRentals = favouriteRentals.filter((x) => x?.rental_id !== rental?.rental_id);
      }
      // filter duplicate rental_ids
      newFavoriteRentals = newFavoriteRentals.filter((item, idx, arr) => arr.indexOf(item) === idx);
      favouriteRentals = newFavoriteRentals;
    }
    dispatch(searchActions.updatePadRentals(favouriteRentals));
    if (!propertyPad.id) {
      propertyPad.favouriteRentals = favouriteRentals;
      const newPad = await sendPropertyPadToServer(propertyPad);
      dispatch(searchActions.setPropertyPad(newPad.data));
    } else {
      const syncedPad = await syncPropertyPadWithBackend(propertyPad.id, action, rental.rental_id);
      dispatch(searchActions.setPropertyPad(syncedPad.data));
    }
  };

export const searchReducer = persistReducer(persistConfig, searchSlice.reducer);

export const searchActions = searchSlice.actions;

export const { caseReducers } = searchSlice;

// Selectors
export const selectSearchAddress = (state: FSAppState) => state.search.address;
export const selectSearchTerm = (state: FSAppState) => state.search.searchTerm;
export const selectFilterOption = (state: FSAppState) => state.search.filterOption;
export const selectParams = (state: FSAppState) => state.search.params;
export const selectRentals = (state: FSAppState) => state.search.rentals;
export const selectRentalCoordinates = (state: FSAppState): Coordinate[] =>
  state.search.rentals.data.map((rental) => ({
    id: rental.id,
    gpsLat: rental.gpsLat,
    gpsLong: rental.gpsLong
  }));
export const selectTotalPages = (state: FSAppState) =>
  Math.ceil(state.search.rentals.total / state.search.params.limit);
export const selectActiveRental = (state: FSAppState) => state.search.activeRental;
export const selectFavoriteRentalsIds = (state: FSAppState) =>
  state.search.propertypad.favouriteRentals?.map((x) => x?.rental_id) || [];
export const selectFavoriteRentals = (state: FSAppState) => state.search.propertypad.favouriteRentals ?? [];
export const selectPropertyPad = (state: FSAppState) => state.search.propertypad;
