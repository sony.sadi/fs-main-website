import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { FSAppState } from '@stores';

const isBrowser = typeof window !== 'undefined';
interface CommonState {
  loading: boolean;
  isMobile: boolean;
  isMobileOnly: boolean;
  isIpad: boolean;
}

const initialState: CommonState = {
  loading: false,
  isMobile: true,
  isMobileOnly: true,
  isIpad: isBrowser && window.innerWidth === 768
};

const commonSlice = createSlice({
  name: 'common',
  initialState,
  reducers: {
    setLoading(state, action: PayloadAction<boolean>) {
      state.loading = action.payload;
    },

    setViewPort(state, action: PayloadAction<number>) {
      const windowWidth = action.payload;

      state.isMobile = windowWidth <= 768;
      state.isMobileOnly = windowWidth <= 480;
    }
  }
});

export const commonReducer = commonSlice.reducer;

export const commonActions = commonSlice.actions;

export const { caseReducers } = commonSlice;

// Selectors
export const selectLoading = (state: FSAppState) => state.common.loading;
export const selectIsMobile = (state: FSAppState) => state.common.isMobile;
export const selectIsMobileOnly = (state: FSAppState) => state.common.isMobileOnly;
export const selectIsIpad = (state: FSAppState) => state.common.isIpad;
