import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AxiosResponse } from 'axios';
import omit from 'lodash/omit';
import get from 'lodash/get';
import head from 'lodash/head';

import { getPropertyById, getPublicRentalsCoordinates } from '@services/http-client/map';
import { FSAppState, FSAppThunk } from '@stores';
import { selectParams, selectRentalCoordinates } from '@stores/slices/search';
import { Listing } from '@interfaces/listing';
import { Coordinate, CoordinateParams, RentalsCoordinateParams, RentalsCoordinateResponse } from '@interfaces/map';
import { SearchRequest } from '@interfaces/search';
import { MAX_FILTERING_FLOOR_SIZE, MAX_FILTERING_PRICE, WATTHANA_COORDINATES } from '@constants';

export interface MapState {
  coords: RentalsCoordinateResponse;
  center: google.maps.LatLngLiteral;
  activeProperty?: Listing;
}

const initialState: MapState = {
  coords: {
    page: 0,
    perPage: 0,
    total: 0,
    data: []
  },
  center: {
    lat: WATTHANA_COORDINATES.LAT,
    lng: WATTHANA_COORDINATES.LNG
  }
};

const mapSlice = createSlice({
  name: 'map',
  initialState,
  reducers: {
    setCoords(state, action: PayloadAction<RentalsCoordinateResponse>) {
      state.coords = action.payload;
    },

    setCenter(state, action: PayloadAction<google.maps.LatLngLiteral>) {
      const { lat, lng } = action.payload;
      state.center = { lat: lat || WATTHANA_COORDINATES.LAT, lng: lng || WATTHANA_COORDINATES.LNG };
    },

    setActiveProperty(state, action: PayloadAction<Listing | undefined>) {
      state.activeProperty = action.payload;
    }
  }
});

export const { caseReducers, reducer: mapReducer, actions: mapActions } = mapSlice;

// Selectors
export const selectCoords = (state: FSAppState) => state.map.coords;
export const selectCenter = (state: FSAppState) => state.map.center;
export const selectActiveProperty = (state: FSAppState) => state.map.activeProperty;

// Side effects
export const fetchRentalsCoords =
  (coordinateParams: CoordinateParams): FSAppThunk =>
  async (dispatch, getState) => {
    try {
      const searchParams: SearchRequest = selectParams(getState());

      const params: RentalsCoordinateParams = { ...searchParams, ...coordinateParams, page: 1 };
      if (!params.maxPrice) {
        params.maxPrice = MAX_FILTERING_PRICE;
      }

      if (!params.floorSize_lte) {
        params.floorSize_lte = MAX_FILTERING_FLOOR_SIZE;
      }

      const res: AxiosResponse<RentalsCoordinateResponse> = await getPublicRentalsCoordinates(omit(params, 'where'));

      const rentalCoordinates: Coordinate[] = selectRentalCoordinates(getState());
      const map = new Map();
      res.data.data.forEach((rc) => {
        map.set(rc.id, rc);
      });
      rentalCoordinates.forEach((item) => {
        if (!map.has(item.id)) {
          res.data.data.push(item);
        }
      });
      dispatch(mapActions.setCoords(res.data));
    } catch (error) {
      console.error(error);
    }
  };

export const fetchPropertyById =
  (id: number, property?: Listing): FSAppThunk =>
  async (dispatch, getState) => {
    try {
      const res = await getPropertyById(id);
      const data = property || res.data;
      const activeProperty: Listing = {
        ...data,
        featuredImageUrl: get(head(data.cdnImages), 'url', '')
      };

      dispatch(mapActions.setActiveProperty(activeProperty));
    } catch (error) {
      console.error(error);
    }
  };
