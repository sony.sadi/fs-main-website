import { combineReducers } from '@reduxjs/toolkit';
import { commonReducer } from '@stores/slices/common';
import { searchReducer } from '@stores/slices/search';
import { mapReducer } from '@stores/slices/map';

const rootReducer = combineReducers({
  common: commonReducer,
  search: searchReducer,
  map: mapReducer
});

export default rootReducer;
