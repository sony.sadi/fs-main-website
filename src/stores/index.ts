import { useDispatch } from 'react-redux';
import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import { createStore } from 'redux';
import { persistStore, FLUSH, REHYDRATE, REGISTER, PURGE, PERSIST, PAUSE } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import rootReducer from './reducers/index';

const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => {
    const middleware = getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]
      }
    });

    return middleware;
  },
  devTools: process.env.NEXT_PUBLIC_NODE_ENV === 'development' || process.env.NEXT_PUBLIC_NODE_ENV === 'local'
});

export type FSAppState = ReturnType<typeof rootReducer>;
export type FSAppDispatch = typeof store.dispatch;
export type FSAppThunk<ReturnType = void> = ThunkAction<ReturnType, FSAppState, unknown, Action<string>>;

export const useAppDispatch = () => useDispatch<FSAppDispatch>();

const persistor = persistStore(store);

export { store, persistor };
