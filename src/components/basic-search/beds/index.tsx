import { useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import cntl from 'cntl';

import Svg from '@components/core/svg';
import { getBedroomOptions } from '@utils/search';
import { NumberOfBed, Params } from '@flexstay.rentals/fs-url-stringify';
import { searchActions, selectParams } from '@stores/slices/search';
import { useTranslation } from 'next-i18next';
import { BasicSearchTheme, Language } from '@constants/enum';

const themeText = {
  [BasicSearchTheme.Light]: 'text-white',
  [BasicSearchTheme.Dark]: 'text-primary-gray'
};

function HeroBeds({ theme }: { theme: BasicSearchTheme }) {
  const { i18n, t } = useTranslation();
  const dispatch = useDispatch();
  const containerRef = useRef<HTMLDivElement>(null);
  const searchParams = useSelector(selectParams);
  const options = useMemo(() => getBedroomOptions(t), [t]);

  const [visible, setVisible] = useState(false);
  const [selected, setSelected] = useState(searchParams.numberBedrooms);

  useEffect(() => {
    document.addEventListener('click', handleClickOutside);
    return () => document.removeEventListener('click', handleClickOutside);
  }, []);

  const handleClickOutside = (e: MouseEvent) => {
    if (containerRef?.current) {
      if (!containerRef.current.contains(e.target as Node)) {
        setVisible(false);
      }
    }
  };

  const handleClick = (bed: NumberOfBed) => {
    return () => {
      setSelected(bed);
    };
  };

  const handleApplyFilter = () => {
    dispatch(
      searchActions.setRentalsParam({
        key: Params.NumberBedrooms,
        value: selected
      })
    );
    setVisible(false);
  };

  const handleToggle = () => {
    setVisible(!visible);
  };

  const renderLabel = () => {
    if (searchParams.numberBedrooms === NumberOfBed.AnyType) return t('beds');

    const option = options.find(({ value }) => value === searchParams.numberBedrooms);
    return `${option?.label} ${t('beds')}`;
  };

  const renderPopup = () => {
    return (
      <div className={classes.popup}>
        <ul className={classes.options({ isFlex: i18n.language === Language.TH })}>
          {options.map((option) => (
            <li
              aria-hidden
              key={option.value}
              className={classes.option({
                active: option.value === selected,
                isLongText: i18n.language === Language.TH
              })}
              data-label={option.label}
              onClick={handleClick(option.value)}>
              {option.label}
            </li>
          ))}
        </ul>
        <button className={classes.applyButton} onClick={handleApplyFilter}>
          {t('applyFilter')}
        </button>
      </div>
    );
  };

  return (
    <div ref={containerRef} className="relative select-none">
      <span className={classes.filterButton(theme)} aria-hidden onClick={handleToggle}>
        {renderLabel()}
        <Svg className={classes.downArrow(theme)} name="down-arrow" />
      </span>
      {visible && renderPopup()}
    </div>
  );
}

const classes = {
  popup: cntl`
    absolute top-8
    w-72 px-3 py-4
    text-center shadow
    bg-white rounded-2xl
    transform -translate-x-1/2
  `,
  options: ({ isFlex }: { isFlex: boolean }) => cntl`
    ${isFlex ? 'flex flex-wrap' : 'grid grid-cols-3 gap-2'}
  `,
  option: ({ active, isLongText }: { active: boolean; isLongText: boolean }) => cntl`
    ${
      isLongText ? 'mr-1 mb-1 last:mr-0 last:mb-0' : 'w-20'
    } px-2 py-1 text-center text-base text-black border rounded-lg
    cursor-pointer
    hover:bg-primary-pink
    ${active ? 'bg-primary-pink border-primary-orange' : ''}
    hero-beds__option
  `,
  filterButton: (theme: BasicSearchTheme) => cntl`
    flex items-center
    text-base
    cursor-pointer mr-7
    fs-home-search__unit-type
    ${themeText[theme]}
  `,
  applyButton: cntl`
    mt-5 px-5 py-2 text-xs bg-primary-orange text-white rounded-md hero-beds__apply-filter
  `,
  downArrow: (theme: BasicSearchTheme) => cntl`
    w-2 h-3 ml-1
    fill-current
    ${themeText[theme]}
`
};

export default HeroBeds;
