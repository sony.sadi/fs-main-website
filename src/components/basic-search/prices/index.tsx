import { useState, useEffect, useRef, useMemo, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';
import dynamic from 'next/dynamic';
import cntl from 'cntl';
import throttle from 'lodash/throttle';

import Svg from '@components/core/svg';
import { formatPrice } from '@utils';
import { MAX_FILTERING_PRICE, MAX_FILTERING_SALE_PRICE } from '@constants';
import { BasicSearchTheme, Page } from '@constants/enum';
import { searchActions, selectFilterOption, selectParams } from '@stores/slices/search';

const RangeInputPrice = dynamic(() => import('@components/forms/range-input/price'));

interface IProps {
  theme: BasicSearchTheme;
  page: Page;
}

const themeText = {
  [BasicSearchTheme.Light]: 'text-white',
  [BasicSearchTheme.Dark]: 'text-primary-gray'
};

function HeroPrices({ theme, page }: IProps) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const containerRef = useRef<HTMLDivElement>(null);
  const searchParams = useSelector(selectParams);

  const [visible, setVisible] = useState(false);
  const filterOptionValue = useSelector(selectFilterOption);
  const [minPrice, setMinPrice] = useState<string | number>(0);
  const [maxPrice, setMaxPrice] = useState<string | number>(MAX_FILTERING_PRICE);
  const [railMaxPrice, setRailMaxPrice] = useState<number>(MAX_FILTERING_PRICE);

  const minValue = useMemo(() => Number(String(minPrice).replace(/[^\d.]/g, '')), [minPrice]);
  const maxValue = useMemo(() => Number(String(maxPrice).replace(/[^\d.]/g, '')), [maxPrice]);

  useEffect(() => {
    document.addEventListener('click', handleClickOutside);
    return () => document.removeEventListener('click', handleClickOutside);
  }, []);

  /**
   * Set price filter each time switch filter option
   */
  useEffect(() => {
    // Max price for sale
    if (filterOptionValue === 'sales') {
      setRailMaxPrice(MAX_FILTERING_SALE_PRICE);
      setMinPrice(0);
      setMaxPrice(MAX_FILTERING_SALE_PRICE);
      return;
    }
    // Max price for rent
    setMinPrice(0);
    setMaxPrice(MAX_FILTERING_PRICE);
    setRailMaxPrice(MAX_FILTERING_PRICE);
  }, [filterOptionValue]);

  const handleClickOutside = (e: MouseEvent) => {
    if (containerRef?.current) {
      if (!containerRef.current.contains(e.target as Node)) {
        setVisible(false);
      }
    }
  };

  const handleToggle = () => {
    setVisible(!visible);
  };

  const handleApplyFilter = () => {
    dispatch(
      searchActions.setRentalsParams({
        ...searchParams,
        minPrice: minValue,
        maxPrice: maxValue
      })
    );

    setVisible(false);
  };

  const handlePriceChange = useCallback(
    throttle((min: number, max: number) => {
      setMinPrice(formatPrice(Math.round(min)));
      setMaxPrice(formatPrice(Math.round(max)));
    }, 500),
    []
  );

  const renderLabel = () => {
    const { minPrice, maxPrice } = searchParams;

    if (!minPrice && !maxPrice) return t('anyPrice');

    const max = `฿${formatPrice(Number(maxPrice))}`;
    const min = `฿${formatPrice(Number(minPrice))}`;

    if (!maxPrice) {
      return min;
    }

    return `${min}-${max}`;
  };

  const renderPopup = () => {
    return (
      <div className={classes.popup}>
        <RangeInputPrice
          railMax={railMaxPrice}
          page={page}
          min={minValue}
          max={maxValue}
          onChange={handlePriceChange}
        />

        <div className="mt-5 text-center">
          <button className={classes.popupButton} onClick={handleApplyFilter}>
            {t('applyFilter')}
          </button>
        </div>
      </div>
    );
  };

  return (
    <div ref={containerRef} className="relative">
      <span className={classes.selectItem(theme)} aria-hidden onClick={handleToggle}>
        {renderLabel()}
        <Svg className={classes.downArrow(theme)} name="down-arrow" />
      </span>
      {visible && renderPopup()}
    </div>
  );
}

const classes = {
  popup: cntl`
    absolute top-8 left-1/2
    w-72 px-3 py-4 shadow
    bg-white rounded-2xl
    transform -translate-x-1/2
  `,
  options: cntl`
    grid grid-cols-3 gap-2
  `,
  option: ({ active }: { active: boolean }) => cntl`
    w-20 px-2 py-1 text-center text-base text-black border rounded-lg
    cursor-pointer
    hover:bg-primary-pink
    ${active ? 'bg-primary-pink border-primary-orange' : ''}
  `,
  popupButton: cntl`
    px-5 py-2 text-xs
    bg-primary-orange text-white rounded-md focus:outline-none
    hero-prices__apply-filter
  `,
  selectItem: (theme: BasicSearchTheme) => cntl`
  flex items-center
  mr-7 text-base cursor-pointer
  fs-home-search__any-price
  ${themeText[theme]}
  `,
  downArrow: (theme: BasicSearchTheme) => cntl`
  w-2 h-3 ml-1
  fill-current
  ${themeText[theme]}
`
};

export default HeroPrices;
