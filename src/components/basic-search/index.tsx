import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import cntl from 'cntl';
import { useTranslation } from 'next-i18next';
import omit from 'lodash/omit';

import HeroBeds from '@components/basic-search/beds';
import HeroPrices from '@components/basic-search/prices';
import { searchActions, selectFilterOption, selectParams } from '@stores/slices/search';

import {
  stringifyUrl,
  Params,
  PoiType,
  getDefaultRadius,
  Language,
  SearchRequest
} from '@flexstay.rentals/fs-url-stringify';
import PoisAutocomplete from '@components/common/pois-autocomplete';
import { PoiSuggestion } from '@interfaces/search';
import { BasicSearchTheme, Page } from '@constants/enum';

interface IProps {
  theme?: BasicSearchTheme;
  page: Page;
}

function BasicSearch({ theme = BasicSearchTheme.Light, page }: IProps) {
  const router = useRouter();
  const dispatch = useDispatch();
  const { t, i18n } = useTranslation();
  const language = i18n.language as Language;
  const rentalsParams = useSelector(selectParams);
  const filterOptionValue = useSelector(selectFilterOption);
  const [isPoiSelecting, setIsPoiSelecting] = useState(false);

  const onInputFocus = () => {
    const userAgent = navigator.userAgent || navigator.vendor;
    if (/android/i.test(userAgent)) {
      window.scrollTo({ top: 150, behavior: 'smooth' });
    }
  };

  const handleNavigate = () => {
    dispatch(searchActions.setSearchAddress(rentalsParams.where as string));
    dispatch(searchActions.setSearchTerm(rentalsParams.where as string));
    router.push(
      stringifyUrl({ request: { ...rentalsParams, serp_type: filterOptionValue, index_serp_types: ['rents'] }, t }).path
    );
  };

  /**
   * Resetting values below when inputting another text to remove the selected POI before if any
   * @param term an inputting value
   */
  const handleMapInputChange = (term: string) => {
    dispatch(
      searchActions.setRentalsParams({
        ...rentalsParams,
        buildingId: undefined,
        areaId: undefined,
        transportationId: undefined,
        transportationSystem: undefined,
        coords: '',
        keyword: term,
        where: term,
        radius: 0
      })
    );
  };

  const handlePoiSelect = (poi: PoiSuggestion) => {
    setIsPoiSelecting(true);
    const address = poi.name[language];

    const params: SearchRequest = omit(
      {
        ...rentalsParams,
        [Params.Radius]: getDefaultRadius({ address, type: poi?.poi_type }),
        [Params.Where]: address,
        keyword: address,
        [Params.Coords]: ''
      },
      ['buildingId', 'areaId', 'transportationId', 'transportationSystem', 'building_type']
    );

    switch (poi.poi_type) {
      case PoiType.BUILDING:
        params.buildingId = poi.id;
        params.building_type = poi.building_type;
        break;
      case PoiType.AREA:
        params.areaId = poi.id;
        break;
      case PoiType.TRANSPORTATION:
        params.transportationId = poi.id;
        params.transportationSystem = poi.transportation_system;
        break;
      case PoiType.GOOGLE:
        params.coords = `${poi.lat}-${poi.lng}`;
        break;
      default:
        break;
    }

    /**
     * Ensuring this update goes after an input's change event `handleMapInputChange` above
     */
    setTimeout(() => {
      dispatch(searchActions.setRentalsParams(params));
      setIsPoiSelecting(false);
    }, 500);
  };

  return (
    <>
      <div className="w-full max-w-sm md:max-w-lg mt-6">
        <PoisAutocomplete
          theme={BasicSearchTheme.Dark}
          placeholder={t('whereToStay')}
          onInputChange={handleMapInputChange}
          onSelectChange={handlePoiSelect}
          onSearch={handleNavigate}
          onFocus={onInputFocus}
          isShowFilter
        />
      </div>
      <div className="flex w-auto flex-wrap justify-center mt-3">
        <HeroPrices page={page} theme={theme} />
        <HeroBeds theme={theme} />
      </div>
      <div className="mt-7">
        <button
          disabled={isPoiSelecting}
          id="search-target"
          className={classes.searchBtn(isPoiSelecting)}
          onClick={handleNavigate}>
          {t('searchNow')}
        </button>
      </div>
    </>
  );
}

const classes = {
  searchBtn: (disabled: boolean) => cntl`
    px-4 py-1
    text-lg font-medium
    rounded-lg border-0 focus:outline-none
    fs-home-search-button
    ${disabled ? 'bg-secondary-gray text-primary-gray' : 'bg-primary-orange text-white'}
  `
};

export default BasicSearch;
