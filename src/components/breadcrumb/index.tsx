import { Fragment, useCallback, useMemo, useState } from 'react';
import cntl from 'cntl';
import sumBy from 'lodash/sumBy';
import trimEnd from 'lodash/trimEnd';
import { join } from 'path';
import { GeneratedBreadcrumb } from '@interfaces/breadcrumb';
import { useTranslation } from 'react-i18next';
import { Language } from '@constants/enum';

const BREADCRUM_MAX_CHARACTER = 80;
const NUMBER_CHARACTER_SHOW_FIRST = 35;
const NUMBER_CHARACTER_SHOW_LAST = 35;
const HOME_URL = trimEnd(process.env.NEXT_PUBLIC_HOME_URL, '/');

interface BreadcrumbProps {
  list: GeneratedBreadcrumb[];
}

function Breadcrumb({ list }: BreadcrumbProps) {
  if (!list.length) return <></>;

  const { i18n } = useTranslation();
  const language = i18n.language as Language;

  const baseURL = useMemo(() => {
    return language === Language.TH ? HOME_URL : `${HOME_URL}/${language}`;
  }, [language]);

  const breadcrumbStringLength = useMemo(() => {
    return sumBy(list, (generatedBreadcrumb) => generatedBreadcrumb.label.length);
  }, [list]);

  const shouldTruncate = useMemo(() => {
    return breadcrumbStringLength > BREADCRUM_MAX_CHARACTER;
  }, [breadcrumbStringLength, BREADCRUM_MAX_CHARACTER]);

  const [showFull, setShowFull] = useState(false);

  const getBreadcrumbsPosition = useCallback(() => {
    const leftBreadcrumbs: GeneratedBreadcrumb[] = [];
    const centerBreadcrumbs: GeneratedBreadcrumb[] = [];
    const rightBreadcrumbs: GeneratedBreadcrumb[] = [];

    let labelTotalLength = 0;

    list.forEach((generatedBreadcrumb, index) => {
      const labelLength = generatedBreadcrumb.label.length;
      const isRightBreadcrumb =
        index === list.length - 1 || labelTotalLength >= breadcrumbStringLength - NUMBER_CHARACTER_SHOW_LAST;
      const isLeftBreadcrumb = labelTotalLength + labelLength <= NUMBER_CHARACTER_SHOW_FIRST;

      if (isRightBreadcrumb) {
        rightBreadcrumbs.push(generatedBreadcrumb);
      } else if (isLeftBreadcrumb) {
        leftBreadcrumbs.push(generatedBreadcrumb);
      } else {
        centerBreadcrumbs.push(generatedBreadcrumb);
      }
      labelTotalLength += labelLength;
    });

    return [leftBreadcrumbs, centerBreadcrumbs, rightBreadcrumbs];
  }, [list, breadcrumbStringLength]);

  const separation = useMemo(() => {
    return (
      <li className="mx-2">
        <span className="text-base text-primary-lightgray">&gt;</span>
      </li>
    );
  }, []);

  const renderBreadcrumbItem = ({ item, isLastItem }: { item: GeneratedBreadcrumb; isLastItem: boolean }) => {
    const href = `${baseURL}${join('/', item.path, '/')}`;
    const alternateProps: { href: string; rel?: string } = {
      href,
      rel: item.rel
    };
    return (
      <>
        <li className={classes.listItem}>
          <a className={classes.text({ bold: isLastItem })} {...alternateProps}>
            {item.label}
          </a>
        </li>
        {!isLastItem && separation}
      </>
    );
  };

  const handleShowFullBreadcrumb = () => {
    setShowFull(true);
  };

  const renderBreadcrumbs = useMemo(() => {
    if (!shouldTruncate) {
      return (
        <>
          {list.map((item, index) => (
            <Fragment key={index}>{renderBreadcrumbItem({ item, isLastItem: index === list.length - 1 })}</Fragment>
          ))}
        </>
      );
    }

    const [leftSideItems, centerItems, rightSideItems] = getBreadcrumbsPosition();

    return (
      <>
        {leftSideItems.map((item, index) => (
          <Fragment key={index}>{renderBreadcrumbItem({ item, isLastItem: false })}</Fragment>
        ))}
        <>
          {showFull ? (
            centerItems.map((item, index) => (
              <Fragment key={index}>{renderBreadcrumbItem({ item, isLastItem: false })}</Fragment>
            ))
          ) : (
            <>
              <li className={classes.listItem}>
                <span aria-hidden="true" onClick={handleShowFullBreadcrumb} className={classes.ellipsis}>
                  ...
                </span>
              </li>
              {separation}
            </>
          )}
        </>
        {rightSideItems.map((item, index) => (
          <Fragment key={index}>
            {renderBreadcrumbItem({ item, isLastItem: index === rightSideItems.length - 1 })}
          </Fragment>
        ))}
      </>
    );
  }, [shouldTruncate, list, showFull]);

  return (
    <div className="w-full mt-5 px-9 md:px-28">
      <ul className="flex flex-wrap">{renderBreadcrumbs}</ul>
    </div>
  );
}

const classes = {
  text: ({ bold }: { bold: boolean }) => cntl`
    text-sm ${bold ? 'text-black font-bold' : ''}
  `,
  listItem: cntl`
    grid grid-flow-col gap-4 
    last:mr-0 
    items-center text-primary-lightgray
  `,
  ellipsis: cntl`px-2 text-primary-lightgray
  cursor-pointer`
};

export default Breadcrumb;
