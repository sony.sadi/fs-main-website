/* eslint-disable react/no-danger */
import React, { useMemo } from 'react';
import Head from 'next/head';
import { GeneratedBreadcrumb } from '@interfaces/breadcrumb';
import trimEnd from 'lodash/trimEnd';
import trimStart from 'lodash/trimStart';
import join from 'lodash/join';
import { useTranslation } from 'next-i18next';
import { Language } from '@constants/enum';

const { NEXT_PUBLIC_HOME_URL } = process.env;

const HOME_URL = trimEnd(NEXT_PUBLIC_HOME_URL, '/');

interface BreadcrumbSchemaProps {
  breadcrumbs: GeneratedBreadcrumb[];
}

function BreadcrumbSchema({ breadcrumbs }: BreadcrumbSchemaProps) {
  if (!breadcrumbs.length) return <></>;
  const { i18n } = useTranslation();
  const language = i18n.language as Language;

  const schema = useMemo(() => {
    const localePrefix = language === Language.US ? Language.US : '';
    const originUrl = trimEnd(`${HOME_URL}/${localePrefix}`, '/');
    const itemListElement = breadcrumbs.map((breadcrumb, index) => ({
      '@type': 'ListItem',
      position: index + 1,
      name: breadcrumb.label,
      item: join([originUrl, trimStart(breadcrumb.path, '/')], '/')
    }));

    return JSON.stringify({
      '@context': 'https://schema.org',
      '@type': 'BreadcrumbList',
      itemListElement
    });
  }, [breadcrumbs]);

  return (
    <Head>
      <script type="application/ld+json" dangerouslySetInnerHTML={{ __html: schema }} />
    </Head>
  );
}

export default BreadcrumbSchema;
