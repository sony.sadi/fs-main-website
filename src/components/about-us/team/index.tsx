import cntl from 'cntl';
import { useTranslation } from 'next-i18next';

import { LocaleNamespace } from '@constants/enum';

function AboutUsTeam() {
  const { t } = useTranslation(LocaleNamespace.About);

  return (
    <div className={classes.container}>
      <h2 className={classes.heading}>{t('aboutTeamHead')}</h2>
      <p className={classes.description}>{t('aboutTeamDesc')}</p>

      <div className={classes.picture} />
    </div>
  );
}

const classes = {
  container: cntl`
    mx-9 md:mx-28 my-14
  `,
  heading: cntl`
    text-center md:text-left text-4xl font-bold
  `,
  description: cntl`
    max-w-18.75rem md:max-w-none mx-auto
    mt-4 md:mt-2 mb-8
    text-lg text-center md:text-left
  `,
  picture: cntl`
    bg-about-us-team-mobile md:bg-about-us-team-laptop 2xl:bg-about-us-team-desktop w-full
    bg-no-repeat bg-cover
    pb-about-us-team-mobile md:pb-about-us-team-laptop 2xl:pb-about-us-team-desktop
  `
};

export default AboutUsTeam;
