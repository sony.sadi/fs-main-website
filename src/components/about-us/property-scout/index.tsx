import cntl from 'cntl';
import { useTranslation } from 'next-i18next';
import { useMemo } from 'react';

import Link from '@components/core/link';
import Svg from '@components/core/svg';
import { LocaleNamespace } from '@constants/enum';
import { getSerpBaseUrl } from '@utils';

function AboutUsPropertyScout() {
  const { t } = useTranslation(LocaleNamespace.About);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);

  const statistics = [
    { value: t('aboutScoutStat1Head'), text: t('aboutScoutStat1Desc'), gte: true },
    { value: t('aboutScoutStat2Head'), text: t('aboutScoutStat2Desc'), gte: true },
    { value: t('aboutScoutStat3Head'), text: t('aboutScoutStat3Desc'), gte: true },
    { value: t('aboutScoutStat4Head'), text: t('aboutScoutStat4Desc'), gte: true },
    {
      value: t('aboutScoutStat5Head'),
      text: t('aboutScoutStat5Desc'),
      gte: true
    },
    {
      value: t('aboutScoutStat6Head'),
      text: t('aboutScoutStat6Desc')
    }
  ];

  /**
   * Rent base url
   */
  const serpBaseUrl = useMemo(() => {
    return getSerpBaseUrl(tCommon);
  }, [tCommon]);

  /**
   * Sale base url
   */
  const saleSerpBaseUrl = useMemo(() => {
    return getSerpBaseUrl(tCommon, 'sales');
  }, [tCommon]);

  return (
    <div className="mx-9 md:mx-28 my-14">
      <h2 className={classes.heading}>{t('aboutScoutHeading')}</h2>
      <ul className={classes.numbers}>
        {statistics.map((stat, index) => (
          <li key={index} className={classes.number}>
            <strong className="text-3xl">
              {stat.gte && <span className="text-3xl text-primary-orange">&gt;</span>}
              {stat.value}
            </strong>
            <p className="mt-3">{stat.text}</p>
          </li>
        ))}
      </ul>
      <div className={classes.linkContainer}>
        <Link href={serpBaseUrl.path} rel={serpBaseUrl.rel} className={classes.link}>
          {t('aboutScoutShowAll')} <Svg className={classes.linkIcon} name="red-arrow-right" />
        </Link>
        <Link href={saleSerpBaseUrl.path} rel={saleSerpBaseUrl.rel} className={classes.link}>
          {t('aboutScoutShowAllSales')} <Svg className={classes.linkIcon} name="red-arrow-right" />
        </Link>
      </div>
    </div>
  );
}

const classes = {
  heading: cntl`
    text-center md:text-left text-4xl font-bold
  `,
  linkContainer: cntl`
  flex flex-col items-center  md:items-end
 `,
  numbers: `
    grid md:grid-cols-3 gap-3 content-center mt-11
  `,
  number: `
    flex flex-col justify-center p-7 text-center text-white bg-primary-dark rounded-2xl
  `,
  link: cntl`
    flex items-center justify-center md:justify-end
    mt-8
    text-sm underline
  `,
  linkIcon: cntl`ml-2 w-1.5 h-2.5`
};

export default AboutUsPropertyScout;
