import clsx from 'clsx';
import cntl from 'cntl';
import React from 'react';
import { useTranslation } from 'next-i18next';

import { LocaleNamespace } from '@constants/enum';

function AboutUsMission() {
  const { t } = useTranslation(LocaleNamespace.About);

  return (
    <div className="mx-9 md:mx-28 my-14">
      <h2 className="text-4xl font-bold">{t('aboutMissionHeading')}</h2>
      <p className={clsx(classes.paragraph, 'font-bold')}>{t('aboutMissionDesc1')}</p>
      <p className={classes.paragraph}>{t('aboutMissionDesc2')}</p>
      <p className={classes.paragraph}>{t('aboutMissionDesc3')}</p>
      <p className={classes.paragraph}>{t('aboutMissionDesc4')}</p>
    </div>
  );
}

const classes = {
  paragraph: cntl`
    mt-7 text-base
  `
};

export default AboutUsMission;
