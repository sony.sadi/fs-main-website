import { DOMAttributes, useMemo } from 'react';
import { useTranslation } from 'next-i18next';
import cntl from 'cntl';
import Slider from 'react-slick';
import { useSelector } from 'react-redux';

import Svg from '@components/core/svg';
import Link from '@components/core/link';
import Image from '@components/core/image';
import { selectIsMobile } from '@stores/slices/common';
import ourValues from '@assets/data/our-values.json';
import { getSerpBaseUrl } from '@utils';
import { LocaleNamespace } from '@constants/enum';
import clsx from 'clsx';

function NextArrow(props: DOMAttributes<HTMLSpanElement>) {
  return (
    <span aria-hidden className={classes.arrow(false)} onClick={props.onClick}>
      <Svg className={classes.arrowSvg} name="right-arrow" />
    </span>
  );
}

function PrevArrow(props: DOMAttributes<HTMLSpanElement>) {
  return (
    <span aria-hidden className={classes.arrow(true)} onClick={props.onClick}>
      <Svg className={classes.arrowSvg} name="left-arrow" />
    </span>
  );
}

function AboutUsValues() {
  const { t } = useTranslation(LocaleNamespace.About);

  const isMobile = useSelector(selectIsMobile);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);

  const width = isMobile ? 404 : 177;
  const height = isMobile ? 217 : 177;

  const settings = {
    dots: false,
    infinite: true,
    // autoplay: true,
    draggable: false,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 1,
    dotsClass: classes.valuesDots,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,

    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          infinite: false,
          arrows: false,
          dots: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: '30px',

          dotsClass: classes.dot,
          appendDots: (dots: React.ReactNode) => (
            <div>
              <ul className={classes.valuesSlickDots}>{dots}</ul>
            </div>
          ),
          customPaging: () => {
            return <span className={classes.valuesCustomPaging} />;
          }
        }
      }
    ]
  };

  /**
   * Rent serp base url
   */
  const serpBaseUrl = useMemo(() => {
    return getSerpBaseUrl(tCommon);
  }, [tCommon]);

  /**
   * Sale serp page Url
   */
  const saleSerpBaseUrl = useMemo(() => {
    return getSerpBaseUrl(tCommon, 'sales');
  }, [tCommon]);

  return (
    <section className="my-14">
      <h2 className={classes.heading}>{t('aboutValueHead')}</h2>

      <div className={classes.ourValueContainer}>
        {ourValues.length && (
          <Slider className={classes.slider} {...settings}>
            {ourValues.map(({ value, text, imageUrl }, index) => (
              <div key={index}>
                <div className={classes.ourValueBox}>
                  <Image
                    className={classes.ourValueImage}
                    src={imageUrl}
                    sizes="300px"
                    layout="responsive"
                    width={width}
                    height={height}
                    objectFit="cover"
                  />
                  <div className={classes.ourContent}>
                    <div className="px-5 pb-5 md:p-0">
                      <strong className={classes.ourValue}>{t(value)}</strong>
                      <p>{t(text)}</p>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </Slider>
        )}
      </div>

      <div className={classes.linkContainer}>
        <Link href={serpBaseUrl.path} rel={serpBaseUrl.rel} className={classes.link}>
          {t('aboutValueShowAll')} <Svg className={classes.linkIcon} name="red-arrow-right" />
        </Link>
        <Link href={saleSerpBaseUrl.path} rel={saleSerpBaseUrl.rel} className={clsx('mt-8', classes.link)}>
          {t('aboutScoutShowAllSales')} <Svg className={classes.linkIcon} name="red-arrow-right" />
        </Link>
      </div>
    </section>
  );
}

const classes = {
  heading: cntl`
    mx-9 md:mx-28 text-center md:text-left text-4xl font-bold
  `,
  linkContainer: cntl`
  flex flex-col items-center  md:items-end
 `,
  dot: cntl`absolute left-1/2 transform -translate-x-1/2 bottom-0`,
  slider: cntl`sm:mx-20 md:px-4 pb-8 md:pb-0`,
  valuesDots: cntl`mt-10 absolute left-1/2 transform -translate-x-1/2`,
  valuesSlickDots: cntl`grid grid-flow-col gap-1 buildings__slick-dots`,
  valuesCustomPaging: cntl`block w-6 h-2 rounded bg-home-silver hover:bg-primary-orange buildings__slick-paging`,
  ourValueContainer: cntl`mt-8 mb-20 xl:mb-10 xl:px-0`,
  ourValueBox: cntl`
    group bg-white
    rounded-xl rounded-b-xl cursor-pointer overflow-hidden
    px-2 h-80	sm:h-auto
    flex flex-col sm:block
  `,
  ourValueImage: cntl`h-40 sm:h-auto rounded-t-2xl xl:rounded-b-2xl`,
  ourValue: cntl`
    text-lg font-bold group-hover:text-primary-orange block pb-2 pt-4 md:pb-0 md:pt-0
  `,
  ourContent: cntl`
    flex-grow md:py-5 rounded-b-3xl border-2 border-solid border-t-0 md:border-none
  `,
  link: cntl`
    flex items-center justify-center md:justify-end
    md:mr-28
    text-sm underline
  `,
  linkIcon: cntl`ml-2 w-1.5 h-2.5`,
  arrow: (isPrev: boolean) => cntl`
    group
    absolute top-1/2 ${isPrev ? '-left-8 -translate-x-1/2' : '-right-8 translate-x-1/2'}
    flex-center
    w-12 h-12
    bg-white hover:bg-primary-orange rounded-full
    shadow-xl
    transform -translate-y-1/2 z-10
    cursor-pointer
  `,
  arrowSvg: cntl`
    w-3 h-5
    fill-current text-primary-orange group-hover:text-white
  `
};

export default AboutUsValues;
