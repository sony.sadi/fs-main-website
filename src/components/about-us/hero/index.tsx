import cntl from 'cntl';
import { useTranslation } from 'next-i18next';

import { LocaleNamespace } from '@constants/enum';

const AboutUsHero = () => {
  const { t } = useTranslation(LocaleNamespace.About);

  return (
    <section className={classes.container}>
      <div className={classes.box}>
        <h1 className={classes.heading}>{t('aboutHeading')}</h1>
        <p className={classes.description}>{t('aboutDesc')}</p>
      </div>
    </section>
  );
};

const classes = {
  container: cntl`
    bg-about-us-mobile md:bg-about-us-tablet lg:bg-about-us-laptop 2xl:bg-about-us-desktop w-full
    bg-no-repeat bg-cover bg-center
    px-9 md:px-28
    pt-16 md:pt-24 2xl:pt-32
    pb-16 md:pb-32 2xl:pb-48
  `,
  box: cntl`
    top-20 left-16 md:top-36 md:left-40 text-white
  `,
  heading: cntl`
    max-w-15.5rem md:max-w-md pr-1 md:pr-0 text-4xl font-bold
  `,
  description: cntl`
    max-w-17.5rem md:max-w-md mt-5 text-lg
  `
};

export default AboutUsHero;
