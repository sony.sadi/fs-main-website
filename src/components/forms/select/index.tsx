import { useState, useEffect, useRef, useMemo, useCallback } from 'react';
import { FixedSizeList } from 'react-window';
import AutoSizer from 'react-virtualized-auto-sizer';
import clsx from 'clsx';

import Svg from '@components/core/svg';
import { FsSelectTheme } from '@constants/enum';
import cntl from 'cntl';

export interface SelectOption {
  label: string;
  value: any;
}

interface SelectProps {
  className?: string;
  options: SelectOption[];
  leftIcon?: string;
  placeholder?: string;
  defaultSelected?: string | number;
  theme?: FsSelectTheme;
  windowing?: boolean;
  onSelect?: (selectedValue: any) => void;
}

const Select = ({ theme = FsSelectTheme.Light, ...props }: SelectProps) => {
  const [selected, setSelect] = useState<string | number | undefined>(props.defaultSelected || '');
  const [open, setOpen] = useState(false);
  const selectRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    setSelect(props.defaultSelected);
  }, [props.defaultSelected]);

  const textColorClassMap = useMemo(() => {
    return theme === FsSelectTheme.Light ? 'text-white' : 'text-primary-gray';
  }, [theme]);

  useEffect(() => {
    document.addEventListener('click', handleClick);
    document.addEventListener('keyup', handleKeyup);
    return () => {
      document.removeEventListener('click', handleClick);
      document.removeEventListener('keyup', handleKeyup);
    };
  }, []);

  const handleClick = (e: MouseEvent) => {
    if (selectRef.current) {
      if (!selectRef.current.contains(e.target as Node)) {
        setOpen(false);
      }
    }
  };

  const handleKeyup = (e: KeyboardEvent) => {
    if (e.key === 'Escape') {
      setOpen(false);
    }
  };

  const toggleOptionList = () => setOpen(!open);

  const handleSelectValue = (value: any) => {
    setSelect(value);
    setOpen(false);
    props.onSelect && props.onSelect(value);
  };

  const renderOption = useCallback((option: SelectOption, style = {}) => {
    return (
      <div
        style={style}
        className={classes.optionContainer}
        aria-hidden="true"
        onClick={() => {
          handleSelectValue(option.value);
        }}
        key={option.value}>
        <span className="text-primary-gray whitespace-nowrap">{option.label}</span>
      </div>
    );
  }, []);

  const renderOptions = useCallback(() => {
    if (props.windowing) {
      return (
        <AutoSizer>
          {({ height, width }) => (
            <FixedSizeList width={width} height={height} itemCount={props.options.length} itemSize={37}>
              {({ index, style }) => renderOption(props.options[index], style)}
            </FixedSizeList>
          )}
        </AutoSizer>
      );
    }

    return props.options.map((option) => renderOption(option));
  }, [props.windowing, props.options]);

  return (
    <div
      ref={selectRef}
      className={clsx('relative flex flex-col items-center', props.className)}
      aria-hidden="true"
      onClick={toggleOptionList}>
      <div className={classes.selectCotainer}>
        {props.placeholder && !selected && (
          <p className={clsx('whitespace-nowrap', textColorClassMap)}>{props.placeholder}</p>
        )}
        {!!selected && (
          <span className={clsx('whitespace-nowrap', textColorClassMap)}>
            {props.options.find((option) => option.value === selected)?.label}
          </span>
        )}
        <Svg className={classes.downArrow} name="down-arrow" />
      </div>
      <div
        className={clsx(
          classes.optionsContainer,
          open && 'open border',
          !props.windowing && 'overflow-y-auto',
          props.windowing && 'windowing'
        )}>
        {renderOptions()}
      </div>
    </div>
  );
};

const classes = {
  optionContainer: cntl`
    flex-center p-2 border-b cursor-pointer hover:bg-secondary-gray fs-select__option
  `,
  selectCotainer: cntl`
    flex items-center justify-center 
    w-full text-sm cursor-pointer
  `,
  downArrow: cntl`
    w-3.5 h-2 ml-1 
    fill-current text-black
  `,
  optionsContainer: cntl`
    absolute top-full w-full
    flex flex-col
    rounded-md bg-white shadow-xl z-10 
    fs-select-options
  `
};

export default Select;
