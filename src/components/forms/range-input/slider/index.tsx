import { useEffect, useRef, useState } from 'react';

import Svg from '@components/core/svg';
import Slider from '@utils/range-slider';
import cntl from 'cntl';

interface Props {
  railMin: number;
  railMax: number;
  minValue: number;
  maxValue: number;
  onMinChange: (value: number) => void;
  onMaxChange: (value: number) => void;
}

function RangeInputSlider(props: Props) {
  const widgetRef = useRef<HTMLDivElement>(null);
  const minRef = useRef<HTMLElement & SVGSVGElement>(null);
  const maxRef = useRef<HTMLElement & SVGSVGElement>(null);

  const [minleft, setMinLeft] = useState(0);
  const [maxleft, setMaxLeft] = useState(0);
  const [min, setMin] = useState(props.minValue);
  const [max, setMax] = useState(props.maxValue);

  const progressMin = Math.min(minleft, maxleft);
  const progressMax = Math.max(minleft, maxleft);

  // FS-536 - avoid bar extending left of slider when at minimum value
  const highlightProgressStyle = {
    left: `${progressMin >= 0 ? progressMin : 0}px`,
    width: `${progressMax - progressMin}px`
  };

  useEffect(() => {
    setMin(props.minValue);
  }, [props.minValue]);

  useEffect(() => {
    setMax(props.maxValue);
  }, [props.maxValue]);

  useEffect(() => {
    if (!minRef.current) return;

    const minSlider = new Slider(minRef.current, {
      onChange(changes) {
        setMin(changes.value);
        setMinLeft(changes.left);
        props.onMinChange(changes.value);
      }
    });
    minSlider.init();
  }, [minRef, min]);

  useEffect(() => {
    if (!maxRef.current) return;

    const maxSlider = new Slider(maxRef.current, {
      onChange(changes) {
        setMax(changes.value);
        setMaxLeft(changes.left);
        props.onMaxChange(changes.value);
      }
    });
    maxSlider.init();
  }, [maxRef, max]);

  return (
    <div ref={widgetRef} className="relative aria-widget-slider">
      <div className={classes.container}>
        <div className={classes.absoluteBg} style={highlightProgressStyle} />
        <div className="rail">
          <Svg
            ref={minRef}
            name="min-thumb-slider"
            role="slider"
            className={classes.thumb}
            aria-valuemin={props.railMin}
            aria-valuenow={min}
            aria-valuemax={props.railMax}
          />

          <Svg
            ref={maxRef}
            name="max-thumb-slider"
            role="slider"
            className={classes.thumb}
            aria-valuemin={props.railMin}
            aria-valuenow={max}
            aria-valuemax={props.railMax}
          />
        </div>
      </div>
    </div>
  );
}

const classes = {
  container: cntl`relative bg-gray-300 h-1 w-full rounded`,
  absoluteBg: cntl`absolute top-0 h-full bg-primary-orange`,
  thumb: cntl`absolute w-5 h-5 transform -translate-y-1/2 cursor-pointer`
};

export default RangeInputSlider;
