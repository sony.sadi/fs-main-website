import { useState, useMemo, useCallback, useEffect, useRef, ChangeEvent } from 'react';
import cntl from 'cntl';
import throttle from 'lodash/throttle';

import RangeInputSlider from '@components/forms/range-input/slider';
import { formatPrice } from '@utils';
import { Page } from '@constants/enum';
import { useDispatch, useSelector } from 'react-redux';
import { searchActions, selectFilterOption, selectParams } from '@stores/slices/search';
import { SerpType } from '@flexstay.rentals/fs-url-stringify';

export interface LabelProps {
  htmlFor: string;
}

interface Props {
  className?: string;
  page: Page;
  min?: number;
  max?: number;
  railMin: number;
  railMax: number;
  unit?: string;
  renderMinLabel: (props: LabelProps) => JSX.Element;
  renderMaxLabel: (props: LabelProps) => JSX.Element;
  onChange: (min: number, max: number) => void;
}

function RangeInput(props: Props) {
  const mountedRef = useRef<boolean>();
  const [minPrice, setMinPrice] = useState<string | number>(props.min || props.railMin);
  const [maxPrice, setMaxPrice] = useState<string | number>(props.max || props.railMax);
  const filterOptionValue = useSelector(selectFilterOption);
  const dispatch = useDispatch();
  const searchParams = useSelector(selectParams);
  const [filterState, setFilterState] = useState<SerpType>(filterOptionValue);
  const minValue = useMemo(() => Number(String(minPrice).replace(/[^\d.]/g, '')), [minPrice]);
  const maxValue = useMemo(() => Number(String(maxPrice).replace(/[^\d.]/g, '')), [maxPrice]);

  /**
   * Change price state if detect props changed
   */
  useEffect(() => {
    if (filterState !== filterOptionValue) {
      const minRail = String(props.railMin).replace(/[^\d.]/g, '');
      const maxRail = String(props.railMax).replace(/[^\d.]/g, '');
      setMinPrice(formatPrice(Number(minRail)));
      setMaxPrice(formatPrice(Number(maxRail)));
    }

    setFilterState(filterOptionValue);
  }, [filterOptionValue]);

  const formClasses = useMemo(() => {
    switch (props.page) {
      case Page.Home:
      case Page.Notfound:
        return 'gap-x-2 pb-4';

      case Page.Listings:
        return 'gap-x-10 pb-6';

      default:
        return '';
    }
  }, [props.page]);

  const controlFontSize = useMemo(() => {
    switch (props.page) {
      case Page.Home:
      case Page.Notfound:
        return 'text-xs';

      case Page.Listings:
        return 'text-base';

      default:
        return '';
    }
  }, [props.page]);

  useEffect(() => {
    if (mountedRef.current) {
      props.onChange && props.onChange(minValue, maxValue);
    }

    mountedRef.current = true;
  }, [minValue, maxValue]);

  const handleMinChange = (event: ChangeEvent<HTMLInputElement>) => {
    const value = String(event.target.value).replace(/[^\d.]/g, '');
    if (value.endsWith('.')) return false;
    setMinPrice(formatPrice(Number(value)));
  };

  const handleMaxChange = (event: ChangeEvent<HTMLInputElement>) => {
    const value = String(event.target.value).replace(/[^\d.]/g, '');
    if (value.endsWith('.')) return false;
    setMaxPrice(formatPrice(Number(value)));
  };

  const handleMinSliderChange = useCallback(
    throttle((value: number) => {
      setMinPrice(formatPrice(Math.round(value)));
    }, 500),
    []
  );

  const handleMaxSliderChange = useCallback(
    throttle((value: number) => {
      setMaxPrice(formatPrice(Math.round(value)));
    }, 500),
    []
  );

  return (
    <div className={classes.slider(props.className)}>
      <div className={classes.form({ classes: formClasses })}>
        <div>
          {props.renderMinLabel({ htmlFor: 'min' })}
          <div className={classes.control({ fontSize: controlFontSize })}>
            {props.unit}
            &nbsp;
            <input
              type="text"
              id="min"
              className="w-full focus:outline-none range-input__min"
              value={minPrice}
              onChange={handleMinChange}
            />
          </div>
        </div>
        <div>
          {props.renderMaxLabel({ htmlFor: 'max' })}
          <div className={classes.control({ fontSize: controlFontSize })}>
            {props.unit}
            &nbsp;
            <input
              type="text"
              id="max"
              className="w-full focus:outline-none range-input__max"
              value={maxPrice}
              onChange={handleMaxChange}
            />
          </div>
        </div>
      </div>

      <RangeInputSlider
        railMin={props.railMin}
        railMax={props.railMax}
        minValue={minValue}
        maxValue={maxValue}
        onMinChange={handleMinSliderChange}
        onMaxChange={handleMaxSliderChange}
      />
    </div>
  );
}

const classes = {
  slider: (className?: string) => cntl`
    w-full ${className}
  `,
  form: ({ classes }: { classes: string }) => cntl`
    grid grid-cols-2 ${classes}
  `,
  control: ({ fontSize }: { fontSize: string }) => cntl`
    flex mt-2 px-3 py-2 border border-black rounded-lg ${fontSize}
  `,
  options: cntl`
    grid grid-cols-3 gap-2
  `,
  option: ({ active }: { active: boolean }) => cntl`
    w-20 px-2 py-1 text-center text-base text-black border rounded-lg
    cursor-pointer
    hover:bg-primary-pink
    ${active ? 'bg-primary-pink border-primary-orange' : ''}
  `
};

export default RangeInput;
