import { useMemo } from 'react';
import { useTranslation } from 'next-i18next';
import { TFunctionResult } from 'i18next';
import cntl from 'cntl';

import RangeInput, { LabelProps as RangeInputLabelProps } from '@components/forms/range-input';
import { MAX_FILTERING_FLOOR_SIZE } from '@constants';
import { Page } from '@constants/enum';

interface Props {
  className?: string;
  page: Page;
  min: number;
  max: number;
  onChange: (min: number, max: number) => void;
}

function RangeInputFloorSize(props: Props) {
  const { t } = useTranslation();

  return (
    <RangeInput
      {...props}
      railMin={0}
      railMax={MAX_FILTERING_FLOOR_SIZE}
      renderMinLabel={({ htmlFor }) => (
        <Label page={props.page} htmlFor={htmlFor}>
          {t('minSize')}
        </Label>
      )}
      renderMaxLabel={({ htmlFor }) => (
        <Label page={props.page} htmlFor={htmlFor}>
          {t('maxSize')}
        </Label>
      )}
    />
  );
}

interface LabelProps extends RangeInputLabelProps {
  page: Page;
  children: TFunctionResult;
}

function Label(props: LabelProps) {
  const labelFontSize = useMemo(() => {
    switch (props.page) {
      case Page.Home:
        return 'text-base';

      case Page.Listings:
        return 'text-base';

      default:
        return '';
    }
  }, [props.page]);

  return (
    <label {...props} className={classes.label({ fontSize: labelFontSize })}>
      {props.children}
    </label>
  );
}

const classes = {
  label: ({ fontSize }: { fontSize: string }) => cntl`
    text-base font-bold ${fontSize}
  `
};

export default RangeInputFloorSize;
