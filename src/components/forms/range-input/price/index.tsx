import { useMemo } from 'react';
import { useTranslation } from 'next-i18next';
import { TFunctionResult } from 'i18next';
import cntl from 'cntl';

import RangeInput, { LabelProps as RangeInputLabelProps } from '@components/forms/range-input';
import { MAX_FILTERING_PRICE } from '@constants';
import { Page } from '@constants/enum';

interface Props {
  className?: string;
  page: Page;
  min: number;
  max: number;
  onChange: (min: number, max: number) => void;
  railMax?: number;
}

function RangeInputPrice(props: Props) {
  const { t } = useTranslation();

  return (
    <RangeInput
      {...props}
      unit="฿"
      railMin={0}
      railMax={props.railMax || MAX_FILTERING_PRICE}
      renderMinLabel={({ htmlFor }) => (
        <Label page={props.page} htmlFor={htmlFor}>
          {t('minPrice')}
        </Label>
      )}
      renderMaxLabel={({ htmlFor }) => (
        <Label page={props.page} htmlFor={htmlFor}>
          {t('maxPrice')}
        </Label>
      )}
    />
  );
}

interface LabelProps extends RangeInputLabelProps {
  page: Page;
  children: TFunctionResult;
}

function Label(props: LabelProps) {
  const labelFontSize = useMemo(() => {
    switch (props.page) {
      case Page.Home:
      case Page.Notfound:
        return 'text-base';

      case Page.Listings:
        return 'text-lg';

      default:
        return '';
    }
  }, [props.page]);

  return (
    <label {...props} className={classes.label({ fontSize: labelFontSize })}>
      {props.children}
    </label>
  );
}

const classes = {
  label: ({ fontSize }: { fontSize: string }) => cntl`
    text-base font-bold ${fontSize}
  `
};

export default RangeInputPrice;
