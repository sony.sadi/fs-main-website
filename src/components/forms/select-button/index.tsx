import { HTMLAttributes } from 'react';
import cntl from 'cntl';

interface FsSelectButtonProps extends HTMLAttributes<HTMLDivElement> {
  selected?: boolean;
  value: string | number;
  label: string;
  onSelectChange?: (value: string | number, selected: boolean) => void;
  labelClass?: string;
  isMobile: boolean;
}

const SelectButton = ({ label, value, isMobile, onSelectChange, ...props }: FsSelectButtonProps) => {
  const handleClick = () => {
    onSelectChange && onSelectChange(value, !props.selected);
  };

  return (
    <button className={classes.button(props.selected, isMobile)} data-label={label} onClick={handleClick}>
      <span className={classes.label(props.labelClass)}>{label}</span>
    </button>
  );
};

const classes = {
  button: (selected?: boolean, isMobile?: boolean) => cntl`
    focus:outline-none px-3 py-1.5 border rounded-md w-full
    ${!isMobile && 'hover:bg-primary-pink'}
    ${selected ? 'border-primary-orange bg-primary-pink' : 'border-secondary-lightgray'}
    select-button
  `,
  label: (className?: string) => cntl`
    text-primary-black ${className}
  `
};

export default SelectButton;
