import { useState, useEffect } from 'react';
import cntl from 'cntl';

import Svg from '@components/core/svg';

interface Props {
  className?: string;
  placeholder?: string;
  defaultValue: number;

  min?: number;
  max?: number;
  onChange: (value: number) => void;
}

const InputNumber = ({ className, defaultValue = 0, placeholder, max, min, ...props }: Props) => {
  const [value, setValue] = useState<number | string>(defaultValue);

  useEffect(() => {
    setValue(defaultValue);
  }, [defaultValue]);

  const handleInputChange = (newValue: number | string) => {
    if (!newValue) {
      return setValue(newValue);
    }

    newValue = Number(newValue);
    if (min !== undefined && newValue < min) return setValue(min);
    if (max !== undefined && newValue > max) return setValue(max);

    setValue(newValue);
    props.onChange(newValue);
  };

  return (
    <div className={classes.container(className)}>
      <button
        onClick={() => {
          handleInputChange(Number(value) - 1);
        }}
        className={classes.minusBtn}
        data-testid="input-number-minus">
        <Svg name="circle-minus" className="w-5 h-5" />
      </button>
      <div className="flex-1">
        <input
          className={classes.input}
          placeholder={placeholder}
          onChange={({ target }) => handleInputChange(target.value)}
          value={value}
          type="number"
        />
      </div>
      <button
        onClick={() => {
          handleInputChange(Number(value) + 1);
        }}
        className={classes.plusBtn}
        data-testid="input-number-plus">
        <Svg name="circle-plus" className="w-5 h-5" />
      </button>
    </div>
  );
};

const classes = {
  container: (className?: string) => cntl`
    flex justify-center w-full h-full items-end
    ${className}
  `,
  input: cntl`
    h-8 w-full 
    text-center text-primary-gray border-b border-primary-lightgray 
    focus:outline-none
    input-number__input
  `,
  minusBtn: cntl`
    w-8 h-8 focus:outline-none flex-center
  `,
  plusBtn: cntl`
    w-8 h-8 focus:outline-none flex items-center justify-center
  `
};

export default InputNumber;
