import { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

import SelectButton from '@components/forms/select-button';
import InputNumber from '@components/forms/input-number';
import FloorSize from '@components/listings/floor-size';
import { selectIsMobile } from '@stores/slices/common';
import { searchActions, selectFilterOption, selectParams, selectSearchAddress } from '@stores/slices/search';
import { SearchRequest } from '@interfaces/search';
import { Amenity, Params, PropertyType } from '@flexstay.rentals/fs-url-stringify';
import { getAmenities, getBudgetTypes, getPropertyTypes } from '@utils/search';
import { Page } from '@constants/enum';

interface Props {
  page: Page;
}

const ListingsMoreFilter = (props: Props) => {
  const dispatch = useDispatch();
  const rentalsParams = useSelector(selectParams);
  const isMobile = useSelector(selectIsMobile);
  const searchAddress = useSelector(selectSearchAddress);
  const filterOptionValue = useSelector(selectFilterOption);
  const { t } = useTranslation();

  /**
   * Check if sale page
   */
  const checkIsSalePage = useMemo(() => {
    if (props.page === Page.Listings && rentalsParams.serp_type === 'sales') return true;
    return false;
  }, [rentalsParams.serp_type]);

  /**
   * Generate radius title
   */
  const radiusTitle = useMemo(() => {
    let title = t('propertyRadius');

    if (rentalsParams.transportationId || rentalsParams.buildingId || rentalsParams.coords) {
      title = t('radiusPois', { name: searchAddress || `(${rentalsParams.coords.replace('-', ',')})` });
    }

    if (rentalsParams.areaId) {
      title = t('radiusArea', { name: searchAddress || `(${rentalsParams.coords.replace('-', ',')})` });
    }

    return title;
  }, [rentalsParams, searchAddress]);

  const propertyTypes = useMemo(() => getPropertyTypes(t), [t]);
  const budgetTypes = useMemo(() => getBudgetTypes(t), [t]);
  const amenities = useMemo(() => getAmenities(t), [t]);

  const handleAmenitiesChange = useCallback(
    (value: string | number, selected: boolean) => {
      const selectedAmenities = rentalsParams.amenities;
      const setAmenities = setValue(Params.Amenities);

      if (selected) {
        return setAmenities(selectedAmenities.concat(value as Amenity));
      }

      setAmenities(selectedAmenities.filter((aminity: string | number) => aminity !== value));
    },
    [rentalsParams.amenities]
  );

  const handleMinLeaseChange = useCallback(
    (value: string | number, selected: boolean) => {
      const setMinLease = setValue(Params.MinLease);

      setMinLease(value);
    },
    [rentalsParams.minLease]
  );

  const handlePropertyTypesChange = useCallback(
    (value: string | number, selected: boolean) => {
      const selectedTypes = rentalsParams.propertyType;
      const setPropertyTypes = setValue(Params.PropertyType);
      const newValues = selected
        ? selectedTypes.concat(value as PropertyType)
        : selectedTypes.filter((type: string | number) => type !== value);

      setPropertyTypes(newValues);
    },
    [rentalsParams.propertyType]
  );

  const setValue = (key: keyof SearchRequest) => {
    return (value: SearchRequest[keyof SearchRequest]) => {
      dispatch(
        searchActions.setRentalsParam({
          key,
          value
        })
      );
    };
  };

  return (
    <div>
      <div className="py-4">
        <p className="mb-2 text-lg font-semibold">{t('propertyTypes')}</p>
        <div className="flex flex-wrap">
          {propertyTypes.map((type) => (
            <div key={type.id} className="md:w-1/2 xl:w-1/3 mb-2 px-1">
              <SelectButton
                onSelectChange={handlePropertyTypesChange}
                label={type.name}
                value={type.id}
                isMobile={isMobile}
                selected={rentalsParams.propertyType.includes(type.id)}
              />
            </div>
          ))}
        </div>
      </div>
      {filterOptionValue === 'rents' && (
        <div className="py-4">
          <p className="mb-2 text-lg font-semibold">{t('propertyMinLease')}</p>
          <div className="flex flex-wrap">
            {budgetTypes.map((type) => (
              <div key={type.id} className="md:w-1/2 xl:w-1/3 mb-2 px-1">
                <SelectButton
                  onSelectChange={handleMinLeaseChange}
                  label={type.name}
                  value={type.id}
                  isMobile={isMobile}
                  selected={rentalsParams.minLease === type.id}
                />
              </div>
            ))}
          </div>
        </div>
      )}

      <div className="py-4">
        <p className="mb-2 text-lg font-semibold">{t('propertyFloorSize')}</p>
        <FloorSize />
      </div>
      <div className="py-4">
        <p className="mb-2 text-lg font-semibold">{t('propertyAmenities')}</p>
        <div className="flex flex-wrap">
          {amenities.map((amenity) => (
            <div key={amenity.value} className="mb-2 mr-2">
              <SelectButton
                onSelectChange={handleAmenitiesChange}
                label={amenity.name}
                value={amenity.value}
                isMobile={isMobile}
                selected={rentalsParams.amenities.includes(amenity.value)}
              />
            </div>
          ))}
        </div>
      </div>
      <div className="py-4">
        <p className="mb-2 text-lg font-semibold">{`${radiusTitle}:`}</p>
        <div className="w-full md:w-4/12 mb-3">
          <InputNumber
            defaultValue={rentalsParams.radius}
            min={0}
            className="w-full fs-filter-options__radius"
            onChange={setValue(Params.Radius)}
          />
        </div>
      </div>
    </div>
  );
};

export default ListingsMoreFilter;
