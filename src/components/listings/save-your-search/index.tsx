import { useSelector } from 'react-redux';
import cntl from 'cntl';

import Svg from '@components/core/svg';
import { selectIsMobileOnly } from '@stores/slices/common';

function ListingsSaveYourSearch() {
  const isMobileOnly = useSelector(selectIsMobileOnly);

  return (
    <div className={classes.container(isMobileOnly)}>
      <Svg className="mr-2 w-7 h-7" name="red-pentagon" />
      Save your search
    </div>
  );
}

const classes = {
  container: (isMobileOnly: boolean) => cntl`
    flex items-center 
    ${
      isMobileOnly
        ? `
            fixed bottom-4 left-1/2 transform -translate-x-1/2
            px-10 py-3
            bg-white rounded-xl shadow-lg
          `
        : 'xl:mt-2 xl:ml-2'
    }
  `
};

export default ListingsSaveYourSearch;
