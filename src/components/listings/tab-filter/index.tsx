import Svg from '@components/core/svg';
import { ListingTabs } from '@constants/enum';
import cntl from 'cntl';

interface Props {
  title: string;
  tab: ListingTabs;
  activeTabName: ListingTabs;
  onSelectChange?: (value: ListingTabs) => void;
}

const ListingsTab = ({ onSelectChange, ...props }: Props) => {
  const isActive = props.tab === props.activeTabName;

  const handleClick = () => {
    const tab = isActive ? ListingTabs.Default : props.tab;
    onSelectChange && onSelectChange(tab);
  };

  return (
    <button
      className={classes.button(isActive, `tab-filter__${props.tab.toLowerCase()}`)}
      data-testid={props.tab}
      onClick={handleClick}>
      {props.title}
      <Svg className={classes.arrowIcon(isActive)} name="down-arrow" />
    </button>
  );
};

const classes = {
  button: (isActive: boolean, className: string) => cntl`
    group flex-1 md:flex-initial
    md:w-32 h-11
    ml-2 first:ml-0 px-4 py-1
    text-base rounded-10
    focus:outline-none
    hover:bg-primary-pink hover:border hover:border-primary-orange hover:font-bold
    flex justify-center items-center
    ${isActive ? 'bg-primary-pink font-bold border border-primary-orange' : 'bg-secondary-darkwhite'}
    ${className}
  `,
  arrowIcon: (isActive: boolean) => cntl`
    h-2 w-3.5 ml-1
    fill-current
    transform
    ${isActive ? 'rotate-180' : 'text-footer-black'}
  `
};

export default ListingsTab;
