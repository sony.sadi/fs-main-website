import React, { useCallback, useMemo } from 'react';
import { useDispatch } from 'react-redux';
import debounce from 'lodash/debounce';
import cntl from 'cntl';
import CardRentalLink from '@components/common/cards/rental-link';
import { Listing } from '@interfaces/listing';
import { searchActions } from '@stores/slices/search';
import { Page } from '@constants/enum';
import { SerpType } from '@flexstay.rentals/fs-url-stringify';

interface Props {
  listings: Listing[];
  page?: Page;
  serpType?: SerpType;
}

function ListingsCards({ listings, page, serpType }: Props) {
  const dispatch = useDispatch();

  const handleMouseOver = useCallback(
    debounce((rental: Listing) => {
      dispatch(searchActions.setActiveRental(rental));
    }, 500),
    []
  );

  const renderListingsCards = useMemo(() => {
    return listings.map((item) => {
      return (
        <CardRentalLink
          key={item.id}
          deal={item}
          page={page}
          cardProps={{
            className: classes.deal,
            onMouseOver: handleMouseOver,
            showId: page === Page.Propertypad,
            isAllowContentVisibility: true
          }}
          serpType={serpType}
          linkProps={{
            className: classes.link
          }}
        />
      );
    });
  }, [listings]);

  return <>{renderListingsCards}</>;
}

const classes = {
  deal: cntl`bg-secondary-darkwhite h-full`,
  link: cntl`w-full`
};

export default ListingsCards;
