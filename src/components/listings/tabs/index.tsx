import React from 'react';
import TabFilter from '@components/listings/tab-filter';
import { ListingTabs } from '@constants/enum';

interface Tab {
  title: string;
  name: ListingTabs;
}

interface Props {
  tabs: Tab[];
  activeTabName: ListingTabs;
  selectTab?: (value: ListingTabs) => void;
}

const ListingsTabsFilter = ({ selectTab, ...props }: Props) => {
  const handleSelectChange = (value: ListingTabs) => {
    selectTab && selectTab(value);
  };

  return (
    <>
      {props.tabs.map((tab, index) => (
        <TabFilter
          key={index}
          title={tab.title}
          tab={tab.name}
          activeTabName={props.activeTabName}
          onSelectChange={handleSelectChange}
        />
      ))}
    </>
  );
};

export default React.memo(ListingsTabsFilter);
