import React from 'react';
import { useSelector } from 'react-redux';

import Pagination from '@components/common/pagination';
import { selectParams, selectTotalPages } from '@stores/slices/search';

interface Props {
  onChange: (page: number) => void;
}

function ListingsPagination(props: Props) {
  const totalPages = useSelector(selectTotalPages);
  const rentalsParams = useSelector(selectParams);

  if (totalPages <= 1) return null;

  return <Pagination className="mt-6" onPageChange={props.onChange} pageCount={totalPages} page={rentalsParams.page} />;
}

export default React.memo(ListingsPagination);
