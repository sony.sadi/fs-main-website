import { HTMLAttributes, useState, useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';
import cntl from 'cntl';
import omit from 'lodash/omit';

import { searchActions, selectParams, selectSearchAddress } from '@stores/slices/search';
import { selectIsMobileOnly } from '@stores/slices/common';
import { ListingTabs, Page } from '@constants/enum';
import { Params, PoiType, getDefaultRadius, Language } from '@flexstay.rentals/fs-url-stringify';
import { PoiSuggestion, SearchRequest } from '@interfaces/search';
import ListingsBed from '@components/listings/bed';
import ListingsMoreFilter from '@components/listings/more-filter';
import ListingsPriceRange from '@components/listings/price-range';
import ListingsTabsFilter from '@components/listings/tabs';
import { getFilteringTabs } from '@utils/search';
import PoisAutocomplete from '@components/common/pois-autocomplete';

export interface Props extends HTMLAttributes<HTMLDivElement> {
  onSearch: () => void;
  onLocationChange: (params: SearchRequest) => void;
  page: Page;
}

const ListingsFilterOptions = ({ ...props }: Props) => {
  const { t, i18n } = useTranslation();
  const language = i18n.language as Language;
  const dispatch = useDispatch();
  const isMobileOnly = useSelector(selectIsMobileOnly);
  const rentalsParams = useSelector(selectParams);
  const searchAddress = useSelector(selectSearchAddress);
  const [activeTabName, setActiveTabName] = useState(ListingTabs.Default);
  const [isPoiSelecting, setIsPoiSelecting] = useState(false);

  const tabs = useMemo(() => getFilteringTabs(t), [t]);

  const handleInputChange = (term: string) => {
    dispatch(
      searchActions.setRentalsParams({
        ...rentalsParams,
        keyword: term,
        where: term
      })
    );
  };

  const handlePoiSelect = (poi: PoiSuggestion) => {
    setIsPoiSelecting(true);
    const address = poi.name[language];

    const params: SearchRequest = omit(
      {
        ...rentalsParams,
        [Params.Radius]: getDefaultRadius({ address, type: poi?.poi_type }),
        [Params.Where]: address,
        keyword: address,
        [Params.Coords]: ''
      },
      ['buildingId', 'areaId', 'transportationId', 'transportationSystem', 'building_type']
    );

    switch (poi.poi_type) {
      case PoiType.BUILDING:
        params.buildingId = poi.id;
        params.building_type = poi.building_type;
        break;
      case PoiType.AREA:
        params.areaId = poi.id;
        break;
      case PoiType.TRANSPORTATION:
        params.transportationId = poi.id;
        params.transportationSystem = poi.transportation_system;
        break;
      case PoiType.GOOGLE:
        params.coords = `${poi.lat}-${poi.lng}`;
        break;
      default:
        break;
    }

    setTimeout(() => {
      dispatch(searchActions.setRentalsParams(params));
      setIsPoiSelecting(false);
    }, 500);

    if (isMobileOnly) {
      params.page = 1;
      props.onLocationChange(params);
    }
  };

  const handleOnSelectChangeTab = useCallback(
    (value: ListingTabs) => {
      setActiveTabName(value);
    },
    [activeTabName]
  );

  const handleSearch = useCallback(() => {
    setActiveTabName(ListingTabs.Default);
    props.onSearch();
  }, [props.onSearch]);

  return (
    <div className={classes.container}>
      <div className="flex mb-3">
        <div className={classes.autoCompleteContainer}>
          <PoisAutocomplete
            isListingsResult
            placeholder={t('whereToStay')}
            onInputChange={handleInputChange}
            onSelectChange={handlePoiSelect}
            onSearch={handleSearch}
            address={searchAddress}
          />
        </div>
        {!isMobileOnly && (
          <button
            id="search-target"
            data-testid="search-now"
            className={classes.searchBtn(isPoiSelecting)}
            disabled={isPoiSelecting}
            onClick={handleSearch}>
            {t('searchNow')}
          </button>
        )}
      </div>
      <div className={classes.tabFilterContainer}>
        <ListingsTabsFilter tabs={tabs} activeTabName={activeTabName} selectTab={handleOnSelectChangeTab} />
        {/* <ListingsSaveYourSearch /> */}
      </div>
      {activeTabName !== ListingTabs.Default && (
        <>
          <div className="flex py-8">
            {activeTabName === ListingTabs.Price && <ListingsPriceRange />}
            {activeTabName === ListingTabs.Bed && <ListingsBed />}
            {activeTabName === ListingTabs.MoreFilter && <ListingsMoreFilter page={props.page} />}
          </div>

          {isMobileOnly && (
            <div className="text-center">
              <button
                id="search-target"
                className={classes.applyFilterButton}
                data-testid="apply-filter"
                onClick={handleSearch}>
                {t('applyFilter')}
              </button>
            </div>
          )}
        </>
      )}
    </div>
  );
};

const classes = {
  container: cntl`
    flex flex-col h-auto pt-6
    pb-2 md:pb-10
    items-stretch fs-filter-options
  `,
  applyFilterButton: cntl`
    w-44 h-11
    text-lg font-medium text-white bg-primary-orange
    rounded-lg border-0 focus:outline-none
    fs-filter-options__search-button
  `,
  searchBtn: (disabled: boolean) => cntl`
    ml-4 px-4 py-1 h-11
    text-lg font-medium
    rounded-lg border-0 focus:outline-none
    fs-filter-options__search-button
    whitespace-nowrap
    ${disabled ? 'bg-secondary-gray text-primary-gray' : 'bg-primary-orange text-white'}
  `,
  autoCompleteContainer: cntl`
    w-full
  `,
  tabFilterContainer: cntl`
    flex flex-wrap justify-between md:justify-start
  `
};

export default ListingsFilterOptions;
