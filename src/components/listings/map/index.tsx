import { useRef, useState, useMemo, useCallback, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Map, Marker, GoogleApiWrapper, InfoWindow, IMapProps, IMarkerProps } from 'google-maps-react';
import debounce from 'lodash/debounce';

import { useTranslation } from 'next-i18next';
import { Coordinate } from '@interfaces/map';
import { selectRentals, selectActiveRental, selectParams } from '@stores/slices/search';
import {
  fetchRentalsCoords,
  fetchPropertyById,
  mapActions,
  selectActiveProperty,
  selectCenter,
  selectCoords
} from '@stores/slices/map';
import { getNumberOfMarker, getTextPrice } from '@utils';
import CompactRental from '@components/common/cards/compact-rental';
import ListingLink from '@components/listing/link';
import cntl from 'cntl';
import { LocaleNamespace, Page } from '@constants/enum';

const ZOOM = {
  DEFAULT: 12,
  MIN: 5,
  MAX: 20
};

interface Props extends IMapProps {
  language: string;
  isSearchingMap: boolean;
  page: Page;
}

const ListingsMap = (props: Props) => {
  const { isSearchingMap } = props;
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);
  const center = useSelector(selectCenter);
  const rentals = useSelector(selectRentals);
  const coords = useSelector(selectCoords);
  const activeProperty = useSelector(selectActiveProperty);
  const activeRental = useSelector(selectActiveRental);
  const listingParams = useSelector(selectParams);
  const gMap = useRef<any>();

  const [zoom, setZoom] = useState(ZOOM.DEFAULT);
  const [activeMarker, setActiveMarker] = useState<google.maps.Marker>();
  const [markerElements, setMarkerElements] = useState<{ [key: number]: google.maps.Marker }>({});

  /**
   *  Trigger active rental when User on Desktop hover to Rental item
   */
  useEffect(() => {
    if (!activeRental) return;

    dispatch(mapActions.setActiveProperty(activeRental));
    setActiveMarker(markerElements[activeRental.id]);
  }, [activeRental]);

  useEffect(() => {
    setZoom(ZOOM.DEFAULT);
    loadCoordinates(gMap.current?.map);
  }, [rentals.data]);

  const handleCloseInfoWindow = () => {
    setActiveMarker(undefined);
  };

  const handleDisplayInfoWindow = (coordinate: Coordinate) => {
    return (_?: IMarkerProps, marker?: google.maps.Marker) => {
      setActiveMarker(marker);

      const property = rentals.data.find((rental: { id: number }) => rental.id === coordinate.id);
      dispatch(fetchPropertyById(coordinate.id, property));
    };
  };

  /**
   * Load list of marker infor will be showed on viewport of map
   */
  const loadCoordinates = useCallback(
    debounce((map?: google.maps.Map) => {
      const bounds = map?.getBounds();

      if (bounds && isSearchingMap) {
        const ne = bounds.getNorthEast();
        const sw = bounds.getSouthWest();
        const zoom = map?.getZoom();
        const limit = getNumberOfMarker(Number(zoom));

        if (sw.lat() === ne.lat() || sw.lng() === ne.lng()) return;

        dispatch(
          fetchRentalsCoords({
            minMapLat: sw.lat(),
            minMapLng: sw.lng(),
            maxMapLat: ne.lat(),
            maxMapLng: ne.lng(),
            limit
          })
        );
      }
    }, 300),
    []
  );

  const handleMapZoomChanged = useCallback(
    (_?: IMapProps, map?: google.maps.Map) => {
      if (map) setZoom(Number(map.getZoom()));
      if (activeMarker) return;
      loadCoordinates(map);
    },
    [activeMarker]
  );

  const handleMarkerMounted = (element: any) => {
    if (element) {
      const key: number = element.props['data-key'];
      setMarkerElements((elements) => ({ ...elements, [key]: element.marker }));
    }
  };

  const renderMarkers = useMemo(
    () =>
      coords.data.map((coord) => {
        return (
          <Marker
            ref={handleMarkerMounted}
            key={coord.id}
            data-key={coord.id}
            position={{ lat: coord.gpsLat, lng: coord.gpsLong }}
            onClick={handleDisplayInfoWindow(coord)}
          />
        );
      }),
    [coords.data]
  );

  const renderPropertDetails = useCallback(() => {
    const { location } = window;
    let price = getTextPrice(activeProperty?.lowestPrice || 0, t);
    if (props.page === Page.Listings && listingParams.serp_type === 'sales')
      price = getTextPrice(activeProperty?.salePrice || 0, t, 'sales');

    return (
      activeProperty && (
        <ListingLink listing={activeProperty}>
          <CompactRental price={price} t={tCommon} {...activeProperty} />
        </ListingLink>
      )
    );
  }, [activeProperty, listingParams]);

  return (
    <div className="w-full sticky top-16 fs-map-pin">
      <Map
        {...props}
        ref={gMap}
        initialCenter={center}
        center={center}
        zoomControlOptions={{ position: google.maps.ControlPosition.RIGHT_BOTTOM }}
        streetViewControl={false}
        mapTypeControl={false}
        zoom={zoom}
        maxZoom={ZOOM.MAX}
        minZoom={ZOOM.MIN}
        onMouseout={handleCloseInfoWindow}
        onCenterChanged={handleMapZoomChanged}
        onZoomChanged={handleMapZoomChanged}
        // onDragend={handleMapZoomChanged}
        onClick={handleCloseInfoWindow}>
        {renderMarkers}
        <InfoWindow
          map={gMap.current?.map}
          google={google}
          marker={activeMarker || new google.maps.Marker()}
          visible={!!activeMarker}>
          <div className="w-64">{renderPropertDetails()}</div>
        </InfoWindow>
      </Map>
      <p className={classes.paragraph}>{t('listingShown', { shown: coords.data.length, total: rentals.total })}</p>
    </div>
  );
};

const classes = {
  paragraph: cntl`
    abs-horizontal-center bottom-3
    p-2 rounded bg-gray-400 text-white
  `
};

export default GoogleApiWrapper((props: Props) => {
  return {
    apiKey: String(process.env.NEXT_PUBLIC_GOOGLE_MAP_API_KEY),
    language: props.language,
    version: '3'
  };
})(ListingsMap);
