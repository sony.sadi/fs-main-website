import { useState, useCallback, useMemo, useRef, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';
import cntl from 'cntl';

import Svg from '@components/core/svg';
import { selectParams } from '@stores/slices/search';
import { getSortingOptions } from '@utils';
import { SortType } from '@flexstay.rentals/fs-url-stringify';
import { Page } from '@constants/enum';

export interface SelectOption {
  label: string;
  value: SortType;
}

interface Props {
  onSelect: (type: SortType) => void;
  defaultSortLabel?: string;
  page: Page;
}

const ListingsSort = (props: Props) => {
  const { t } = useTranslation();
  const searchParams = useSelector(selectParams);
  const [open, setOpen] = useState(false);
  const { defaultSortLabel = t('sortBy') } = props;
  const [isSalePage, setIsSalePage] = useState<boolean>(false);

  const containerRef = useRef<HTMLDivElement>(null);

  /**
   * Check is Sale page
   */
  useEffect(() => {
    const { location } = window;
    if ((props.page === Page.Listings && searchParams.serp_type === 'sales') || props.page === Page.Propertypad) {
      setIsSalePage(true);
      return;
    }
    setIsSalePage(false);
  }, [searchParams.serp_type]);

  useEffect(() => {
    document.addEventListener('click', handleClickOutside);
    return () => document.removeEventListener('click', handleClickOutside);
  }, []);

  const handleClickOutside = (e: MouseEvent) => {
    if (containerRef?.current) {
      if (!containerRef.current.contains(e.target as Node)) {
        setOpen(false);
      }
    }
  };

  const options = useMemo(
    () => getSortingOptions(t, isSalePage).filter(({ value }) => value !== searchParams.sortBy),
    [t, searchParams.sortBy, isSalePage]
  );
  const selectedLabel = useMemo(() => {
    const found = getSortingOptions(t, isSalePage).find(({ value }) => searchParams.sortBy === value);
    return found?.label || defaultSortLabel;
  }, [t, searchParams.sortBy, isSalePage]);

  const toggle = () => setOpen(!open);

  const handleChange = (value: SortType) => () => {
    props.onSelect && props.onSelect(value);
  };

  const renderOption = useCallback(
    (option: SelectOption, style = {}) => {
      return (
        <div
          style={style}
          className={classes.option}
          aria-hidden
          data-label={option.label}
          onClick={handleChange(option.value)}
          key={option.value}>
          <span className="text-primary-gray">{option.label}</span>
        </div>
      );
    },
    [handleChange]
  );

  return (
    <div ref={containerRef} className={classes.dropdown} aria-hidden onClick={toggle}>
      <div className={classes.selected}>
        {selectedLabel}
        <Svg className={classes.arrow(open)} name="down-arrow" />
      </div>
      <div className={classes.dropdownMenu(open)}>{options.map((option) => renderOption(option))}</div>
    </div>
  );
};

const classes = {
  dropdown: cntl`
    relative flex flex-col items-center
    md:w-56 p-2
    bg-secondary-darkwhite text-secondary-darkgray rounded-md
    fs-filter-options__sorting-dropdown
  `,
  dropdownMenu: (open: boolean) => cntl`
    absolute top-full overflow-y-auto z-10
    w-full mt-1 
    bg-secondary-darkwhite text-secondary-darkgray
    rounded-md shadow-xl
    fs-select-options
    ${open ? 'open border' : ''}
  `,
  selected: cntl`
    flex items-center justify-center 
    w-full text-sm cursor-pointer
  `,
  arrow: (open: boolean) => cntl`
    w-3.5 h-2 ml-1
    fill-current text-black
    transform ${open ? 'rotate-180' : ''}
  `,
  option: cntl`
    flex-center p-2 
    cursor-pointer 
    hover:bg-secondary-gray
    fs-select__option
  `
};

export default ListingsSort;
