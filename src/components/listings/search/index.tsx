/* eslint-disable react/no-danger */
import { useMemo, useCallback, useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import dynamic from 'next/dynamic';

import {
  searchActions,
  selectFilterOption,
  selectParams,
  selectSearchAddress,
  selectSearchTerm
} from '@stores/slices/search';
import { mapActions } from '@stores/slices/map';
import { SearchRequest, SearchResponse } from '@interfaces/search';
import { echoHTML, getCounterText, getPaginationText } from '@utils';
import { stringifyUrl, SortType, Params, SerpType, MinLease } from '@flexstay.rentals/fs-url-stringify';
import { useTranslation } from 'next-i18next';

import Layout from '@components/layout';
import ListingsFilterOptions from '@components/listings/filter-options';
import ListingsPagination from '@components/listings/pagination';
import Breadcrumb from '@components/breadcrumb';
import ListingsSort from '@components/listings/sort';
import ListingsCards from '@components/listings/cards';
import { BreadcrumbItem } from '@interfaces/breadcrumb';
import cntl from 'cntl';
import { generateUrlFromBreadcrumb } from '@utils/breadcrumb';
import BreadcrumbSchema from '@components/breadcrumb/schema';
import SchemaMarkupSerps from '@components/schema/serps';
import { AssembleSeoResponse } from '@interfaces/seo';
import { Language, Page } from '@constants/enum';
import { BuildingObject, getLayoutData } from '@utils/buildings';

const FsMapPin = dynamic(() => import('@components/listings/map'));

const PAGINATION_TEXT_LENGTH = 60;

interface ListingsSearchProps {
  rentals: SearchResponse;
  rentalsParams: SearchRequest;
  breadcrumbs: BreadcrumbItem[];
  handlePageChange?: (value: number) => void;
  handleSortChange?: (value: SortType) => void;
  isSearchingMap?: boolean;
  address?: string;
  defaultSortLabel?: string;
  defaultPagingText?: string;
  allowSchema?: boolean;
  seo?: AssembleSeoResponse;
  page: Page;
  buildingData: BuildingObject;
  serpType?: SerpType;
}

const ListingsSearch = ({
  rentals,
  rentalsParams,
  breadcrumbs,
  isSearchingMap = true,
  address,
  defaultSortLabel,
  defaultPagingText,
  allowSchema,
  seo,
  page,
  buildingData,
  serpType,
  ...props
}: ListingsSearchProps) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { t, i18n } = useTranslation();
  const language = i18n.language as Language;
  const [isShowMap, setIsShowMap] = useState(process.browser && window.innerWidth >= 1024);
  const filterOptionValue = useSelector(selectFilterOption);
  const searchTerm = useSelector(selectSearchTerm);
  const updatedRentalsParams = useSelector(selectParams);
  const searchAddress = useSelector(selectSearchAddress);

  /**
   * Set default filter option
   */
  useEffect(() => {
    if (serpType !== filterOptionValue) {
      dispatch(searchActions.setFilterOption(serpType || 'rents'));
    }
  }, [serpType]);

  const generatedBreadcrumbs = useMemo(() => {
    return generateUrlFromBreadcrumb({
      breadcrumbs,
      t,
      currentPath: { href: router.asPath },
      searchTerm: searchAddress,
      serpType
    });
  }, [breadcrumbs, t, router.asPath, searchAddress]);

  const titleText = useMemo(() => {
    /**
     * SEO title apply with `serp` page
     * calculated text apply with `propertypad` page
     */
    if (seo?.title) return seo.title;
    if (rentals.total) {
      return getPaginationText({
        total: rentals.total,
        page: rentals.page,
        pageSize: rentals.perPage,
        address: searchAddress,
        areaId: rentalsParams?.areaId,
        transportationId: rentalsParams?.transportationId,
        transportationSystem: rentalsParams?.transportationSystem,
        buildingId: rentalsParams?.buildingId,
        coords: rentalsParams?.coords,
        defaultPagingText,
        t
      });
    }
    if (page === Page.Propertypad) {
      return t('noPropertyPad');
    }
    return t('noProperty');
  }, [rentals, rentals.page, t, searchAddress, updatedRentalsParams.where, seo]);

  const descriptionText = useMemo(() => {
    return page === Page.Propertypad ? titleText : seo?.description;
  }, [titleText, seo, page]);

  const setMapCenter = useCallback(() => {
    const [lat, lng] = rentalsParams.coords.split(',');
    dispatch(
      mapActions.setCenter({
        lat: Number(lat),
        lng: Number(lng)
      })
    );
  }, [rentalsParams?.coords]);

  const search = useCallback(
    (params: SearchRequest) => {
      const currentParams = { ...params, serp_type: filterOptionValue };
      if (filterOptionValue === 'sales') {
        currentParams.minLease = MinLease.TwelveMonths;
      }
      dispatch(searchActions.setSearchTerm((params.keyword || params.where) as string));
      router.replace(stringifyUrl({ request: { ...currentParams, index_serp_types: ['rents'] }, t }).path);
      setMapCenter();
    },
    [dispatch, router, t, filterOptionValue]
  );

  const handlePageChange = useCallback(
    (value: number) => {
      dispatch(searchActions.setRentalsParam({ key: Params.Page, value }));
      search({ ...updatedRentalsParams, page: value, where: updatedRentalsParams.where || searchTerm });
    },
    [updatedRentalsParams, search, dispatch, searchTerm]
  );

  const handleSortChange = (value: SortType) => {
    if (page === Page.Propertypad) {
      return props.handleSortChange?.(value);
    }
    search({ ...updatedRentalsParams, page: 1, sortBy: value, where: updatedRentalsParams.where || searchTerm });
  };

  const handleSearch = () => {
    let newAddress = updatedRentalsParams.keyword || updatedRentalsParams.where || searchTerm;
    if (updatedRentalsParams.transportationId) {
      const splitSearchTerm = newAddress.split(' ');
      newAddress = `${
        splitSearchTerm[0] !== updatedRentalsParams.transportationSystem
          ? updatedRentalsParams.transportationSystem
          : ''
      } ${newAddress}`;
    }

    dispatch(searchActions.setSearchAddress(newAddress.trim()));
    handlePageChange(1);
  };

  const handleLocationChange = (params: SearchRequest) => {
    search(params);
  };

  const handleResizeWindow = () => {
    setIsShowMap(window.innerWidth >= 1024);
  };

  useEffect(() => {
    window.addEventListener('resize', handleResizeWindow);
    return () => {
      window.removeEventListener('resize', handleResizeWindow);
    };
  }, []);

  const counterText = useMemo(() => {
    /**
     * SEO title apply with `serp` page
     * calculated text apply with `propertypad` page
     */
    if (page === Page.Propertypad) {
      return getCounterText({
        total: rentals.total,
        page: rentals.page,
        pageSize: rentals.perPage,
        t
      });
    }
    return seo?.title_counter || '';
  }, [rentals, t]);

  const truncatedText = useMemo(() => {
    const paths = titleText.split(',');
    const displayPaths: string[] = [];
    let textLength = 0;
    for (let index = 0; index < paths.length; index++) {
      const path = paths[index];
      if (textLength + path.length >= PAGINATION_TEXT_LENGTH) {
        const words = path.split(' ');
        const displayWords: string[] = [];
        for (let i = 0; i < words.length; i++) {
          const word = words[i];
          textLength += word.length;
          displayWords.push(word);
          if (textLength >= PAGINATION_TEXT_LENGTH) break;
        }
        displayPaths.push(displayWords.join(' '));
        break;
      }
      textLength += path.length;
      displayPaths.push(path);
    }
    return displayPaths.join(',');
  }, [titleText, counterText]);

  const getShouldTrunate = useCallback(() => {
    return titleText.length > truncatedText.length;
  }, [titleText, truncatedText]);

  const [truncated, setTruncated] = useState(getShouldTrunate());

  const handleDotsClick = useCallback(() => {
    setTruncated(false);
  }, []);

  return (
    <>
      <BreadcrumbSchema breadcrumbs={generatedBreadcrumbs} />
      {allowSchema && (
        <SchemaMarkupSerps
          descriptionText={seo?.schema_description}
          paginationText={seo?.schema_title || titleText}
          rentals={rentals}
        />
      )}

      <Layout layoutData={getLayoutData({ buildingData })}>
        <div className={classes.container}>
          <div className="flex h-full">
            <div className="w-full lg:w-7/12 lg:pr-6">
              <ListingsFilterOptions page={page} onSearch={handleSearch} onLocationChange={handleLocationChange} />
              <div className={classes.sortContainer}>
                <div className="relative">
                  <div className={classes.titleContainer(truncated)}>
                    <h1 className={classes.sortPagination}>{titleText}</h1>
                    <span className="px-1" />
                    {!!counterText && <span className={classes.sortPagination}>{counterText}</span>}
                  </div>
                  {truncated && (
                    <div aria-hidden="true" className={classes.titleContainer(false)}>
                      <span className={classes.sortPagination}>{truncatedText}</span>
                      <span aria-hidden="true" onClick={handleDotsClick} className={classes.dots}>
                        ...
                      </span>
                      {!!counterText && <span className={classes.sortPagination}>{counterText}</span>}
                    </div>
                  )}
                </div>
                <ListingsSort page={page} onSelect={handleSortChange} />
              </div>
              <div className={classes.cardContainer}>
                <ListingsCards serpType={serpType} page={page} listings={rentals.data} />
              </div>
              <ListingsPagination onChange={props.handlePageChange ?? handlePageChange} />
              {!!descriptionText && (
                <div className={classes.pagination} dangerouslySetInnerHTML={{ __html: echoHTML(descriptionText) }} />
              )}
            </div>
            {isShowMap && (
              <div className={classes.mapContainer}>
                <FsMapPin page={page} isSearchingMap={isSearchingMap} language={language} />
              </div>
            )}
          </div>
        </div>
        <Breadcrumb list={generatedBreadcrumbs} />
      </Layout>
    </>
  );
};

const classes = {
  container: cntl`
    h-full px-4 lg:px-0 lg:pl-5 xl:pl-28
  `,
  sortContainer: cntl`
    flex flex-col md:flex-row md:justify-between md:items-center
    pt-5 pb-10
  `,
  sortPagination: cntl`
  my-2 md:my-0 inline text-lg font-bold
`,
  pagination: cntl`
    mt-6 px-5 md:px-24 text-template text-base
  `,
  cardContainer: cntl`
    grid grid-cols-1 md:grid-cols-2 gap-x-2 gap-y-5
  `,
  mapContainer: cntl`
    w-0 lg:w-5/12 fs-gmap hidden lg:block
  `,
  sortPaginationHidden: cntl`
  absolute w-full
  `,
  dots: cntl`
  text-lg font-bold inline
  px-2 cursor-pointer 
  `,
  titleContainer: (hidden: boolean) => cntl`
  text-center md:text-left 
  ${hidden ? 'w-0 h-0 overflow-hidden' : 'w-full'}`
};

export default ListingsSearch;
