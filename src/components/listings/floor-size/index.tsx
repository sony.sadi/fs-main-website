import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import debounce from 'lodash/debounce';

import RangeInputFloorSize from '@components/forms/range-input/floor-size';
import { Page } from '@constants/enum';
import { searchActions, selectParams } from '@stores/slices/search';

const ListingsFloorSize = () => {
  const dispatch = useDispatch();
  const params = useSelector(selectParams);

  const handleChange = useCallback(
    debounce((floorSizeGte: number, floorSizeLte: number) => {
      dispatch(
        searchActions.setRentalsParams({
          ...params,
          floorSize_gte: floorSizeGte,
          floorSize_lte: floorSizeLte
        })
      );
    }, 500),
    [params]
  );

  return (
    <RangeInputFloorSize
      className="listings-floor-size"
      page={Page.Listings}
      min={params.floorSize_gte}
      max={params.floorSize_lte}
      onChange={handleChange}
    />
  );
};

export default ListingsFloorSize;
