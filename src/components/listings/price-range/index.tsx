import { useCallback, useMemo, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import debounce from 'lodash/debounce';

import RangeInputPrice from '@components/forms/range-input/price';
import { searchActions, selectFilterOption, selectParams } from '@stores/slices/search';
import { MAX_FILTERING_PRICE, MAX_FILTERING_SALE_PRICE } from '@constants';
import { Page } from '@constants/enum';
import { SerpType } from '@flexstay.rentals/fs-url-stringify';

const ListingsPriceRange = () => {
  const dispatch = useDispatch();
  const params = useSelector(selectParams);
  const filterOptionValue = useSelector(selectFilterOption);
  const [filterState, setFilterState] = useState<SerpType>(filterOptionValue);

  const isSaleListing = useMemo(() => {
    if (filterOptionValue === 'sales') return true;
    return false;
  }, [filterOptionValue]);

  const handleChange = useCallback(
    debounce((minPrice: number, maxPrice: number) => {
      dispatch(
        searchActions.setRentalsParams({
          ...params,
          minPrice,
          maxPrice
        })
      );
    }, 500),
    []
  );

  return (
    <RangeInputPrice
      className="listings-price-range"
      page={Page.Listings}
      min={params.minPrice}
      max={params.maxPrice}
      railMax={isSaleListing ? MAX_FILTERING_SALE_PRICE : MAX_FILTERING_PRICE}
      onChange={handleChange}
    />
  );
};

export default ListingsPriceRange;
