import { useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import cntl from 'cntl';
import { useTranslation } from 'next-i18next';

import { getBedroomOptions } from '@utils/search';
import { NumberOfBed, Params } from '@flexstay.rentals/fs-url-stringify';
import { searchActions, selectParams } from '@stores/slices/search';
import { Language } from '@constants/enum';

function ListingsBeds() {
  const { i18n, t } = useTranslation();
  const dispatch = useDispatch();
  const searchParams = useSelector(selectParams);
  const options = useMemo(() => getBedroomOptions(t), [t]);

  const handleClick = (bed: NumberOfBed) => {
    return () => {
      dispatch(
        searchActions.setRentalsParam({
          key: Params.NumberBedrooms,
          value: bed
        })
      );
    };
  };

  return (
    <ul className={classes.options}>
      {options.map((option) => (
        <li
          aria-hidden
          key={option.value}
          className={classes.option({
            active: option.value === searchParams.numberBedrooms,
            isLongText: i18n.language === Language.TH
          })}
          data-label={option.label}
          onClick={handleClick(option.value)}>
          {option.label}
        </li>
      ))}
    </ul>
  );
}

const classes = {
  options: cntl`
    flex flex-wrap w-full 
  `,
  option: ({ active, isLongText }: { active: boolean; isLongText: boolean }) => cntl`
    flex flex-1 ${isLongText ? 'flex-initial mb-1' : 'md:flex-initial'} items-center justify-center
    md:min-w-24 h-10 md:h-11
    mr-3 px-2
    text-center text-base text-black
    border rounded-lg
    cursor-pointer
    hover:bg-primary-pink
    ${active ? 'bg-primary-pink border-primary-orange' : ''}
    listings-bed__option
  `
};

export default ListingsBeds;
