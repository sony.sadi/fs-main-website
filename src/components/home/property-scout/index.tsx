import React from 'react';
import cntl from 'cntl';
import { useTranslation } from 'next-i18next';

import Svg from '@components/core/svg';
import Link from '@components/core/link';
import { LocaleNamespace } from '@constants/enum';
import { selectIsMobile } from '@stores/slices/common';
import { useSelector } from 'react-redux';
import ourOffer from '@assets/data/property-scout.json';

function HomePropertyScout() {
  const { t } = useTranslation(LocaleNamespace.Home);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);

  const isMobile = useSelector(selectIsMobile);
  const offers = isMobile ? ourOffer.mobiles : ourOffer.desktops;

  return (
    <section id="why-rent-with-property-scout" className={classes.propertyScout}>
      <header className={classes.header}>
        <h2 className={classes.headerHeading}>{t('propertyScoutHeading')}</h2>
        <p className={classes.headerDescription}>{t('propertyScoutDescription')}</p>
      </header>

      <ul className={classes.ourOffers}>
        {offers.map((offer) => (
          <li className={classes.offerItem} key={offer.id}>
            <Svg name={offer.icon} className="w-28 h-28" />
            <strong className={classes.offerTitle}>{t(offer.title)}</strong>
            <p className="mt-2 text-base">{t(offer.description)}</p>
          </li>
        ))}
      </ul>

      <div className="text-center md:text-right">
        <Link href={tCommon('howDoesItWorkPath')} className={classes.linkContact}>
          {t('propertyScoutFindOutAboutUs')} <Svg className={classes.linkIcon} name="red-arrow-right" />
        </Link>
      </div>
    </section>
  );
}

const classes = {
  propertyScout: cntl`
    py-14
    px-4 sm:px-20 md:px-8 xl:px-14
  `,
  header: cntl`
    items-center mb-5 md:mb-10 xs:px-4 md:px-0 xl:pr-0 xl:pl-10
    text-center md:text-left
  `,
  headerHeading: cntl`text-4xl font-bold`,
  headerDescription: cntl`text-lg mt-1`,
  ourOffers: cntl`
    xl:mt-14
    grid md:grid-cols-2 xl:grid-cols-4
  `,
  offerItem: cntl`
    px-10 pt-5 pb-10
    group hover:shadow-2xl
    cursor-pointer
    grid justify-items-center xl:block
    rounded-3xl
    text-center xl:text-left 
  `,
  offerTitle: cntl`
    block
    mt-8
    text-lg font-bold group-hover:text-primary-orange
  `,
  linkContact: cntl`inline-flex items-center justify-center text-sm underline xs:mt-8 xl:mt-0 xl:mr-10`,
  linkIcon: cntl`ml-4 w-1 h-2`
};

export default React.memo(HomePropertyScout);
