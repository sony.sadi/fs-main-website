import React from 'react';
import OurPartners from '@components/common/our-partners';
import { Page } from '@constants/enum';

function HomeOurPartners() {
  return <OurPartners page={Page.Home} />;
}

export default React.memo(HomeOurPartners);
