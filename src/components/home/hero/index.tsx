import cntl from 'cntl';
import { useTranslation } from 'next-i18next';

import BasicSearch from '@components/basic-search';
import Svg from '@components/core/svg';
import { formatViewNumber } from '@utils/format';
import { Page } from '@constants/enum';

interface HomeHeroProps {
  statistics: { i18nKey: string; total: number }[];
}

const HomeHero = (props: HomeHeroProps) => {
  const { t } = useTranslation();

  const { statistics } = props;

  return (
    <>
      <header className={classes.search}>
        <div className={classes.contentContainer}>
          <h1 className={classes.primaryHeading}>{t('homePrimaryHeading')}</h1>
          <p className={classes.quote}>{t('homeQuote')}</p>
          <BasicSearch page={Page.Home} />
        </div>

        <span className="pb-4">
          <Svg className="w-4 h-5" name="mouse" />
        </span>
      </header>

      <footer className={classes.infoContainer}>
        <ul className={classes.infoListContainer}>
          {(statistics || []).map((stat) => (
            <li className={classes.stati} key={stat.i18nKey}>
              <div className="flex-center font-bold lowercase">
                <span className="text-2xl">{formatViewNumber(stat.total)}</span>
                <b className="text-3xl text-primary-orange">+</b>
              </div>
              <p className="mt-1 text-base">{t(stat.i18nKey)}</p>
            </li>
          ))}
        </ul>
      </footer>
    </>
  );
};

const classes = {
  search: cntl`
    relative
    flex flex-col items-center
    w-full h-home-hero-tablet
    bg-home-hero-mobile lg:bg-home-hero-tablet xl:bg-home-hero-desktop
    bg-center bg-no-repeat bg-cover
  `,
  contentContainer: cntl`
    flex-1 flex-center flex-col pt-6 px-4
  `,
  infoContainer: cntl`
    bottom-0 w-full bg-primary-dark py-4 px-4 
  `,
  infoListContainer: cntl`
    grid grid-cols-3 w-full max-w-2xl mx-auto
  `,
  primaryHeading: cntl`
    text-4xl md:text-45px mb-1 font-bold text-white text-center
  `,
  quote: cntl`
    mt-1 sm:mt-3 text-lg text-white text-center
  `,
  stati: cntl`
    flex flex-col items-center
    px-3
    text-white text-center
    border-r last:border-r-0 border-white
  `
};

export default HomeHero;
