import { DOMAttributes, useMemo } from 'react';
import Slider, { Settings } from 'react-slick';
import cntl from 'cntl';
import { useTranslation } from 'next-i18next';

import Svg from '@components/core/svg';
import Link from '@components/core/link';
import CardRentalLink from '@components/common/cards/rental-link';

import { LocaleNamespace } from '@constants/enum';
import { Listing } from '@interfaces/listing';
import { getDefaultParams, SortType, stringifyUrl } from '@flexstay.rentals/fs-url-stringify';
import clsx from 'clsx';
import styles from './BestRental.module.scss';

function NextArrow(props: DOMAttributes<HTMLSpanElement>) {
  return (
    <span aria-hidden className={clsx('group flex-center', styles['arrow--right'])} onClick={props.onClick}>
      <Svg className={clsx('group-hover:text-white', styles.arrow__svg)} name="right-arrow" />
    </span>
  );
}

function PrevArrow(props: DOMAttributes<HTMLSpanElement>) {
  return (
    <span aria-hidden className={clsx('group flex-center', styles['arrow--left'])} onClick={props.onClick}>
      <Svg className={clsx('group-hover:text-white', styles.arrow__svg)} name="left-arrow" />
    </span>
  );
}

interface HomeBestRentalsProps {
  deals: Listing[];
}

const HomeBestRentals = ({ deals }: HomeBestRentalsProps) => {
  const { t } = useTranslation(LocaleNamespace.Home);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);

  const settings: Settings = {
    dots: false,
    infinite: true,
    // autoplay: true,
    lazyLoad: 'ondemand',
    draggable: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,

    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          infinite: false,
          arrows: false,
          dots: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: '30px',

          dotsClass: styles.slider__dot_container,
          appendDots: (dots: React.ReactNode) => (
            <div>
              <ul className={clsx('searches__slick-dots', styles.slider__dot)}>{dots}</ul>
            </div>
          ),
          customPaging: () => {
            return <span className={clsx('searches__slick-paging', styles.slider__paging)} />;
          }
        }
      }
    ]
  };

  const cheapestSerpLink = useMemo(() => {
    const baseParams = getDefaultParams();
    return stringifyUrl({
      t: tCommon,
      request: { ...baseParams, sortBy: SortType.PriceLow, index_serp_types: ['rents'] }
    });
  }, [tCommon]);

  return (
    <section className={clsx('cnt-vis--auto', styles.section)}>
      <header className={styles.header}>
        <h2 className={styles.header__heading}>{t('bestRentalsHeading')}</h2>
        <p className={styles.header__description}>{t('bestRentalsDescription')}</p>
      </header>

      {deals && deals.length && (
        <Slider className={clsx('home-list__slider', styles.slider)} {...settings}>
          {deals.map((deal, index) => (
            <CardRentalLink
              key={index}
              deal={deal}
              linkProps={{
                className: clsx('best-rentals__slide', styles.deal)
              }}
            />
          ))}
        </Slider>
      )}

      <div className={styles.link__container}>
        <Link href={cheapestSerpLink.path} rel={cheapestSerpLink.rel} className={styles.link__contact}>
          {t('bestRentalsShowAllBestDeals')} <Svg className={styles.link__icon} name="red-arrow-right" />
        </Link>
      </div>
    </section>
  );
};

export default HomeBestRentals;
