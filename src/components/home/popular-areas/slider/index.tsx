import { useCallback } from 'react';
import Slider from 'react-slick';
import cntl from 'cntl';
import { useTranslation } from 'next-i18next';

import Link from '@components/core/link';
import CardAreaMobile from '@components/common/cards/area/mobile';
import { Area } from '@interfaces/search';
import { stringifyUrl, getDefaultParams } from '@flexstay.rentals/fs-url-stringify';
import { Language, LocaleNamespace } from '@constants/enum';
import clsx from 'clsx';
import styles from './PopularAreaSlider.module.scss';

interface Props {
  areas: Area[];
}

function PopularAreasSlider(props: Props) {
  const { i18n } = useTranslation();
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);
  const language = i18n.language as Language;

  const settings = {
    infinite: true,
    // autoplay: true,
    draggable: false,
    speed: 300,
    dotsClass: styles.slider__dot,
    slidesToShow: 2,
    rows: 1,
    slidesPerRow: 2,
    dots: true,
    arrows: false,
    centerMode: true,
    centerPadding: '20px',
    appendDots: (dots: React.ReactNode) => (
      <div>
        <ul className={clsx('area-mobile__slick-dots', styles.slider__dot_container)}>{dots}</ul>
      </div>
    ),
    customPaging: () => {
      return <div className={clsx(styles.slider__paging, 'area-mobile__slick-paging')} />;
    }
  };

  const getAreaLink = useCallback(
    (area: Area) => {
      const baseParams = getDefaultParams();
      return stringifyUrl({
        request: { ...baseParams, where: area.name[language], areaId: area.id, index_serp_types: ['rents'] },
        t: tCommon
      });
    },
    [language, tCommon]
  );

  return (
    <Slider {...settings} className="py-10">
      {props.areas.map((area) => {
        const areaLink = getAreaLink(area);
        return (
          <Link href={areaLink.path} rel={areaLink.rel} key={area.id}>
            <CardAreaMobile area={area} />
          </Link>
        );
      })}
    </Slider>
  );
}

export default PopularAreasSlider;
