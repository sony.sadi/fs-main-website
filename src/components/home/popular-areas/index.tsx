import { useMemo } from 'react';
import dynamic from 'next/dynamic';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

import Svg from '@components/core/svg';
import Link from '@components/core/link';

import { selectIsMobile } from '@stores/slices/common';
import { LocaleNamespace } from '@constants/enum';
import { Area } from '@interfaces/search';
import { getSerpBaseUrl } from '@utils';
import styles from './PopularArea.module.scss';

const CardAreaDesktop = dynamic(() => import('@components/common/cards/area/dekstop'));
const PopularAreasSlider = dynamic(() => import('@components/home/popular-areas/slider'), { ssr: false });

interface HomePopularAreasProps {
  areas: Area[];
}

function HomePopularAreas({ areas }: HomePopularAreasProps) {
  const { t } = useTranslation(LocaleNamespace.Home);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);
  const isMobile = useSelector(selectIsMobile);

  /**
   * Rent serp url
   */
  const serpUrl = useMemo(() => {
    return getSerpBaseUrl(tCommon);
  }, [tCommon]);

  /**
   * Sale serp url
   */
  const saleSerpUrl = useMemo(() => {
    return getSerpBaseUrl(tCommon, 'sales');
  }, [tCommon]);

  return (
    <section className={styles.container}>
      <header className={styles.header}>
        <h2 className={styles.header__heading}>{t('popularAreasHeading')}</h2>
        <p className={styles.header__description}>{t('popularAreasDescription')}</p>
      </header>
      {isMobile ? <PopularAreasSlider areas={areas} /> : <CardAreaDesktop areas={areas} />}

      <div className={styles.link__container}>
        <Link href={serpUrl.path} rel={serpUrl.rel} className={styles.link__contact}>
          {t('searchesShowAll')} <Svg className={styles.link__icon} name="red-arrow-right" />
        </Link>
        <Link href={saleSerpUrl.path} rel={saleSerpUrl.rel} className={styles.link__contact}>
          {t('searchesShowAllSales')} <Svg className={styles.link__icon} name="red-arrow-right" />
        </Link>
      </div>
    </section>
  );
}

export default HomePopularAreas;
