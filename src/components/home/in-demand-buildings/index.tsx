import React, { useCallback, useMemo } from 'react';

import Svg from '@components/core/svg';
import Link from '@components/core/link';
import { useTranslation } from 'next-i18next';
import { LocaleNamespace } from '@constants/enum';
import { InDemandBuilding } from '@interfaces/search';
import { getSerpBaseUrl } from '@utils';
import InDemandSlider from './slider';
import styles from './Indemand.module.scss';

interface HomeInDemandBuildingsProps {
  buildings: InDemandBuilding[];
}

function HomeInDemandBuildings({ buildings }: HomeInDemandBuildingsProps) {
  const { t } = useTranslation(LocaleNamespace.Home);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);

  /**
   * Rent base url
   */
  const serpBaseUrl = useMemo(() => {
    return getSerpBaseUrl(tCommon);
  }, [tCommon]);

  /**
   * Sale base url
   */
  const saleBaseUrl = useMemo(() => {
    return getSerpBaseUrl(tCommon, 'sales');
  }, [tCommon]);

  /**
   * Render slider
   */
  const renderSlider = useCallback(() => {
    return <InDemandSlider buildings={buildings} />;
  }, [buildings]);

  return (
    <section className={styles.container}>
      <header className={styles.header}>
        <h2 className={styles.header__title}>{t('buildingsHeading')}</h2>
        <p className={styles.header__description}>{t('buildingsDescription')}</p>
      </header>

      <div className={styles.slider}>{renderSlider()}</div>

      <div className={styles.link__container}>
        <Link href={serpBaseUrl.path} rel={serpBaseUrl.rel} className={styles.link__contact}>
          {t('searchesShowAll')} <Svg className={styles.link__icon} name="red-arrow-right" />
        </Link>
        <Link href={saleBaseUrl.path} rel={saleBaseUrl.rel} className={styles.link__contact}>
          {t('searchesShowAllSales')} <Svg className={styles.link__icon} name="red-arrow-right" />
        </Link>
      </div>
    </section>
  );
}

export default React.memo(HomeInDemandBuildings);
