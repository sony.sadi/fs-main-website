import React, { useCallback, useMemo } from 'react';
import cntl from 'cntl';
import Slider, { Settings } from 'react-slick';

import Link from '@components/core/link';
import CardBuilding from '@components/common/cards/building';
import { useTranslation } from 'next-i18next';
import { Language, LocaleNamespace } from '@constants/enum';
import { InDemandBuilding } from '@interfaces/search';
import { stringifyUrl, getDefaultParams } from '@flexstay.rentals/fs-url-stringify';
import clsx from 'clsx';
import styles from './IndemandSlider.module.scss';

interface InDemandSliderProps {
  buildings: InDemandBuilding[];
}

function InDemandSlider({ buildings }: InDemandSliderProps) {
  const { t, i18n } = useTranslation(LocaleNamespace.Home);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);
  const language = i18n.language as Language;

  const settings: Settings = {
    dots: true,
    infinite: false,
    // autoplay: true,
    // draggable: false,
    lazyLoad: 'ondemand',
    speed: 300,
    slidesToShow: 5,
    dotsClass: styles.slider__dot,
    centerMode: false,
    draggable: false,
    arrows: false,
    className: 'in-demand-buildings__slider',

    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1
          // dots: false
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          dots: true
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true,
          centerMode: true,
          centerPadding: '30px'
        }
      }
    ],
    appendDots: (dots: React.ReactNode) => (
      <div>
        <ul className={clsx(styles.slider__slick_dot, 'buildings__slick-dots')}>{dots}</ul>
      </div>
    ),
    customPaging: () => {
      return <span className={clsx('buildings__slick-paging', styles.slider__custom_paging)} />;
    }
  };

  const baseParams = useMemo(() => {
    return getDefaultParams();
  }, []);

  const getBuildingLink = useCallback(
    (building: InDemandBuilding) => {
      return stringifyUrl({
        request: {
          ...baseParams,
          where: building.name[language],
          building_type: building.building_type,
          buildingId: building.id,
          index_serp_types: ['rents']
        },
        t: tCommon
      });
    },
    [language, tCommon]
  );

  return (
    <>
      {buildings.length && (
        <Slider {...settings}>
          {buildings.map((building, index) => {
            const buildingLink = getBuildingLink(building);
            return (
              <Link className={styles.link} href={buildingLink.path} rel={buildingLink.rel} key={building.id}>
                <CardBuilding building={building} key={index} />
              </Link>
            );
          })}
        </Slider>
      )}
    </>
  );
}

export default InDemandSlider;
