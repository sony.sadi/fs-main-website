import React from 'react';
import Slider from 'react-slick';
import cntl from 'cntl';

import Link from '@components/core/link';
import Svg from '@components/core/svg';

import clsx from 'clsx';
import styles from './PopularSearchSlider.module.scss';

interface SearchDataLink {
  name: string;
  path: string;
  rel?: string | undefined;
}
interface SearchData {
  icon: string;
  link: { rent: SearchDataLink; sale: SearchDataLink };
}

interface PopularSearchSliderProps {
  searches: SearchData[];
}

function PopularSearchSlider({ searches }: PopularSearchSliderProps) {
  const settings = {
    dots: false,
    infinite: false,
    // autoplay: true,
    draggable: false,
    speed: 300,
    slidesToShow: 4,
    dotsClass: styles.slider__dot_container,

    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          dots: true
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          rows: 2,
          slidesPerRow: 2,
          dots: true
        }
      }
    ],
    arrows: false,
    appendDots: (dots: React.ReactNode) => (
      <div>
        <ul className={clsx('searches__slick-dots', styles.slider__dot)}>{dots}</ul>
      </div>
    ),
    customPaging: () => {
      return <span className={clsx('searches__slick-paging', styles.slider__paging)} />;
    }
  };

  return (
    <Slider {...settings} className={styles.slider}>
      {searches.map(({ link, icon }) => (
        <div key={icon}>
          <div className={styles.link_container}>
            <div className={styles.slider__item}>
              <Svg className={styles.slider__icon} name={icon} />
            </div>
            <Link href={link.rent.path} rel={link.rent.rel} className={styles.slider__name}>
              {link.rent.name}
            </Link>
            {/* <Link href={link.sale.path} rel={link.sale.rel} className={styles.slider__name}>
              {link.sale.name}
            </Link> */}
          </div>
        </div>
      ))}
    </Slider>
  );
}

export default PopularSearchSlider;
