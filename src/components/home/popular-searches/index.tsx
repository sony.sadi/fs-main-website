import React, { useMemo, useCallback } from 'react';
import { useTranslation } from 'next-i18next';
import cntl from 'cntl';

import Link from '@components/core/link';
import Svg from '@components/core/svg';
import { LocaleNamespace } from '@constants/enum';
import { getPopularSearches } from '@utils/assets';
import { getSerpBaseUrl } from '@utils';
import PopularSearchSlider from './slider';
import styles from './PopularSearch.module.scss';

function HomePopularSearches() {
  const { t: tHome, i18n } = useTranslation(LocaleNamespace.Home);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);

  const searches = useMemo(() => {
    return getPopularSearches({ tCommon, tHome });
  }, [i18n.language]);

  /**
   * Rent serp url
   */
  const serpUrl = useMemo(() => {
    return getSerpBaseUrl(tCommon);
  }, [tCommon]);

  /**
   * Sale serp url
   */
  const saleSerpUrl = useMemo(() => {
    return getSerpBaseUrl(tCommon, 'sales');
  }, [tCommon]);

  /**
   * Render slider
   */
  const renderSlider = useCallback(() => {
    return <PopularSearchSlider searches={searches} />;
  }, [searches]);

  return (
    <section className={styles.container}>
      <header className={styles.header}>
        <h2 className={styles.header__title}>{tHome('searchesHeading')}</h2>
        <p className={styles.header__description}>{tHome('searchesDescription')}</p>
      </header>

      {renderSlider()}
      <div className={styles.link__container}>
        <Link href={serpUrl.path} rel={serpUrl.rel} className={styles.link__contact}>
          {tHome('searchesShowAll')} <Svg className={styles.link__icon} name="red-arrow-right" />
        </Link>
        <Link href={saleSerpUrl.path} rel={saleSerpUrl.rel} className={styles.link__contact}>
          {tHome('searchesShowAllSales')} <Svg className={styles.link__icon} name="red-arrow-right" />
        </Link>
      </div>
    </section>
  );
}

export default React.memo(HomePopularSearches);
