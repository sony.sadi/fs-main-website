import React, { useMemo } from 'react';
import cntl from 'cntl';
import { useTranslation } from 'next-i18next';

import Link from '@components/core/link';
import Svg from '@components/core/svg';
import { LocaleNamespace } from '@constants/enum';
import { getSerpBaseUrl } from '@utils';

function HomeChoosingPropertyScout() {
  const { t } = useTranslation(LocaleNamespace.Home);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);

  const serpUrl = useMemo(() => {
    return getSerpBaseUrl(tCommon);
  }, [tCommon]);

  return (
    <section className={classes.chooseProperty}>
      <h3 className={classes.title}>{t('choosePropertyTitle')}</h3>
      <p className={classes.description}>{t('choosePropertyDescription')}</p>
      <Link href={serpUrl.path} rel={serpUrl.rel}>
        <Svg className={classes.icon} name="white-red-play" />
      </Link>
    </section>
  );
}

const classes = {
  chooseProperty: cntl`
    flex-center flex-col
    h-home-choosing-property-scout
    text-white
    bg-home-choosing-property-scout xl:bg-home-choosing-property-scout-desktop
    bg-no-repeat bg-cover bg-center
  `,
  title: cntl`text-5xl text-center font-bold mx-20`,
  description: cntl`px-4 text-center text-lg mt-3`,
  icon: cntl`w-14 h-14 mt-6 cursor-pointer`
};

export default React.memo(HomeChoosingPropertyScout);
