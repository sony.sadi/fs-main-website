import React from 'react';
import cntl from 'cntl';

import Link from '@components/core/link';
import Svg from '@components/core/svg';
import { useTranslation } from 'next-i18next';
import { LocaleNamespace } from '@constants/enum';

import ourServices from '@assets/data/our-services.json';

function HomeOurServices() {
  const { t } = useTranslation(LocaleNamespace.Home);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);

  return (
    <section className={classes.ourServices}>
      <header className={classes.header}>
        <h2 className={classes.headerTitle}>{t('ourServicesHeading')}</h2>
        <p className={classes.headerDescription}>{t('ourServicesDescription')}</p>
      </header>

      <div className={classes.servicesSection}>
        <ul className={classes.services}>
          {ourServices.map(({ icon, name, description }) => (
            <li key={name} className={classes.serviceItem}>
              <Svg className={classes.serviceIcon} name={icon} />
              <strong className={classes.serviceName}>{t(name)}</strong>
              <p className={classes.serviceDescription}>{t(description)}</p>
            </li>
          ))}
        </ul>
      </div>

      <div className="text-center md:text-right">
        <Link href={tCommon('listYourPropertyPath')} className={classes.linkContact}>
          {t('ourServicesWithUs')} <Svg className={classes.linkIcon} name="red-arrow-right" />
        </Link>
      </div>
    </section>
  );
}

const classes = {
  ourServices: cntl`bg-white py-14 px-4 sm:px-8 xl:px-0`,
  header: cntl`md:px-0 xl:px-24 text-center md:text-left`,
  headerTitle: cntl`text-4xl font-bold`,
  headerDescription: cntl`mt-3 text-lg`,
  servicesSection: cntl`mt-8 xl:pl-partner-tablet xl:pr-16`,
  services: cntl`
    pb-10
    grid gap-y-4 md:grid-cols-2 xl:grid-cols-4 xl:gap-y-7
  `,
  serviceItem: cntl`
    group hover:border hover:border-primary-orange hover:bg-primary-pink
    py-4 px-2 xl:px-6 xl:pl-11 xl:pr-8 xl:py-5
    cursor-pointer
    grid justify-items-center
    rounded-2xl xl:block text-center xl:text-left xl:h-item-partner
  `,
  serviceIcon: cntl`
    w-16 h-16 md:w-11 md:h-11
    group-hover:bg-primary-pink group-hover:border group-hover:border-primary-orange
  `,
  serviceName: cntl`xl:my-3 text-lg`,
  serviceDescription: cntl`font-light text-base xl:mt-2`,
  linkContact: cntl`
    xl:mr-24
    inline-flex items-center justify-center
    text-sm underline
  `,
  linkIcon: cntl`ml-4 w-1 h-2`
};

export default React.memo(HomeOurServices);
