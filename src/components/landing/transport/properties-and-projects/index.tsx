import { Language, LocaleNamespace } from '@constants/enum';
import { useTranslation } from 'next-i18next';
import { LandingTransportContent } from '@interfaces/landing-transport';
import React, { useMemo } from 'react';
import { Transportation } from '@interfaces/search';
import { getDefaultParams, stringifyUrl, toPoiNameSlug } from '@flexstay.rentals/fs-url-stringify';
import { Listing } from '@interfaces/listing';
import styles from './landing-properties-and-projects.module.scss';
import LandingSectionInfomation from '../section-infomation';
import LandingTransportPropertiesNearby from '../properties-nearby';

interface IProps {
  content: LandingTransportContent;
  transportation: Transportation;
  listingsForRent: Listing[];
  listingsForRentSale: Listing[];
}

const LandingTransportPropertiesAndProjects = ({
  content,
  transportation,
  listingsForRent,
  listingsForRentSale
}: IProps) => {
  const { i18n, t } = useTranslation(LocaleNamespace.LandingTransportation);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);
  const language = i18n.language as Language;

  const rentShowMoreProps = useMemo(() => {
    const { rel, path } = stringifyUrl({
      request: {
        ...getDefaultParams(),
        transportationId: transportation.id,
        transportationSystem: transportation.transportation_system,
        where: transportation.station[language]
      },
      t: tCommon
    });
    return {
      rel,
      href: path,
      content: content.hyperlink_rent[language]
    };
  }, [transportation, language, tCommon, content]);

  const rentSaleShowMoreProps = useMemo(() => {
    const { rel, path } = stringifyUrl({
      request: {
        ...getDefaultParams(),
        transportationId: transportation.id,
        transportationSystem: transportation.transportation_system,
        where: transportation.station[language],
        serp_type: 'sales'
      },
      t: tCommon
    });
    return {
      rel,
      href: path,
      content: content.hyperlink_sale[language]
    };
  }, [transportation, language, tCommon]);

  return (
    <>
      <LandingSectionInfomation
        id={t('siteId', {
          transportation_system: transportation.transportation_system.toLowerCase(),
          name: toPoiNameSlug(transportation.station[language])
        })}
        title={content.site_title[language]}
        content={content.site_content[language]}
      />
      <LandingTransportPropertiesNearby
        title={content.recent_rent_title[language]}
        properties={listingsForRent}
        showMoreProps={rentShowMoreProps}
      />
      <LandingTransportPropertiesNearby
        title={content.recent_sale_title[language]}
        properties={listingsForRentSale}
        showMoreProps={rentSaleShowMoreProps}
      />
      <LandingSectionInfomation
        element="h3"
        titleClassName={styles.child_title}
        title={content.top_five_title[language]}
        content={content.top_five_content[language]}
      />
    </>
  );
};

export default LandingTransportPropertiesAndProjects;
