import CardRentalLink from '@components/common/cards/rental-link';
import InfomationSection from '@components/common/infomation-section';
import SliderController from '@components/listing/basic-information/shared/slider-controller';
import { Listing } from '@interfaces/listing';
import clsx from 'clsx';
import React, { useRef } from 'react';
import Slider, { Settings } from 'react-slick';
import styles from './landing-properties-nearby.module.scss';

interface IProps {
  properties: Listing[];
  showMoreProps: {
    href: string;
    rel?: string;
    content: string;
  };
  title: string;
}

const MAX_VISIBILITY_ITEMS = 2;

const LandingTransportPropertiesNearby = ({ properties, showMoreProps, title }: IProps) => {
  if (!properties.length || !title) return <></>;
  const sliderRef = useRef<Slider>(null);

  const settings: Settings = {
    dots: false,
    infinite: properties.length > MAX_VISIBILITY_ITEMS,
    draggable: false,
    speed: 300,
    lazyLoad: 'ondemand',
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          infinite: false,
          arrows: false,
          dots: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: '0px',

          dotsClass: styles.dot_container,
          appendDots: (dots: React.ReactNode) => (
            <div>
              <ul className={clsx('searches__slick-dots', styles.dot)}>{dots}</ul>
            </div>
          ),
          customPaging: () => {
            return <span className={clsx('searches__slick-paging', styles.paging)} />;
          }
        }
      }
    ]
  };

  return (
    <InfomationSection titleClassName={styles.title} title={title}>
      <>
        <Slider ref={sliderRef} className={clsx('slick-left', styles.slider)} {...settings}>
          {properties.map((item) => (
            <CardRentalLink
              key={item.id}
              deal={item}
              cardProps={{
                cardContentClassName: styles.card_content
              }}
              linkProps={{
                className: styles.deal
              }}
            />
          ))}
        </Slider>
        <SliderController
          href={showMoreProps.href}
          rel={showMoreProps.rel}
          onPrevArrowClick={() => sliderRef.current?.slickPrev()}
          onNextArrowClick={() => sliderRef.current?.slickNext()}
          content={showMoreProps.content}
        />
      </>
    </InfomationSection>
  );
};

export default LandingTransportPropertiesNearby;
