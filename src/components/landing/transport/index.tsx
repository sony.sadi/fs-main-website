import Inquiry from '@components/listing/inquiry';
import { Listing } from '@interfaces/listing';
import { Transportation } from '@interfaces/search';
import { selectIsMobile } from '@stores/slices/common';
import get from 'lodash/get';
import React, { useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';
import { Language, LocaleNamespace, Page } from '@constants/enum';
import { LandingTransportContent } from '@interfaces/landing-transport';
import HeaderOverlay from '@components/common/header-overlay';
import { getImageUrl } from '@utils';
import ContactForm from '@forms/contact';
import LandingTransportFAQ from './faqs';
import LandingStationContent from './station-content';
import styles from './landing-transport.module.scss';
import LandingTransportLocationAndNeighborhood from './location-and-neighborhood';
import LandingTransportPropertiesAndProjects from './properties-and-projects';

interface IProps {
  transportation: Transportation;
  listingsForRent: Listing[];
  listingsForRentSale: Listing[];
  content: LandingTransportContent;
}

const spacingFromTop = 300;

const LandingTransportInformation = ({ transportation, listingsForRent, listingsForRentSale, content }: IProps) => {
  const isMobile = useSelector(selectIsMobile);
  const [shouldShowInquiry, setShouldShowInquiry] = useState(false);
  const { t: tContactForm } = useTranslation(LocaleNamespace.ContactForm);
  const { t, i18n } = useTranslation(LocaleNamespace.LandingTransportation);

  const imgWidth = isMobile ? 768 : 1354;
  const imgHeight = isMobile ? 270 : 246;

  const language = i18n.language as Language;

  useEffect(() => {
    if (isMobile) {
      const onWindowScroll = () => {
        setShouldShowInquiry(window.scrollY > spacingFromTop);
      };
      window.addEventListener('scroll', onWindowScroll);
      return () => {
        window.removeEventListener('scroll', onWindowScroll);
      };
    }
  }, []);

  const transportationSystemPlaceholder = useMemo(() => {
    return `/images/landing-transportation/${transportation.transportation_system}_Placeholder.png`;
  }, [transportation.transportation_system]);

  const placeholderImage = useMemo(() => {
    const transportationImage = get(transportation, 'medias[0]');
    return transportationImage
      ? getImageUrl(transportationImage, process.env.NEXT_PUBLIC_EGSWING_API_BASE_URL)
      : transportationSystemPlaceholder;
  }, [transportation, transportationSystemPlaceholder]);

  const contactTitle = useMemo(() => {
    return (
      <>
        {tContactForm('contactFormTranspoHead', {
          station: transportation.station
        })}
        <br />
        {tContactForm('contactFormTranspoDesc', {
          station: transportation.station
        })}
      </>
    );
  }, [tContactForm, transportation]);

  return (
    <div id="landing-page-container" className={styles.container}>
      <HeaderOverlay
        id="search-target"
        width={imgWidth}
        height={imgHeight}
        page={Page.LandingTransportDetail}
        src={placeholderImage}
        objectFit="cover"
        title={content.station_title[language]}
      />
      <div className={styles.secondary_nav} />
      <div className={styles.wrapper}>
        <div className={styles.col_left}>
          <LandingStationContent transportation={transportation} content={content} />
          <LandingTransportLocationAndNeighborhood transportation={transportation} content={content} />
          <LandingTransportPropertiesAndProjects
            listingsForRent={listingsForRent}
            listingsForRentSale={listingsForRentSale}
            transportation={transportation}
            content={content}
          />
          <LandingTransportFAQ
            faq_ask={content.faq_ask[language]}
            faqs={content.faqs}
            title={content.faq_title[language]}
          />
        </div>
        {!isMobile && (
          <div className={styles.col_right}>
            <div id={t('contactFormId')} className={styles.inquiry_sticky}>
              <ContactForm transportation={transportation} title={contactTitle} page={Page.LandingTransportDetail} />
            </div>
          </div>
        )}
      </div>
      {isMobile && shouldShowInquiry && (
        <Inquiry
          title={contactTitle}
          transportation={transportation}
          page={Page.LandingTransportDetail}
          id={t('contactFormId')}
          phoneNumber="+66 92 264 3444"
        />
      )}
    </div>
  );
};

export default LandingTransportInformation;
