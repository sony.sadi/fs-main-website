import { Language, LocaleNamespace } from '@constants/enum';
import { useTranslation } from 'next-i18next';
import { LandingTransportContent } from '@interfaces/landing-transport';
import React from 'react';
import InfomationLocationMap from '@components/common/infomation-location-map';
import { Transportation } from '@interfaces/search';
import styles from './landing-location-and-neighborhood.module.scss';
import LandingSectionInfomation from '../section-infomation';

interface IProps {
  content: LandingTransportContent;
  transportation: Transportation;
}

const LandingTransportLocationAndNeighborhood = ({ content, transportation }: IProps) => {
  const { i18n, t } = useTranslation(LocaleNamespace.LandingTransportation);
  const language = i18n.language as Language;

  return (
    <>
      <LandingSectionInfomation
        id={t('locationId')}
        title={content.location_title[language]}
        content={content.location_content[language]}
      />
      <InfomationLocationMap
        titleElement="h3"
        titleClassName={styles.map_title}
        coord={{ id: transportation.id, gpsLat: transportation.gps_lat, gpsLong: transportation.gps_long }}
        language={language}
        title={content.location_map_title[language]}
      />
    </>
  );
};

export default LandingTransportLocationAndNeighborhood;
