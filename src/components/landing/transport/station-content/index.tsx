/* eslint-disable react/no-danger */
import { Language, LocaleNamespace } from '@constants/enum';
import { toPoiNameSlug } from '@flexstay.rentals/fs-url-stringify';
import { LandingTransportContent } from '@interfaces/landing-transport';
import { Transportation } from '@interfaces/search';
import { useEffect, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import styles from './landing-station-content.module.scss';

interface IProps {
  content: LandingTransportContent;
  transportation: Transportation;
}

const LandingStationContent = ({ content, transportation }: IProps) => {
  const { i18n, t } = useTranslation(LocaleNamespace.LandingTransportation);
  const language = i18n.language as Language;

  const separation = useMemo(() => {
    return <span className={styles.separation}>&#x0007C;</span>;
  }, []);

  useEffect(() => {
    const handleHashchange = (e: Event) => {
      e.preventDefault();
    };
    window.addEventListener('hashchange', handleHashchange);
    return () => {
      window.removeEventListener('hashchange', handleHashchange);
    };
  }, []);

  return (
    <>
      <div
        className={styles.content}
        dangerouslySetInnerHTML={{
          __html: content.station_content[language]
        }}
      />
      <div className={styles.secondary_nav}>
        <div className={styles.link_wrapper}>
          <a className={styles.link} href={`#${t('locationId')}`}>
            {t('locationNavigation')}
          </a>
          {separation}
          {!!content.site_title[language] && (
            <>
              <a
                className={styles.link}
                href={`#${t('siteId', {
                  transportation_system: transportation.transportation_system.toLowerCase(),
                  name: toPoiNameSlug(transportation.station[language])
                })}`}>
                {t('siteNavigation', transportation)}
              </a>
              {separation}
            </>
          )}
          <a className={styles.link} href={`#${t('faqId')}`}>
            {t('faqNavigation')}
          </a>
        </div>
      </div>
    </>
  );
};

export default LandingStationContent;
