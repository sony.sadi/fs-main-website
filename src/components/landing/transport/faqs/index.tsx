/* eslint-disable react/no-danger */
import FAQItem from '@components/common/faq-item';
import InfomationSection from '@components/common/infomation-section';
import { Language, LocaleNamespace } from '@constants/enum';
import { useTranslation } from 'next-i18next';
import Image from '@components/core/image';
import { ReactHTML, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { selectIsMobile } from '@stores/slices/common';
import styles from './landing-transport-faqs.module.scss';

interface IProps {
  title: string;
  faqs: Array<{
    question: Record<Language, string>;
    answer: Record<Language, string>;
  }>;
  faq_ask: string;
}

const LandingTransportFAQ = (props: IProps) => {
  const { i18n, t } = useTranslation(LocaleNamespace.LandingTransportation);
  const language = i18n.language as Language;
  const isMobile = useSelector(selectIsMobile);

  useEffect(() => {
    const handleHashchange = (e: Event) => {
      e.preventDefault();
    };
    window.addEventListener('hashchange', handleHashchange);
    return () => {
      window.removeEventListener('hashchange', handleHashchange);
    };
  }, []);

  const handleClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    if (!isMobile) return;
    const target = e.target as Element;
    const tagName = target.tagName.toLowerCase() as keyof ReactHTML;
    if (tagName !== 'a') return;
    const href = target.getAttribute('href') as string;
    if (!/^#/.test(href)) return;
    e.preventDefault();
    window.location.hash = href;
    window.dispatchEvent(new Event('hashchange'));
  };

  return (
    <>
      <InfomationSection className={styles.margin_scroll} id={t('faqId')} title={props.title}>
        <div className={styles.main}>
          <div className={styles.image_wrapper}>
            <Image width={600} height={478} layout="responsive" src="/images/faq.png" className={styles.image} />
          </div>
          <div className={styles.faqs_wrapper}>
            {props.faqs.map((faq, index) => {
              return (
                <FAQItem key={index} index={index} question={faq.question[language]} answers={[faq.answer[language]]} />
              );
            })}
          </div>
        </div>
      </InfomationSection>
      <div
        aria-hidden="true"
        onClick={handleClick}
        className={styles.template}
        dangerouslySetInnerHTML={{
          __html: props.faq_ask
        }}
      />
    </>
  );
};

export default LandingTransportFAQ;
