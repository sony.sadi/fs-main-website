/* eslint-disable react/no-danger */
import React, { ReactHTML } from 'react';
import InfomationSection from '@components/common/infomation-section';
import styles from './landing-section-infomation.module.scss';

interface IProps {
  id?: string;
  title: string;
  content: string;
  element?: keyof ReactHTML;
  titleClassName?: string;
}

const LandingSectionInfomation = ({ id, title, content, element, titleClassName }: IProps) => {
  if (!title || !content) return <></>;

  return (
    <InfomationSection
      className={styles.margin_scroll}
      titleClassName={titleClassName}
      element={element}
      id={id}
      title={title}>
      <div
        className={styles.template}
        dangerouslySetInnerHTML={{
          __html: content
        }}
      />
    </InfomationSection>
  );
};

export default LandingSectionInfomation;
