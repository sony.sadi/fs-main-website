import dynamic from 'next/dynamic';
import { useSelector } from 'react-redux';
import { selectIsMobile } from '@stores/slices/common';

const HeaderDesktop = dynamic(() => import('@components/layout/header/desktop'));
const HeaderMobile = dynamic(() => import('@components/layout/header/mobile'));

const Header = () => {
  const isMobile = useSelector(selectIsMobile);
  return isMobile ? <HeaderMobile /> : <HeaderDesktop />;
};

export default Header;
