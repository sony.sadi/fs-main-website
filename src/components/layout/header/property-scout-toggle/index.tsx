/* eslint-disable max-len */
/* eslint-disable react/no-danger */
import React, { useEffect, useRef } from 'react';
import cntl from 'cntl';
import { useTranslation } from 'next-i18next';
import { selectPropertyPad } from '@stores/slices/search';
import { useSelector } from 'react-redux';

import Image from '@components/core/image';
import { useRouter } from 'next/router';

interface Props {
  className?: string;
  onDismiss: (e: MouseEvent | React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
}

function HeartIconOutline(classes: string) {
  return `<svg id="icon-outline-heart" class ="${classes}" viewBox="0 0 37 32">
  <path  d="M18.4 31.1c-0.3 0-0.6-0.1-0.9-0.3-1.5-1.1-15.4-10.5-17.2-18.9-0.6-3-0.1-5.9 1.5-8.1 1.5-2.1 3.8-3.4 6.7-3.7 1-0.1 2.9-0.2 5 0.7 1.9 0.7 3.6 2 4.9 3.6 1.3-1.6 3-2.9 4.9-3.6 2.2-0.8 4.1-0.8 5-0.7 2.8 0.3 5.1 1.6 6.7 3.7 1.6 2.2 2.1 5.1 1.5 8.1v0c-1.8 8.6-16.6 18.5-17.2 18.9-0.3 0.2-0.6 0.3-0.9 0.3zM18.3 29.3c0 0-0.1 0 0 0-0.1 0-0.1 0 0 0zM18.6 29.3c0 0 0 0 0 0s0 0 0 0zM9.6 1.9c-0.3 0-0.6 0-0.9 0-2.3 0.3-4.2 1.3-5.4 3-1.3 1.8-1.7 4.1-1.2 6.6 1.3 6.2 11.1 14.2 16.3 17.6 1.7-1.1 14.8-10.3 16.3-17.6v0c0.5-2.5 0.1-4.9-1.2-6.6-1.2-1.7-3.1-2.7-5.4-2.9-0.8-0.1-2.4-0.1-4.2 0.6-1.9 0.8-3.6 2.1-4.8 4l-0.8 1.2-0.8-1.2c-1.2-1.9-2.9-3.2-4.8-4-1.1-0.6-2.3-0.7-3.1-0.7z"></path>
  </svg>`;
}

function PropertyScoutToggle(props: Props) {
  const { t } = useTranslation();
  const { identifier, favouriteRentals } = useSelector(selectPropertyPad);
  const router = useRouter();
  const wrapperRef = useRef<HTMLDivElement>(null);

  const handleClick = (e: MouseEvent | React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    props.onDismiss(e);
    router.push(`/propertypad/${identifier}`);
  };

  useEffect(() => {
    const handleClickOutside = (e: MouseEvent) => {
      if (!wrapperRef.current) return;
      if (!wrapperRef.current.contains(e.target as Node)) {
        props.onDismiss(e);
      }
    };
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, []);

  return (
    <div ref={wrapperRef} className={`${classes.propertyScoutToggle} ${props?.className}`}>
      <div className="absolute fs-arrow-up-wrap">
        <div className="fs-arrow-up" />
      </div>
      <div className={classes.logoIconWrap}>
        <Image unoptimized src="/images/logo-icon.png" alt="PropertyScout Logo" width={45} height={42} />
      </div>
      <span className={classes.text}>{t('propertyPadPopoverTitle')}</span>
      <div className={classes.propertyPadToggleItem}>
        <div
          dangerouslySetInnerHTML={{
            __html: t('propertyPadInstruction', {
              html: HeartIconOutline(classes.heartIcon)
            })
          }}
        />
      </div>
      <div onClick={handleClick} aria-hidden className={classes.openPropertyPadPageButton}>
        <p dangerouslySetInnerHTML={{ __html: t('openPropertyPadPage', { count: favouriteRentals?.length || 0 }) }} />
      </div>
    </div>
  );
}

const classes = {
  logoIconWrap: cntl`
    flex items-center justify-items-center mt-4
  `,
  logoIcon: cntl`
    w-14 h-12
  `,
  text: cntl`
    mt-2 text-lg font-bold leading-5 text-center mb-2.5
  `,
  heartIcon: cntl`
    w-7 h-5 inline-block mx-1 fill-current text-primary-orange
  `,
  propertyPadToggleItem: cntl`
    rounded-xl border border-primary-orange text-center bg-primary-pink mb-2.5 mr-2.5 ml-2.5 flex 
    flex-row px-1 py-2
  `,
  openPropertyPadPageButton: cntl`
    rounded-xl border border-primary-orange text-center bg-primary-pink mb-2.5 mr-2.5 ml-2.5 flex 
    flex-row px-1 py-2 cursor-pointer
  `,
  propertyScoutToggle: cntl`
    mt-5 absolute top-full box-shadow
    property-scout-wrap rounded-xl bg-white  w-56 flex-col items-center
  `
};

export default PropertyScoutToggle;
