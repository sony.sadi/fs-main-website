import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import cntl from 'cntl';
import { useTranslation } from 'next-i18next';
import { getStoredState } from 'redux-persist';
import throttle from 'lodash/throttle';

import Svg from '@components/core/svg';
import Link from '@components/core/link';
import LanguageToggleDesktop from '@components/layout/header/language-toggle/desktop';
import { LocaleNamespace } from '@constants/enum';
import LogoTypeface from '@components/SvgLogos/LogoTypeface';
import { getSerpBaseUrl } from '@utils';
import { fetchPropertyPad, persistConfig, selectFavoriteRentals } from '@stores/slices/search';
import PropertyScoutToggle from '@components/layout/header/property-scout-toggle';

function HeaderDesktop() {
  const { t } = useTranslation(LocaleNamespace.Common);
  const wrapperRef = useRef<SVGSVGElement>(null);

  const links = useMemo(() => {
    const serpUrl = getSerpBaseUrl(t);
    const saleSerpUrl = getSerpBaseUrl(t, 'sales');
    return [
      { href: serpUrl.path, text: t('rent'), rel: serpUrl.rel, value: 'rent' },
      { href: saleSerpUrl.path, text: t('buyFilter'), rel: 'nofollow', value: 'sell' }, // FS-1463 use saleSerpUrl.rel when release home sale
      { href: t('listYourPropertyPath'), text: t('saleHeader'), value: 'list-your-property' }
    ];
  }, [t]);

  const [reachedTop, setReachedTop] = useState(true);
  const favoriteRentals = useSelector(selectFavoriteRentals);
  const [toggleActive, setToggleActive] = useState(false);
  const likeQuantity = favoriteRentals.length;
  const dispatch = useDispatch();

  const heartIconName = useMemo(() => {
    return favoriteRentals.length ? 'liked-heart' : 'outline-heart';
  }, [favoriteRentals]);

  useEffect(() => {
    document.body.style.overflowX = 'initial'; // to enable scrolling listener
    window.addEventListener('scroll', handleScroll, { passive: true });

    return () => {
      document.body.style.overflowX = 'hidden';
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  const handleToggle = () => {
    setToggleActive(!toggleActive);
  };

  const handleScroll = useCallback(
    throttle(() => {
      setReachedTop(window.pageYOffset <= 10);
    }, 500),
    []
  );

  const refreshCachedPad = async () => {
    const state: any = await getStoredState(persistConfig);
    if (state.propertypad && state.propertypad.identifier) {
      dispatch(fetchPropertyPad(state.propertypad.identifier));
    }
  };

  const onDismiss = (e: MouseEvent | React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    if (wrapperRef.current?.contains(e.target as Node)) return;
    setToggleActive(false);
  };

  return (
    <header className={classes.header({ reachedTop })}>
      <Link href="/" className="w-logo">
        <div onClick={() => refreshCachedPad()} aria-hidden="true">
          <LogoTypeface />
        </div>
      </Link>
      <nav className="flex flex-1 justify-between max-w-sm">
        {links.map((link) => (
          <Link
            dataLabel={link.value}
            href={link.href}
            key={link.href}
            rel={link.rel}
            className="group flex items-center text-lg font-bold">
            <Svg className={classes.pentagonSvg} name="red-pentagon" />
            {link.text}
          </Link>
        ))}
      </nav>
      <div className="flex-center pr-4">
        <LanguageToggleDesktop />
        <div className={classes.likeSection}>
          <span className={classes.likeQuantity(likeQuantity > 0)}>{likeQuantity}</span>
          <Svg ref={wrapperRef} onClick={handleToggle} className={classes.heartIcon} name={heartIconName} />
          {toggleActive && <PropertyScoutToggle onDismiss={onDismiss} className="flex" />}
        </div>
        <Link href={t('contactUsPath')} className={classes.listProperty}>
          {t('contactUs')}
        </Link>
      </div>
    </header>
  );
}

const classes = {
  header: ({ reachedTop }: { reachedTop: boolean }) => cntl`
    fixed top-0 z-50
    flex items-center justify-between
    w-full h-menu
    pl-16
    bg-white border-b
    ${!reachedTop && 'shadow-md'}
    header
  `,
  pentagonSvg: cntl`
    w-2 h-2 opacity-0 group-hover:opacity-100 mr-2
  `,
  listProperty: cntl`
    block ml-4 px-4 py-1 text-lg text-white bg-primary-orange font-medium rounded-lg
  `,
  likeSection: cntl`
    flex items-center ml-4 text-lg font-medium relative
  `,
  heartIcon: cntl`
    select-none w-6 h-5 ml-1.5 fill-current  cursor-pointer text-primary-orange
  `,
  likeQuantity: (show: boolean) => cntl`
    ${show ? 'visible' : 'invisible'} text-lg leading-6
  `
};

export default HeaderDesktop;
