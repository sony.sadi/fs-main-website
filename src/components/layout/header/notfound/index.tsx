import Link from '@components/core/link';
import LogoTypeface from '@components/SvgLogos/LogoTypeface';
import cntl from 'cntl';
import React, { useCallback, useEffect, useState } from 'react';
import throttle from 'lodash/throttle';

export const NotFoundHeader = () => {
  const [reachedTop, setReachedTop] = useState(true);

  useEffect(() => {
    document.body.style.overflowX = 'initial'; // to enable scrolling listener
    window.addEventListener('scroll', handleScroll, { passive: true });

    return () => {
      document.body.style.overflowX = 'hidden';
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  const handleScroll = useCallback(
    throttle(() => {
      setReachedTop(window.pageYOffset <= 10);
    }, 500),
    []
  );

  return (
    <header className={classes.header({ reachedTop })}>
      <Link href="/" className="w-logo">
        <LogoTypeface />
      </Link>
    </header>
  );
};

const classes = {
  header: ({ reachedTop }: { reachedTop: boolean }) => cntl`
      fixed top-0 z-50
      flex items-center justify-center
      w-full h-menu
      bg-white border-b
      ${!reachedTop && 'shadow-md'}
      header
    `
};

export default NotFoundHeader;
