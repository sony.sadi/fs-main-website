import React, { useCallback, useState, useEffect, useMemo, useRef } from 'react';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import cntl from 'cntl';
import { useTranslation } from 'next-i18next';
import throttle from 'lodash/throttle';

import Image from '@components/core/image';
import Link from '@components/core/link';
import Svg from '@components/core/svg';
import HeaderSearch from '@components/layout/header/search';
import HeaderMobileLink from '@components/layout/header/mobile/link';

import { Language, LocaleNamespace } from '@constants/enum';
import { selectIsMobileOnly } from '@stores/slices/common';
import { getMobileMenuLinks } from '@utils/assets';
import { selectFavoriteRentals } from '@stores/slices/search';
import LogoIcon from '@components/SvgLogos/LogoIcon';
import LogoTypeface from '@components/SvgLogos/LogoTypeface';
import LogoText from '@components/SvgLogos/LogoText';
import PropertyScoutToggle from '../property-scout-toggle';
import LanguageToggleMobile from '../language-toggle/mobile';

function HeaderMobile() {
  const { i18n, t } = useTranslation();
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);
  const { t: tContact } = useTranslation(LocaleNamespace.ContactForm);
  const router = useRouter();
  const isMobileOnly = useSelector(selectIsMobileOnly);
  const favoriteRentals = useSelector(selectFavoriteRentals);
  const heartIconName = favoriteRentals?.length ? 'liked-heart' : 'outline-heart';
  const likeQuantity = favoriteRentals?.length || 0;
  const wrapperRef = useRef<SVGSVGElement>(null);

  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const [isToggeActive, setIsToggleActive] = useState(false);
  const [passedSearchNow, setPassedSearchNow] = useState(false);
  const showSearch = passedSearchNow && isMobileOnly;

  const language = (i18n.language as Language) || Language.US;

  const mobileMenuLinks = useMemo(() => {
    return getMobileMenuLinks({ tCommon });
  }, [language]);

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, []);

  useEffect(() => {
    handleClose();
  }, [router.asPath]);

  const handleScroll = useCallback(
    throttle(() => {
      const target = document.querySelector('#search-target') as HTMLElement;
      if (!target) return;

      setPassedSearchNow(window.pageYOffset >= target.offsetTop + target.clientHeight);
    }, 500),
    []
  );

  const handleOpen = () => {
    document.body.classList.add('overflow-y-hidden');
    setIsMenuOpen(true);
    setIsToggleActive(false);
  };

  const handleClose = () => {
    document.body.classList.remove('overflow-y-hidden');
    setIsMenuOpen(false);
  };

  const handleToggle = () => {
    setIsToggleActive(!isToggeActive);
  };

  const onDismiss = (e: MouseEvent | React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    if (wrapperRef.current?.contains(e.target as Node)) return;
    setIsToggleActive(false);
  };

  return (
    <header className={classes.headerContainer}>
      <Link href="/" className={classes.logo}>
        <div className="logo-icon-mobile">
          <LogoIcon />
        </div>
        {!showSearch && (
          <figure className="ml-1 leading-none logo-text-mobile">
            <LogoText />
          </figure>
        )}
      </Link>
      {showSearch && (
        <div className="flex-1 mx-4" data-testid="header-search">
          <HeaderSearch />
        </div>
      )}

      <div className={classes.menuBarIconContainer}>
        <div className={classes.likeSection}>
          <span className={classes.quantityText(likeQuantity > 0)}>{likeQuantity}</span>
          <Svg
            ref={wrapperRef}
            onClick={handleToggle}
            className={classes.heartIcon(likeQuantity > 0)}
            name={heartIconName}
          />
          {isToggeActive && <PropertyScoutToggle onDismiss={onDismiss} className="flex z-50" />}
          <div className={classes.modal(isToggeActive)} aria-hidden onClick={() => setIsToggleActive(false)} />
        </div>
        <Svg className="w-7 h-5" name="red-menu-bar" data-testid="red-menu-bar" onClick={handleOpen} />
      </div>

      <div className={classes.menu(isMenuOpen)}>
        <div aria-hidden className={classes.closeButton} onClick={handleClose} />
        <Svg className={classes.closeIcon} name="close" onClick={handleClose} />
        <nav id="fs-mobile-menu" className={classes.nav}>
          <Link href="/" className="ml-4 logo-menu-mobile">
            <LogoTypeface />
          </Link>

          <LanguageToggleMobile />

          <ul className="mt-8">
            {mobileMenuLinks.map((link, index) => (
              <HeaderMobileLink key={index} indexItem={index} {...link} subItems={link.children} />
            ))}
          </ul>

          <Link href={t('contactUsPath')} className={classes.listProperty}>
            {tContact('contactFormContactHead')}
          </Link>
        </nav>
      </div>
    </header>
  );
}

const classes = {
  logo: cntl`
    flex items-end
  `,
  headerContainer: cntl`
    fixed top-0 z-50 
    flex items-center justify-between 
    h-menu-mobile w-full px-4 bg-white
  `,
  menu: (show: boolean) => cntl`
    absolute top-0 left-0
    w-full h-screen
    transform duration-300 ${show ? 'opacity-100' : 'translate-x-full opacity-0'}
    transition
  `,
  menuBarIconContainer: cntl`
   flex items-center
  `,
  closeButton: cntl`
    absolute top-0 left-0 w-full h-full 
    bg-black opacity-70
  `,
  closeIcon: cntl`
    absolute top-2 sm:top-6 left-2 sm:left-6 
    w-9 h-9 fill-current text-white
  `,
  nav: cntl`
    absolute top-0 right-0 w-full h-full max-w-xs sm:max-w-sm px-10 pt-6 pb-14 bg-white
  `,
  pentagonSvg: cntl`
    w-2 h-2 mr-2
  `,
  listProperty: cntl`
    block mt-4 ml-4 py-2 text-lg text-center text-white bg-primary-orange font-medium rounded-lg
  `,
  arrow: (visible: boolean) => cntl`
    w-3 h-4 transform transition ${visible ? 'rotate-180' : ''}
  `,
  likeSection: cntl`
    px-2 py-1 flex items-center mr-1 text-lg font-medium relative
  `,
  heartIcon: (isHaveQuantity: boolean) => cntl`
  select-none w-5 h-4 ${isHaveQuantity && 'ml-1.5'} fill-current text-primary-orange cursor-pointer
`,
  modal: (show: boolean) => cntl`
  ${show ? 'block' : 'hidden'} fixed top-0 right-0 bottom-0 left-0 z-10 mt-75px
    modal-back-ground
  `,
  quantityText: (show: boolean) => cntl`
    ${show ? 'visible' : 'invisible'} text-lg leading-6
  `
};

export default React.memo(HeaderMobile);
