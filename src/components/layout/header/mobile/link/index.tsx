import React, { useState } from 'react';
import cntl from 'cntl';
import { useTranslation } from 'next-i18next';

import Link from '@components/core/link';
import Svg from '@components/core/svg';

interface LinkProps {
  href: string;
  i18nKey: string;
}

interface Props extends LinkProps {
  subItems?: Array<LinkProps & { rel?: string }>;
  indexItem?: number;
  rel?: string;
}

function HeaderMobileLink(props: Props) {
  const { t } = useTranslation();
  const [collapsed, setCollapsed] = useState(true);

  const toggle = () => {
    setCollapsed(!collapsed);
  };

  return (
    <li className="py-3">
      <div className="flex items-center justify-between">
        <Link href={props.href} rel={props.rel} className="flex items-center text-2xl font-bold">
          <Svg className={classes.pentagon(collapsed)} name="red-pentagon" />
          {t(props.i18nKey)}
        </Link>
        {props.subItems && (
          <Svg
            data-testid={`mobile-arrow-${props.indexItem}`}
            className={classes.arrow(!collapsed)}
            name="down-arrow"
            onClick={toggle}
          />
        )}
      </div>
      {props.subItems && (
        <ul className={classes.subItems(collapsed)}>
          {props.subItems.map(({ i18nKey, href }) => (
            <Link key={i18nKey} href={href} className="block ml-4 underline py-2">
              {t(i18nKey)}
            </Link>
          ))}
        </ul>
      )}
    </li>
  );
}

const classes = {
  pentagon: (invisible: boolean) => cntl`
    w-2 h-2 mr-2 ${invisible ? 'invisible' : ''}
  `,
  arrow: (rotate: boolean) => cntl`
    w-3 h-4 transform transition ${rotate ? 'rotate-180' : ''}
  `,
  subItems: (collapsed: boolean) => cntl`
    transition duration-500 ${collapsed ? 'h-0 invisible opacity-0' : ''}
  `
};

export default React.memo(HeaderMobileLink);
