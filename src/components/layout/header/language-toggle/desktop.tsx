import { useState, useCallback, useMemo } from 'react';
import cntl from 'cntl';
import filter from 'lodash/filter';
import { useTranslation } from 'next-i18next';

import Svg from '@components/core/svg';
import { Language } from '@constants/enum';
import useChangeLanguage from '@hooks/useChangeLanguage';
import { getLanguageSelectOptions } from '@utils';
import { LANGUAGES } from '@constants';

const LanguageToggleDesktop = () => {
  const { i18n } = useTranslation();

  const [visible, setVisible] = useState(false);
  const language = (i18n.language as Language) || Language.US;
  const changeLanguage = useChangeLanguage();

  const languageSelectOptions = useMemo(() => {
    return getLanguageSelectOptions();
  }, []);

  const toggle = useCallback(() => {
    setVisible(!visible);
  }, [visible]);

  const handleChange = (selected: Language) => {
    if (selected === language) return;
    return async () => {
      await changeLanguage(selected);
      setVisible(false);
    };
  };

  if (languageSelectOptions.length <= 1) return <></>;

  return (
    <div className="relative w-14">
      <div className={classes.container(visible)}>
        <Svg
          id="fs-language-toggle"
          className="w-7 h-5 rounded"
          name={LANGUAGES[language].icon}
          data-testid={LANGUAGES[language].name}
          onClick={toggle}
        />
        {visible &&
          filter(languageSelectOptions, (option) => option.name !== language).map((lang) => (
            <Svg
              key={lang.name}
              className="w-7 h-5 mt-4 rounded"
              name={lang.icon}
              data-testid={lang.name}
              onClick={handleChange(lang.name)}
            />
          ))}
      </div>
    </div>
  );
};

const classes = {
  container: (visible: boolean) => cntl`
    px-4 py-2 bg-white rounded-xl cursor-pointer select-none
    ${visible ? 'absolute shadow-md transform -translate-y-1/4' : ''} 
  `
};

export default LanguageToggleDesktop;
