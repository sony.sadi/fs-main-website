import cntl from 'cntl';
import filter from 'lodash/filter';

import Svg from '@components/core/svg';
import { useCallback, useMemo, useState } from 'react';
import { LANGUAGES } from '@constants';
import { Language } from '@constants/enum';
import { useTranslation } from 'next-i18next';
import useChangeLanguage from '@hooks/useChangeLanguage';
import { getLanguageSelectOptions } from '@utils';

function LanguageToggleMobile() {
  const { i18n, t } = useTranslation();
  const [visible, setVisible] = useState(false);
  const language = (i18n.language as Language) || Language.US;

  const changeLanguage = useChangeLanguage();

  const languageSelectOptions = useMemo(() => {
    return getLanguageSelectOptions();
  }, []);

  const toggle = useCallback(() => {
    setVisible(!visible);
  }, [visible]);

  const handleChange = (selected: Language) => {
    return async () => {
      await changeLanguage(selected);
      setVisible(false);
    };
  };

  if (languageSelectOptions.length <= 1) return <></>;

  return (
    <div className="flex justify-between mt-10 ml-4">
      <ul>
        <li className={classes.selectedLang(visible)}>
          <Svg name={LANGUAGES[language].icon} className="w-7 h-5 rounded mr-2" />
          {LANGUAGES[language].text}
        </li>
        {filter(languageSelectOptions, (option) => option.name !== language).map((lang) => (
          <li aria-hidden key={lang.name} className={classes.lang(visible)} onClick={handleChange(lang.name)}>
            <Svg name={lang.icon} className="w-7 h-5 rounded mr-2" />
            {lang.text}
          </li>
        ))}
      </ul>
      <Svg className={classes.arrow(visible)} name="down-arrow" onClick={toggle} data-testid="selected-toggle" />
    </div>
  );
}

const classes = {
  lang: (visible: boolean) => cntl`
    flex mt-3 transition ${visible ? 'opacity-100' : 'opacity-0'}
  `,
  selectedLang: (disabled: boolean) => cntl`
    flex ${disabled ? 'disabled:opacity-40' : ''}
  `,
  arrow: (visible: boolean) => cntl`
    w-3 h-4 transform transition ${visible ? 'rotate-180' : ''}
  `
};
export default LanguageToggleMobile;
