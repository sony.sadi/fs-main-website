import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { useTranslation } from 'next-i18next';
import omit from 'lodash/omit';

import { searchActions, selectFilterOption, selectParams } from '@stores/slices/search';
import {
  stringifyUrl,
  PoiType,
  Params,
  getDefaultRadius,
  SearchRequest,
  Language
} from '@flexstay.rentals/fs-url-stringify';
import PoisAutocomplete from '@components/common/pois-autocomplete';
import { PoiSuggestion } from '@interfaces/search';

function HeaderSearch() {
  const { t, i18n } = useTranslation();
  const lang = i18n.language as Language;
  const dispatch = useDispatch();
  const filterValueOption = useSelector(selectFilterOption);
  const router = useRouter();
  const rentalsParams = useSelector(selectParams);

  const handleSearch = () => {
    router.push(
      stringifyUrl({ request: { ...rentalsParams, serp_type: filterValueOption, index_serp_types: ['rents'] }, t }).path
    );
  };

  const handleInputChange = (term: string) => {
    dispatch(
      searchActions.setRentalsParams({
        ...rentalsParams,
        buildingId: undefined,
        areaId: undefined,
        transportationId: undefined,
        transportationSystem: undefined,
        coords: '',
        keyword: term,
        where: term,
        radius: 0
      })
    );
  };

  const handleSelectChange = (poi: PoiSuggestion) => {
    const address = poi.name[lang];

    const params: SearchRequest = omit(
      {
        ...rentalsParams,
        [Params.Radius]: getDefaultRadius({ address, type: poi?.poi_type }),
        [Params.Where]: address,
        keyword: address,
        serp_type: filterValueOption,
        [Params.Coords]: ''
      },
      ['buildingId', 'areaId', 'transportationId', 'transportationSystem', 'building_type']
    );

    switch (poi.poi_type) {
      case PoiType.BUILDING:
        params.buildingId = poi.id;
        params.building_type = poi.building_type;
        break;
      case PoiType.AREA:
        params.areaId = poi.id;
        break;
      case PoiType.TRANSPORTATION:
        params.transportationId = poi.id;
        params.transportationSystem = poi.transportation_system;
        break;
      case PoiType.GOOGLE:
        params.coords = `${poi.lat}-${poi.lng}`;
        break;
      default:
        break;
    }

    setTimeout(() => {
      router.push(
        stringifyUrl({
          request: {
            ...params,
            index_serp_types: ['rents']
          },
          t
        }).path
      );
    }, 500);
  };

  return (
    <PoisAutocomplete
      isHeader
      placeholder={t('searchHere')}
      onInputChange={handleInputChange}
      onSelectChange={handleSelectChange}
      onSearch={handleSearch}
      isShowFilter
    />
  );
}

export default React.memo(HeaderSearch);
