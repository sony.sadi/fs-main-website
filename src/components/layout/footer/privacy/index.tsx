import { useCallback } from 'react';
import { useTranslation } from 'next-i18next';
import { IconSocial, LocaleNamespace } from '@constants/enum';
import { AllRightReserved } from '@interfaces/home';
import cntl from 'cntl';
import Link from '@components/core/link';
import Svg from '@components/core/svg';
import styles from './FooterPrivacy.module.scss';

interface Props {
  privacy: AllRightReserved;
}

const FooterPrivacy = (props: Props) => {
  const { t } = useTranslation(LocaleNamespace.Footer);

  const handleRenderIcon = useCallback((type) => {
    const icon = {
      [IconSocial.Instagram]: {
        name: 'red-instagram',
        className: 'w-4 h-4'
      },
      [IconSocial.Twitter]: {
        name: 'red-twitter',
        className: 'w-5 h-4'
      },
      [IconSocial.Linkedin]: {
        name: 'red-linkedin',
        className: 'w-5 h-4'
      }
    }[type as IconSocial];

    const iconName = icon.name;

    return <Svg className={icon.className} name={iconName} />;
  }, []);

  return (
    <div className={styles.privacy}>
      <div className={styles.year}>{t(props.privacy.year)}</div>

      <div className={styles.term}>
        {props.privacy.terms.map((term, index) => (
          <Link key={index} className={styles.term_label} href={term.path}>
            {t(term.label)}
          </Link>
        ))}
      </div>

      <div className={styles.social}>
        {props.privacy.socials.map((social, index) => (
          <a href={social.href} key={index} className={styles.social__item}>
            <div className={styles.social__icon}>{handleRenderIcon(social.icon)}</div>
          </a>
        ))}
      </div>
    </div>
  );
};

export default FooterPrivacy;
