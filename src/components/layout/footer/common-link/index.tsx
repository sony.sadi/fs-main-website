import cntl from 'cntl';
import Link from '@components/core/link';
import { Property } from '@interfaces/home';
import Svg from '@components/core/svg';
import clsx from 'clsx';
import styles from './FooterCommonLink.module.scss';

interface Props {
  title: string;
  idSection: string;
  linkData: {
    label: string;
    path: string;
    rel?: string;
  }[];
}

const CommonLink = (props: Props) => {
  const { title, idSection, linkData } = props;
  return (
    <>
      <input type="checkbox" id={`${idSection}`} className="slide-toggle__checkbox" />
      <label htmlFor={`${idSection}`} className={clsx('slide-toggle__label', styles.toggle_label)}>
        <h3 className={styles.property_title}>{title}</h3>
        <Svg className={clsx('slide-toggle__icon', styles.icon_arrow_bottom)} name="red-arrow-down" />
      </label>
      <div className={clsx('slide-toggle__content', styles.link_container)}>
        {linkData.map((item, index) => (
          <div key={index} className={styles.link}>
            <Link href={item.path} rel={item.rel} className={clsx('group', styles.link_item)}>
              <Svg className={clsx('group-hover:opacity-100', styles.petagon_icon)} name="red-pentagon" />
              {item.label}
            </Link>
          </div>
        ))}
      </div>
    </>
  );
};

const classes = {
  iconArrowBottom: cntl`slide-toggle__icon h-1.5 w-3 self-center lg:hidden`,
  slideToggleCheckbox: cntl`slide-toggle__checkbox`,
  slideToggleLabel: cntl`slide-toggle__label flex justify-between pb-2.5 lg:pb-0 lg:pointer-events-none`,
  slideToggleContent: cntl`slide-toggle__content`
};

export default CommonLink;
