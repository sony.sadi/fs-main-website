import { useMemo, useCallback } from 'react';
import cntl from 'cntl';
import Link from '@components/core/link';
import { Property } from '@interfaces/home';
import Svg from '@components/core/svg';
import { useTranslation } from 'next-i18next';
import { Area, InDemandBuilding } from '@interfaces/search';
import { Language, LocaleNamespace } from '@constants/enum';
import { getDefaultParams, stringifyUrl, SerpType } from '@flexstay.rentals/fs-url-stringify';
import orderBy from 'lodash/orderBy';
import clsx from 'clsx';
import CommonLink from '../common-link';
import styles from './FooterProperty.module.scss';

interface Props {
  property: Property;
  popularAreas: Area[];
  indemandBuildings: InDemandBuilding[];
  idSection: string;
  serpType: SerpType;
}
const FooterProperty = (props: Props) => {
  const { indemandBuildings, popularAreas, idSection, serpType } = props;
  const { t, i18n } = useTranslation(LocaleNamespace.Home);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);
  const { t: tFooter } = useTranslation(LocaleNamespace.Footer);
  const language = i18n.language as Language;

  const baseParams = useMemo(() => {
    return getDefaultParams(serpType);
  }, []);

  const getBuildingLink = useCallback(
    (building: InDemandBuilding) => {
      return stringifyUrl({
        request: {
          ...baseParams,
          where: building.name[language],
          building_type: building.building_type,
          buildingId: building.id,
          index_serp_types: ['rents']
        },
        t: tCommon
      });
    },
    [language, tCommon]
  );

  /**
   * Get popular area data
   */
  const indemandBuildingData = useMemo(() => {
    const data = indemandBuildings.map((value) => {
      const areaLink = getBuildingLink(value);
      return {
        label: value.name ? value.name[language] : '',
        path: areaLink.path,
        rel: areaLink.rel || ''
      };
    });

    return data;
  }, []);

  const getAreaLink = useCallback(
    (area: Area) => {
      return stringifyUrl({
        request: {
          ...baseParams,
          where: area.name[language],
          areaId: area.id,
          index_serp_types: ['rents']
        },
        t: tCommon
      });
    },
    [language, tCommon]
  );

  /**
   * Get popular area data
   */
  const popularAreaData = useMemo(() => {
    const datas = popularAreas.map((value) => {
      const areaLink = getAreaLink(value);
      return {
        label: value.name ? value.name[language] : '',
        path: areaLink.path,
        rel: areaLink.rel || ''
      };
    });

    const sortData = orderBy(datas, [(data) => data.label.toLowerCase()], ['asc']);

    return sortData;
  }, []);

  const generateTitle = {
    title: {
      sales: 'footerSalePropertyTitle',
      rents: 'footerPropertyTitle'
    },
    property: {
      sales: 'footerPropertiesSale',
      rents: 'footerPropertiesRent'
    },
    condo: {
      sales: 'footerCondoSale',
      rents: 'footerCondoRent'
    }
  };

  return (
    <div className={styles.property}>
      <input type="checkbox" id={idSection} className="slide-toggle__checkbox" />
      <label htmlFor={idSection} className={clsx('slide-toggle__label', styles.slide_toggle_label)}>
        <h3 className={styles.property__title}>{tFooter(generateTitle.title[serpType])}</h3>
        <Svg className={clsx('slide-toggle__icon', styles.icon_arrow_bottom)} name="red-arrow-down" />
      </label>
      <div className="slide-toggle__content">
        <div className={styles.link_container}>
          {props.property.items.map((item, index) => (
            <div key={index} className={styles.link}>
              <Link href={item.path} rel={item.rel} className={clsx('group', styles.link_item)}>
                <Svg className={clsx('group-hover:opacity-100', styles.pentagon_icon)} name="red-pentagon" />
                {item.label}
              </Link>
            </div>
          ))}
        </div>

        <div className={clsx('mt-6', styles.link_container)}>
          {popularAreaData.map((item, index) => (
            <div key={index} className={styles.link}>
              <Link href={item.path} rel={item.rel} className={clsx('group', styles.link_item)}>
                <Svg className={clsx('group-hover:opacity-100', styles.pentagon_icon)} name="red-pentagon" />
                {tFooter(generateTitle.property[serpType], { place: item.label })}
              </Link>
            </div>
          ))}
        </div>
        <div className={clsx('mt-6', styles.link_container)}>
          {indemandBuildingData.map((item, index) => (
            <div key={index} className={styles.link}>
              <Link href={item.path} rel={item.rel} className={clsx('group', styles.link_item)}>
                <Svg className={clsx('group-hover:opacity-100', styles.pentagon_icon)} name="red-pentagon" />
                {tFooter(generateTitle.condo[serpType], { place: item.label })}
              </Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default FooterProperty;
