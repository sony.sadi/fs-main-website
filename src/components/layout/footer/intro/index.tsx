import { useTranslation } from 'next-i18next';
import cntl from 'cntl';

import { LocaleNamespace } from '@constants/enum';
import { Property } from '@interfaces/home';
import CommonLink from '../common-link';
import styles from './FooterIntro.module.scss';

interface Props {
  intros: string[];
  property: Property;
}

const FooterIntro = (props: Props) => {
  const { t } = useTranslation(LocaleNamespace.Footer);

  return (
    <div className={styles.intro}>
      <h3 className={styles.title}>{t('propertyScout')}</h3>
      {props.intros.map((intro, index) => (
        <p className={styles.text} key={index}>
          {intro}
        </p>
      ))}
      <div className={styles.common_link}>
        <CommonLink idSection="slide-toggle-about" title={props.property.title} linkData={props.property.items} />
      </div>
    </div>
  );
};

export default FooterIntro;
