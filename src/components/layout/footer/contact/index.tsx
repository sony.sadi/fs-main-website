import { Offices } from '@interfaces/home';
import cntl from 'cntl';
import styles from './FooterContact.module.scss';

interface Props {
  offices: Offices[];
}

const FooterContact = (props: Props) => {
  return (
    <>
      {props.offices.map((office, index) => (
        <div key={index} className={styles.office}>
          <b className={styles.office__name}>{office.name}</b>
          <p className={styles.office__name}>{office.building}</p>
          <p className={styles.office__name}>{office.address}</p>
        </div>
      ))}
    </>
  );
};

export default FooterContact;
