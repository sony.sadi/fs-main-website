import { AnchorHTMLAttributes, useCallback } from 'react';
import cntl from 'cntl';

import { IconContact } from '@constants/enum';
import Svg from '@components/core/svg';
import { Basics } from '@interfaces/home';
import { isUrl } from '@utils';
import clsx from 'clsx';
import styles from './FooterBasic.module.scss';

interface Props {
  basics: Basics[];
}

const FooterBasic = (props: Props) => {
  const renderIcon = useCallback((type) => {
    const icon = {
      [IconContact.Facebook]: {
        name: 'red-fb',
        className: 'w-2 h-3.5'
      },
      [IconContact.Line]: {
        name: 'red-line',
        className: 'w-3.5 h-3.5'
      },
      [IconContact.Phone]: {
        name: 'red-phone',
        className: 'w-3 h-3'
      },
      [IconContact.PhoneOutLine]: {
        name: 'red-phone-outline',
        className: 'w-4 h-4'
      },
      [IconContact.MailBox]: {
        name: 'red-mailbox',
        className: 'w-3.5 h-2.5'
      }
    }[type as IconContact];

    const iconName = icon.name;

    return <Svg className={icon.className} name={iconName} />;
  }, []);

  const renderContent = useCallback(
    (basics: Basics) => {
      const { content, href } = basics;
      const tagProps: AnchorHTMLAttributes<HTMLAnchorElement> = {
        href
      };
      if (isUrl(href)) {
        tagProps.target = '_blank';
      }
      return (
        <a className={styles.contact_text} {...tagProps}>
          {content}
        </a>
      );
    },
    [props.basics]
  );

  return (
    <>
      {props.basics.map((basic, index) => (
        <div className={styles.basic} key={index}>
          <div className={clsx('footer-contact-icon', styles.contact_icon)}>{renderIcon(basic.icon)}</div>
          {renderContent(basic)}
        </div>
      ))}
    </>
  );
};

export default FooterBasic;
