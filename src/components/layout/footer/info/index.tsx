import cntl from 'cntl';
import { useTranslation } from 'next-i18next';

import { Basics, Contact } from '@interfaces/home';
import Svg from '@components/core/svg';
import FooterContact from '@components/layout/footer/contact';
import FooterBasic from '@components/layout/footer/basic';
import { LocaleNamespace } from '@constants/enum';
import Link from '@components/core/link';
import clsx from 'clsx';
import styles from './FooterInfo.module.scss';

interface Props {
  basics: Basics[];
  contact: Contact;
}

const FooterInfo = (props: Props) => {
  const { t } = useTranslation(LocaleNamespace.Footer);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);

  return (
    <div className={styles.info_contact}>
      <input type="checkbox" id="slide-toggle-contact" className="slide-toggle__checkbox" />
      <label htmlFor="slide-toggle-contact" className={clsx('slide-toggle__label', styles.slide_toggle_label)}>
        <h3 className={styles.contact__title}>{t('footerContact')}</h3>
        <Svg className={clsx('slide-toggle__icon', styles.icon_arrow_bottom)} name="red-arrow-down" />
      </label>
      <div className="slide-toggle__contact">
        <div className={styles.basics}>
          <FooterBasic basics={props.basics} />
        </div>
        <Link href={tCommon('contactUsPath')}>
          <span className={styles.contact__button}>{t('footerContact')}</span>
        </Link>
      </div>

      <div>
        <FooterContact offices={props.contact.offices} />
      </div>
    </div>
  );
};

export default FooterInfo;
