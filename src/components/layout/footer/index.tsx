import { useMemo } from 'react';
import cntl from 'cntl';

import Link from '@components/core/link';
import FooterIntro from '@components/layout/footer/intro';
import FooterInfo from '@components/layout/footer/info';
import FooterPrivacy from '@components/layout/footer/privacy';
import FooterProperty from '@components/layout/footer/property';
import { getFooterAssets } from '@utils/assets';
import { LocaleNamespace } from '@constants/enum';
import { useTranslation } from 'next-i18next';
import LogoTypeface from '@components/SvgLogos/LogoTypeface';
import { BuildingObject } from '@utils/buildings';
import styles from './Footer.module.scss';

interface FooterProps {
  footerBuilding: BuildingObject;
}

const Footer = (props: FooterProps) => {
  const { footerBuilding } = props;

  const { t: tFooter } = useTranslation(LocaleNamespace.Footer);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);

  const footer = useMemo(() => {
    return getFooterAssets({ tFooter, tCommon });
  }, [tFooter, tCommon]);

  return (
    <div id="footer" className={styles.footer}>
      <Link className={styles.logo_link} href="/">
        <div className={styles.logo}>
          <LogoTypeface />
        </div>
      </Link>
      <div className={styles.top_footer}>
        <FooterIntro intros={footer.intros} property={footer.about} />
        <FooterProperty
          property={footer.property}
          indemandBuildings={footerBuilding.indemandBuilding}
          popularAreas={footerBuilding.popularAreas}
          serpType="rents"
          idSection="slide-toggle-properties-rent"
        />
        <FooterProperty
          property={footer.propertySale}
          indemandBuildings={footerBuilding.indemandBuilding}
          popularAreas={footerBuilding.popularAreas}
          serpType="sales"
          idSection="slide-toggle-properties-sales"
        />
        <FooterInfo basics={footer.basics} contact={footer.contact} />
      </div>
      <FooterPrivacy privacy={footer.allRightsReserved} />
    </div>
  );
};

export default Footer;
