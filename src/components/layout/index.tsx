import Header from '@components/layout/header';
import Footer from '@components/layout/footer';
import { BuildingObject } from '@utils/buildings';

interface LayoutData {
  footerBuilding: BuildingObject;
}

interface Props {
  children: React.ReactChild | React.ReactChild[];
  layoutData: LayoutData;
}

function Layout(props: Props) {
  return (
    <>
      <Header />
      <main className="pt-75px lg:pt-65px">{props.children}</main>
      <Footer {...props.layoutData} />
    </>
  );
}

export default Layout;
