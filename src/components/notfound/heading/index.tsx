import { LocaleNamespace } from '@constants/enum';
import cntl from 'cntl';
import { useTranslation } from 'next-i18next';

const NotFoundHeading = () => {
  const { t } = useTranslation(LocaleNamespace.NotFound);

  return (
    <>
      <h1 className={classes.errorMessage}>{t('errorMessage')}</h1>
      <p className={classes.ctaMessage}>{t('ctaMessage')}</p>
    </>
  );
};

const classes = {
  errorMessage: cntl`text-2xl md:text-3xl font-bold text-center`,
  ctaMessage: cntl`text-primary-orange text-xl 
  md:text-2xl font-bold text-center`
};

export default NotFoundHeading;
