/* eslint-disable react/no-danger */
import { useMemo } from 'react';
import Head from 'next/head';
import { SearchResponse } from '@interfaces/search';

import { getObjectSchemeSerps } from '@utils';
import { Language } from '@constants/enum';
import { useTranslation } from 'next-i18next';

interface SchemaMarkupSerpsProps {
  rentals: SearchResponse;
  paginationText: string;
  descriptionText?: string;
}

function SchemaMarkupSerps({ rentals, paginationText, descriptionText = '' }: SchemaMarkupSerpsProps) {
  const { i18n } = useTranslation();
  const schemaJsonString = useMemo(() => {
    const language = i18n.language as Language;
    const objectSchema = getObjectSchemeSerps({
      rentals,
      paginationText,
      language,
      descriptionText
    });

    return JSON.stringify(objectSchema);
  }, [rentals, paginationText]);

  return (
    <Head>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: schemaJsonString
        }}
      />
    </Head>
  );
}

export default SchemaMarkupSerps;
