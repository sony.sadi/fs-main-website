import Script from 'next/script';
import React, { useMemo } from 'react';

interface IProps {
  name: string;
  branchCode: string;
  openingHours: string;
  streetAddress: string;
  addressLocality: string;
  addressRegion: string;
  postalCode: string;
  addressCountry: string;
  latitude: number;
  longitude: number;
  url: string;
  image: string;
  additionalProperty: string;
  containedInPlace: string;
}

/**
 * see: https://schema.org/SubwayStation
 */
const SubwayStationSchema = (props: IProps) => {
  const schemaString = useMemo(() => {
    return JSON.stringify({
      '@context': 'https://schema.org',
      '@type': 'SubwayStation',
      name: props.name,
      branchCode: props.branchCode,
      openingHours: props.openingHours,
      address: {
        '@type': 'PostalAddress',
        streetAddress: props.streetAddress,
        addressLocality: props.addressLocality,
        addressRegion: props.addressRegion,
        postalCode: props.postalCode,
        addressCountry: props.addressCountry
      },
      latitude: props.latitude.toString(),
      longitude: props.longitude.toString(),
      url: props.url,
      image: props.image,
      additionalProperty: props.additionalProperty,
      containedInPlace: props.containedInPlace
    });
  }, [props]);

  return (
    <>
      <Script id="subway-station-schema" type="application/ld+json" strategy="afterInteractive">
        {`${schemaString}`}
      </Script>
    </>
  );
};

export default SubwayStationSchema;
