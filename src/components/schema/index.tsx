/* eslint-disable react/no-danger */
import { useMemo } from 'react';
import Head from 'next/head';

interface SchemaMarkupProps {
  data: Record<string, any>;
}

function SchemaMarkup({ data }: SchemaMarkupProps) {
  const schemaJsonString = useMemo(() => {
    return JSON.stringify(data);
  }, [data]);

  return (
    <Head>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: schemaJsonString
        }}
      />
    </Head>
  );
}

export default SchemaMarkup;
