import Script from 'next/script';
import { useMemo } from 'react';

interface IProps {
  name: string;
  faqs: Array<{ question: string; answer: string }>;
}

/**
 * see: https://schema.org/FAQPage
 */
const FaqSchema = ({ faqs, name }: IProps) => {
  const schemaString = useMemo(() => {
    return JSON.stringify({
      '@context': 'https://schema.org',
      '@type': 'FAQPage',
      name,
      mainEntity: faqs.map((faq) => ({
        '@type': 'Question',
        name: faq.question,
        acceptedAnswer: {
          '@type': 'Answer',
          text: faq.answer
        }
      }))
    });
  }, [faqs, name]);

  return (
    <>
      <Script id="faq-schema" type="application/ld+json" strategy="afterInteractive">
        {`${schemaString}`}
      </Script>
    </>
  );
};

export default FaqSchema;
