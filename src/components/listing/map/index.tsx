import { useMemo, useRef, useCallback, useEffect } from 'react';
import { Map, Marker, GoogleApiWrapper, IMapProps } from 'google-maps-react';
import { Coordinate } from '@interfaces/map';
import { useOnScreen } from '@hooks/useOnScreen';

interface Props extends IMapProps {
  language: string;
  coord: Coordinate;
  className?: string;
}

function ListingMap({ className, ...props }: Props) {
  const center: google.maps.LatLngLiteral = useMemo(
    () => ({
      lat: props.coord.gpsLat,
      lng: props.coord.gpsLong
    }),
    []
  );

  const mapRef = useRef<HTMLInputElement>(null);
  const currentMapVisible = useRef<boolean>(false);
  const isVisible = useOnScreen(mapRef, true);

  const renderMarker = () => {
    return <Marker key={props.coord.id} position={{ lat: props.coord.gpsLat, lng: props.coord.gpsLong }} />;
  };

  const renderListingMap = useCallback(() => {
    if (isVisible && !currentMapVisible.current) currentMapVisible.current = isVisible;
    if (!currentMapVisible.current) return null;
    return (
      <Map {...props} initialCenter={center} streetViewControl={false} mapTypeControl={false} zoom={17}>
        {renderMarker()}
      </Map>
    );
  }, [isVisible]);

  return (
    <div ref={mapRef} className={className || 'relative h-vh-60'}>
      {renderListingMap()}
    </div>
  );
}

export default GoogleApiWrapper((props: Props) => {
  return {
    apiKey: String(process.env.NEXT_PUBLIC_GOOGLE_MAP_API_KEY),
    language: props.language,
    version: '3'
  };
})(ListingMap);
