import { useCallback, useMemo } from 'react';
import { useSelector } from 'react-redux';
import cntl from 'cntl';
import take from 'lodash/take';

import { useTranslation } from 'next-i18next';
import Image from '@components/core/image';
import { selectIsMobile, selectIsIpad } from '@stores/slices/common';
import { ListingCdnImage } from '@interfaces/listing';
import { getImageUrl } from '@utils';
import { LocaleNamespace } from '@constants/enum';
import Svg from '@components/core/svg';

interface Props {
  images: ListingCdnImage[];
  onPhotoClick: (index: number) => void;
}

function Photos(props: Props) {
  const { t } = useTranslation(LocaleNamespace.Listing);

  const isMobile = useSelector(selectIsMobile);
  const isIpad = useSelector(selectIsIpad);
  const numberOfImages = useMemo(() => {
    if (isIpad) return 5;
    if (isMobile) return 1;
    return 5;
  }, [isMobile, isIpad]);

  const getImageSizes = useCallback((index) => {
    if (index > 0) return '300px';
    return '(min-width: 1600px) 700px, (min-width: 900px) 500px, 300px';
  }, []);

  const handleClick = useCallback((index) => () => props.onPhotoClick(index), []);

  /**
   * Fix dimension for photos
   */
  const heightSection = useMemo(() => {
    if (!window) return 0;
    let padding = 0;
    switch (true) {
      case window.innerWidth > 1400:
        padding = 320;
        break;
      case window.innerWidth > 1200:
        padding = 192;
        break;
      default:
        padding = 32;
        break;
    }
    const sectionWidth = window.innerWidth - padding;
    return `${((sectionWidth / 2) * 3) / 4}px`;
  }, []);

  return (
    <section
      style={{ height: heightSection || 'auto' }}
      id="photo-section-listing-detail"
      className="relative listing-photos">
      {take(props.images, numberOfImages).map((image, index) => (
        <Image
          className="cursor-pointer"
          key={image.id}
          src={getImageUrl(image.url)}
          alt={image.alt}
          sizes={getImageSizes(index)}
          width={4}
          height={3}
          layout="responsive"
          objectFit="cover"
          onClick={handleClick(index)}
        />
      ))}

      {props.images.length > numberOfImages && (
        <button className={classes.showAllPhotosCN} onClick={handleClick(0)}>
          <Svg name="grid" className={classes.showAllPhotoIcon} />
          {t('showAllPhotos')}
        </button>
      )}
    </section>
  );
}

const classes = {
  showAllPhotosCN: cntl`
    absolute right-5 bottom-5
    pl-2 py-1 pr-2
    font-medium font-semibold
    bg-white
    rounded-lg
    shadow
    flex-row
    flex
    items-center
    justify-center
    md:justify-start
  `,
  showAllPhotoIcon: cntl`
    w-2 h-2 inline-block mr-2 
    items-center justify-center
  `
};

export default Photos;
