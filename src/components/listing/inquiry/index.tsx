import { useCallback, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import cntl from 'cntl';

import { useTranslation } from 'next-i18next';
import { selectIsMobile } from '@stores/slices/common';
import { LocaleNamespace, Page } from '@constants/enum';
import ContactForm from '@forms/contact';
import Svg from '@components/core/svg';
import { Listing } from '@interfaces/listing';
import { Transportation } from '@interfaces/search';

interface InquiryProps {
  phoneNumber?: string;
  id?: string;
  title?: string | JSX.Element;
  page?: Page;
  property?: Listing;
  transportation?: Transportation;
}

function Inquiry({ phoneNumber, id, title, page = Page.Listing, property, transportation }: InquiryProps) {
  const { t } = useTranslation(LocaleNamespace.Listing);
  const isMobile = useSelector(selectIsMobile);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    if (isMobile) {
      document.body.classList.add('pb-10');
    }

    return () => {
      document.body.classList.remove('pb-10');
    };
  }, [isMobile]);

  const handleOpen = useCallback(() => {
    setVisible(true);
  }, []);

  const handleClose = useCallback(() => {
    setVisible(false);
  }, []);

  useEffect(() => {
    if (id) {
      const handleHashchange = () => {
        if (window.location.hash !== `#${id}`) return;
        handleOpen();
      };
      window.addEventListener('hashchange', handleHashchange);

      return () => {
        window.removeEventListener('hashchange', handleHashchange);
      };
    }
  }, [id]);

  return isMobile ? (
    <>
      <div className={classes.modal({ visible })}>
        <div className={classes.contactFormConmtainer}>
          <Svg className={classes.closeIcon} name="close" onClick={handleClose} />
          {visible && <ContactForm transportation={transportation} property={property} title={title} page={page} />}
        </div>
      </div>

      <div className={classes.overlayContainer({ hidden: visible })}>
        {phoneNumber && (
          <button
            className={classes.overlayButton({ isDark: true })}
            onClick={() => window.open(`tel:${phoneNumber}`, '_self')}>
            {t('callUsNow')}
          </button>
        )}
        <button className={classes.overlayButton({ isDark: false })} onClick={handleOpen}>
          {t('inquireNow')}
        </button>
      </div>
    </>
  ) : (
    <ContactForm transportation={transportation} property={property} title={title} page={page} />
  );
}

const classes = {
  modal: ({ visible }: { visible: boolean }) => cntl`
    fixed top-0 left-0 w-full h-full
    flex items-center justify-center
    bg-black text-center
    bg-opacity-70	
    transform
    transition-transform
    duration-300
    z-10000
    ${visible ? cntl`scale-100` : cntl`scale-0`}
  `,
  overlayContainer: ({ hidden }: { hidden: boolean }) => cntl`
    fixed left-0 bottom-0 right-0 p-4
    text-center
    z-50
    bg-gradient-to-b from-transparent to-primary-whitesmoke
    ${hidden ? cntl`hidden` : cntl`grid grid-cols-2 gap-1 sm:block`}
  `,
  overlayButton: ({ isDark }: { isDark: boolean }) => cntl`
    py-3 px-0 sm:px-10 mx-0 sm:mx-2
    text-md sm:text-lg font-medium font-semibold text-white tracking-widest
    rounded-md
    focus:outline-none focus:border-transparent
    ${isDark ? cntl`bg-primary-dark` : cntl`bg-primary-orange`}
  `,
  closeIcon: cntl`
    cursor-pointer w-9 h-9 fill-current text-white mb-2
  `,
  contactFormConmtainer: cntl`
    abs-vertical-center flex flex-col items-center
  `
};

export default Inquiry;
