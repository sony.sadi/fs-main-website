import { i18n } from 'next-i18next';

import Link from '@components/core/link';
import { Language } from '@constants/enum';
import { Listing } from '@interfaces/listing';
import { getListingDetailLink } from '@utils/listing';

interface Props {
  children: React.ReactChild;
  listing: Listing;
  className?: string;
}

function ListingLink({ children, listing, className }: Props) {
  const { href, as, rel } = getListingDetailLink(listing, i18n?.language as Language);

  return (
    <Link href={href} as={as} rel={rel} locale={i18n?.language} className={className}>
      {children}
    </Link>
  );
}

export default ListingLink;
