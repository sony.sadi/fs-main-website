import { useMemo } from 'react';
import { useTranslation } from 'next-i18next';
import Svg from '@components/core/svg';
import { LocaleNamespace } from '@constants/enum';
import { Listing } from '@interfaces/listing';
import BasicInfomationSection from '@components/listing/basic-information/shared/basic-infomation-section';
import amenitiesMapping from '@components/listing/basic-information/amenities/mapping';

const ListingInfomationAmenities = ({ property }: { property: Listing }) => {
  const { t } = useTranslation();
  const { t: tListing } = useTranslation(LocaleNamespace.Listing);

  const amenities = useMemo(
    () =>
      Object.keys(property).filter(
        (item) =>
          (item.startsWith('amenity') || item.endsWith('Allowed')) &&
          (property as any)[item] &&
          amenitiesMapping(item)?.i18nLabel
      ),
    [property]
  );

  if (!amenities.length) return <></>;

  return (
    <BasicInfomationSection title={tListing('amenities')}>
      <ul className="flex flex-wrap">
        {amenities.map((item) => (
          <li className="mr-6 mb-2 last:mr-0" key={item}>
            <Svg name={amenitiesMapping(item)?.iconName || ''} className="w-5 h-5 inline-block mr-2" />
            <span className="ml-3 text-xs">{tListing(t(amenitiesMapping(item)?.i18nLabel || ''))}</span>
          </li>
        ))}
      </ul>
    </BasicInfomationSection>
  );
};

export default ListingInfomationAmenities;
