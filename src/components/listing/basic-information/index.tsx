import { Listing } from '@interfaces/listing';
import ListingInfomationAmenities from '@components/listing/basic-information/amenities';
import ListingInfomationBuildingFeature from '@components/listing/basic-information/building-feature';
import ListingInfomationInfo from '@components/listing/basic-information/info';
import ListingInfomationPropertyScout from '@components/listing/basic-information/property-scout';
import ListingInfomationSummary from '@components/listing/basic-information/sumary';
import ListingInfomationAboutListing from '@components/listing/basic-information/about';
import InfomationLocationMap from '@components/common/infomation-location-map';
import { useTranslation } from 'next-i18next';
import { Language, LocaleNamespace } from '@constants/enum';

interface Props {
  property: Listing;
}

function BasicInformation({ property }: Props) {
  const { i18n, t } = useTranslation(LocaleNamespace.Listing);

  return (
    <>
      <ListingInfomationInfo property={property} />
      <ListingInfomationSummary property={property} />
      <ListingInfomationAboutListing property={property} />
      <ListingInfomationAmenities property={property} />
      <div id="map-pin" />
      <ListingInfomationBuildingFeature property={property} />
      <InfomationLocationMap
        title={t('locationMap')}
        language={i18n.language as Language}
        coord={{ id: property.id, gpsLat: property.gpsLat, gpsLong: property.gpsLong }}
      />
      <ListingInfomationPropertyScout property={property} />
    </>
  );
}

export default BasicInformation;
