import cntl from 'cntl';

// eslint-disable-next-line import/prefer-default-export
export const basicInfomationClasses = {
  summary: cntl`
      grid grid-cols-2 lg:grid-cols-4 gap-x-2 gap-y-4
      py-10
    `,
  summaryIcon: ({ fa }: { fa: string }) => cntl`
      text-base text-primary-lightgray ${fa}
    `,
  summaryItemValue: cntl`
      mt-2 text-xs color-home-dark
    `,
  propertyItems: cntl`
      grid grid-cols-2 gap-4
    `,
  propertyItem: cntl`
      flex items-center property-item
    `
};
