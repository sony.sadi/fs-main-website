import { ReactNode } from 'react';
import clsx from 'clsx';

interface Props {
  title: string;
  titleClassName?: string;
  subTitle?: string;
  subTitleClassName?: string;
  className?: string;
  children: ReactNode;
}

const BasicInfomationSection = ({ className, title, titleClassName, subTitle, subTitleClassName, children }: Props) => {
  return (
    <section className={clsx('cnt-vis--auto py-8 md:py-4', className)}>
      <h2
        className={clsx(
          'md:text-4xl text-2xl font-semibold',
          { 'mb-2': !!subTitle, 'mb-4': !subTitle },
          titleClassName
        )}>
        {title}
      </h2>
      {!!subTitle && <h5 className={clsx('text-base mb-4', subTitleClassName)}>{subTitle}</h5>}
      {children}
    </section>
  );
};

export default BasicInfomationSection;
