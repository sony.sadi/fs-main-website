import cntl from 'cntl';
import { useSelector } from 'react-redux';

import { selectIsMobile } from '@stores/slices/common';
import Link from '@components/core/link';
import Svg from '@components/core/svg';
import { DOMAttributes } from 'react';

function NextArrow(props: DOMAttributes<HTMLSpanElement>) {
  return (
    <span aria-hidden className={classes.arrow(false)} onClick={props.onClick}>
      <Svg className={classes.arrowSvg} name="right-arrow" />
    </span>
  );
}

function PrevArrow(props: DOMAttributes<HTMLSpanElement>) {
  return (
    <span aria-hidden className={classes.arrow(true)} onClick={props.onClick}>
      <Svg className={classes.arrowSvg} name="left-arrow" />
    </span>
  );
}

interface Props {
  onPrevArrowClick?: () => void;
  onNextArrowClick?: () => void;
  href: string;
  rel?: string;
  content?: string;
}

const SliderController = ({ onPrevArrowClick, onNextArrowClick, href, content, rel }: Props) => {
  const isMobile = useSelector(selectIsMobile);

  return (
    <div className="mt-8 flex items-center justify-center md:justify-between">
      {!isMobile && (
        <div className="flex">
          <PrevArrow onClick={onPrevArrowClick} />
          <NextArrow onClick={onNextArrowClick} />
        </div>
      )}
      {content && (
        <Link href={href} rel={rel} className="underline flex items-center">
          {content}
          <Svg name="right-arrow" className="h-2 w-2 ml-3" />
        </Link>
      )}
    </div>
  );
};

const classes = {
  slider: cntl`md:pb-0`,
  deal: cntl`block px-2 focus:outline-none best-rentals__slide`,
  cardContent: cntl`bg-secondary-darkwhite`,
  arrow: (isPrev: boolean) => cntl`
      group shadow-md
      flex-center
      w-10 h-10 
      bg-white hover:bg-primary-orange rounded-full z-10
      cursor-pointer
      ${isPrev ? 'mr-2' : 'mr-0'}
    `,
  arrowSvg: cntl`
      w-3 h-5
      fill-current text-primary-orange group-hover:text-white
    `
};

export default SliderController;
