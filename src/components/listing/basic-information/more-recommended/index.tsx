import { useMemo, useRef } from 'react';
import cntl from 'cntl';
import Slider, { Settings } from 'react-slick';
import { useTranslation } from 'next-i18next';
import startCase from 'lodash/startCase';
import CardRental from '@components/common/cards/rental';
import CardRentalLink from '@components/common/cards/rental-link';
import ListingLink from '@components/listing/link';
import { Language, LocaleNamespace } from '@constants/enum';
import { Listing } from '@interfaces/listing';
import BasicInfomationSection from '@components/listing/basic-information/shared/basic-infomation-section';
import SliderController from 'components/listing/basic-information/shared/slider-controller';
import { useSelector } from 'react-redux';
import { selectIsMobile } from '@stores/slices/common';
import { stringifyUrl, PropertyType, getDefaultParams, SerpType } from '@flexstay.rentals/fs-url-stringify';
import clsx from 'clsx';

const MAX_VISIBILITY_ITEMS = 4;

const ListingInfomationMoreRecommedned = ({
  property,
  rentals,
  title,
  hyperlinkText,
  serpType
}: {
  property: Listing;
  rentals: Listing[];
  title: string;
  hyperlinkText: string;
  serpType: SerpType;
}) => {
  const { t, i18n } = useTranslation(LocaleNamespace.Listing);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);
  const language = i18n.language as Language;
  const sliderRef = useRef<Slider>(null);

  const isMobile = useSelector(selectIsMobile);

  const recommendedUnits = useMemo(() => {
    return rentals.filter((rent) => rent.id !== property.id);
  }, [rentals, property.id]);

  const settings: Settings = {
    dots: false,
    infinite: recommendedUnits.length > MAX_VISIBILITY_ITEMS,
    draggable: false,
    speed: 300,
    lazyLoad: 'ondemand',
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: false,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          infinite: false,
          arrows: false,
          dots: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: '30px',

          dotsClass: classes.dotContainer,
          appendDots: (dots: React.ReactNode) => (
            <div>
              <ul className={classes.dot}>{dots}</ul>
            </div>
          ),
          customPaging: () => {
            return <span className={classes.paging} />;
          }
        }
      }
    ]
  };

  /**
   * https://flexstay.atlassian.net/browse/FS-716
   */
  const shouldNotRender =
    !recommendedUnits.length ||
    [PropertyType.Apartment, PropertyType.ServicedApartment].includes(property.propertyType as PropertyType);

  const buildingName = useMemo(() => {
    return language === Language.US ? property.buildingId?.name_en : property.buildingId?.name_th;
  }, [property, language]);

  const buildingLink = useMemo(() => {
    const baseParams = getDefaultParams();
    const buildingId = property.buildingId?.building_id;
    const building_type = property.buildingId?.building_type;
    return stringifyUrl({
      request: {
        ...baseParams,
        buildingId,
        building_type,
        where: buildingName,
        serp_type: serpType,
        index_serp_types: ['rents']
      },
      t: tCommon
    });
  }, [property, tCommon, buildingName]);

  if (shouldNotRender) return <></>;

  return (
    <BasicInfomationSection
      className="text-center md:text-left"
      titleClassName={classes.title}
      subTitleClassName={classes.subTitle}
      title={t(title, { name: startCase(buildingName) })}>
      <>
        <Slider ref={sliderRef} className={clsx('listing-detail__slider', classes.slider)} {...settings}>
          {recommendedUnits.slice(0, isMobile ? 10 : undefined).map((unit, index) => (
            <CardRentalLink
              key={index}
              deal={unit}
              cardProps={{
                cardContentClassName: classes.cardContent
              }}
              linkProps={{
                className: classes.unit
              }}
            />
          ))}
        </Slider>
        <SliderController
          href={buildingLink.path}
          rel={buildingLink.rel}
          onPrevArrowClick={() => sliderRef.current?.slickPrev()}
          onNextArrowClick={() => sliderRef.current?.slickNext()}
          content={t(hyperlinkText, { name: startCase(buildingName) })}
        />
      </>
    </BasicInfomationSection>
  );
};

const classes = {
  slider: cntl`pb-8 md:pb-0 slick-left -mx-4 sm:mx-0`,
  unit: cntl`block px-2 focus:outline-none best-rentals__slide`,
  cardContent: cntl`bg-secondary-darkwhite`,
  dotContainer: cntl`
    absolute left-1/2 transform -translate-x-1/2 bottom-0
  `,
  dot: cntl`
    grid grid-flow-col gap-1 searches__slick-dots
  `,
  title: cntl`
    text-4xl leading-snug 
    max-w-3/4 md:max-w-full mx-auto md:mx-0 
    text-center md:text-left
  `,
  subTitle: cntl`
    max-w-3/4 md:max-w-full mx-auto md:mx-0 
    text-center md:text-left
  `,
  paging: cntl`
    block w-3 h-2 rounded 
    bg-home-silver hover:bg-primary-orange 
    searches__slick-paging
  `,
  arrowSvg: cntl`
    w-3 h-5
    fill-current text-primary-orange group-hover:text-white
  `
};

export default ListingInfomationMoreRecommedned;
