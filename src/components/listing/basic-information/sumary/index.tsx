import Svg from '@components/core/svg';
import { Language, LocaleNamespace } from '@constants/enum';
import { FunishedType } from '@constants/enum/listing';
import { PropertyType } from '@flexstay.rentals/fs-url-stringify';
import { Listing } from '@interfaces/listing';
import { getBathroomNumber, getBedroomNumber, getClosestTransportSystem, getOrdinals, getTextDistance } from '@utils';
import { basicInfomationClasses } from 'components/listing/basic-information/shared';
import { useTranslation } from 'next-i18next';
import { useMemo } from 'react';
import round from 'lodash/round';
import { RADIUS_TO_SHOW_CLOSEST_TRANSPORT } from '@constants';

interface SummaryItem {
  iconName: string;
  value: string | number;
}

const ListingInfomationSummary = ({ property }: { property: Listing }) => {
  const { t, i18n } = useTranslation(LocaleNamespace.Listing);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);

  const renderFunitureValue = () => {
    if (
      property.furnishing === FunishedType.FullyFurnished ||
      property.furnishing === FunishedType.FullyFurnished_live
    ) {
      return t('fullyFurnished');
    }
    return property.furnishing === FunishedType.PartlyFurnished ? t('partlyFunished') : false;
  };

  const summary = useMemo(() => {
    const bedroomCount = getBedroomNumber(property.numberBedrooms);
    const bathroomCount = getBathroomNumber(property.numberBathrooms);

    return [
      {
        iconName: 'detail-bed',
        value: t(bedroomCount > 1 ? 'numberOfBedrooms' : 'numberOfBedroom', { count: bedroomCount })
      },
      {
        iconName: 'detail-bathroom',
        value: t(bathroomCount > 1 ? 'numberOfBathrooms' : 'numberOfBathroom', { count: bathroomCount })
      },
      { iconName: 'detail-sqm', value: tCommon('sqm', { count: property.floorSize }) },
      {
        iconName: 'detail-house',
        value: tCommon('lowestPricePerSqm', { count: round(property.lowestPrice / property.floorSize) })
      },
      {
        iconName: 'detail-step',
        value:
          property.floorLevel &&
          t('floorLevelPosition', {
            ordinal:
              [PropertyType.Condo, PropertyType.Apartment, PropertyType.ServicedApartment].includes(
                property.propertyType as PropertyType
              ) && getOrdinals(property.floorLevel, i18n.language as Language)
          })
      },
      {
        iconName: 'detail-parking',
        value:
          (property.amenityParking ||
            property.communalFreeParking ||
            property.communalCoveredCarPark ||
            property.communalOpenCarPark ||
            property.communalStreetParking) &&
          t('parkingAvl')
      },
      { iconName: 'pet-friendly-condos', value: property.petsAllowed && t('petFriendly') },
      {
        iconName: 'detail-furnished',
        value: renderFunitureValue()
      },
      { iconName: 'detail-balcony', value: property.amenityBalcony && t('balcony') },
      { iconName: 'detail-kitchen', value: property.amenityKitchen && t('kitchen') },
      {
        iconName: 'detail-bts-phrom-phong',
        value: getTextDistance({
          closestTransport: property.closestTransport,
          closestTransportKm: property.closestTransportKm,
          closestArl: property.closestArl,
          closestBts: property.closestBts,
          closestMrt: property.closestMrt,
          t: tCommon,
          radius: RADIUS_TO_SHOW_CLOSEST_TRANSPORT
        })
      }
    ];
  }, [property]);

  return (
    <ul className={basicInfomationClasses.summary}>
      {(summary as SummaryItem[]).map(
        (item, index) =>
          !!item.value && (
            <li key={index} className="items-center flex-row">
              <Svg name={item.iconName} className="w-5 h-5 inline-block mr-2" />
              <span className={basicInfomationClasses.summaryItemValue}>{item.value}</span>
            </li>
          )
      )}
    </ul>
  );
};

export default ListingInfomationSummary;
