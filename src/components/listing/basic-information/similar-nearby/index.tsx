import { useMemo, useRef } from 'react';
import { useTranslation } from 'next-i18next';
import cntl from 'cntl';
import Slider, { Settings } from 'react-slick';

import CardRentalLink from '@components/common/cards/rental-link';
import { LocaleNamespace } from '@constants/enum';
import { Listing } from '@interfaces/listing';
import BasicInfomationSection from '@components/listing/basic-information/shared/basic-infomation-section';
import startCase from 'lodash/startCase';
import SliderController from '@components/listing/basic-information/shared/slider-controller';
import { useSelector } from 'react-redux';
import { selectIsMobile } from '@stores/slices/common';
import { stringifyUrl, getDefaultParams, SerpType } from '@flexstay.rentals/fs-url-stringify';
import clsx from 'clsx';

const MAX_VISIBILITY_ITEMS = 4;

const MOBILE_ITEM_SHOW = 10;

const ListingInfomationSimilarListingNearby = ({
  property,
  rentals,
  title,
  hyperLinkText,
  serpType
}: {
  property: Listing;
  rentals: Listing[];
  title: string;
  hyperLinkText: string;
  serpType: SerpType;
}) => {
  const { t } = useTranslation(LocaleNamespace.Listing);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);
  const sliderRef = useRef<Slider>(null);

  const isMobile = useSelector(selectIsMobile);

  const similarNearby = useMemo(() => {
    return rentals.filter((rent) => rent.id !== property.id).slice(0, isMobile ? MOBILE_ITEM_SHOW : undefined);
  }, [isMobile, rentals, property.id]);

  const settings: Settings = {
    dots: false,
    infinite: similarNearby.length > MAX_VISIBILITY_ITEMS,
    draggable: false,
    speed: 300,
    lazyLoad: 'ondemand',
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: false,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          infinite: false,
          arrows: false,
          dots: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: '30px',

          dotsClass: classes.dotContainer,
          appendDots: (dots: React.ReactNode) => (
            <div>
              <ul className={classes.dot}>{dots}</ul>
            </div>
          ),
          customPaging: () => {
            return <span className={classes.paging} />;
          }
        }
      }
    ]
  };

  const similarNearbyLink = useMemo(() => {
    const baseParams = getDefaultParams();
    // If listing for Sale -> get salePrice
    // If listing for Rent -> get lowestPrice
    const currentPrice = serpType === 'sales' ? property.salePrice || 0 : property.lowestPrice;
    const coords = `${property.gpsLat}-${property.gpsLong}`;
    const minPrice = currentPrice - currentPrice * 0.2;
    const maxPrice = currentPrice + currentPrice * 0.2;
    return stringifyUrl({
      request: { ...baseParams, coords, minPrice, maxPrice, serp_type: serpType, index_serp_types: ['rents'] },
      t: tCommon
    });
  }, [property, tCommon]);

  if (!similarNearby.length) return <></>;

  return (
    <BasicInfomationSection
      className="text-center md:text-left "
      titleClassName={classes.title}
      subTitleClassName={classes.subTitle}
      title={t(title, { name: startCase(property.buildingName) })}>
      <>
        <Slider ref={sliderRef} className={clsx('listing-detail__slider', classes.slider)} {...settings}>
          {similarNearby.map((item) => (
            <CardRentalLink
              key={item.id}
              deal={item}
              cardProps={{
                cardContentClassName: classes.cardContent
              }}
              linkProps={{
                className: classes.deal
              }}
            />
          ))}
        </Slider>
        <SliderController
          href={similarNearbyLink.path}
          rel={similarNearbyLink.rel}
          onPrevArrowClick={() => sliderRef.current?.slickPrev()}
          onNextArrowClick={() => sliderRef.current?.slickNext()}
          content={t(hyperLinkText)}
        />
      </>
    </BasicInfomationSection>
  );
};

const classes = {
  slider: cntl`pb-8 md:pb-0 slick-left -mx-4 sm:mx-0`,
  deal: cntl`block px-2 focus:outline-none best-rentals__slide`,
  cardContent: cntl`bg-secondary-darkwhite`,
  paging: cntl`
    block w-3 h-2 rounded 
    bg-home-silver hover:bg-primary-orange 
    searches__slick-paging
  `,
  dot: cntl`
    grid grid-flow-col gap-1 searches__slick-dots
  `,
  dotContainer: cntl`
    absolute left-1/2 transform -translate-x-1/2 bottom-0
  `,
  title: cntl`
    text-4xl leading-snug max-w-3/4 md:max-w-full 
    mx-auto md:mx-0 text-center md:text-left
  `,
  subTitle: cntl`
    max-w-3/4 md:max-w-full mx-auto md:mx-0 
    text-center md:text-left
  `,
  arrowSvg: cntl`
    w-3 h-5
    fill-current text-primary-orange group-hover:text-white
  `
};

export default ListingInfomationSimilarListingNearby;
