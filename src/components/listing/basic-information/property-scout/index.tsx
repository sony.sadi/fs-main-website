import { useMemo } from 'react';
import { useTranslation } from 'next-i18next';
import startCase from 'lodash/startCase';

import Svg from '@components/core/svg';
import { LocaleNamespace } from '@constants/enum';
import { Listing } from '@interfaces/listing';
import BasicInfomationSection from '@components/listing/basic-information/shared/basic-infomation-section';
import cntl from 'cntl';

const ListingInfomationPropertyScout = ({ property }: { property: Listing }) => {
  const { t } = useTranslation(LocaleNamespace.Listing);
  const { t: tHome, i18n: i18nHome } = useTranslation(LocaleNamespace.Home);

  const propertyScountFeature = useMemo(() => {
    return [
      {
        iconName: 'largest-selection',
        name: tHome('propertyLargestSelectionTitle'),
        description: tHome('propertyLargestSelectionDescription')
      },
      {
        iconName: 'professional-property',
        name: tHome('propertyProfessionalTitle'),
        description: tHome('propertyProfessionalDescription')
      },
      {
        iconName: 'free-for-you',
        name: tHome('propertyFreeForYouTitle'),
        description: tHome('propertyFreeForYouDescription')
      },
      {
        iconName: 'best-deals',
        name: tHome('propertyBestDealsTitle'),
        description: tHome('propertyBestDealsDescription')
      }
    ];
  }, [property, i18nHome.language]);

  return (
    <BasicInfomationSection
      titleClassName={classes.title}
      subTitleClassName={classes.subTitle}
      subTitle={t('findSuitableProperty')}
      title={t('whyRentWithPropertyScout', { name: startCase(property.buildingName) })}>
      <ul className={classes.listContainer}>
        {propertyScountFeature.map((item, index) => (
          <li key={index}>
            <div className="relative inline-block">
              <Svg name={item.iconName} className="w-20 h-24" />
            </div>
            <div>
              <h3 className="text-lg font-bold mb-2">{item.name}</h3>
              <p className="text-base">{item.description}</p>
            </div>
          </li>
        ))}
      </ul>
    </BasicInfomationSection>
  );
};

const classes = {
  title: cntl`
    text-4xl leading-snug 
    max-w-3/4 md:max-w-full mx-auto md:mx-0 
    text-center md:text-left
  `,
  subTitle: cntl`
    max-w-3/4 md:max-w-full mx-auto md:mx-0 
    text-center md:text-left
  `,
  listContainer: cntl`
    grid gap-4 grid-cols-1 md:grid-cols-2 
    max-w-3/4 text-center md:text-left 
    mx-auto md:mx-0
  `
};

export default ListingInfomationPropertyScout;
