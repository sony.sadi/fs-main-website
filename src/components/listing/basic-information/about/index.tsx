import { useEffect, useMemo, useRef, useState } from 'react';
import clsx from 'clsx';
import { useTranslation } from 'next-i18next';

import Svg from '@components/core/svg';
import { Language, LocaleNamespace } from '@constants/enum';
import { Listing } from '@interfaces/listing';
import { echoHTML } from '@utils';
import BasicInfomationSection from 'components/listing/basic-information/shared/basic-infomation-section';

const ListingInfomationAboutListing = ({ property }: { property: Listing }) => {
  const { t, i18n } = useTranslation(LocaleNamespace.Listing);
  const descriptionRef = useRef<HTMLDivElement>(null);
  const minHeightToDisplayShowMore = 300;
  const language = i18n.language as Language;

  const [showMore, setShowMore] = useState<boolean>(false);

  useEffect(() => {
    if (descriptionRef?.current && descriptionRef.current.clientHeight < minHeightToDisplayShowMore) {
      setShowMore(true);
    }
  }, []);

  const description = useMemo(() => {
    switch (language) {
      case Language.US:
        return property.descriptionEn;
      case Language.TH:
        return property.descriptionTh;
      default:
        return '';
    }
  }, [language]);

  if (!description) return <></>;

  return (
    <BasicInfomationSection title={t('aboutThisListing')}>
      <div className="relative" ref={descriptionRef}>
        <div
          className={clsx('text-base mt-4', 'property-listing-about', { overflowed: !showMore })}
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{ __html: echoHTML(description) }}
        />
        {!showMore && (
          <button onClick={() => setShowMore(true)} className="absolute pl-5 bottom-0 right-0 bg-white font-bold ">
            <div className="flex items-center">
              <span className="underline mr-1">{t('readMore')}</span>
              <Svg name="red-arrow-down" className="w-2 h-2" />
            </div>
          </button>
        )}
      </div>
    </BasicInfomationSection>
  );
};

export default ListingInfomationAboutListing;
