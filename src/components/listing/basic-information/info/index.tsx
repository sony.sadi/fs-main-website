import { useMemo } from 'react';
import { useTranslation } from 'next-i18next';

import { Listing } from '@interfaces/listing';
import { formatPrice, getTextPrice } from '@utils';
import { getListingAdress } from '@utils/listing';
import { Language, LocaleNamespace } from '@constants/enum';
import cntl from 'cntl';

const ListingInfomationInfo = ({ property }: { property: Listing }) => {
  const { t, i18n } = useTranslation(LocaleNamespace.Common);
  const language = i18n.language as Language;

  const price = useMemo(() => {
    return formatPrice(property.lowestPrice);
  }, [property, language]);

  const title = useMemo(() => {
    switch (language) {
      case Language.US:
        return property.listingTitleEnGenStandard || property.listingTitleEn;
      case Language.TH:
        return property.listingTitleThGenStandard || property.listingTitleTh;
      default:
        return '';
    }
  }, [property, language]);

  /**
   * Generate listing price
   */
  const listingPrice = useMemo(() => {
    if (property.tenure === 'sell') {
      return getTextPrice(property.salePrice || 0, t, 'sales');
    }
    return getTextPrice(property.lowestPrice || 0, t);
  }, [property]);

  const address = useMemo(() => getListingAdress(property, language), [property, language]);

  return (
    <>
      <h1 className={classes.title}>{title}</h1>
      <p className="text-lg flex items-center">{address}</p>
      {(property.tenure === 'sell' || property.tenure === 'rentsell') && (
        <div className="mt-3">
          <p className="text-xl font-semibold">
            {property.tenure === 'rentsell'
              ? t('saleListingLabel', { price: getTextPrice(property.salePrice || 0, t, 'sales') })
              : listingPrice}
          </p>
        </div>
      )}
      {(property.tenure === 'rent' || property.tenure === 'rentsell') && (
        <div className="mt-3">
          <p className="text-xl font-semibold">
            {property.tenure === 'rentsell'
              ? t('rentListingLabel', { price: getTextPrice(property.lowestPrice || 0, t) })
              : listingPrice}
          </p>
        </div>
      )}
    </>
  );
};

const classes = {
  title: cntl`leading-snug text-3xl md:text-4xl font-bold mb-3`
};

export default ListingInfomationInfo;
