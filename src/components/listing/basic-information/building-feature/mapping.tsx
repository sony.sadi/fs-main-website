export interface BuildingFeatureMappingProps {
  iconName: string | null;
  i18nLabel: string | null;
}

const buildingFeatureMapping = (feature: string): BuildingFeatureMappingProps | null => {
  const defaultIconName = 'detail-checkbox';

  const [iconName, i18nLabel] = {
    communalGarden: ['detail-garden', 'garden'],
    communalGym: ['detail-gym', 'gym'],
    communalSwimmingPool: ['detail-pool', 'swimmingPool'],
    communalBadmintonCourt: [defaultIconName, 'badmintonCourt'],
    communalBasketballCourt: [defaultIconName, 'basketballCourt'],
    communalBbqGrill: [defaultIconName, 'bbqGrill'],
    communalBikePark: [defaultIconName, 'bikePark'],
    communalCctv: ['detail-cctv', 'cctv'],
    communalConcierge: [defaultIconName, 'concierge'],
    communalConvenienceStore: [defaultIconName, 'convenienceStore'],
    communalCoveredCarPark: ['detail-covered-parking', 'coveredCarPark'],
    communalElevator: ['detail-elevator', 'elevator'],
    communalFreeParking: ['detail-parking', 'freeParking'],
    communalFunctionalRoom: ['detail-business-area', 'functionalRoom'],
    communalJoggingTrack: [defaultIconName, 'joggingTrack'],
    communalKaraoke: [defaultIconName, 'karaoke'],
    communalKidsPlayground: ['detail-kids-playground', 'kidsPlayground'],
    communalKidsSwimmingPool: ['detail-kids-pool', 'kidsSwimmingPool'],
    communalLaundryRoom: ['detail-washer', 'laundryRoom'],
    communalLibrary: ['detail-study-room', 'library'],
    communalOpenCarPark: ['detail-open-parking', 'openCarPark'],
    communalPoolTable: [defaultIconName, 'poolTable'],
    communalReception: [defaultIconName, 'reception'],
    communalRestaurantOnPremises: [defaultIconName, 'restaurantOnPremises'],
    communalRooftopTerrace: [defaultIconName, 'rooftopTerrace'],
    communalSauna: ['detail-sauna', 'sauna'],
    communalSecurity24Hrs: ['detail-24h-sercurity', 'security24Hrs'],
    communalShopOnPremises: [defaultIconName, 'shopOnPremises'],
    communalShuttleService: ['detail-shuttle-service', 'shuttleService'],
    communalSquashCourt: [defaultIconName, 'squashCourt'],
    communalSteamroom: [defaultIconName, 'steamroom'],
    communalStreetParking: ['detail-parking', 'streetParking'],
    communalTennisCourt: [defaultIconName, 'tennisCourt'],
    communalWifi: ['detail-wifi', 'communalWifi']
  }[feature] || [null, null];

  return i18nLabel ? { iconName, i18nLabel } : null;
};

export default buildingFeatureMapping;
