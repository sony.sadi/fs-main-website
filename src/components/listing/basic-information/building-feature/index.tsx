import { useMemo } from 'react';
import { useTranslation } from 'next-i18next';
import Svg from '@components/core/svg';
import { LocaleNamespace } from '@constants/enum';
import { Listing } from '@interfaces/listing';
import BasicInfomationSection from '@components/listing/basic-information/shared/basic-infomation-section';
import buildingFeatureMapping from '@components/listing/basic-information/building-feature/mapping';

const ListingInfomationBuildingFeature = ({ property }: { property: Listing }) => {
  const { t } = useTranslation();
  const { t: tListing } = useTranslation(LocaleNamespace.Listing);

  const features = useMemo(
    () =>
      Object.keys(property).filter(
        (item) => item.startsWith('communal') && (property as any)[item] && buildingFeatureMapping(item)?.i18nLabel
      ),
    [property]
  );

  if (!features.length) return <></>;

  return (
    <BasicInfomationSection title={tListing('buildingFeature')}>
      <ul className="flex flex-wrap">
        {features.map((item) => (
          <li className="mr-6 mb-3 last:mr-0" key={item}>
            <Svg name={buildingFeatureMapping(item)?.iconName || ''} className="w-5 h-5 inline-block mr-2" />
            <span className="ml-3 text-xs">{tListing(t(buildingFeatureMapping(item)?.i18nLabel || ''))}</span>
          </li>
        ))}
      </ul>
    </BasicInfomationSection>
  );
};

export default ListingInfomationBuildingFeature;
