import { ListingCdnImage } from '@interfaces/listing';
import { useState } from 'react';
import Slider, { Settings } from 'react-slick';

import Image from '@components/core/image';
import { getImageUrl } from '@utils';
import cntl from 'cntl';

interface Props {
  images: ListingCdnImage[];
}

const PhotoSlider = ({ images }: Props) => {
  const [currentIndex, setCurrentIndex] = useState<number>(1);

  const settings: Settings = {
    arrows: false,
    infinite: true,
    speed: 500,
    lazyLoad: 'ondemand',
    slidesToShow: 1,
    slidesToScroll: 1,
    className: 'placeholder-slider',
    autoplay: true,
    afterChange: (current: number) => setCurrentIndex(current + 1)
  };

  return (
    <div className="relative">
      <div className={classes.pageContainer}>
        <h5 className="text-white">
          {currentIndex}/{images.length}
        </h5>
      </div>
      <Slider {...settings}>
        {images.map((image, index) => (
          <Image
            key={index}
            src={getImageUrl(image.url)}
            alt={image.alt}
            className="rounded-md"
            sizes="(min-width: 600px) 400px, 200px"
            width={4}
            height={2}
            layout="responsive"
            objectFit="cover"
          />
        ))}
      </Slider>
    </div>
  );
};

const classes = {
  pageContainer: cntl`
    absolute bottom-4 right-3 
    px-3 z-10 bg-opacity-50 bg-black 
    rounded-sm text-xs
  `
};

export default PhotoSlider;
