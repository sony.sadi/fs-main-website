import { useEffect, useRef } from 'react';
import Slider, { Settings } from 'react-slick';
import cntl from 'cntl';

import { ListingCdnImage } from '@interfaces/listing';
import { getImageUrl } from '@utils';
import Image from '@components/core/image';

interface Props {
  images: ListingCdnImage[];
  activeSlide: number;
  onClose: () => void;
  onChange: (slide: number) => void;
}

function PhotosModal(props: Props) {
  const slider = useRef<Slider>(null);

  const settings: Settings = {
    dots: false,
    infinite: true,
    lazyLoad: 'ondemand',
    fade: true,
    speed: 300,
    adaptiveHeight: true,
    afterChange(currentSlide: number) {
      props.onChange(currentSlide);
    }
  };

  const visible = props.activeSlide !== -1;

  useEffect(() => {
    if (visible) slider.current?.slickGoTo(props.activeSlide);
  }, [props.activeSlide]);

  return (
    <div className={classes.modal({ visible })}>
      <span className={classes.close} onClick={props.onClose} aria-hidden="true">
        &times;
      </span>
      <span className="mt-10 block text-base font-semibold">
        {props.activeSlide + 1}/{props.images.length}
      </span>

      <div className="abs-center w-10/12 xl:w-7/12 mx-auto">
        {visible && (
          <Slider ref={slider} {...settings}>
            {props.images.map((image) => (
              <div key={image.id}>
                <Image
                  src={getImageUrl(image.url)}
                  className={classes.img}
                  alt={image.alt}
                  layout="responsive"
                  objectFit="contain"
                  loading="lazy"
                  width={16}
                  height={9}
                />
              </div>
            ))}
          </Slider>
        )}
      </div>
    </div>
  );
}

const classes = {
  modal: ({ visible }: { visible: boolean }) => cntl`
    fixed top-0 left-0 w-full h-full
    bg-white text-center
    transform
    transition-transform
    duration-300
    z-photos-modal

    ${visible ? cntl`scale-100` : cntl`scale-0`}
  `,
  img: cntl`photo-modal-img w-full object-cover`,
  close: cntl`
    absolute top-4 right-8
    text-6xl text-primary-gray hover:text-primary-black cursor-pointer
  `
};

export default PhotosModal;
