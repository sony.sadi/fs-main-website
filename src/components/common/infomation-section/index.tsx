import React, { ReactHTML, ReactNode, useMemo } from 'react';
import clsx from 'clsx';

interface Props {
  title: string;
  titleClassName?: string;
  subTitle?: string;
  subTitleClassName?: string;
  className?: string;
  children: ReactNode;
  element?: keyof ReactHTML;
  id?: string;
}

const InfomationSection = ({
  className,
  title,
  titleClassName,
  subTitle,
  subTitleClassName,
  children,
  element = 'h2',
  id
}: Props) => {
  const header = useMemo(() => {
    const className = clsx(
      titleClassName || 'md:text-4xl text-2xl',
      {
        'mb-2': !!subTitle,
        'mb-4': !subTitle
      },
      'font-semibold'
    );
    return React.createElement(element, { className }, title);
  }, [element, title, subTitle, titleClassName]);

  return (
    <section id={id} className={clsx('cnt-vis--auto py-8 md:py-4', className)}>
      {header}
      {!!subTitle && <h5 className={clsx('text-base mb-4', subTitleClassName)}>{subTitle}</h5>}
      {children}
    </section>
  );
};

export default InfomationSection;
