import { useEffect, useMemo, useRef, useState } from 'react';
import cntl from 'cntl';

import Svg from '@components/core/svg';
import { useTranslation } from 'next-i18next';
import { BasicSearchTheme } from '@constants/enum';
import { filterOptions } from '@constants';
import find from 'lodash/find';
import { useDispatch, useSelector } from 'react-redux';
import { searchActions, selectFilterOption, selectParams } from '@stores/slices/search';
import { SerpType } from '@flexstay.rentals/fs-url-stringify';
import clsx from 'clsx';
import styles from './FilterSearch.module.scss';

const themeText = {
  [BasicSearchTheme.Light]: 'text-white',
  [BasicSearchTheme.Dark]: 'text-primary-gray'
};

function FilterSearch({ theme }: { theme: BasicSearchTheme }) {
  const { i18n, t } = useTranslation();
  const containerRef = useRef<HTMLDivElement>(null);
  const filterOptionValue = useSelector(selectFilterOption);
  const dispatch = useDispatch();
  const searchParams = useSelector(selectParams);
  const [valueFilter, setValueFilter] = useState<string>(filterOptionValue);
  const [visible, setVisible] = useState<boolean>(false);

  useEffect(() => {
    document.addEventListener('click', handleClickOutside);
    return () => document.removeEventListener('click', handleClickOutside);
  }, []);

  useEffect(() => {
    if (filterOptionValue !== valueFilter) {
      setValueFilter(filterOptionValue);
    }
  }, [filterOptionValue]);

  const handleClickOutside = (e: MouseEvent) => {
    if (containerRef?.current) {
      if (!containerRef.current.contains(e.target as Node)) {
        setVisible(false);
      }
    }
  };

  /**
   * Render label of Filter
   */
  const renderLabel = useMemo(() => {
    const label = find(filterOptions, (val) => val.value === valueFilter)?.filterLabel || 'rentFilter';
    return t(label);
    // return t(find(filterOptions, (val) => val.value === valueFilter));
  }, [valueFilter]);

  /**
   * Render dropdown filter
   * @returns
   */
  const renderDropdown = () => {
    return (
      <div className={styles.popup}>
        <ul className={styles.popup__option}>
          {filterOptions.map((option, index) => (
            <li
              aria-hidden
              key={option.value}
              className={clsx(!!(index === 0) && 'border-black border-b', styles.popup__option)}
              onClick={handleClick(option.value)}
              data-label={option.filterLabel}>
              {t(option.filterLabel)}
            </li>
          ))}
        </ul>
      </div>
    );
  };

  /**
   * Click filter
   * @param value
   * @returns
   */
  const handleClick = (value: SerpType) => {
    return () => {
      setValueFilter(value);
      dispatch(searchActions.setFilterOption(value));
      dispatch(
        searchActions.setRentalsParams({
          ...searchParams,
          minPrice: 0,
          maxPrice: 0
        })
      );
      setVisible(false);
    };
  };

  const handleToggle = () => {
    setVisible(!visible);
  };

  return (
    <div className={styles.filter_search}>
      <div className={styles.filter_divider} />
      <div
        id="toggle-serp-type"
        data-testid="toggle-serp-type"
        ref={containerRef}
        className={styles.container}
        aria-hidden
        onClick={handleToggle}>
        <span className={clsx('fs-home-search__unit-type-filter', `${themeText[theme]}`, styles.filter_button)}>
          {renderLabel}
        </span>
        <Svg
          className={clsx(`${themeText[theme]}`, visible && styles.rotate_icon, styles.down_arrow)}
          name="down-arrow"
        />
        {visible && renderDropdown()}
      </div>
    </div>
  );
}

export default FilterSearch;
