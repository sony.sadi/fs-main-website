import { useRef, useState } from 'react';
import { Map, Marker, GoogleApiWrapper, IMapProps, InfoWindow } from 'google-maps-react';
import cntl from 'cntl';
import Svg from '@components/core/svg';
import { useTranslation } from 'next-i18next';

import { LocaleNamespace } from '@constants/enum';

interface Props extends IMapProps {
  className?: string;
  language: string;
}

function SimpleMap(props: Props) {
  const { t } = useTranslation(LocaleNamespace.Contact);

  const mapEl = useRef<any>();
  const [activeMarker, setActiveMarker] = useState<google.maps.Marker>(new google.maps.Marker());

  const handleMarkerMounted = (markerProps: any) => {
    setActiveMarker(markerProps?.marker);
  };

  return (
    <div className={classes.mapContainer(props.className)}>
      <Map ref={mapEl} {...props} streetViewControl={false} mapTypeControl={false} zoom={props.zoom}>
        <Marker ref={handleMarkerMounted} position={props.initialCenter} />
        <InfoWindow map={mapEl.current} position={props.initialCenter} google={google} marker={activeMarker} visible>
          <Svg name="logo-full" className="w-36 h-10" />
        </InfoWindow>
      </Map>
      <div className={classes.popup}>
        <p className={classes.location}>{t('contactMapHead')}</p>
        <p className="font-bold">{t('contactMapName')}</p>
        <p>{t('contactMapAddress')}</p>
        <span>{t('contactMapAddress2')}</span>
      </div>
    </div>
  );
}

const classes = {
  mapContainer: (className?: string) => cntl`
    relative h-vh-60
    ${className}
  `,
  popup: cntl`
    absolute left-1/2 transform -translate-x-1/2 bottom-10
    w-full md:w-96 h-40 px-8 py-5
    bg-white rounded-2xl
  `,
  location: cntl`
    text-lg text-primary-orange font-bold
  `
};

export default GoogleApiWrapper((props: Props) => {
  return {
    apiKey: String(process.env.NEXT_PUBLIC_GOOGLE_MAP_API_KEY),
    language: props.language,
    version: '3'
  };
})(SimpleMap);
