import { useMemo } from 'react';
import cntl from 'cntl';
import { useTranslation } from 'next-i18next';
import { useSelector } from 'react-redux';

import { LocaleNamespace, Page } from '@constants/enum';
import { selectIsMobile } from '@stores/slices/common';
import ourPartners from '@assets/data/our-partners.json';

interface Props {
  page: Page;
}

const OurPartners = (props: Props) => {
  const ourPartnersClasses = useMemo(() => {
    switch (props.page) {
      case Page.ListYourProperty:
        return 'px-0 lg:rounded-3xl';

      default:
        return 'px-4 sm:px-8 lg:px-14';
    }
  }, [props.page]);

  const ourDesktopClasses = useMemo(() => {
    switch (props.page) {
      case Page.ListYourProperty:
        return 'xl:grid-cols-3';

      default:
        return 'xl:grid-cols-5';
    }
  }, [props.page]);

  const { t } = useTranslation(LocaleNamespace.Home);

  const isMobile = useSelector(selectIsMobile);
  const partners = isMobile ? ourPartners.mobile : ourPartners.desktop;

  return (
    <section className={classes.ourPartners({ classes: ourPartnersClasses })}>
      <header className={classes.header}>
        <h2 className={classes.headerTitle}>{t('ourPartnersHeading')}</h2>
        <p className={classes.headerDescription}>{t('ourPartnersDescription')}</p>
      </header>

      <ul className={classes.partners({ classes: ourDesktopClasses })}>
        {partners.map((partner, index) => (
          <li
            className={classes.partner(index)}
            key={partner.name}
            style={{
              backgroundImage: `url(${partner.imageUrl})`,
              backgroundBlendMode: 'multiply'
            }}
          />
        ))}
        <div className={classes.partnerMixColor} style={{ mixBlendMode: 'color' }} />
      </ul>
    </section>
  );
};

const classes = {
  ourPartners: ({ classes }: { classes: string }) => cntl`
    bg-secondary-darkwhite py-14
    ${classes}
  `,
  header: cntl`
    px-4 sm:px-16 md:px-0 lg:px-10
    xl:pb-0 mb-4 xl:mb-0
    text-center md:text-left
  `,
  headerTitle: cntl`text-4xl font-bold`,
  headerDescription: cntl`mt-1 text-base xl:text-lg`,
  partners: ({ classes }: { classes: string }) => cntl`
    relative
    p-4 xl:px-0 xl:py-5
    grid grid-cols-2 grid-cols-12 justify-center
    gap-x-1 gap-y-3 xl:gap-4
    ${classes}
  `,
  partner: (index: number) =>
    cntl`h-10 bg-no-repeat bg-secondary-darkwhite bg-center bg-contain col-span-1 xl:col-span-1 ${
      index < 6 ? 'col-span-6 sm:col-span-4' : 'col-span-6 sm:col-span-3'
    }`,
  partnerMixColor: cntl`absolute top-0 left-0 w-full h-full bg-black`
};

export default OurPartners;
