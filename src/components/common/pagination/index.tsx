import { HTMLAttributes, ButtonHTMLAttributes, useMemo } from 'react';
import Svg from '@components/core/svg';
import cntl from 'cntl';

interface PaginationProps extends HTMLAttributes<HTMLDivElement> {
  pageCount: number;
  page: number;
  onPageChange: (page: number) => void;
}

const Pagination = ({ pageCount, page, onPageChange, ...props }: PaginationProps) => {
  const PaginationButton = ({ children, ...buttonProps }: ButtonHTMLAttributes<HTMLButtonElement>) => {
    return (
      <button {...buttonProps} className={classes.buttonPagination}>
        {children}
      </button>
    );
  };

  const PaginationPageNumber = ({ pageNumber }: { pageNumber: number }) => {
    return (
      <PaginationButton
        onClick={() => {
          onPageChange(pageNumber);
        }}
        disabled={pageNumber === page}>
        {pageNumber === page ? <Svg className="w-8 h-8" name="red-pentagon" /> : ''}
        <span className={classes.numberPage(pageNumber === page)}>{pageNumber}</span>
      </PaginationButton>
    );
  };

  const PaginationMore = () => {
    return (
      <PaginationButton disabled className="cursor-default">
        <span className="text-primary-gray">...</span>
      </PaginationButton>
    );
  };

  const pageNumbers = useMemo(() => {
    const pages = [];
    for (let i = page - 1; i <= page + 1; i++) {
      if (i > 1 && i < pageCount) pages.push(i);
    }
    return pages;
  }, [page, pageCount]);

  const handlePrevClick = () => {
    onPageChange(page - 1);
  };

  const handleNextClick = () => {
    onPageChange(page + 1);
  };

  return (
    <div {...props}>
      <ul className="flex-center">
        <li className="flex">
          <button
            className={classes.arrow({ isPrev: false, isDisabled: page === 1 })}
            disabled={page === 1}
            data-testid="pagination-prev"
            onClick={handlePrevClick}>
            <Svg className={classes.arrowSvg} name="left-arrow" />
          </button>
        </li>
        <li>
          <PaginationPageNumber pageNumber={1} />
        </li>
        {page > 3 && <PaginationMore />}
        {pageNumbers.map((number, index) => (
          <li key={index}>
            <PaginationPageNumber pageNumber={number} />
          </li>
        ))}
        {pageCount - 2 > page && <PaginationMore />}
        {pageCount > 1 && (
          <li>
            <PaginationPageNumber pageNumber={pageCount} />
          </li>
        )}
        <li className="flex">
          <button
            className={classes.arrow({ isPrev: true, isDisabled: page === pageCount })}
            disabled={page === pageCount}
            data-testid="pagination-next"
            onClick={handleNextClick}>
            <Svg className={classes.arrowSvg} name="right-arrow" />
          </button>
        </li>
      </ul>
    </div>
  );
};

const classes = {
  buttonPagination: cntl`
    relative  
    focus:outline-none
    h-9 w-auto min-w-9 mx-1`,
  arrow: ({ isPrev, isDisabled }: { isPrev: boolean; isDisabled: boolean }) => cntl`
    hidden xs:block
    cursor-pointer group
    ${isPrev ? 'ml-2' : 'mr-2'}
    ${isDisabled ? 'opacity-50' : ''}
  `,
  arrowSvg: cntl`
    w-5 h-2.5
    fill-current text-footer-black
    hover:text-primary-orange
  `,
  numberPage: (isActive: boolean) => cntl`
    absolute left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2
    ${isActive ? 'text-white' : 'text-primary-gray hover:text-primary-orange hover:font-bold'}
  `
};

export default Pagination;
