import { useCallback, useMemo, Fragment, AnchorHTMLAttributes } from 'react';
import cntl from 'cntl';
import { useTranslation } from 'next-i18next';

import contacts from '@assets/data/contacts.json';
import Svg from '@components/core/svg';
import { Page, LocaleNamespace } from '@constants/enum';
import { isUrl } from '@utils';

interface Props {
  page: Page;
}

interface ContactItem {
  info: {
    name: string;
    content: string;
    href: string;
  };
  icon: string;
}

const Contact = (props: Props) => {
  const { t } = useTranslation(LocaleNamespace.Common);
  const { t: tHowItWorks } = useTranslation(LocaleNamespace.HowItWorks);

  const contactClasses = useMemo(() => {
    switch (props.page) {
      case Page.HowItWorks:
      case Page.ListYourProperty:
        return '';

      case Page.Contact:
        return '';

      default:
        return '';
    }
  }, [props.page]);

  const itemClasses = useMemo(() => {
    switch (props.page) {
      case Page.HowItWorks:
      case Page.ListYourProperty:
        return 'xl:col-span-2';

      default:
        return '';
    }
  }, [props.page]);

  const renderContact = useCallback(
    (contact: ContactItem) => {
      const { icon, info } = contact;
      const tagProps: AnchorHTMLAttributes<HTMLAnchorElement> = {
        href: props.page === Page.ListYourProperty && info.name === 'contactEmail' ? t('toSaleMail') : info.href
      };
      if (isUrl(info.href)) {
        tagProps.target = '_blank';
      }
      return (
        <a className={classes.item(itemClasses)} {...tagProps}>
          <Svg name={icon} className="w-8 h-8" />
          <div>
            <p className="text-sm mb-1 font-bold">{tHowItWorks(info.name)}</p>
            <p className="text-13 truncate-1-line">
              {props.page === Page.ListYourProperty && info.name === 'contactEmail' ? t('saleMail') : info.content}
            </p>
          </div>
        </a>
      );
    },
    [contacts]
  );

  return (
    <div className={classes.contact(contactClasses)}>
      {contacts.map((contact, index) => (
        <Fragment key={index}>{renderContact(contact)}</Fragment>
      ))}
    </div>
  );
};

const classes = {
  contact: (classes: string) => cntl`
    grid sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-6 gap-3 sm:gap-7 lg:gap-3
    ${classes}
  `,
  item: (classes: string) => cntl`{
    rounded-3xl bg-secondary-darkwhite
    p-8 sm:px-4 sm:py-6 md:px-4
    flex flex-col justify-between
    h-48 cursor-pointer
    group hover:bg-white hover:shadow-2xl hover:border hover:border-white
    ${classes}
  }`
};

export default Contact;
