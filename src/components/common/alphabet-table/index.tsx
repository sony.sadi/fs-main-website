import Link from '@components/core/link';
import { selectIsMobile } from '@stores/slices/common';
import cntl from 'cntl';
import sortBy from 'lodash/sortBy';
import React, { Fragment, HTMLAttributes, useMemo } from 'react';
import { useSelector } from 'react-redux';

interface IProps extends HTMLAttributes<HTMLDivElement> {
  groups: Record<string, Array<{ path: string; label: string }>>;
  getId: (char: string) => string;
}

const AlphabetTable = ({ groups, getId, ...props }: IProps) => {
  const characters = useMemo(() => {
    return Object.keys(groups).filter(Boolean);
  }, [groups]);

  const isMobile = useSelector(selectIsMobile);
  const headerHeight = isMobile ? 75 : 64;

  return (
    <div {...props}>
      <div className="hidden md:flex items-center flex-wrap">
        {characters.map((character, index) => {
          const isLast = index === characters.length - 1;
          return (
            <Fragment key={character}>
              {groups[character]?.length ? (
                <a className={classes.word} href={`#${getId(character)}`} rel="">
                  {character}
                </a>
              ) : (
                <span className={classes.word}>{character}</span>
              )}
              {!isLast && <span className="text-2xl">&#x0007C;</span>}
            </Fragment>
          );
        })}
      </div>
      <div className="px-6">
        {sortBy(Object.keys(groups)).map((key) => {
          const group = sortBy(groups[key], (item) => item.label);
          return (
            <div
              id={getId(key)}
              key={key}
              style={{
                scrollMarginTop: headerHeight
              }}>
              <div className="h-8 flex items-center px-2">{!!key && <p className="font-bold uppercase">{key}</p>}</div>
              <div className="flex flex-wrap">
                {group.map((item, index) => {
                  return (
                    <div className="w-1/2 md:w-1/3 px-2 pb-0.5" key={index}>
                      <Link className="underline w-full" href={item.path}>
                        {item.label}
                      </Link>
                    </div>
                  );
                })}
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

const classes = {
  word: cntl`text-base uppercase underline px-2`
};

export default AlphabetTable;
