import Map from '@components/listing/map';
import { Language } from '@constants/enum';
import { Coordinate } from '@interfaces/map';
import InfomationSection from '@components/common/infomation-section';
import { ReactHTML } from 'react';
import styles from './infomation-location-map.module.scss';

interface IProps {
  coord: Coordinate;
  language: Language;
  title: string;
  id?: string;
  titleElement?: keyof ReactHTML;
  titleClassName?: string;
}

const InfomationLocationMap = ({ coord, language, title, id, titleElement, titleClassName }: IProps) => {
  return (
    <InfomationSection titleClassName={titleClassName} element={titleElement} id={id} title={title}>
      <Map
        className={styles.map}
        containerStyle={{ borderRadius: 15, overflow: 'hidden' }}
        language={language}
        coord={coord}
      />
    </InfomationSection>
  );
};

export default InfomationLocationMap;
