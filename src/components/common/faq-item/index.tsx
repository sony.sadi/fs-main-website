import cntl from 'cntl';
import Svg from '@components/core/svg';
import { useCallback, useState } from 'react';

interface Props {
  question: string;
  answers: string[];
  index: number;
  closed?: boolean;
}

const FAQItem = ({ ...props }: Props) => {
  const [closed, setClosed] = useState(props.closed || false);

  const handleToggle = useCallback(() => {
    setClosed(!closed);
  }, [closed]);

  return (
    <div key={props.index}>
      <input
        onChange={handleToggle}
        checked={closed}
        type="checkbox"
        id={`slide-toggle-property-${props.index}`}
        className={classes.slideToggleCheckbox}
      />
      <label htmlFor={`slide-toggle-property-${props.index}`} className={classes.slideToggleLabel}>
        <h3 className="text-lg font-bold">{props.question}</h3>
        <Svg className={classes.iconArrowBottom} name="top-arrow" />
      </label>
      <div className={classes.slideToggleFAQ}>
        {props.answers.map((answer: string, index: number) => (
          <p className="text-base mb-8" key={index}>
            {answer}
          </p>
        ))}
      </div>
    </div>
  );
};

const classes = {
  iconArrowBottom: cntl`slide-toggle__icon ml-3 h-2 w-4 self-center`,
  slideToggleCheckbox: cntl`slide-toggle__checkbox`,
  slideToggleLabel: cntl`slide-toggle__label flex pb-2.5 lg:pb-0 cursor-pointer mb-5 content`,
  slideToggleFAQ: cntl`slide-toggle__faq content`
};

export default FAQItem;
