import { ButtonHTMLAttributes } from 'react';
import clsx from 'clsx';

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  icon?: string;
  color?: 'primary' | 'info';
}

const BgColorMap = {
  primary: 'bg-primary-red',
  info: 'bg-whitesmoke'
};

const TextColorMap = {
  primary: 'text-white',
  info: 'text-primary-gray'
};

const Button = ({ children, className, icon, color = 'primary', title, ...props }: ButtonProps) => {
  return (
    <button
      className={clsx(
        'w-full px-5 py-2.5 rounded-md focus:outline-none hover:opacity-90 shadow-md fs-button',
        className,
        BgColorMap[color]
      )}
      {...props}>
      {icon && <i className={clsx('mr-2', icon)} aria-hidden="true" />}
      {title ? <span className={TextColorMap[color]}>{title}</span> : children}
    </button>
  );
};

export default Button;
