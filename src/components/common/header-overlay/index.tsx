import Image from '@components/core/image';
import { Page } from '@constants/enum';
import { ImageProps } from 'next/image';
import { useMemo } from 'react';
import styles from './header-overlay.module.scss';

interface IProps extends ImageProps {
  title: string;
  subTitle?: string;
  width: number | string;
  height: number | string;
  page: Page;
  id?: string;
}

const HeaderOverlay = ({ title, subTitle, page, id, ...props }: IProps) => {
  const pageStyle = useMemo(() => {
    switch (page) {
      case Page.LandingTransportDetail:
        return {
          overlay: styles.overlay_left,
          titleBox: styles.title_box_left,
          title: styles.landing_transport_detail_title
        };
      case Page.LandingTransportList:
        return {
          overlay: styles.overlay_center,
          titleBox: styles.title_box_center,
          title: styles.landing_transport_list_title
        };
      default:
        return {};
    }
  }, [page]);

  return (
    <div id={id} className={styles.container}>
      <Image {...props} layout="responsive" />
      <div className={pageStyle.overlay}>
        <div className={pageStyle.titleBox}>
          <h1 className={pageStyle.title}>{title}</h1>
          {!!subTitle && <h2 className={styles.sub_title}>{subTitle}</h2>}
        </div>
      </div>
    </div>
  );
};

export default HeaderOverlay;
