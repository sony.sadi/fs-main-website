import React, { useMemo } from 'react';

import ListingLink from '@components/listing/link';
import { Listing } from '@interfaces/listing';
import { Page } from '@constants/enum';
import { SerpType } from '@flexstay.rentals/fs-url-stringify';
import CardRental from '../rental';

interface Props {
  deal: Listing;
  page?: Page;
  serpType?: SerpType;
  cardProps?: {
    cardContentClassName?: string;
    onMouseOver?: (deal: Listing) => void;
    className?: string;
    showId?: boolean;
    isAllowContentVisibility?: boolean;
  };
  linkProps?: {
    className?: string;
  };
}

function CardRentalLink({ deal, cardProps, linkProps, page, serpType }: Props) {
  const isActive = !deal.disabled;
  return (
    <>
      {isActive ? (
        <ListingLink listing={deal} {...linkProps}>
          <CardRental serpType={serpType} page={page} isActive={isActive} deal={deal} {...cardProps} />
        </ListingLink>
      ) : (
        <div className={linkProps && linkProps.className ? linkProps.className : ''}>
          <CardRental
            serpType={serpType}
            page={page}
            isActive={isActive}
            deal={deal}
            {...cardProps}
            onMouseOver={undefined}
          />
        </div>
      )}
    </>
  );
}

export default CardRentalLink;
