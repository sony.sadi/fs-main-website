import startCase from 'lodash/startCase';
import get from 'lodash/get';
import cntl from 'cntl';

import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';
import Image from '@components/core/image';
import { selectIsMobile } from '@stores/slices/common';
import { InDemandBuilding } from '@interfaces/search';
import { Language } from '@constants/enum';
import { getImageUrl } from '@utils';

interface Props {
  building: InDemandBuilding;
}

function CardBuilding({ building }: Props) {
  const isMobile = useSelector(selectIsMobile);
  const { i18n } = useTranslation();
  const language = i18n.language as Language;

  const width = isMobile ? 404 : 177;
  const height = isMobile ? 217 : 177;

  return (
    <div className={classes.cardBuilding}>
      <Image
        className={classes.cardImage}
        src={getImageUrl(get(building, 'images[0].url'), process.env.NEXT_PUBLIC_EGSWING_API_BASE_URL)}
        layout="responsive"
        sizes="300px"
        width={width}
        height={height}
        objectFit="cover"
      />
      <div className="md:py-5 md:border-none">
        <div className="px-5 pb-5">
          <strong className={classes.cardName}>{building.name && startCase(building.name[language])}</strong>
          <p className="truncate-5-line">{building.short_description && building.short_description[language]}</p>
        </div>
      </div>
    </div>
  );
}

const classes = {
  cardBuilding: cntl`
    group h-full bg-white
    rounded-2xl border border-solid border-2 md:border-none
    cursor-pointer overflow-hidden
    mx-2 xl:px-0 xl:mx-4
  `,
  cardImage: cntl`rounded-t-2xl xl:rounded-b-2xl`,
  cardName: cntl`
    text-lg font-bold group-hover:text-primary-orange block pb-2 pt-4 md:pb-0 md:pt-0
  `
};

export default CardBuilding;
