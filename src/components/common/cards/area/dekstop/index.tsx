import cntl from 'cntl';
import { useTranslation } from 'next-i18next';
import get from 'lodash/get';

import Image from '@components/core/image';
import Link from '@components/core/link';
import { Language, LocaleNamespace } from '@constants/enum';
import { Area } from '@interfaces/search';
import { getImageUrl } from '@utils';
import { stringifyUrl, getDefaultParams } from '@flexstay.rentals/fs-url-stringify';
import { useCallback } from 'react';

interface Props {
  areas: Area[];
}

function CardAreaDesktop(props: Props) {
  const { t, i18n } = useTranslation(LocaleNamespace.Home);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);
  const language = i18n.language as Language;

  const getAreaLink = useCallback(
    (area: Area) => {
      return stringifyUrl({
        request: {
          ...getDefaultParams(),
          where: area.name[language],
          areaId: area.id,
          index_serp_types: ['rents']
        },
        t: tCommon
      });
    },
    [language, tCommon]
  );

  return (
    <div className={classes.container}>
      <div className={classes.areas}>
        {props.areas.map((area) => {
          const teaser = area.short_teaser || area.teaser;
          const areaLink = getAreaLink(area);
          return (
            <Link href={areaLink.path} rel={areaLink.rel} className={classes.areaLink} key={area.id}>
              <figure className="col-start-1 col-end-3">
                <Image
                  className="rounded-l-2xl"
                  src={getImageUrl(get(area, 'images[0].url'), process.env.NEXT_PUBLIC_EGSWING_API_BASE_URL)}
                  layout="responsive"
                  sizes="300px"
                  width={196}
                  height={215}
                  objectFit="cover"
                />
              </figure>
              <div className="col-start-3 col-end-6 p-4">
                {area.name && <h3 className="text-lg font-bold">{area.name[language]}</h3>}
                {teaser && <p className="md:my-2 truncate-3-line">{teaser[language]}</p>}
                <button className={classes.findOutNow}>{t('findOutNow')}</button>
              </div>
            </Link>
          );
        })}
      </div>
    </div>
  );
}

const classes = {
  container: cntl`
    my-8 md:px-8 xl:pl-24 xl:pr-14
  `,
  areas: cntl`
    grid grid-cols-2 2xl:grid-cols-3 gap-5
    max-h-96 2xl:max-h-full
    pr-10
    overflow-y-auto
  `,
  areaLink: cntl`
    md:grid md:grid-cols-5 bg-white rounded-2xl
  `,
  findOutNow: cntl`
    px-4 py-2
    bg-primary-orange
    text-base text-white font-medium
    rounded-lg
    hover:bg-primary-dark
    hover:text-white
    hidden md:block
  `
};

export default CardAreaDesktop;
