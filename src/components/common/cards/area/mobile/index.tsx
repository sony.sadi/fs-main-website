import { useTranslation } from 'react-i18next';
import cntl from 'cntl';
import get from 'lodash/get';

import Image from '@components/core/image';
import { Area } from '@interfaces/search';
import { Language } from '@constants/enum';
import { getImageUrl } from '@utils';

interface Props {
  area: Area;
}

const CardAreaMobile = ({ area }: Props) => {
  const { i18n } = useTranslation();
  const language = i18n.language as Language;

  return (
    <div className="m-2 md:m-3">
      <Image
        className="rounded-2xl"
        src={getImageUrl(get(area, 'images[0].url'), process.env.NEXT_PUBLIC_EGSWING_API_BASE_URL)}
        layout="responsive"
        sizes="300px"
        width={196}
        height={163}
        objectFit="cover"
      />
      {area.name && <span className={classes.name}>{area.name[language]}</span>}
    </div>
  );
};

const classes = {
  name: cntl`
    truncate-1-line block mt-1 mb-3 font-bold text-center text-lg
  `
};

export default CardAreaMobile;
