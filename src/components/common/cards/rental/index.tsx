/* eslint-disable react/no-danger */
import React, { DOMAttributes, useEffect, useMemo, useState } from 'react';
import cntl from 'cntl';
import startCase from 'lodash/startCase';
import clsx from 'clsx';
import Slider, { Settings } from 'react-slick';
import { Trans, useTranslation } from 'next-i18next';
import { useSelector, useDispatch } from 'react-redux';
import get from 'lodash/get';
import range from 'lodash/range';
import { selectIsMobile } from '@stores/slices/common';
import Image from '@components/core/image';
import Svg from '@components/core/svg';
import { Listing } from '@interfaces/listing';
import {
  getAddress,
  getImageUrl,
  getPropertyTitle,
  getTextBathroomByNumber,
  getTextBedroomByNumber,
  getTextDistance,
  getTextPrice
} from '@utils';
import { Language, Page } from '@constants/enum';
import { MAX_CARD_RENTAL_IMAGES } from '@constants';
import { selectFavoriteRentalsIds, selectParams, storePadRental } from '@stores/slices/search';
import { FavouriteRental } from '@interfaces/search';
import trimEnd from 'lodash/trimEnd';
import { ImageLoaderProps } from 'next/image';
import { SerpType } from '@flexstay.rentals/fs-url-stringify';
import styles from './Rental.module.scss';

interface ArrowProps extends DOMAttributes<HTMLSpanElement> {
  show: boolean;
}

function NextArrow(props: ArrowProps) {
  return (
    <span
      aria-hidden
      className={clsx(styles['arrow--right'], props.show ? 'opacity-100' : 'opacity-0', 'flex-center')}
      onClick={props.onClick}>
      <Svg className={styles.arrow__svg} name="right-arrow" />
    </span>
  );
}

function PrevArrow(props: ArrowProps) {
  return (
    <span
      aria-hidden
      className={clsx(styles['arrow--left'], props.show ? 'opacity-100' : 'opacity-0', 'flex-center')}
      onClick={props.onClick}>
      <Svg className={styles.arrow__svg} name="left-arrow" />
    </span>
  );
}

interface Props {
  className?: string;
  deal: Listing;
  cardContentClassName?: string;
  onMouseOver?: (deal: Listing) => void;
  isActive?: boolean;
  isAllowLoaded?: boolean;
  isAllowContentVisibility?: boolean; // allow css content-visibility: auto; on each item
  showId?: boolean;
  page?: Page;
  serpType?: SerpType;
}

const HOME_URL = trimEnd(process.env.NEXT_PUBLIC_HOME_URL, '/');

function CardRental({
  deal,
  cardContentClassName,
  onMouseOver,
  isActive = true,
  showId,
  page,
  serpType,
  ...props
}: Props) {
  const { t, i18n } = useTranslation();
  const language = i18n.language as Language;
  const dispatch = useDispatch();
  const [showArrows, setShowArrows] = useState(false);
  const [showLikeText, setShowLikeText] = useState(false);
  const [showSlider, setShowSlider] = useState<boolean>(false);
  const [initSlider, setInitSlider] = useState<boolean>(false);
  const isMobile = useSelector(selectIsMobile);
  const favoriteRentalsIds = useSelector(selectFavoriteRentalsIds);
  const listParams = useSelector(selectParams);
  const width = isMobile ? 404 : 343;
  const height = isMobile ? 246 : 276;
  const name = getPropertyTitle(deal, language);

  const [isLiked, setLiked] = useState<boolean>(false);
  const distanceText = useMemo(
    () =>
      getTextDistance({
        closestTransport: deal.closestTransport,
        closestTransportKm: deal.closestTransportKm,
        closestArl: deal.closestArl,
        closestBts: deal.closestBts,
        closestMrt: deal.closestMrt,
        t
      }),
    [deal.closestTransport, deal.closestTransportKm, deal.closestArl, deal.closestBts, deal.closestMrt, t]
  );

  /**
   * Generate price of Listing item
   * @returns
   */
  const generateListingPrice = () => {
    // Check if page === SerpPage, will return only 1 price type
    if (page === Page.Listings) {
      return (
        <b className={styles.card__content__price}>
          {serpType === 'sales'
            ? getTextPrice(deal.salePrice || 0, t, 'sales')
            : getTextPrice(deal.lowestPrice || 0, t)}
        </b>
      );
    }
    // Check if other pages, will show 2 type of price
    return (
      <>
        {(deal.tenure === 'sell' || deal.tenure === 'rentsell') && (
          <b className={styles.card__content__price}>{getTextPrice(deal.salePrice || 0, t, 'sales')}</b>
        )}
        {(deal.tenure === 'rent' || deal.tenure === 'rentsell') && (
          <b className={styles.card__content__price}>{getTextPrice(deal.lowestPrice || 0, t)}</b>
        )}
      </>
    );
  };

  const settings: Settings = {
    dots: true,
    arrows: true,
    infinite: true,
    lazyLoad: 'ondemand',
    // autoplay: true,
    draggable: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <NextArrow show={showArrows} />,
    prevArrow: <PrevArrow show={showArrows} />,
    dotsClass: styles.slider__dots,
    appendDots: (dots: React.ReactNode) => (
      <div>
        <ul className={clsx(styles.slider__item_dots, 'card-rental__slick-dots')}>{dots}</ul>
      </div>
    ),
    customPaging: () => <div className={clsx(styles.slider__custom, 'card-rental__slick-paging')} />
  };

  const handleShowSlider = () => {
    if (!showSlider) {
      setShowSlider(true);
    }
  };

  const handleMouseEnter = () => {
    handleShowSlider();
    setShowArrows(true);
  };

  const handleMouseLeave = () => {
    setShowArrows(false);
  };

  const handleTouchMove = (e: React.TouchEvent<HTMLDivElement>) => {
    e.stopPropagation();
  };

  const handleMarkerMouseOver = (e: React.SyntheticEvent) => {
    e.preventDefault();
    e.stopPropagation();
    onMouseOver && onMouseOver(deal);
  };

  const handleClickHeartIcon = (e: React.TouchEvent<HTMLDivElement> | React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    e.preventDefault();
    const favouriteTemplate: FavouriteRental = {
      user_id: 'anonymous',
      added_at: new Date().toISOString(),
      rental_id: deal.id,
      source: 'WEBSITE'
    };
    dispatch(storePadRental(favouriteTemplate));
    setShowLikeText(false);
  };

  const handleHoverHeartIcon = () => {
    !isLiked && setShowLikeText(true);
  };

  const handleBlurHeartIcon = () => {
    setShowLikeText(false);
  };

  /**
   * Add logic when image in slider finish load
   */
  const finishLoadImageSlider = () => {
    if (!initSlider) {
      setInitSlider(true);
    }
  };

  /**
   * Dots are used for renderPreFocusSlider
   * @returns
   */
  const renderPreDots = () => {
    const lengthArrays = deal.cdnImages && Array.isArray(deal.cdnImages) ? deal.cdnImages.length : 0;
    const dotCount = Math.min(MAX_CARD_RENTAL_IMAGES, lengthArrays);
    const classDots = clsx(
      styles.slider__dots,
      styles.slider__item_dots,
      'card-rental-prefocus__slick-dots card-rental__slick-dots'
    );

    return (
      <div className={classDots}>
        {range(dotCount).map((value, index) => {
          return (
            <div
              key={index}
              className={clsx(value === 0 && 'slick-active', 'card-rental__slick-paging', styles.slider__custom)}
            />
          );
        })}
      </div>
    );
  };

  /**
   * Custom loader for image
   * @param param0
   * @returns
   */
  const customLoader = ({ src, width, quality }: ImageLoaderProps) => {
    return `${HOME_URL}/_next/image?url=${getImageUrl(src)}&w=${width}&q=${quality || 75}`;
  };

  /**
   * When User do not focus or touch to image
   * show default image instead Slider to avoid large DOM rendered
   * @returns
   */
  const renderPreFocusSlider = () => {
    const classPreFocus = clsx(initSlider ? 'invisible z-1' : 'z-2', styles.card__pre_forcus);
    return (
      <div className={classPreFocus}>
        {deal.cdnImages.slice(0, MAX_CARD_RENTAL_IMAGES) && (
          <Image
            alt={`image_rental-${get(deal.cdnImages.slice(0, MAX_CARD_RENTAL_IMAGES)[0], 'id')}`}
            loader={customLoader}
            className={styles.card__pre_focus}
            src={get(deal.cdnImages.slice(0, MAX_CARD_RENTAL_IMAGES)[0], 'url')}
            sizes="(min-width: 1600px) 500px, 300px"
            layout="responsive"
            width={width}
            height={height}
            objectFit="cover"
            priority
          />
        )}
        {renderPreDots()}
      </div>
    );
  };

  const renderIdBox = useMemo(() => {
    if (!showId) return <></>;
    return (
      <div className={styles.id_box}>
        <div>
          <span>ID: {deal.id}</span>
        </div>
      </div>
    );
  }, [deal.id, showId]);

  useEffect(() => {
    setLiked(favoriteRentalsIds.includes(deal.id));
  }, [favoriteRentalsIds, deal.id, setLiked]);

  return (
    // eslint-disable-next-line jsx-a11y/mouse-events-have-key-events
    <div
      className={clsx(
        'cnt-vis--auto',
        styles.card,
        isActive ? 'cursor-pointer' : 'cursor-not-allowed',
        props.className
      )}
      onMouseOver={handleMarkerMouseOver}>
      <div
        className="relative"
        onTouchStart={handleShowSlider}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        onTouchMove={handleTouchMove}>
        {showSlider && (
          <div className={clsx(initSlider ? 'z-2' : 'z-1', styles['slider--absolute'])}>
            <Slider {...settings}>
              {deal.cdnImages.slice(0, MAX_CARD_RENTAL_IMAGES).map((item, index) => (
                <Image
                  key={index}
                  className={styles.card__pre_focus}
                  src={item.url}
                  sizes="(min-width: 1600px) 500px, 300px"
                  layout="responsive"
                  width={width}
                  height={height}
                  objectFit="cover"
                  priority
                  loader={customLoader}
                  onLoad={(e) => finishLoadImageSlider()}
                />
              ))}
            </Slider>
          </div>
        )}
        {!isActive && (
          <div className={styles.active}>
            <p className="truncate-2-line">{t('inactiveRental')}</p>
          </div>
        )}
        {renderPreFocusSlider()}
        {renderIdBox}
      </div>
      <div
        className={clsx(
          styles.card__content,
          cardContentClassName,
          (page === Page.Listings || page === Page.Propertypad) && 'bg-secondary-darkwhite'
        )}>
        <div className={styles.card__content_head}>
          <div className={styles.card__content_head__text}>
            <strong className={clsx(styles.card__name, 'truncate-2-line')}>{startCase(name)}</strong>
          </div>
          <div className={styles.card__content_head__icon}>
            <div
              aria-hidden="true"
              style={!isActive ? { zIndex: 6 } : {}}
              className="cursor-pointer"
              onTouchEnd={(e) => isMobile && handleClickHeartIcon(e)}
              onClick={(e) => !isMobile && handleClickHeartIcon(e)}>
              <Svg
                onMouseOver={handleHoverHeartIcon}
                onMouseLeave={handleBlurHeartIcon}
                className={styles.card__icon_heart}
                name={isLiked ? 'liked-heart' : 'outline-heart'}
              />
            </div>
            <span className={clsx(styles.like, showLikeText ? 'visible' : 'invisible')}>like</span>
          </div>
        </div>
        <p className={styles.card__address}>{getAddress(deal, language)}</p>
        <div className={styles.listing_price}>{generateListingPrice()}</div>

        <div className={styles.card__info}>
          <div className={styles.card__icon_detail__bed}>
            <Svg className={styles.card__icon_svg} name="detail-bed" />
            {getTextBedroomByNumber(deal.numberBedrooms, t)}
          </div>
          <span className={styles.card__icon_detail__bathroom}>
            <Svg className={styles.card__icon_svg} name="detail-bathroom" />
            {getTextBathroomByNumber(deal.numberBathrooms, t)}
          </span>
          <span className={styles.card__icon_detail__sqm}>
            <Svg className={styles.card__icon_svg} name="detail-sqm" />
            {/*
              - This strange "a" is to avoid:
                Trans(...): Nothing was returned from render
              - Tested and it does not affect how real text is displayed, will check again later for a better solution
            */}
            <Trans i18nKey="floorSizeHtml" values={{ size: deal.floorSize }} components={{ sup: <sup /> }}>
              a
            </Trans>
          </span>

          {deal.petsAllowed && (
            <span className={styles.card__icon_detail__condo}>
              <Svg className={styles.card__icon_svg} name="detail-condo" />
              {t('petFriendly')}
            </span>
          )}
          {deal.closestTransport && deal.closestTransportKm && (
            <span className={styles.card__icon_detail__train}>
              <Svg className={styles.card__icon_svg} name="gray-train" />
              {distanceText}
            </span>
          )}
        </div>
      </div>
      {!isActive && <div aria-hidden className={styles.active_overlay} style={{ zIndex: 5 }} />}
    </div>
  );
}

export default CardRental;
