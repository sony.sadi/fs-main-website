import { useCallback } from 'react';
import cntl from 'cntl';
import { TFunction, useTranslation } from 'next-i18next';

import Svg from '@components/core/svg';
import Image from '@components/core/image';
import { Listing } from '@interfaces/listing';
import { getImageUrl, getBedroomNumber, getBathroomNumber, formatPrice } from '@utils';
import { LocaleNamespace } from '@constants/enum';

interface FsCompactCardProps extends Listing {
  // eslint-disable-next-line
  className?: string;
  t: TFunction;
  price: string;
}

function FsCompactCard(props: FsCompactCardProps) {
  const { t } = props;
  const name = props.buildingName || props.listingTitleEn;
  const ratio = { width: 16, height: 9 };

  const handleError = useCallback((event) => {
    event.target.src = '/default.gif';
  }, []);

  return (
    <div className={classes.container}>
      <div className={classes.imageContainer}>
        <div className="relative">
          <Image
            src={getImageUrl(props.featuredImageUrl)}
            className="rounded-t-lg"
            layout="responsive"
            objectFit="cover"
            sizes="300px"
            loading="eager"
            width={ratio.width}
            height={ratio.height}
            alt={name}
            onError={handleError}
          />
        </div>

        <div className={classes.backdrop}>
          <div className={classes.overlay}>
            <div className="flex text-white">
              <div className="mr-2 md:mr-3 flex items-center">
                <Svg className={classes.icon} name="detail-bed" />
                <span>{getBedroomNumber(props.numberBedrooms)}</span>
              </div>
              <div className="mr-2 md:mr-3 flex items-center">
                <Svg className={classes.icon} name="detail-bathroom" />
                <span>{getBathroomNumber(props.numberBathrooms)}</span>
              </div>
              <div className="mr-2 md:mr-3 flex items-center">
                <Svg className={classes.icon} name="detail-sqm" />
                <span>{props.floorSize}</span>
              </div>
            </div>
          </div>
          <div className="mb-4 mx-5 text-white">
            <b className="text-lg">{props.price}</b>
          </div>
        </div>
      </div>
    </div>
  );
}

const classes = {
  container: cntl`cursor-pointer rounded-lg`,
  imageContainer: cntl`relative overflow-hidden fs-card-image`,
  backdrop: cntl`abs-full bottom-0 flex flex-col-reverse bg-card-backdrop`,
  overlay: cntl`flex justify-between mx-5 mb-3`,
  icon: cntl`mr-2 w-3 h-3 fill-current text-white`
};

export default FsCompactCard;
