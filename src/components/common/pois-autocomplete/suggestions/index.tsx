import cntl from 'cntl';
import { useCallback, useEffect, useRef, useState } from 'react';

import Svg from '@components/core/svg';
import { PoiSuggestion } from '@interfaces/search';
import { PoiType } from '@flexstay.rentals/fs-url-stringify';
import { useTranslation } from 'react-i18next';
import { Language } from '@constants/enum';
import { startCasePoiName } from '@utils';

const POI_ITEM_HEIGHT = 32;

interface SuggestionItem extends PoiSuggestion {
  description: string;
}

interface SuggestionsProps {
  isHeader: boolean;
  highlightedIndex: number;
  pois: PoiSuggestion[];
  onSelectChange: (poi: SuggestionItem) => void;
}

function Suggestions(props: SuggestionsProps) {
  const { i18n } = useTranslation();
  const containerRef = useRef<HTMLDivElement>(null);
  const [internalPois, setInternalPois] = useState<SuggestionItem[]>([]);
  const [googlePois, setGooglePois] = useState<SuggestionItem[]>([]);
  const language = i18n.language as Language;

  const getIconName = useCallback((type: PoiType) => {
    return {
      [PoiType.AREA]: 'rounded-corners-square',
      [PoiType.BUILDING]: 'detail-condo',
      [PoiType.TRANSPORTATION]: 'gray-train',
      [PoiType.GOOGLE]: 'marker'
    }[type];
  }, []);

  const mapSuggestion = useCallback(
    (poi: PoiSuggestion): SuggestionItem => {
      let description = startCasePoiName(poi.name[language]);

      // Prefix transportationSystem before search keyword
      if (PoiType.TRANSPORTATION === poi.poi_type) {
        description = `${poi.transportation_system} ${description}`;
      }
      return { ...poi, description };
    },
    [language]
  );

  const setPois = useCallback((pois: PoiSuggestion[]) => {
    const gmapPoisValue: SuggestionItem[] = [];
    const internalPoisValue: SuggestionItem[] = [];
    pois.forEach((poi) => {
      const poiSuggestion = mapSuggestion(poi);
      if (poi.poi_type === PoiType.GOOGLE) {
        gmapPoisValue.push(poiSuggestion);
      } else {
        internalPoisValue.push(poiSuggestion);
      }
    });
    setInternalPois(internalPoisValue);
    setGooglePois(gmapPoisValue);
  }, []);

  useEffect(() => {
    setPois(props.pois);
  }, [props.pois]);

  const getHighligh = useCallback(
    (poi: PoiSuggestion) => {
      const highlightPoi = props.pois[props.highlightedIndex];
      if (!highlightPoi) return false;
      return poi.poi_type === PoiType.GOOGLE ? poi.place_id === highlightPoi.place_id : poi.id === highlightPoi.id;
    },
    [props.pois, props.highlightedIndex]
  );

  const renderItems = useCallback(
    (pois: SuggestionItem[]) => {
      return pois.map((poi) => {
        const { poi_type } = poi;
        const key = `${poi.poi_type}:${poi.id || poi.place_id}`;
        return (
          <div
            aria-hidden="true"
            onClick={() => props.onSelectChange(poi)}
            className={classes.suggestion(getHighligh(poi))}
            key={key}>
            <Svg className={classes.icon(poi_type === PoiType.GOOGLE)} name={getIconName(poi_type)} />
            <span className="truncate">{poi.description}</span>
          </div>
        );
      });
    },
    [props.highlightedIndex]
  );

  useEffect(() => {
    containerRef.current?.scrollTo(0, POI_ITEM_HEIGHT * props.highlightedIndex);
  }, [props.highlightedIndex]);

  return (
    <div ref={containerRef} className={classes.container(props.isHeader)}>
      {renderItems(internalPois)}
      {!!googlePois.length && !!internalPois.length && <div className="border-b" />}
      {renderItems(googlePois)}
    </div>
  );
}

const classes = {
  container: (isHeader: boolean) => cntl` top-full
    absolute z-40 max-h-48 overflow-y-auto px-4 py-1 border shadow rounded w-full bg-white
    ${isHeader ? 'mt-6 left-1/2 max-w-sm transform -translate-x-1/2' : 'mt-3'}
  `,
  suggestion: (active: boolean) => cntl`
    flex items-center cursor-pointer py-1.5 text-sm hover:text-primary-red fs-map-input-suggestion
    ${active ? 'text-primary-red' : ''}
  `,
  icon: (gray?: boolean) => cntl`
    flex-shrink-0 w-3 h-4 mr-5
    ${gray ? 'fill-current text-primary-lightgray' : ''}
  `
};

export default Suggestions;
