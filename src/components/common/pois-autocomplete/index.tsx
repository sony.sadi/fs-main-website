import React, { ChangeEvent, HTMLAttributes, useCallback, useEffect, useRef, useState } from 'react';
import cntl from 'cntl';
import Svg from '@components/core/svg';
import { PoiSuggestion } from '@interfaces/search';
import { useTranslation } from 'react-i18next';
import { BasicSearchTheme, Language } from '@constants/enum';
import { getPoisSuggestion } from '@services/http-client/search';
import { KEYBOARD_CODE } from '@constants';
import Suggestions from './suggestions';
import FilterSearch from '../filter-search';

interface Props extends HTMLAttributes<HTMLDivElement> {
  isHeader?: boolean;
  onSelectChange?: (poi: PoiSuggestion) => void;
  onInputChange?: (term: string) => void;
  onSearch?: () => void;
  isListingsResult?: boolean;
  address?: string;
  onFocus?: (e: React.FocusEvent<HTMLInputElement>) => void;
  theme?: BasicSearchTheme;
  isShowFilter?: boolean;
}

const PoisAutocomplete = ({
  isHeader,
  isListingsResult,
  onInputChange,
  onSelectChange,
  onSearch,
  isShowFilter,
  theme = BasicSearchTheme.Light,
  ...props
}: Props) => {
  const { i18n } = useTranslation();
  const wrapperRef = useRef<HTMLDivElement>(null);
  const [pois, setPois] = useState<PoiSuggestion[]>([]);
  const locale = i18n.language as Language;
  const [highlightedItemIndex, setHighlightedItemIndex] = useState(-1);
  const [focused, setFocused] = useState(false);
  const [address, setAddress] = useState(props.address || '');

  const searchPois = useCallback(
    async (_q: string) => {
      try {
        const { data } = await getPoisSuggestion({ _q, limit: 100, locale });
        setPois(data.data);
      } catch (error) {
        console.error(error);
      }
    },
    [locale]
  );

  const handleChange = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    const inputValue = e.target.value;
    setAddress(inputValue);
    !inputValue && setPois([]);
  }, []);

  useEffect(() => {
    let timeout: NodeJS.Timeout;
    if (address && focused) {
      timeout = setTimeout(() => {
        onInputChange?.(address);
        searchPois(address);
      }, 300);
    }
    return () => clearTimeout(timeout);
  }, [address]);

  const handleFocus = useCallback((e: React.FocusEvent<HTMLInputElement>) => {
    setFocused(true);
    props.onFocus?.(e);
  }, []);

  const handleKeyDown = (event: React.KeyboardEvent) => {
    if (event.key === 'ArrowDown') {
      setHighlightedItemIndex((highlightedItemIndex + 1) % pois.length);
    } else if (event.key === 'ArrowUp') {
      setHighlightedItemIndex(highlightedItemIndex <= 0 ? pois.length - 1 : highlightedItemIndex - 1);
    } else if (event.keyCode === KEYBOARD_CODE.ENTER) {
      const highlightPoi = pois[highlightedItemIndex];
      if (highlightPoi) {
        return onSelectChange?.(highlightPoi);
      }
      onSearch?.();
    }
  };

  const handleSelectChange = useCallback(
    (poi: PoiSuggestion & { description: string }) => {
      setAddress(poi.description);
      setFocused(false);
      onSelectChange?.(poi);
    },
    [onSelectChange]
  );

  useEffect(() => {
    const handleClickOutside = (e: MouseEvent) => {
      if (!wrapperRef.current) return;
      if (!wrapperRef.current.contains(e.target as Node)) {
        setFocused(false);
      }
    };
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, []);

  useEffect(() => {
    setAddress(props.address || '');
  }, [props.address]);

  useEffect(() => {
    !focused && setPois([]);
  }, [focused]);

  return (
    <div ref={wrapperRef} className="h-11 relative">
      <div className={classes.searchBox(!!isHeader, !!isListingsResult, theme)}>
        <Svg className="w-5 h-5 cursor-pointer" name="search" />
        <input
          value={address}
          onKeyDown={handleKeyDown}
          className={classes.searchInput(!!isHeader, !!isListingsResult)}
          autoComplete="off"
          {...props}
          onChange={handleChange}
          onFocus={handleFocus}
        />
        <FilterSearch theme={BasicSearchTheme.Dark} />
      </div>
      {!!pois.length && focused && (
        <Suggestions
          onSelectChange={handleSelectChange}
          isHeader={!!isHeader}
          highlightedIndex={highlightedItemIndex}
          pois={pois}
        />
      )}
    </div>
  );
};

const classes = {
  searchBox: (isHeader: boolean, isListingsResult: boolean, theme: BasicSearchTheme) => cntl`
    absolute top-0
    flex items-center w-full h-11 ${!isListingsResult && 'max-w-lg'}  px-3 bg-white rounded-md
    ${
      // eslint-disable-next-line
      isHeader ? 'bg-home-gray' : isListingsResult ? 'bg-secondary-darkwhite' : ''
    }
    ${isListingsResult ? 'rounded-xl' : ''}
    ${theme === BasicSearchTheme.Dark && 'border border-primary-gray'}
  `,
  searchInput: (isHeader: boolean, isListingsResult: boolean) => cntl`
    truncate
    flex-1
    ml-2
    text-sm text-primary-gray font-light 
    placeholder-primary-gray focus:outline-none
    ${isHeader || isListingsResult ? 'bg-transparent' : ''}
    fs-map-input
  `,
  searchBtn: (disabled: boolean) => cntl`
    px-4 py-1 
    text-lg font-medium  
    rounded-lg border-0 focus:outline-none
    fs-home-search-button
    ${disabled ? 'bg-secondary-gray text-primary-gray' : 'bg-primary-orange text-white'}
  `
};

export default PoisAutocomplete;
