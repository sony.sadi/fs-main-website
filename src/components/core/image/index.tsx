import NextImage, { ImageProps } from 'next/image';
import { useUnoptimizedImage } from '@utils';

function Image(props: ImageProps) {
  return <NextImage unoptimized={useUnoptimizedImage(props.src as string)} {...props} />;
}

export default Image;
