import { useState, useRef, useEffect } from 'react';

interface Props {
  children: (visible: boolean) => JSX.Element | boolean;
  root?: Element | null;
  offsetBottom?: string;
}

const Observer = (props: Props) => {
  const container = useRef<HTMLDivElement>(null);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    if (!container.current) return;

    const options: IntersectionObserverInit = {
      root: props.root,
      rootMargin: `0px 0px ${props.offsetBottom || '0px'} 0px`
    };

    const callback: IntersectionObserverCallback = ([entry], observer) => {
      if (entry.isIntersecting) {
        setVisible(entry.isIntersecting);
        observer.unobserve(entry.target);
      }
    };

    const io = new IntersectionObserver(callback, options);
    io.observe(container.current);

    return () => {
      io.disconnect();
    };
  }, []);

  return <div ref={container}>{props.children(visible)}</div>;
};

export default Observer;
