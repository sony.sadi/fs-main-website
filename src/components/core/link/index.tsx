import NextJSLink, { LinkProps } from 'next/link';
import trimEnd from 'lodash/trimEnd';
import isString from 'lodash/isString';
import { Language } from '@constants/enum';
import { useTranslation } from 'react-i18next';
import { i18n as i18nNext } from 'next-i18next';
import { useMemo } from 'react';
import { join } from 'path';
import { isUrl } from '@utils';

const HOME_URL = trimEnd(process.env.NEXT_PUBLIC_HOME_URL, '/');

interface Props extends React.PropsWithChildren<LinkProps> {
  className?: string;
  rel?: string;
  dataLabel?: string;
}

function Link({ className, children, prefetch = false, rel, dataLabel, ...props }: Props) {
  const { i18n } = useTranslation();
  const language = (i18n.language as Language) || i18nNext?.language;

  const base = useMemo(() => {
    return language === Language.TH ? HOME_URL : `${HOME_URL}/${language}`;
  }, [language]);

  const href = useMemo(() => {
    if (!isString(props.href)) return props.href;
    return isUrl(props.href) ? props.href : `${base}${join('/', props.href, '/')}`;
  }, [props.href, base]);

  const as = useMemo(() => {
    if (!props.as) return props.as;

    if (!isString(props.as)) return props.as;

    return isUrl(props.as) ? props.as : `${base}${join('/', props.as, '/')}`;
  }, [props.as, base]);

  return (
    <NextJSLink {...props} href={href} as={as} prefetch={prefetch}>
      <a data-label={dataLabel} {...{ rel }} className={className}>
        {children}
      </a>
    </NextJSLink>
  );
}

export default Link;
