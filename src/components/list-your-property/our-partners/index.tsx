import OurPartners from '@components/common/our-partners';
import { Page } from '@constants/enum';

function ListYourPropertyOurPartners() {
  return <OurPartners page={Page.ListYourProperty} />;
}

export default ListYourPropertyOurPartners;
