import cntl from 'cntl';
import { useTranslation } from 'next-i18next';

import faqs from '@assets/data/list-your-property-FAQ.json';
import HowItWorksContact from '@components/how-it-works/contact';
import Svg from '@components/core/svg';
import { LocaleNamespace, Page } from '@constants/enum';
import FAQItem from '@components/common/faq-item';

function FAQ() {
  const { t } = useTranslation(LocaleNamespace.ListYourProperty);

  return (
    <section className={classes.faq}>
      <div className="w-full">
        <h3 className={classes.title}>{t('faqTitle')}</h3>
        {faqs.map((faq, indexFAQ) => {
          const question = t(faq.question);
          const answers = faq.answers.map((answer) => t(answer));
          return <FAQItem key={indexFAQ} index={indexFAQ} question={question} answers={answers} />;
        })}
        <>
          <input type="checkbox" id="slide-toggle-property-contact" className={classes.slideToggleCheckbox} />
          <label htmlFor="slide-toggle-property-contact" className={classes.slideToggleLabel}>
            <h3 className="text-lg font-bold">{t('faqContactQuestion')}</h3>
            <Svg className={classes.iconArrowBottom} name="top-arrow" />
          </label>
          <div className={classes.slideToggleFAQ}>
            <p className={classes.note}>{t('faqContactNote')}</p>
            <HowItWorksContact page={Page.ListYourProperty} />
          </div>
        </>
      </div>
    </section>
  );
}

const classes = {
  iconArrowBottom: cntl`slide-toggle__icon ml-3 h-2 w-4 self-center`,
  slideToggleCheckbox: cntl`slide-toggle__checkbox`,
  slideToggleLabel: cntl`slide-toggle__label flex pb-2.5 lg:pb-0 cursor-pointer mb-5 content`,
  slideToggleFAQ: cntl`slide-toggle__faq content`,

  faq: cntl`
    pt-14
    flex
    px-4 sm:px-8 lg:px-4 xl:px-0
  `,
  title: `text-4xl font-bold mb-8`,
  note: cntl`font-bold text-primary-orange mb-5`
};

export default FAQ;
