import cntl from 'cntl';
import { useTranslation } from 'next-i18next';

import ourServices from '@assets/data/list-your-property-our-services.json';
import { LocaleNamespace } from '@constants/enum';
import OurServicesItem from '@components/list-your-property/our-service-item';

const OurService = () => {
  const { t } = useTranslation(LocaleNamespace.ListYourProperty);

  return (
    <div className={classes.ourServices}>
      <p className={classes.heading}>{t('ourServicesHeading')}</p>
      {ourServices.map(({ icon, name, description, content }) => (
        <OurServicesItem key={name} icon={icon} name={name} description={description} content={content} />
      ))}
    </div>
  );
};

const classes = {
  ourServices: cntl`
  px-4 lg:px-0 grid gap-y-3 pb-5
  `,
  heading: cntl`font-bold text-4xl pb-5 xl:py-5 px-4 lg:px-0`
};

export default OurService;
