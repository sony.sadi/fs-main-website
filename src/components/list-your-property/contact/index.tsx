import Contact from '@components/common/contact';
import { Page } from '@constants/enum';

function ListYourPropertyContact() {
  return <Contact page={Page.ListYourProperty} />;
}

export default ListYourPropertyContact;
