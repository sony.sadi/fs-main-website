import { useCallback, useState } from 'react';
import cntl from 'cntl';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { selectIsMobile } from '@stores/slices/common';
import Svg from '@components/core/svg';
import ContactForm from '@forms/contact';
import { LocaleNamespace, Page } from '@constants/enum';

function Header() {
  const { t } = useTranslation(LocaleNamespace.ListYourProperty);
  const isMobile = useSelector(selectIsMobile);
  const [visible, setVisible] = useState(false);

  const handleOpen = useCallback(() => {
    setVisible(true);
  }, []);

  const handleClose = useCallback(() => {
    setVisible(false);
  }, []);
  return (
    <>
      <section className="px-4 lg:px-0">
        <header className={classes.header}>
          <div className={classes.background}>
            <div className={classes.text}>
              <h2 className="font-bold list-your-property__title">{t('TitleProperty')}</h2>
              <p className={classes.subTitle}>{t('SubTitleProperty')}</p>
              <p className="mt-2 text-base list-your-property__description">{t('Description')}</p>
            </div>
          </div>
        </header>
        <div className={classes.divMobile}>
          {isMobile && (
            <>
              <a href="tel:+66922643444" id="search-target" className={classes.button(true)}>
                <Svg className="fill-current text-white w-5 h-5 mr-2" name="phone" />
                {t('headerCallUsNow')}
              </a>
              <button id="search-target" onClick={handleOpen} className={classes.button(false)}>
                {t('headerContactUs')}
              </button>
            </>
          )}
        </div>
        <div className={classes.modal({ visible })}>
          <div className={classes.close} onClick={handleClose} aria-hidden="true">
            <Svg className={classes.iconClose} name="close" />
          </div>
          {visible && <ContactForm page={Page.ListYourProperty} />}
        </div>
      </section>
      {visible && <div onClick={handleClose} aria-hidden className={classes.closeButton} />}
    </>
  );
}

const classes = {
  background: cntl`
    relative
    bg-list-your-property-mobile lg:bg-list-your-property-desktop
    bg-no-repeat bg-cover
    w-full sm:w-list-your-property-mobile md:w-auto xl:w-full
    h-list-your-property-mobile lg:h-list-your-property-desktop
    grid items-center justify-items-center
  `,
  subTitle: cntl`
    mt-4 text-lg md:text-base font-bold
    list-your-property__secondary-heading
  `,
  header: cntl`
    sm:px-4 lg:px-0 mb-8 xl:mb-0 lg:contents
  `,
  text: cntl`
    p-5 text-white
    flex flex-col justify-center
    list-your-property__text
  `,
  closeButton: cntl`
    fixed h-full w-full inset-0 bg-black bg-opacity-50 z-50
  `,
  divMobile: cntl`
    fixed bottom-0 left-0 right-0 grid grid-cols-2 gap-x-2 items-end xl:block py-5 px-5 xl:py-0 xl:ml-7 xl:px-0 z-50
  `,
  button: (isInquiry: boolean) => cntl`
    px-4 py-1 h-12
    text-lg font-medium text-white
    rounded-lg fs-home-search-button border-0 focus:outline-none
    flex justify-center items-center

    ${isInquiry ? cntl`bg-primary-dark` : cntl`bg-primary-orange`}
  `,
  modal: ({ visible }: { visible: boolean }) => cntl`
    duration-300
    text-center top-32
    top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 fixed transform transition-transform z-photos-modal
    duration-300
    ${visible ? cntl`scale-100` : cntl`scale-0`}
  `,
  close: cntl`absolute -top-12 transform left-1/2 -translate-x-1/2`,
  iconClose: cntl`
  cursor-pointer fill-current h-10 hover:text-primary-black text-6xl text-white w-10
  `,
  overlayContainer: ({ hidden }: { hidden: boolean }) => cntl`
    fixed left-0 bottom-0 right-0
    p-4
    text-center bg-white
    border-t
    border-primary-lightgray
    ${hidden ? cntl`hidden` : cntl`block`}
  `,
  overlayButton: cntl`
    py-2 px-4
    text-sm font-medium font-semibold text-white uppercase tracking-widest
    bg-primary-red rounded-md
    focus:outline-none focus:border-transparent
  `
};

export default Header;
