import cntl from 'cntl';
import { useTranslation } from 'next-i18next';
import Svg from '@components/core/svg';
import { LocaleNamespace } from '@constants/enum';

interface Props {
  icon: string;
  name: string;
  description: string;
  content: string[];
}

function OurServicesItem(props: Props) {
  const { t } = useTranslation(LocaleNamespace.ListYourProperty);

  return (
    <div key={props.name} className={classes.itemServices}>
      <div>
        <Svg name={props.icon} className={classes.iconService} />
      </div>

      <div className="ml-2 lg:ml-4 flex flex-col">
        <div className={classes.boxName}>
          <p className="text-lg font-bold">{t(props.name)}</p>
          <span>{t(props.description)}</span>
        </div>
        <div className={classes.contentService}>
          {props.content.map((item, index) => (
            <div key={index} className="pl-5 relative">
              <Svg name="red-pentagon" className={classes.iconPentagonRed} />
              <span className="">{t(item)}</span>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

const classes = {
  ourServices: cntl`
  px-4 lg:px-0 grid gap-y-3 pb-5
  `,
  heading: cntl`font-bold text-4xl pb-5 xl:py-5 px-4 lg:px-0`,
  itemServices: cntl`
    flex
    m-px
    box-border w-full
    group hover:border hover:border-primary-orange hover:bg-primary-pink hover:m-0 hover:border
    px-4 sm:py-5 rounded-2xl pb-3
  `,
  iconService: cntl`
    w-16 h-16 mt-3
  `,
  boxName: cntl`
    box-border w-full
    bg-how-it-works-step rounded-2xl pl-5 py-3
    group-hover:bg-primary-pink group-hover:border group-hover:border-primary-orange
  `,
  contentService: cntl`gap-y-5 grid mt-7`,
  iconPentagonRed: cntl`absolute top-2 left-1 w-2 h-2`
};

export default OurServicesItem;
