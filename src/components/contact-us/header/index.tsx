import cntl from 'cntl';

import { Page } from '@constants/enum';
import ContactForm from '@forms/contact';

const Header = () => {
  return (
    <header className={classes.headerContact}>
      <div className={classes.form}>
        <ContactForm page={Page.Contact} />
      </div>
    </header>
  );
};

const classes = {
  headerContact: cntl`
    relative
    bg-contact-us-mobile lg:bg-contact-us-desktop w-full
    bg-no-repeat bg-cover
    h-how-it-works-mobile lg:h-how-it-works-tablet
  `,
  form: cntl`
    absolute left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2
    lg:relative lg:flex lg:justify-end 
    lg:px-8 xl:px-24 lg:ml-auto
  `
};

export default Header;
