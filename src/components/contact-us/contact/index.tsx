import { useTranslation } from 'next-i18next';

import Contact from '@components/common/contact';
import { LocaleNamespace, Page } from '@constants/enum';
import cntl from 'cntl';

function ContactUsInfo() {
  const { t } = useTranslation(LocaleNamespace.Contact);
  return (
    <div className={classes.container}>
      <p className={classes.heading}>{t('ourContact')}</p>
      <Contact page={Page.Contact} />
    </div>
  );
}

const classes = {
  container: cntl`
    px-4 sm:px-8 xl:px-20 pt-14
  `,
  heading: cntl`
    text-4xl px-10 text-center mb-7 font-bold xl:text-left xl:px-0
  `
};

export default ContactUsInfo;
