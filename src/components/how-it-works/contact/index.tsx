import Contact from '@components/common/contact';
import { Page } from '@constants/enum';

function HowItWorksContact({ page = Page.HowItWorks }: { page?: Page }) {
  return <Contact page={page} />;
}

export default HowItWorksContact;
