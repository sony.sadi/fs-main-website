import dataSteps from '@assets/data/how-it-works.json';
import { useTranslation } from 'next-i18next';
import { LocaleNamespace } from '@constants/enum';
import cntl from 'cntl';
import Link from '@components/core/link';
import Svg from '@components/core/svg';
import { useMemo } from 'react';
import { getSerpBaseUrl } from '@utils';

function HowItWorks() {
  const { t } = useTranslation(LocaleNamespace.HowItWorks);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);

  /**
   * Rent serpt Url
   */
  const serpUrl = useMemo(() => {
    return getSerpBaseUrl(tCommon);
  }, [tCommon]);

  /**
   * Sale serp Url
   */
  const saleSerpUrl = useMemo(() => {
    return getSerpBaseUrl(tCommon, 'sales');
  }, [tCommon]);

  return (
    <section className={classes.howItWorks}>
      <div className="mb-10">
        <h3 className="text-4xl mb-1.5">{t('howItWorks')}</h3>
        <p className="text-lg">{t('howItWorksFindStep')}</p>
      </div>
      {dataSteps.map((step, index) => (
        <div className={classes.step(index + 1)} key={step.numberStep}>
          <span className={classes.numberStep}>{step.numberStep}</span>
          <span className="text-lg">{t(step.description)}</span>
        </div>
      ))}
      <div className={classes.linkContainer}>
        <Link href={serpUrl.path} rel={serpUrl.rel} className={classes.linkContact}>
          {t('howItWorksSearchAll')} <Svg className={classes.linkIcon} name="red-arrow-right" />
        </Link>
        <Link href={saleSerpUrl.path} rel={saleSerpUrl.rel} className={classes.linkContact}>
          {t('howItWorksSearchAllSale')} <Svg className={classes.linkIcon} name="red-arrow-right" />
        </Link>
      </div>
    </section>
  );
}

const classes = {
  howItWorks: cntl`bg-white px-4 sm:px-8 xl:px-24 py-14`,
  linkContainer: cntl`
  flex flex-col items-center  md:items-end
 `,
  numberStep: cntl`text-9xl text-primary-orange font-bold mr-6 xl:mr-8`,
  step: (index: number) => cntl`
  bg-how-it-works-step min-h-44 h-auto xl:w-how-it-works-step flex xl:justify-items-center items-center px-4 xl:px-6 py-8 lg:px-10 rounded-3xl mb-5 last:mb-0
  group hover:border hover:border-primary-orange hover:bg-primary-pink
  ${index % 2 === 0 ? 'xl:flex-row-reverse xl:ml-auto' : 'xl:mr-auto'}
  `,
  linkContact: cntl`flex items-center justify-center text-sm underline mt-6 md:justify-end`,
  linkIcon: cntl`ml-4 w-1 h-2`
};

export default HowItWorks;
