import { useCallback, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import cntl from 'cntl';

import { useTranslation } from 'next-i18next';
import { selectIsMobile } from '@stores/slices/common';
import InquiryForm from '@forms/inquiry';
import { LocaleNamespace, Page } from '@constants/enum';

function HowItWorksInquiry() {
  const { t } = useTranslation(LocaleNamespace.Listing);
  const isMobile = useSelector(selectIsMobile);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    if (isMobile) {
      document.body.classList.add('pb-10');
    }

    return () => {
      document.body.classList.remove('pb-10');
    };
  }, [isMobile]);

  const handleOpen = useCallback(() => {
    setVisible(true);
  }, []);

  const handleClose = useCallback(() => {
    setVisible(false);
  }, []);

  return isMobile ? (
    <>
      <div className={classes.modal({ visible })}>
        <span className={classes.close} onClick={handleClose} aria-hidden="true">
          &times;
        </span>

        <div className="abs-vertical-center w-full px-4">{visible && <InquiryForm page={Page.HowItWorks} />}</div>
      </div>

      <div className={classes.overlayContainer({ hidden: visible })}>
        <button className={classes.overlayButton} onClick={handleOpen}>
          {t('inquireNow')}
        </button>
      </div>
    </>
  ) : (
    <InquiryForm page={Page.HowItWorks} />
  );
}

const classes = {
  modal: ({ visible }: { visible: boolean }) => cntl`
    fixed top-0 left-0 w-full h-full
    bg-white text-center
    transform
    transition-transform
    duration-300
    z-photos-modal

    ${visible ? cntl`scale-100` : cntl`scale-0`}
  `,
  close: cntl`
    absolute top-4 right-8
    text-6xl text-primary-gray hover:text-primary-black cursor-pointer
  `,
  overlayContainer: ({ hidden }: { hidden: boolean }) => cntl`
    fixed left-0 bottom-0 right-0
    p-4
    text-center bg-white
    border-t
    border-primary-lightgray
    ${hidden ? cntl`hidden` : cntl`block`}
  `,
  overlayButton: cntl`
    py-2 px-4
    text-sm font-medium font-semibold text-white uppercase tracking-widest
    bg-primary-red rounded-md
    focus:outline-none focus:border-transparent
`
};

export default HowItWorksInquiry;
