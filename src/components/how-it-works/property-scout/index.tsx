import cntl from 'cntl';
import { useTranslation } from 'next-i18next';

import Link from '@components/core/link';
import offers from '@assets/data/how-it-works-property-scout.json';
import Svg from '@components/core/svg';
import { LocaleNamespace } from '@constants/enum';
import { getSerpBaseUrl } from '@utils';
import { useMemo } from 'react';

function HowItWorksPropertyScout() {
  const { t } = useTranslation(LocaleNamespace.HowItWorks);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);

  /**
   * Rent serp url
   */
  const serpUrl = useMemo(() => {
    return getSerpBaseUrl(tCommon);
  }, [tCommon]);

  /**
   * Sale serp url
   */
  const saleSerpUrl = useMemo(() => {
    return getSerpBaseUrl(tCommon, 'sales');
  }, [tCommon]);

  return (
    <section className={classes.propertyScout}>
      <header className={classes.header}>
        <h2 className={classes.headerHeading}>{t('propertyScoutHeading')}</h2>
      </header>

      <ul className={classes.ourOffers}>
        {offers.map((offer) => (
          <li className={classes.offerItem} key={offer.id}>
            <div className="px-4">
              <Svg name={offer.icon} className="w-28 h-28" />
              <strong className={classes.offerTitle}>{t(offer.title)}</strong>
              <p className="mt-2 mb-6 text-base">{t(offer.subTitle)}</p>
            </div>
            {offer.contents.map((content, index) => (
              <div key={index} className="pl-5 relative">
                <Svg name="red-pentagon" className="absolute top-2 left-1 w-2 h-2" />
                <span className="">{t(content)}</span>
              </div>
            ))}
          </li>
        ))}
      </ul>
      <div className={classes.linkContainer}>
        <Link href={serpUrl.path} rel={serpUrl.rel} className={classes.linkContact}>
          {t('howItWorksSearchAll')} <Svg className={classes.linkIcon} name="red-arrow-right" />
        </Link>
        <Link href={saleSerpUrl.path} rel={saleSerpUrl.rel} className={classes.linkContact}>
          {t('howItWorksSearchAllSale')} <Svg className={classes.linkIcon} name="red-arrow-right" />
        </Link>
      </div>
    </section>
  );
}

const classes = {
  propertyScout: cntl`
    py-14
    px-4 sm:px-8 xl:px-24
    bg-home-white
  `,
  linkContainer: cntl`
   flex flex-col items-center  md:items-end
  `,
  header: cntl`
    items-center mb-5 md:mb-10 xs:px-4 md:px-0
    text-center md:text-left
  `,
  headerHeading: cntl`text-4xl font-bold`,
  ourOffers: cntl`
    xl:mt-14
    grid xl:grid-cols-2 2xl:cols-3 gap-y-10 xl:gap-7
  `,
  offerItem: cntl`
    px-4 py-8 pt-5 pb-10
    group hover:border hover:border-primary-orange hover:bg-primary-pink
    cursor-pointer
    grid xl:block
    rounded-3xl
    bg-white
  `,
  offerTitle: cntl`
    block
    text-lg font-bold
  `,
  linkContact: cntl`flex items-center justify-center text-sm underline mt-6 md:justify-end`,
  linkIcon: cntl`ml-4 w-1 h-2`
};

export default HowItWorksPropertyScout;
