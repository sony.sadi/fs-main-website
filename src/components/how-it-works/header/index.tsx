import { useCallback, useState } from 'react';
import cntl from 'cntl';

import { useSelector } from 'react-redux';
import { selectIsMobile } from '@stores/slices/common';
import Svg from '@components/core/svg';
import ContactForm from '@forms/contact';
import { LocaleNamespace, Page } from '@constants/enum';
import { useTranslation } from 'react-i18next';

function HowItWorksHeader() {
  const isMobile = useSelector(selectIsMobile);
  const [visible, setVisible] = useState(false);
  const { t } = useTranslation(LocaleNamespace.HowItWorks);

  const handleOpen = useCallback(() => {
    setVisible(true);
  }, []);

  const handleClose = useCallback(() => {
    setVisible(false);
  }, []);
  return (
    <>
      <section>
        <header className={classes.header}>
          <div className={classes.text}>
            <h2 className="font-bold how-it-works__title">{t('headerTitle')}</h2>
            <p className="font-bold text-base mt-4">{t('headerSubTitle')}</p>
            <p className="text-base mt-2">{t('headerContent')}</p>
          </div>
          <div className={classes.divMobile}>
            {isMobile ? (
              <>
                <a href="tel:+66922643444" id="search-target" className={classes.button(true)}>
                  <Svg className="fill-current text-white w-5 h-5 mr-2" name="phone" />
                  {t('headerCallUsNow')}
                </a>
                <button id="search-target" onClick={handleOpen} className={classes.button(false)}>
                  {t('headerInquireNow')}
                </button>
                <div className={classes.modal({ visible })}>
                  <div className={classes.close} onClick={handleClose} aria-hidden="true">
                    <Svg className={classes.iconClose} name="close" />
                  </div>

                  {visible && <ContactForm page={Page.HowItWorks} />}
                </div>
              </>
            ) : (
              <ContactForm page={Page.HowItWorks} />
            )}
          </div>
        </header>
      </section>
      {visible && (
        <div onClick={handleClose} aria-hidden="true" className="fixed inset-0 bg-opacity-50 bg-black z-50" />
      )}
    </>
  );
}

const classes = {
  header: cntl`
    relative
    bg-how-it-works-mobile xl:bg-how-it-works-tablet
    w-full h-how-it-works-mobile lg:h-full lg:h-how-it-works
    bg-no-repeat bg-cover
    p-4 lg:px-24 
    grid lg:flex lg:items-center lg:justify-between
  `,
  text: cntl`
    text-white
    self-end md:self-center
    how-it-works__text
  `,

  divMobile: cntl`
    grid grid-cols-2 gap-x-2 items-end lg:block
  `,
  button: (isInquiry: boolean) => cntl`
    px-4 py-1 h-12
    text-lg font-medium text-white
    rounded-lg fs-home-search-button border-0 focus:outline-none
    flex justify-center items-center
    
    ${isInquiry ? cntl`bg-primary-dark` : cntl`bg-primary-orange`}
  `,
  modal: ({ visible }: { visible: boolean }) => cntl`
    duration-300
    text-center top-20
    top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 fixed transform transition-transform z-photos-modal
    duration-300
    ${visible ? cntl`scale-100` : cntl`scale-0`}
  `,
  close: cntl`absolute -top-12 transform left-1/2 -translate-x-1/2`,
  iconClose: cntl`
  cursor-pointer fill-current h-10 hover:text-primary-black text-6xl text-white w-10
  `,
  overlayContainer: ({ hidden }: { hidden: boolean }) => cntl`
    fixed left-0 bottom-0 right-0
    p-4
    text-center bg-white
    border-t
    border-primary-lightgray
    ${hidden ? cntl`hidden` : cntl`block`}
  `,
  overlayButton: cntl`
    py-2 px-4
    text-sm font-medium font-semibold text-white uppercase tracking-widest
    bg-primary-red rounded-md
    focus:outline-none focus:border-transparent
  `
};

export default HowItWorksHeader;
