import OurPartners from '@components/common/our-partners';
import { Page } from '@constants/enum';

function HowItWorksOurPartners() {
  return <OurPartners page={Page.HowItWorks} />;
}

export default HowItWorksOurPartners;
