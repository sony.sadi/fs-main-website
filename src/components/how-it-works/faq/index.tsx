import cntl from 'cntl';
import { useTranslation } from 'next-i18next';
import { useSelector } from 'react-redux';

import faqs from '@assets/data/how-it-works-tenant-FAQ.json';
import HowItWorksContact from '@components/how-it-works/contact';
import Svg from '@components/core/svg';
import { selectIsMobile } from '@stores/slices/common';
import { LocaleNamespace, Page } from '@constants/enum';
import ContactForm from '@forms/contact';
import FAQItem from '@components/common/faq-item';

function FAQ() {
  const { t } = useTranslation(LocaleNamespace.HowItWorks);

  const isMobile = useSelector(selectIsMobile);
  return (
    <section className={classes.faq}>
      <div className="lg:pr-10 w-full">
        <h3 className={classes.title}>{t('howItWorksFAQTitle')}</h3>
        {faqs.map((faq, indexFAQ) => {
          const question = t(faq.question);
          const answers = faq.answers.map((answer) => t(answer));
          return <FAQItem key={indexFAQ} index={indexFAQ} question={question} answers={answers} />;
        })}
        <input type="checkbox" id="slide-toggle-property-contact" className={classes.slideToggleCheckbox} />
        <label htmlFor="slide-toggle-property-contact" className={classes.slideToggleLabel}>
          <h3 className="text-lg font-bold">{t('howItWorksContactQuestion')}</h3>
          <Svg className={classes.iconArrowBottom} name="top-arrow" />
        </label>
        <div className={classes.slideToggleFAQ}>
          <p className={classes.note}>{t('howItWorksContactNote')}</p>
          <HowItWorksContact />
        </div>
      </div>
      {!isMobile && <ContactForm page={Page.HowItWorks} />}
    </section>
  );
}

const classes = {
  iconArrowBottom: cntl`slide-toggle__icon ml-3 h-2 w-4 self-center`,
  slideToggleCheckbox: cntl`slide-toggle__checkbox`,
  slideToggleLabel: cntl`slide-toggle__label flex pb-2.5 lg:pb-0 cursor-pointer mb-5 content`,
  slideToggleFAQ: cntl`slide-toggle__faq content`,

  faq: cntl`
    px-4 sm:px-8 xl:px-24 pt-14
    flex
  `,
  title: `text-4xl font-bold mb-8`,
  note: cntl`font-bold text-primary-orange mb-5`
};

export default FAQ;
