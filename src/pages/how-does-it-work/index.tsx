import { useCallback, useEffect, useMemo } from 'react';
import Head from 'next/head';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { TFunction, useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';

import Layout from '@components/layout';
import { GetStaticProps } from 'next';
import Header from '@components/how-it-works/header';
import PropertyScout from '@components/how-it-works/property-scout';
import HowItWorks from '@components/how-it-works/how';
import OurPartners from '@components/how-it-works/our-partners';
import FAQ from '@components/how-it-works/faq';
import { Language, LocaleNamespace } from '@constants/enum';
import Breadcrumb from '@components/breadcrumb';
import nextI18NextConfig from '@nextI18NextConfig';
import BreadcrumbSchema from '@components/breadcrumb/schema';
import { generateHrefLang, getAlternateLanguage } from '@utils/filter-language';
import { DataLayerObject, updateGTMDataLayer } from '@utils/gtm';
import { pageType } from '@constants';
import { BuildingObject, getBuildingDatas, getLayoutData } from '@utils/buildings';

interface Props {
  buildingData: BuildingObject;
}

function HowDoesItWork({ buildingData }: Props) {
  const { t, i18n } = useTranslation(LocaleNamespace.HowItWorks);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);
  const router = useRouter();

  const breadcrumbs = useMemo(() => {
    return [
      { label: tCommon('home'), path: '/' },
      { label: tCommon('howDoesItWork'), path: tCommon('howDoesItWorkPath') }
    ];
  }, [tCommon]);

  const handleLocaleChanged = useCallback((e: CustomEventInit) => {
    const { locale, tLocale }: { locale: Language; tLocale: TFunction } = e.detail;
    router.replace(tLocale('howDoesItWorkPath'), undefined, {
      locale
    });
  }, []);

  /**
   * Render hreflang for alternate language
   */
  const renderHrefLang = useMemo(() => {
    const alternateLanguage = getAlternateLanguage(i18n.language);
    if (alternateLanguage.length === 0) return [];

    const alternateLinks = alternateLanguage.map((value: string, index: number) => {
      const ti18n = i18n.getFixedT(value);
      return (
        <link
          key={`index_${index}`}
          rel="alternate"
          href={`${generateHrefLang(ti18n('howDoesItWorkPath'), value)}`}
          hrefLang={`${value.toUpperCase()}`}
        />
      );
    });
    return alternateLinks;
  }, [i18n.language]);

  /**
   * Render canonical href
   */
  const renderCanonicalHref = useMemo(() => {
    const ti18n = i18n.getFixedT(i18n.language);

    return `${generateHrefLang(ti18n('howDoesItWorkPath'), i18n.language)}`;
  }, [i18n.language]);

  useEffect(() => {
    document.addEventListener('locale_changed', handleLocaleChanged);
    return () => {
      document.removeEventListener('locale_changed', handleLocaleChanged);
    };
  }, []);

  /**
   * Send data layer
   */
  useEffect(() => {
    const object: DataLayerObject = {
      type: pageType.HOW_DOES_IT_WORK,
      locale: i18n.language
    };
    updateGTMDataLayer(object);
  }, []);

  return (
    <>
      <Head>
        <meta name="description" content={t('pageDescription')} />
        <title>{t('pageTitle')}</title>
        <link rel="canonical" href={renderCanonicalHref} />
        {renderHrefLang.length > 0 && renderHrefLang}
      </Head>
      <BreadcrumbSchema breadcrumbs={breadcrumbs} />
      <Layout layoutData={getLayoutData({ buildingData })}>
        <Header />
        <PropertyScout />
        <HowItWorks />
        <OurPartners />
        <FAQ />
        <Breadcrumb list={breadcrumbs} />
      </Layout>
    </>
  );
}

export const getStaticProps: GetStaticProps = async (context) => {
  console.info(`src/pages/how-does-it-work/index.tsx@getStaticProps invoked`);
  const { locale = 'th' } = context;

  const buildingData = await getBuildingDatas(locale as Language);

  return {
    props: {
      buildingData,
      ...(await serverSideTranslations(
        locale,
        ['common', 'footer', 'how-it-works', 'home', 'listing', 'contact-form'],
        nextI18NextConfig
      ))
    }
  };
};

export default HowDoesItWork;
