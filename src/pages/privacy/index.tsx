import { useCallback, useEffect, useMemo } from 'react';
import { GetStaticProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { TFunction, useTranslation } from 'next-i18next';
import Head from 'next/head';
import { useRouter } from 'next/router';

import Layout from '@components/layout';
import cntl from 'cntl';
import Breadcrumb from '@components/breadcrumb';
import { Language, LocaleNamespace } from '@constants/enum';
import nextI18NextConfig from '@nextI18NextConfig';
import BreadcrumbSchema from '@components/breadcrumb/schema';
import { generateHrefLang, getAlternateLanguage } from '@utils/filter-language';
import { DataLayerObject, updateGTMDataLayer } from '@utils/gtm';
import { pageType } from '@constants';
import { BuildingObject, getBuildingDatas, getLayoutData } from '@utils/buildings';

interface PrivacyProps {
  buildingData: BuildingObject;
}

const Privacy = ({ buildingData }: PrivacyProps) => {
  const { t, i18n } = useTranslation(LocaleNamespace.Privacy);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);
  const router = useRouter();

  const breadcrumbs = useMemo(() => {
    return [
      { label: tCommon('home'), path: '/' },
      { label: tCommon('privacyPolicy'), path: tCommon('privacyPath') }
    ];
  }, [tCommon]);

  const handleLocaleChanged = useCallback((e: CustomEventInit) => {
    const { locale, tLocale }: { locale: Language; tLocale: TFunction } = e.detail;
    router.replace(tLocale('privacyPath'), undefined, {
      locale
    });
  }, []);

  /**
   * Render hreflang for alternate language
   */
  const renderHrefLang = useMemo(() => {
    const alternateLanguage = getAlternateLanguage(i18n.language);
    if (alternateLanguage.length === 0) return [];

    const alternateLinks = alternateLanguage.map((value: string, index: number) => {
      const ti18n = i18n.getFixedT(value);
      return (
        <link
          key={`index_${index}`}
          rel="alternate"
          href={`${generateHrefLang(ti18n('privacyPath'), value)}`}
          hrefLang={`${value.toUpperCase()}`}
        />
      );
    });
    return alternateLinks;
  }, [i18n.language]);

  /**
   * Render canonical href
   */
  const renderCanonicalHref = useMemo(() => {
    const ti18n = i18n.getFixedT(i18n.language);

    return `${generateHrefLang(ti18n('privacyPath'), i18n.language)}`;
  }, [i18n.language]);

  useEffect(() => {
    document.addEventListener('locale_changed', handleLocaleChanged);
    return () => {
      document.removeEventListener('locale_changed', handleLocaleChanged);
    };
  }, []);

  /**
   * Send data layer
   */
  useEffect(() => {
    const object: DataLayerObject = {
      type: pageType.PRIVACY,
      locale: i18n.language
    };
    updateGTMDataLayer(object);
  }, []);

  return (
    <>
      <Head>
        <meta name="description" content={t('pageDescription')} />
        <title>{t('pageTitle')}</title>
        <link rel="canonical" href={renderCanonicalHref} />
        {renderHrefLang.length > 0 && renderHrefLang}
      </Head>
      <BreadcrumbSchema breadcrumbs={breadcrumbs} />
      <Layout layoutData={getLayoutData({ buildingData })}>
        <div className={classes.contentContainer}>
          <h1 className="text-4xl font-bold">{t('privacyHead')}</h1>
          <p className="text-lg">{t('privacyTime')}</p>
          <p className="mt-4 text-base">{t('privacyDesc1')}</p>

          <p className="mt-4 text-base">{t('privacyDesc2')}</p>

          <p className="mt-4 text-base">{t('privacyDesc3')}</p>

          <section className="mt-4">
            <h2 className={classes.title}>{t('privacyInfoHead')}</h2>
            <p>{t('privacyInfoDesc')}</p>
            <h3 className={classes.title}>{t('privacyAccHead')}</h3>
            {t('privacyAccDesc')}
            <h3 className={classes.title}>{t('privacyContentHead')}</h3>
            <p>{t('privacyContentDesc')}</p>
            <h3 className={classes.title}>{t('privacyOtherHead')}</h3>
            <p>{t('privacyOtherDesc1')}</p>
            <p>{t('privacyOtherDesc2')}</p>
            <p>{t('privacyOtherDesc3')}</p>
            <h3 className={classes.title}>{t('privacyNotHead')}</h3>
            <p>{t('privacyNotDesc')}</p>
          </section>

          <section className="mt-4">
            <h2 className={classes.title}>{t('privacyHowHead')}</h2>
            <p>{t('privacyHowDesc')}</p>
            <p className="mt-4">{t('privacyHowDesc1')}</p>
            <p>{t('privacyHowDesc2')}</p>
            <p>{t('privacyHowDesc3')}</p>
            <p>{t('privacyHowDesc4')}</p>
            <p>{t('privacyHowDesc5')}</p>
            <p>{t('privacyHowDesc6')}</p>
            <p>{t('privacyHowDesc7')}</p>
          </section>

          <section className="mt-4">
            <h2 className={classes.title}>{t('privacyWhoHead')}</h2>
            <p>{t('privacyWhoDesc')}</p>
            <h3 className={classes.title}>{t('privacyConsentHead')}</h3>
            <p>{t('privacyConsentDesc')}</p>
            <h3 className={classes.title}>{t('privacyAgentHead')}</h3>
            <p>{t('privacyAgentDesc')}</p>
            <h3 className={classes.title}>{t('privacyBusinessHead')}</h3>
            <p>{t('privacyBusinessDesc')}</p>
            <h3 className={classes.title}>{t('privacyProtectHead')}</h3>
            <p>{t('privacyProtectDesc')}</p>
          </section>

          <section className="mt-4">
            <h2 className={classes.title}>{t('privacyWhatHead')}</h2>
            <h3 className={classes.title}>{t('privacyDeleteHead')}</h3>
            <p>{t('privacyDeleteDesc')}</p>
            <h3 className={classes.title}>{t('privacyLimitHead')}</h3>
            <p>{t('privacyLimitDesc')}</p>
            <h3 className={classes.title}>{t('privacyLawHead')}</h3>
            <p>{t('privacyLawDesc')}</p>
          </section>

          <section className="mt-4">
            <h2 className={classes.title}>{t('privacyOptHead')}</h2>
            <p>{t('privacyOptDesc')}</p>
          </section>

          <section className="mt-4">
            <h2 className={classes.title}>{t('privacyLocationHead')}</h2>
            <p>{t('privacyLocationDesc')}</p>
          </section>

          <section className="mt-4">
            <h2 className={classes.title}>{t('privacySecuHead')}</h2>
            <p>{t('privacySecuDesc')}</p>

            <p className="mt-4">{t('privacySecuDesc1')}</p>

            <p className="mt-4">{t('privacySecuDesc2')}</p>
          </section>

          <section className="mt-4">
            <h2 className={classes.title}>{t('privacyChildHead')}</h2>
            <p>{t('privacyChildDesc')}</p>
          </section>

          <section className="mt-4">
            <h2 className={classes.title}>{t('privacyChangeHead')}</h2>
            <p>{t('privacyChangeDesc')}</p>
          </section>

          <section className="mt-4">
            <h2 className={classes.title}>{t('privacyContactHead')}</h2>
            <p>{t('privacyContactDesc')}</p>
          </section>
        </div>
        <Breadcrumb list={breadcrumbs} />
      </Layout>
    </>
  );
};

const classes = {
  contentContainer: cntl`px-4 sm:px-8 xl:px-24 py-12 text-base`,
  title: cntl`mt-4 text-base font-bold`
};

export const getStaticProps: GetStaticProps = async (context) => {
  console.info(`src/pages/privacy/index.tsx@getStaticProps invoked`);
  const { locale = 'th' } = context;
  const buildingData = await getBuildingDatas(locale as Language);
  return {
    props: {
      buildingData,
      ...(await serverSideTranslations(locale, ['common', 'privacy', 'footer'], nextI18NextConfig))
    }
  };
};

export default Privacy;
