import React, { useEffect } from 'react';
import { Provider, useDispatch } from 'react-redux';
import { createWrapper } from 'next-redux-wrapper';
import App, { AppContext, AppProps, NextWebVitalsMetric } from 'next/app';
import { appWithTranslation } from 'next-i18next';
import Head from 'next/head';
import TagManager from 'react-gtm-module';
import { PersistGate } from 'redux-persist/integration/react';

import { persistor, store } from '@stores';
import { commonActions } from '@stores/slices/common';
import nextI18NextConfig from '@nextI18NextConfig';
import { GOOGLE_TAG_MANAGER_RENDER_INIT_IN_MS, metricKeys } from '@constants';

import '@styles/scss/globals.scss';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import '@styles/scss/index.scss';

const makeStore = () => store;

/**
 * Function send datalayer to GTM
 * @param metric
 */
const handleDataLayerGtm = (metric: NextWebVitalsMetric) => {
  const { id, name, label, value } = metric;
  TagManager.dataLayer({
    dataLayer: {
      event: 'web_vitals',
      webVitalsMetric: {
        id,
        name,
        category: label === 'web-vital' ? 'core' : 'nextjs',
        value: Math.round(name === 'CLS' ? value * 1000 : value)
      }
    }
  });
};

const MyApp = ({ Component, pageProps }: AppProps) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(commonActions.setViewPort(window.innerWidth));

    TagManager.initialize({ gtmId: process.env.NEXT_PUBLIC_GTM_ID || '' });
    // setTimeout(() => {
    //   import('react-gtm-module').then((module) => {
    //     module.default.initialize({ gtmId: process.env.NEXT_PUBLIC_GTM_ID || '' });
    //   });
    // }, GOOGLE_TAG_MANAGER_RENDER_INIT_IN_MS);
  }, []);

  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1" />
      </Head>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          {() => <Component {...pageProps} />}
        </PersistGate>
      </Provider>
    </>
  );
};

export function reportWebVitals(metric: NextWebVitalsMetric) {
  if (metric.label !== 'web-vital') return;
  switch (metric.name) {
    case metricKeys.FCP:
      handleDataLayerGtm(metric);

      break;
    case metricKeys.LCP:
      handleDataLayerGtm(metric);

      break;
    case metricKeys.CLS:
      handleDataLayerGtm(metric);

      break;
    case metricKeys.FID:
      handleDataLayerGtm(metric);

      break;
    case metricKeys.TTFB:
      handleDataLayerGtm(metric);

      break;
    default:
      break;
  }
}

MyApp.getInitialProps = async (appContext: AppContext) => {
  const { router, ctx } = appContext;
  if (ctx.req) {
    const { __NEXT_INIT_QUERY, cookies, url } = ctx.req as any;
    const { lng } = __NEXT_INIT_QUERY || {};
    const { NEXT_LOCALE } = cookies || {};
    const { locale: reqLocale, defaultLocale } = router;

    const locales = router.locales || [];

    const locale = lng || NEXT_LOCALE || reqLocale;

    const desiredLocale = locales.includes(locale) ? locale : defaultLocale;

    if (desiredLocale !== reqLocale) {
      const Location = `${defaultLocale === reqLocale ? '' : `/${reqLocale}`}${url}`;
      if (ctx.res) {
        ctx.res.setHeader('Location', Location);
        ctx.res.setHeader('Set-Cookie', `NEXT_LOCALE=${reqLocale}; Max-Age=31556952; Path=/`);
        ctx.res.statusCode = 302;
      }
    }
  }

  const appProps = await App.getInitialProps(appContext);

  return { ...appProps };
};

export default createWrapper(makeStore).withRedux(appWithTranslation(MyApp, nextI18NextConfig));
