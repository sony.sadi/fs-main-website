import { useCallback, useEffect, useMemo, useState } from 'react';
import { GetStaticPathsResult, GetStaticPropsContext } from 'next';
import Head from 'next/head';
import dynamic from 'next/dynamic';
import { getPropertyById } from '@services/http-client/map';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import clsx from 'clsx';
import cntl from 'cntl';
import trimEnd from 'lodash/trimEnd';
import trimStart from 'lodash/trimStart';

import BasicInformation from '@components/listing/basic-information';
import Inquiry from '@components/listing/inquiry';
import Svg from '@components/core/svg';
import Layout from '@components/layout';
import nextI18NextConfig from '@nextI18NextConfig';

// @todo fix typos in these two component names
import ListingInfomationMoreRecommedned from '@components/listing/basic-information/more-recommended';
import ListingInfomationSimilarListingNearby from '@components/listing/basic-information/similar-nearby';

import { getListingDetailLink } from '@utils/listing';
import { Language, LocaleNamespace } from '@constants/enum';
import { Listing } from '@interfaces/listing';
import { selectIsMobile } from '@stores/slices/common';

import { BreadcrumbItem } from '@interfaces/breadcrumb';
import Breadcrumb from '@components/breadcrumb';
import { generateUrlFromBreadcrumb } from '@utils/breadcrumb';
import BreadcrumbSchema from '@components/breadcrumb/schema';
import { selectFavoriteRentalsIds, storePadRental } from '@stores/slices/search';
import { FavouriteRental } from '@interfaces/search';
import { getListingDetailBreadcrumbs } from '@services/http-client/breadcrumb';
import { generateHrefLang, getAlternateLanguage } from '@utils/filter-language';
import { DataLayerObject, updateGTMDataLayer } from '@utils/gtm';
import { pageType } from '@constants';
import { getRecommendedUnits, getSimilarListingsNearby } from '@services/http-client/search';
import { BuildingObject, getBuildingDatas, getLayoutData } from '@utils/buildings';
import { SerpType } from '@flexstay.rentals/fs-url-stringify';

const PhotoSlider = dynamic(() => import('@components/listing/photos-slider'));
const Photos = dynamic(() => import('@components/listing/photos'));
const PhotosModal = dynamic(() => import('@components/listing/photos-modal'));

const INACTIVE_SLIDE = -1;

const { NEXT_PUBLIC_HOME_URL } = process.env;

const HOME_URL = trimEnd(NEXT_PUBLIC_HOME_URL, '/');

// @todo create a type/interface to represent a property
interface ListingTitleIdProps {
  property: Listing;
  breadcrumbs: BreadcrumbItem[];
  rentsNearby: Listing[];
  rentsRecommended: Listing[];
  saleNearby: Listing[];
  saleRecommended: Listing[];
  listingPath: string;
  buildingData: BuildingObject;
  serpType: SerpType;
}

function ListingTitleId({
  property,
  breadcrumbs,
  rentsNearby,
  rentsRecommended,
  listingPath,
  buildingData,
  saleNearby,
  saleRecommended,
  serpType
}: ListingTitleIdProps) {
  const router = useRouter();

  if (router.isFallback) {
    return null;
  }

  const { i18n, t } = useTranslation();
  const { t: tCommon } = useTranslation(LocaleNamespace.Listing);
  const language = i18n.language as Language;
  const isMobile = useSelector(selectIsMobile);
  const favoriteRentalsIds = useSelector(selectFavoriteRentalsIds);
  const spacingFromTop = 300;
  const isLiked = useMemo(() => favoriteRentalsIds.includes(property.id), [property.id, favoriteRentalsIds]);
  const dispatch = useDispatch();

  const [titleLanguage, setTitleLanguage] = useState(language);
  const [activeSlide, setActiveSlide] = useState(INACTIVE_SLIDE);
  const [shouldShowInquiry, setShouldShowInquiry] = useState(false);
  const [showLikeText, setShowLikeText] = useState(false);

  const handlePhotosModalClose = useCallback(() => setActiveSlide(INACTIVE_SLIDE), []);

  const generatedBreadcrumbs = useMemo(() => {
    return generateUrlFromBreadcrumb({
      breadcrumbs,
      t,
      currentPath: {
        href: listingPath,
        /* FS-1463: noindex all listings link with tenure = sell or rentsell */
        rel: property.tenure && ['sell', 'rentsell'].includes(property.tenure) ? 'nofollow' : undefined
      },
      serpType
    });
  }, [t, breadcrumbs, listingPath, property]);

  const handleClickHeartIcon = (e: React.MouseEvent<SVGSVGElement, MouseEvent>) => {
    e.preventDefault();
    const favouriteTemplate: FavouriteRental = {
      user_id: 'anonymous',
      added_at: new Date().toISOString(),
      rental_id: property.id,
      source: 'WEBSITE'
    };
    dispatch(storePadRental(favouriteTemplate));
    setShowLikeText(false);
  };
  const handleHoverHeartIcon = () => {
    !isLiked && setShowLikeText(true);
  };

  const handleBlurHeartIcon = () => {
    setShowLikeText(false);
  };

  /**
   * Changes the listing's title as the selected language
   */
  useEffect(() => {
    if (titleLanguage !== language) {
      setTitleLanguage(language);

      const { href, as } = getListingDetailLink(property, language);
      router.replace(href, as);
    }
  }, [language, titleLanguage, property]);

  useEffect(() => {
    if (isMobile) {
      const onWindowScroll = () => {
        setShouldShowInquiry(window.scrollY > spacingFromTop);
      };
      window.addEventListener('scroll', onWindowScroll);
      return () => {
        window.removeEventListener('scroll', onWindowScroll);
      };
    }
  }, []);

  const handleLocaleChanged = useCallback(
    (e: CustomEventInit) => {
      const { locale }: { locale: Language } = e.detail;
      const { href, as } = getListingDetailLink(property, locale);
      router.replace(href, as, { locale });
    },
    [property]
  );

  /**
   * Render hreflang for alternate language
   */
  const renderHrefLang = useMemo(() => {
    const alternateLanguage = getAlternateLanguage(i18n.language);
    if (alternateLanguage.length === 0) return [];

    const alternateLinks = alternateLanguage.map((value: string, index: number) => {
      const { href } = getListingDetailLink(property, value as Language);
      return (
        <link
          key={index}
          rel="alternate"
          href={`${generateHrefLang(`/${href.query['listing-title-id']}`, value)}`}
          hrefLang={`${value.toUpperCase()}`}
        />
      );
    });
    return alternateLinks;
  }, [i18n.language]);

  /**
   * Render canonical href
   */
  const renderCanonicalHref = () => {
    const { href } = getListingDetailLink(property, i18n.language as Language);

    return `${generateHrefLang(`/${href.query['listing-title-id']}`, i18n.language)}`;
  };

  useEffect(() => {
    document.addEventListener('locale_changed', handleLocaleChanged);
    return () => {
      document.removeEventListener('locale_changed', handleLocaleChanged);
    };
  }, [property]);

  /**
   * Send data layer
   */
  useEffect(() => {
    const object: DataLayerObject = {
      type: pageType.LISTING,
      locale: i18n.language
    };
    updateGTMDataLayer(object);
  }, []);

  return (
    <>
      <Head>
        {language === Language.US && (
          <>
            <meta name="description" content={property.metaDescription} />
            <title>{property.browserTitle}</title>
          </>
        )}
        <link rel="canonical" href={renderCanonicalHref()} />
        {renderHrefLang.length > 0 && renderHrefLang}
        {/* FS-1463: noindex all listings detail pages with tenure = sell or rentsell */}
        {property.tenure && ['sell', 'rentsell'].includes(property.tenure) && (
          <meta name="robots" content="noindex, nofollow" />
        )}
      </Head>
      {property.province_ps_en === 'Bangkok' && <BreadcrumbSchema breadcrumbs={generatedBreadcrumbs} />}
      <Layout layoutData={getLayoutData({ buildingData })}>
        <div className={classes.wrapper}>
          {isMobile ? (
            <div className="relative">
              <div className="absolute top-0 right-0 mt-4 mr-3  z-10">
                <Svg
                  onClick={handleClickHeartIcon}
                  className={classes.heartIcon(isMobile)}
                  name={isLiked ? 'liked-heart' : 'outline-heart'}
                />
              </div>
              <PhotoSlider images={property.cdnImages} />
            </div>
          ) : (
            <>
              <div className={classes.breadcrumb}>
                <div className="flex flex-col items-center select-none">
                  <Svg
                    onMouseOver={handleHoverHeartIcon}
                    onMouseLeave={handleBlurHeartIcon}
                    onClick={handleClickHeartIcon}
                    className={classes.heartIcon(isMobile)}
                    name={isLiked ? 'liked-heart ' : 'outline-heart'}
                  />
                  <span className={classes.likeText(showLikeText)}>like</span>
                </div>
              </div>
              <Photos images={property.cdnImages} onPhotoClick={setActiveSlide} />
              <PhotosModal
                images={property.cdnImages}
                activeSlide={activeSlide}
                onClose={handlePhotosModalClose}
                onChange={setActiveSlide}
              />
            </>
          )}
          <p />
          <div className={classes.buttonContainer}>
            {/* <button className={clsx('outline-button', classes.outlineButton)} onClick={() => setActiveSlide(0)}>
              <Svg name="detail-image-1" className={classes.outlineButtonIcon} />
              {t('images')}
            </button> */}
            <button
              className={clsx('outline-button', classes.outlineButton)}
              onClick={() => {
                window.location.href = '#map-pin';
              }}>
              <Svg name="detail-map" className={classes.outlineButtonIcon} />
              {tCommon('map')}
            </button>
          </div>
          <div className={classes.container}>
            <div className={classes.infoContainer}>
              <BasicInformation property={property} />
            </div>
            <div className={classes.inquiryContainer}>
              <div className="sticky top-24 flex justify-end">{!isMobile && <Inquiry property={property} />}</div>
            </div>
          </div>
          <>
            <ListingInfomationMoreRecommedned
              title="recommededSaleUnit"
              hyperlinkText="allPropertySale"
              rentals={saleRecommended}
              property={property}
              serpType="sales"
            />

            <ListingInfomationMoreRecommedned
              title="recommededRentUnit"
              hyperlinkText="allPropertyRent"
              rentals={rentsRecommended}
              property={property}
              serpType="rents"
            />

            <ListingInfomationSimilarListingNearby
              title="similarListingsBuyNearby"
              hyperLinkText="allSimilarListingsBuyNearby"
              rentals={saleNearby}
              property={property}
              serpType="sales"
            />

            <ListingInfomationSimilarListingNearby
              title="similarListingsRentNearby"
              hyperLinkText="allSimilarListingsRentNearby"
              rentals={rentsNearby}
              property={property}
              serpType="rents"
            />
          </>
          {isMobile && shouldShowInquiry && <Inquiry property={property} phoneNumber="+66 92 264 3444" />}
        </div>
        {property.province_ps_en === 'Bangkok' ? <Breadcrumb list={generatedBreadcrumbs} /> : <></>}
      </Layout>
    </>
  );
}

const classes = {
  wrapper: cntl`
    pt-0 md:pt-12 px-4 xl:px-24 2xl:px-40
  `,
  container: cntl`
    grid grid-cols-12 gap-4 relative 3xl:mt-8 3xl:pt-10 sm:mt-4
  `,
  buttonContainer: cntl`
    grid grid-cols-3 xl:mt-5 xl:mb-12 my-2 max-w-md
  `,
  infoContainer: cntl`
    col-span-12 3xl:col-span-9 xl:col-span-7 md:col-span-12 sm:col-span-12
  `,
  inquiryContainer: cntl`
    col-span-0 3xl:col-span-3 xl:col-span-5 pb-4 md:col-span-0 sm:col-span-0
  `,
  outlineButton: cntl`
    border border-home-silver hover:border-transparent
    hover:bg-primary-orange
    text-home-silver hover:text-white
    rounded-xl duration-300
    h-10 px-3 mr-3 flex items-center justify-center
    width
  `,
  outlineButtonIcon: cntl`
    h-5 w-5 mr-2 outline-button-icon
  `,
  breadcrumb: cntl`
    flex justify-end mb-5`,
  heartIcon: (isWhite: boolean) => cntl`
  w-8 h-6 cursor-pointer
  fill-current ${isWhite ? 'text-white' : 'text-black'} hover:text-primary-orange z-10
  `,
  likeText: (show: boolean) => cntl`
    ${show ? 'visible' : 'invisible'}
  `
};

export async function getStaticProps({ params, locale }: GetStaticPropsContext) {
  const [listingId] = params && params.slugs ? (params.slugs as Array<string>) : [''];
  const language = locale as Language;

  try {
    const id = Number(listingId.replace(/.+-(\d{1,})$/, '$1'));

    const [propertyResponse, breadcrumbResponse] = await Promise.all([
      getPropertyById(id),
      getListingDetailBreadcrumbs(id, language)
    ]);

    const correctlyUrl = trimStart(getListingDetailLink(propertyResponse.data, language).as, '/');

    if (encodeURIComponent(correctlyUrl) !== encodeURIComponent(listingId)) {
      return {
        notFound: true
      };
    }

    const property = propertyResponse.data;

    const promisesRent =
      property.tenure === 'rent' || property.tenure === 'rentsell'
        ? [
            getSimilarListingsNearby({
              price: property.lowestPrice,
              lat: property.gpsLat,
              long: property.gpsLong,
              serp_type: 'rents'
            })
          ]
        : [];
    if (property.buildingId?.building_id && (property.tenure === 'rent' || property.tenure === 'rentsell')) {
      promisesRent.push(getRecommendedUnits(property.buildingId.building_id, 'rents'));
    }

    const [rentsNearbyResponse, rentsRecommendedResponse] = promisesRent.length
      ? await Promise.all(promisesRent)
      : [undefined, undefined];

    const promisesSale =
      property.tenure === 'sell' || property.tenure === 'rentsell'
        ? [
            getSimilarListingsNearby({
              price: property.salePrice || 0,
              lat: property.gpsLat,
              long: property.gpsLong,
              serp_type: 'sales'
            })
          ]
        : [];
    if (property.buildingId?.building_id && (property.tenure === 'sell' || property.tenure === 'rentsell')) {
      promisesSale.push(getRecommendedUnits(property.buildingId.building_id, 'sales'));
    }

    const [saleNearbyResponse, saleRecommendedResponse] = promisesSale.length
      ? await Promise.all(promisesSale)
      : [undefined, undefined];

    const buildingData = await getBuildingDatas(language);

    const rentsNearby = rentsNearbyResponse ? rentsNearbyResponse.data.data : [];

    const rentsRecommended = rentsRecommendedResponse ? rentsRecommendedResponse.data.data : [];

    const saleNearby = saleNearbyResponse ? saleNearbyResponse.data.data : [];

    const saleRecommended = saleRecommendedResponse ? saleRecommendedResponse.data.data : [];

    const serpType = property.tenure === 'sell' || property.tenure === 'rentsell' ? 'sales' : 'rents';

    return {
      props: {
        property,
        breadcrumbs: breadcrumbResponse.data,
        rentsNearby,
        rentsRecommended,
        saleNearby,
        saleRecommended,
        listingPath: listingId,
        buildingData,
        serpType,
        ...(await serverSideTranslations(
          language,
          ['home', 'common', 'contact-form', 'listing', 'footer'],
          nextI18NextConfig
        ))
      },
      revalidate: parseInt(process.env.NEXT_CACHE_TIME || '300', 10)
    };
  } catch (error) {
    console.info('src/pages/listing/[...slugs].tsx@getStaticProps', error);

    return {
      notFound: true
    };
  }
}

export async function getStaticPaths(): Promise<GetStaticPathsResult> {
  return {
    paths: [],
    fallback: 'blocking'
  };
}

export default ListingTitleId;
