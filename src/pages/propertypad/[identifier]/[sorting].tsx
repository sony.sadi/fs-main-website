import { GetServerSideProps } from 'next';
import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { TFunction, useTranslation } from 'next-i18next';
import Head from 'next/head';

import { SearchRequest } from '@interfaces/search';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import ListingsSearch from '@components/listings/search';
import {
  fetchPropertyPad,
  fetchRentals,
  selectFavoriteRentals,
  selectPropertyPad,
  selectRentals,
  resetPropertyPad,
  searchActions
} from '@stores/slices/search';
import { getSearchRequest, parseSortingSlug } from '@utils/search';
import { Language, LocaleNamespace, Page } from '@constants/enum';
import { useSSRTranslations } from '@utils';
import { DataLayerObject, updateGTMDataLayer } from '@utils/gtm';
import { pageType } from '@constants';
import { getPropertyPad, getPropertyPadRentals } from '@services/http-client/search';
import { mapActions } from '@stores/slices/map';
import { toSortingSlug, SortType, getDefaultParams } from '@flexstay.rentals/fs-url-stringify';
import { BuildingObject, getBuildingDatas } from '@utils/buildings';

interface PropertyPadProps {
  identifier: string;
  rentalsParams: SearchRequest;
  buildingData: BuildingObject;
}

function PropertyPadIdentifierSorting({ identifier, rentalsParams, buildingData }: PropertyPadProps) {
  const favoriteRentals = useSelector(selectFavoriteRentals);
  const [page, setPage] = useState(1);
  const rentals = useSelector(selectRentals);
  const propertyPad = useSelector(selectPropertyPad);
  const dispatch = useDispatch();
  const router = useRouter();
  const { i18n, t } = useTranslation(LocaleNamespace.Common);

  useEffect(() => {
    dispatch(searchActions.setRentalsParams(rentalsParams));
    return () => {
      dispatch(searchActions.resetRentalsParams());
    };
  }, [rentalsParams]);

  useEffect(() => {
    if (identifier) {
      dispatch(fetchPropertyPad(identifier));
    }
  }, [identifier]);

  useEffect(() => {
    if (favoriteRentals.length) {
      const totalAddress = favoriteRentals.length;
      dispatch(fetchRentals({ rentals: favoriteRentals, page, totalAddress, sort: rentalsParams.sortBy }));
    } else {
      dispatch(resetPropertyPad());
    }
  }, [page, favoriteRentals.length, rentalsParams.sortBy]);

  const handlePageChange = useCallback((value: number) => {
    setPage(value);
  }, []);

  const handleSortChange = useCallback((value: SortType) => {
    const sortingSlug = toSortingSlug(value, t);
    router.replace(`/propertypad/${identifier}/${sortingSlug}`);
  }, []);

  const handleLocaleChanged = useCallback(async (e: CustomEventInit) => {
    const { locale, tLocale }: { locale: Language; tLocale: TFunction } = e.detail;
    const sortingSlug = toSortingSlug(rentalsParams.sortBy || SortType.Recommended, tLocale);
    router.replace(`/propertypad/${identifier}/${sortingSlug}`, undefined, {
      locale
    });
  }, []);

  useEffect(() => {
    document.addEventListener('locale_changed', handleLocaleChanged);
    return () => {
      document.removeEventListener('locale_changed', handleLocaleChanged);
    };
  }, []);

  /**
   * Send data layer
   */
  useEffect(() => {
    const object: DataLayerObject = {
      type: pageType.PROPERTYPAD,
      locale: i18n.language
    };
    updateGTMDataLayer(object);
  }, []);

  return (
    <>
      <Head>
        <meta name="robots" content="noindex, nofollow" />
      </Head>
      <ListingsSearch
        handlePageChange={handlePageChange}
        handleSortChange={handleSortChange}
        rentals={rentals}
        rentalsParams={rentalsParams}
        breadcrumbs={[]}
        isSearchingMap={false}
        address={propertyPad?.name}
        defaultSortLabel="Recommended"
        defaultPagingText="availablePropertyPad"
        page={Page.Propertypad}
        buildingData={buildingData}
      />
    </>
  );
}

/**
 * Use getServerSideProps for this page to pre-render for each request
 * to match the property pad data as possible
 * @param context
 * @returns
 */
export const getServerSideProps: GetServerSideProps = async (context) => {
  console.info(`src/pages/propertypad/[identifier]/[sorting].tsx@getStaticProps invoked`);
  const { params = {}, locale = 'th' } = context;
  const ssrConfig = await serverSideTranslations(locale, [
    LocaleNamespace.Common,
    LocaleNamespace.Listing,
    LocaleNamespace.Footer
  ]);
  const { t } = useSSRTranslations(ssrConfig);
  const identifier = params.identifier as string;
  const sorting = parseSortingSlug(params.sorting as string, t);

  if (!sorting || !identifier) {
    return { notFound: true };
  }

  try {
    const searchRequest = await getSearchRequest(context, t);
    searchRequest.sortBy = sorting;

    const buildingData = await getBuildingDatas(locale as Language);

    return {
      props: {
        identifier,
        rentalsParams: searchRequest,
        buildingData,
        ...ssrConfig
      }
    };
  } catch (error) {
    console.info(`src/pages/propertypad/[identifier]/[sorting].tsx@getStaticProps error`, error);
    return {
      notFound: true
    };
  }
};

export default PropertyPadIdentifierSorting;
