import React, { useCallback, useEffect, useState } from 'react';
import { GetServerSideProps } from 'next';
import { useDispatch, useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { TFunction, useTranslation } from 'next-i18next';
import Head from 'next/head';

import { SearchRequest } from '@interfaces/search';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import ListingsSearch from '@components/listings/search';
import {
  fetchPropertyPad,
  fetchRentals,
  selectFavoriteRentals,
  selectPropertyPad,
  selectRentals,
  resetPropertyPad,
  searchActions
} from '@stores/slices/search';
import { getSearchRequest } from '@utils/search';
import { Language, LocaleNamespace, Page } from '@constants/enum';
import { useSSRTranslations } from '@utils';
import { DataLayerObject, updateGTMDataLayer } from '@utils/gtm';
import { LISTINGS_PER_PAGE, pageType } from '@constants';
import { SortType, toSortingSlug } from '@flexstay.rentals/fs-url-stringify';
import { BuildingObject, getBuildingDatas } from '@utils/buildings';

interface PropertyPadProps {
  identifier: string;
  rentalsParams: SearchRequest;
  buildingData: BuildingObject;
}

function PropertyPadIdentifier({ identifier, rentalsParams, buildingData }: PropertyPadProps) {
  const favoriteRentals = useSelector(selectFavoriteRentals);
  const [page, setPage] = useState(1);
  const rentals = useSelector(selectRentals);
  const propertyPad = useSelector(selectPropertyPad);
  const dispatch = useDispatch();
  const router = useRouter();
  const { i18n, t } = useTranslation(LocaleNamespace.Common);

  useEffect(() => {
    if (identifier) {
      dispatch(fetchPropertyPad(identifier));
    }
  }, [identifier]);

  useEffect(() => {
    dispatch(searchActions.setRentalsParams(rentalsParams));
    return () => {
      dispatch(searchActions.resetRentalsParams());
    };
  }, [rentalsParams]);

  useEffect(() => {
    if (favoriteRentals.length && favoriteRentals.length <= (page - 1) * LISTINGS_PER_PAGE) {
      let lastPage = favoriteRentals.length / LISTINGS_PER_PAGE;
      if (lastPage * LISTINGS_PER_PAGE < favoriteRentals.length) {
        lastPage += 1;
      }
      setPage(lastPage);
      return;
    }
    if (favoriteRentals.length) {
      const totalAddress = favoriteRentals.length;
      dispatch(fetchRentals({ rentals: favoriteRentals, page, totalAddress }));
    } else {
      dispatch(resetPropertyPad());
    }
  }, [page, favoriteRentals.length]);

  const handlePageChange = useCallback((value: number) => {
    setPage(value);
  }, []);

  const handleSortChange = useCallback((value: SortType) => {
    const sortingSlug = toSortingSlug(value, t);
    router.replace(`/propertypad/${identifier}/${sortingSlug}`);
  }, []);

  const handleLocaleChanged = useCallback(async (e: CustomEventInit) => {
    const { locale }: { locale: Language; tLocale: TFunction } = e.detail;
    router.replace(router.asPath, undefined, {
      locale
    });
  }, []);

  useEffect(() => {
    document.addEventListener('locale_changed', handleLocaleChanged);
    return () => {
      document.removeEventListener('locale_changed', handleLocaleChanged);
    };
  }, []);

  /**
   * Send data layer
   */
  useEffect(() => {
    const object: DataLayerObject = {
      type: pageType.PROPERTYPAD,
      locale: i18n.language
    };
    updateGTMDataLayer(object);
  }, []);

  return (
    <>
      <Head>
        <meta name="robots" content="noindex, nofollow" />
      </Head>
      <ListingsSearch
        buildingData={buildingData}
        handlePageChange={handlePageChange}
        handleSortChange={handleSortChange}
        rentals={rentals}
        rentalsParams={rentalsParams}
        page={Page.Propertypad}
        breadcrumbs={[]}
        isSearchingMap={false}
        address={propertyPad?.name}
        defaultSortLabel="Recommended"
        defaultPagingText="availablePropertyPad"
      />
    </>
  );
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  console.info(`src/pages/propertypad/[identifier]/index.tsx@getStaticProps invoked`);
  const { params = {}, locale = 'th' } = context;
  const ssrConfig = await serverSideTranslations(locale, [
    LocaleNamespace.Common,
    LocaleNamespace.Listing,
    LocaleNamespace.Footer
  ]);
  const { t } = useSSRTranslations(ssrConfig);
  const { identifier = '' } = params;

  if (!identifier) return { notFound: true };

  try {
    const searchRequest = await getSearchRequest(context, t);
    const buildingData = await getBuildingDatas(locale as Language);
    return {
      props: {
        identifier: identifier as string,
        rentalsParams: searchRequest,
        buildingData,
        ...ssrConfig
      }
    };
  } catch (error) {
    console.info(`src/pages/propertypad/[identifier]/index.tsx@getStaticProps error`, error);
    return {
      notFound: true
    };
  }
};

export default PropertyPadIdentifier;
