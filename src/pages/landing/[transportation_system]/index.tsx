import Head from 'next/head';
import { GetStaticPathsResult, GetStaticProps } from 'next';
import { Language, LocaleNamespace, Page } from '@constants/enum';
import nextI18NextConfig from '@nextI18NextConfig';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import React, { useCallback, useEffect, useMemo } from 'react';
import Layout from '@components/layout';
import { join } from 'path';
import btsStations from '@assets/data/bts-stations.json';
import mrtStations from '@assets/data/mrt-stations.json';
import arlStations from '@assets/data/arl-stations.json';
import { TransportationSystem } from '@constants/enum/search';
import { useTranslation } from 'react-i18next';
import Breadcrumb from '@components/breadcrumb';
import { useRouter } from 'next/router';
import AlphabetTable from '@components/common/alphabet-table';
import HeaderOverlay from '@components/common/header-overlay';
import { toLandingTransportUrl } from '@utils/landing-transport';
import { BuildingObject, getBuildingDatas, getLayoutData } from '@utils/buildings';
import { selectIsMobile } from '@stores/slices/common';
import { useSelector } from 'react-redux';

interface IProps {
  transportation_system: TransportationSystem;
  buildingData: BuildingObject;
}

const TransportationListPage = ({ transportation_system, buildingData }: IProps) => {
  const { t } = useTranslation(LocaleNamespace.LandingTransportation);
  const { t: tCommon, i18n } = useTranslation(LocaleNamespace.Common);
  const language = i18n.language as Language;
  const router = useRouter();
  const isMobile = useSelector(selectIsMobile);

  const imgWidth = isMobile ? 768 : 1354;
  const imgHeight = isMobile ? 270 : 246;

  const breadcrumbs = useMemo(() => {
    const transportation_system_path = transportation_system.toLowerCase();
    const provincePath = tCommon('bangkokProvince');

    return [
      { label: tCommon('home'), path: join('/') },
      { label: transportation_system, path: join('/', transportation_system_path) },
      { label: tCommon('bangkok'), path: join('/', provincePath, transportation_system_path) },
      { label: tCommon('bangkok'), path: join('/', provincePath, transportation_system_path) }
    ];
  }, [tCommon, transportation_system]);

  const handleLocaleChanged = useCallback(async (e: CustomEventInit) => {
    const { locale }: { locale: Language } = e.detail;
    const tI18n = i18n.getFixedT(locale, LocaleNamespace.Common);
    const transportation_system_path = transportation_system.toLowerCase();
    const provincePath = tI18n('bangkokProvince');
    router.replace(join('/', provincePath, transportation_system_path), undefined, {
      locale
    });
  }, []);

  useEffect(() => {
    document.addEventListener('locale_changed', handleLocaleChanged);
    return () => {
      document.removeEventListener('locale_changed', handleLocaleChanged);
    };
  }, []);

  const imagePlaceholder = useMemo(() => {
    return `/images/landing-transportation/${transportation_system}_Placeholder.png`;
  }, [transportation_system]);

  const alphabetGroups = useMemo(() => {
    let stations: Record<Language, Record<string, Array<{ name: string; id: number }>>>;
    switch (transportation_system) {
      case TransportationSystem.ARL:
        stations = arlStations;
        break;
      case TransportationSystem.BTS:
        stations = btsStations;
        break;
      default:
        stations = mrtStations;
        break;
    }
    return Object.fromEntries(
      Object.keys(stations[language]).map((key) => [
        key,
        stations[language][key].map((station) => ({
          label: station.name,
          path: toLandingTransportUrl({
            name: station.name,
            t: tCommon,
            transportation_system
          })
        }))
      ])
    );
  }, [transportation_system, language, tCommon]);

  const getGroupId = useCallback(
    (char: string) => {
      return [transportation_system.toLowerCase(), char.toLowerCase()].join('-');
    },
    [transportation_system]
  );

  const content = useMemo(() => {
    const prefix = transportation_system.toLocaleLowerCase();
    return {
      subTitle: t(`${prefix}SubTitle`),
      title: t(`${prefix}Title`)
    };
  }, [transportation_system, t]);

  return (
    <>
      <Head>
        <title>
          {t('listPageMetaTitle', {
            transportation_system
          })}
        </title>
        <meta
          name="description"
          content={t('listPageMetaDescription', {
            transportation_system
          })}
        />
      </Head>
      <Layout layoutData={getLayoutData({ buildingData })}>
        <HeaderOverlay
          page={Page.LandingTransportList}
          width={imgWidth}
          height={imgHeight}
          subTitle={content.subTitle}
          src={imagePlaceholder}
          objectFit="cover"
          title={content.title}
        />
        <div className="px-4 sm:px-8 xl:px-24 py-8 text-base">
          <AlphabetTable getId={getGroupId} groups={alphabetGroups} />
        </div>
        <Breadcrumb list={breadcrumbs} />
      </Layout>
    </>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  const locale = context.locale as Language;
  const slug = context.params?.transportation_system as string;

  const transportation_system = Object.values(TransportationSystem).find((i) => i.toLowerCase() === slug);

  const buildingData = await getBuildingDatas(locale);

  if (!transportation_system) {
    return {
      notFound: true
    };
  }

  return {
    props: {
      buildingData,
      transportation_system,
      ...(await serverSideTranslations(
        locale,
        [LocaleNamespace.Footer, LocaleNamespace.Common, LocaleNamespace.LandingTransportation],
        nextI18NextConfig
      ))
    }
  };
};

export async function getStaticPaths(): Promise<GetStaticPathsResult> {
  return {
    paths: [],
    fallback: 'blocking'
  };
}

export default TransportationListPage;
