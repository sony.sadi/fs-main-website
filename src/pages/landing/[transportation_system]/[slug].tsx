import React, { useCallback, useEffect, useMemo } from 'react';
import Head from 'next/head';
import get from 'lodash/get';
import { GetStaticPathsResult, GetStaticProps } from 'next';
import { Language, LocaleNamespace } from '@constants/enum';
import nextI18NextConfig from '@nextI18NextConfig';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Breadcrumb from '@components/breadcrumb';
import Layout from '@components/layout';
import { parseLandingTransportRequest, toLandingTransportUrl } from '@utils/landing-transport';
import LandingTransportInformation from '@components/landing/transport';
import { Transportation } from '@interfaces/search';
import { TFunction, useTranslation } from 'next-i18next';
import { getRentNearby, getSaleNearby } from '@services/http-client/search';
import { Listing } from '@interfaces/listing';
import { useRouter } from 'next/router';
import { getLandingTransportationContent } from '@services/http-client/landing-transport';
import { LandingTransportContent } from '@interfaces/landing-transport';
import FaqSchema from '@components/schema/faq';
import trimEnd from 'lodash/trimEnd';
import { join } from 'path';
import { getDefaultRadius, PoiType, toTransportationName } from '@flexstay.rentals/fs-url-stringify';
import SubwayStationSchema from '@components/schema/subway-station';
import { getImageUrl } from '@utils';
import { BuildingObject, getBuildingDatas, getLayoutData } from '@utils/buildings';

const HOME_URL = trimEnd(process.env.NEXT_PUBLIC_HOME_URL, '/');

interface IProps {
  transportation: Transportation;
  listingsForRent: Listing[];
  listingsForRentSale: Listing[];
  content: LandingTransportContent;
  buildingData: BuildingObject;
}

const LandingTransport = (props: IProps) => {
  const { t: tCommon, i18n } = useTranslation(LocaleNamespace.Common);
  const router = useRouter();
  const language = i18n.language as Language;

  const breadcrumbs = useMemo(() => {
    const { transportation_system, station } = props.transportation;
    const transportation_system_path = transportation_system.toLowerCase();
    return [
      { label: tCommon('home'), path: '/' },
      { label: transportation_system, path: `/${transportation_system_path}` },
      { label: tCommon('bangkok'), path: `/${tCommon('bangkokProvince')}/${transportation_system_path}` },
      { label: tCommon('bangkok'), path: `/${tCommon('bangkokProvince')}/${transportation_system_path}` },
      {
        label: station[language],
        path: toLandingTransportUrl({ t: tCommon, name: station[language], transportation_system })
      }
    ];
  }, [tCommon, props.transportation, language]);

  const handleLocaleChanged = useCallback(
    async (e: CustomEventInit) => {
      const { locale, tLocale }: { locale: Language; tLocale: TFunction } = e.detail;
      router.replace(
        toLandingTransportUrl({
          t: tLocale,
          name: props.transportation.station[locale],
          transportation_system: props.transportation.transportation_system
        }),
        undefined,
        {
          locale
        }
      );
    },
    [props.transportation]
  );

  useEffect(() => {
    document.addEventListener('locale_changed', handleLocaleChanged);
    return () => {
      document.removeEventListener('locale_changed', handleLocaleChanged);
    };
  }, [props.transportation]);

  const faqs = useMemo(() => {
    return props.content.faqs.map((faq) => ({
      question: faq.question[language],
      answer: faq.answer[language]
    }));
  }, [props.content.faqs, language]);

  const placeholderImage = useMemo(() => {
    const { transportation } = props;
    const transportationImage = get(transportation, 'medias[0]');
    return transportationImage
      ? getImageUrl(transportationImage, process.env.NEXT_PUBLIC_EGSWING_API_BASE_URL)
      : `/images/landing-transportation/${transportation.transportation_system}_Placeholder.png`;
  }, [props.transportation]);

  const subwayStation = useMemo(() => {
    const { transportation } = props;
    return {
      name: `${transportation.transportation_system} ${transportation.station[language]}`,
      branchCode: transportation.stop_id,
      openingHours: transportation.opening_hours,
      streetAddress: transportation.address_street[language],
      addressLocality: transportation.station[language],
      addressRegion: transportation.province_ps[language],
      postalCode: transportation.postal_code,
      addressCountry: tCommon('thailand'),
      latitude: transportation.gps_lat,
      longitude: transportation.gps_long,
      url: `${HOME_URL}${join('/', router.asPath, '/')}`,
      image: placeholderImage,
      additionalProperty: transportation.transportation_system,
      containedInPlace: transportation.station[language]
    };
  }, [props.transportation, language, router.asPath, tCommon, placeholderImage]);

  return (
    <>
      <Head>
        <title>{props.content.meta_title[language]}</title>
        <meta name="description" content={props.content.meta_description[language]} />
      </Head>
      <FaqSchema
        faqs={faqs}
        name={toTransportationName(props.transportation.station[language], props.transportation.transportation_system)}
      />
      <SubwayStationSchema {...subwayStation} />
      <Layout layoutData={getLayoutData({ buildingData: props.buildingData })}>
        <LandingTransportInformation {...props} />
        <Breadcrumb list={breadcrumbs} />
      </Layout>
    </>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  console.info(`src/pages/landing/transportation/index.tsx@getStaticProps invoked`);
  try {
    const { poi, locale } = await parseLandingTransportRequest(context);

    const nearbyParams = {
      lat: poi.gps_lat,
      lng: poi.gps_long,
      radius: getDefaultRadius({
        type: PoiType.TRANSPORTATION
      }),
      limit: 10
    };

    const [contentResponse, rentResponse, buildingData, saleResponse] = await Promise.all([
      getLandingTransportationContent(poi.id),
      getRentNearby(nearbyParams),
      getBuildingDatas(locale),
      getSaleNearby(nearbyParams)
    ]);

    const ssrConfig = await serverSideTranslations(
      locale,
      [
        LocaleNamespace.Common,
        LocaleNamespace.Footer,
        LocaleNamespace.Listing,
        LocaleNamespace.ContactForm,
        LocaleNamespace.LandingTransportation
      ],
      nextI18NextConfig
    );

    return {
      props: {
        buildingData,
        listingsForRent: rentResponse.data.data,
        listingsForRentSale: saleResponse.data.data,
        transportation: poi,
        content: contentResponse.data,
        ...ssrConfig
      },
      revalidate: parseInt(process.env.NEXT_CACHE_TIME || '300', 10)
    };
  } catch (error) {
    console.info('src/pages/landing/transportation/index.tsx');
    return {
      notFound: true
    };
  }
};

export async function getStaticPaths(): Promise<GetStaticPathsResult> {
  return {
    paths: [],
    fallback: 'blocking'
  };
}

export default LandingTransport;
