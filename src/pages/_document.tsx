/* eslint-disable react/jsx-indent-props */
/* eslint-disable prettier/prettier */
/* eslint-disable react/jsx-indent */
import { iconSizes, fontWeightOption } from '@constants';
import Document, { Html, Head, Main, NextScript } from 'next/document';

const { NEXT_PUBLIC_HOME_URL } = process.env;
export default class MyDocument extends Document {
  /**
   * Function generate apple-touch-icon
   * @returns
   */
  generateAppleTouchIcon = () => {
    return iconSizes.map((value) => {
      return (
        <link
          key={value}
          rel="apple-touch-icon"
          sizes={`${value}x${value}`}
          href={`${[NEXT_PUBLIC_HOME_URL, 'icons', `logo-${value}x${value}.png`].join('/')}`}
        />
      );
    });
  };

  /**
   * Generate link to preload GG fonts
   * @returns 
   */
  generatePreloadFont = () => {
    return fontWeightOption.map((value) => {
      return (
        <link
          key={value}
          rel="preload"
          as="font"
          type="font/woff2"
          href={`/fonts/roboto-v29-latin-${value}.woff2`}
          crossOrigin="anonymous"
        />
      );
    });
  };

  render() {
    return (
      <Html>
        <Head>
          {/* <base href={process.env.NEXT_PUBLIC_HOME_URL} /> */}
          <meta name="theme-color" content="#5bb8ea" />
          <meta charSet="utf-8" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <link rel="manifest" href={`${NEXT_PUBLIC_HOME_URL}/manifest.json`} />
          <link href={`${NEXT_PUBLIC_HOME_URL}/favicon.svg`} rel="icon" type="image/svg+xml" sizes="any" />
          {this.generateAppleTouchIcon()}
          {this.generatePreloadFont()}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
