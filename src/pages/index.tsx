import { useCallback, useEffect, useMemo } from 'react';
import Head from 'next/head';
import { GetStaticProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { floor } from 'lodash';

import Observer from '@components/core/observer';
import Layout from '@components/layout';
import HomeHero from '@components/home/hero';
import HomePropertyScout from '@components/home/property-scout';
import HomeBestRentals from '@components/home/best-rentals';
import HomePopularAreas from '@components/home/popular-areas';
import HomeInDemandBuildings from '@components/home/in-demand-buildings';
import HomePopularSearches from '@components/home/popular-searches';
import HomeOurServices from '@components/home/our-services';
import HomeOurPartners from '@components/home/our-partners';
import HomeChoosingPropertyScout from '@components/home/choosing-property-scout';
import { useRouter } from 'next/router';

import { Listing } from '@interfaces/listing';
import { Area, InDemandBuilding } from '@interfaces/search';
import { Language, LocaleNamespace } from '@constants/enum';
import Breadcrumb from '@components/breadcrumb';
import { getHomeSchema } from '@services/http-client/schema';
import { getBestDeals, getInDemandBuildings, getPopularAreas, getRentalCount } from '@services/http-client/search';
import SchemaMarkup from '@components/schema';
import nextI18NextConfig from '@nextI18NextConfig';
import { filterLanguage, generateHrefLang, getAlternateLanguage } from '@utils/filter-language';
import BreadcrumbSchema from '@components/breadcrumb/schema';
import statistics from '@assets/data/hero-statistics.json';
import { DataLayerObject, updateGTMDataLayer } from '@utils/gtm';
import { pageType } from '@constants';

interface HomeProps {
  bestDeals: Listing[];
  popularAreas: Area[];
  inDemandBuildings: InDemandBuilding[];
  schemaMarkups: Record<string, any>;
  statistics: { i18nKey: string; total: number }[];
}

const Home = (props: HomeProps) => {
  const { t, i18n } = useTranslation(LocaleNamespace.Home);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);
  const router = useRouter();

  const breadcrumbs = useMemo(() => {
    return [{ label: tCommon('home'), path: '/' }];
  }, []);

  const buildingDatas = useMemo(() => {
    return {
      footerBuilding: {
        popularAreas: props.popularAreas,
        indemandBuilding: props.inDemandBuildings
      }
    };
  }, [props.popularAreas, props.inDemandBuildings]);

  const handleLocaleChanged = useCallback((e: CustomEventInit) => {
    const { locale }: { locale: Language } = e.detail;
    router.replace('/', undefined, {
      locale
    });
  }, []);

  /**
   * Render hreflang for alternate language
   */
  const renderHrefLang = useMemo(() => {
    const alternateLanguage = getAlternateLanguage(i18n.language);
    if (alternateLanguage.length === 0) return [];

    const alternateLinks = alternateLanguage.map((value: string, index: number) => {
      return (
        <link
          key={`index_${index}`}
          rel="alternate"
          href={`${generateHrefLang('', value)}`}
          hrefLang={`${value.toUpperCase()}`}
        />
      );
    });
    return alternateLinks;
  }, [i18n.language]);

  /**
   * Render canonical href
   */
  const renderCanonicalHref = useMemo(() => {
    return `${generateHrefLang('', i18n.language)}`;
  }, [i18n.language]);

  useEffect(() => {
    document.addEventListener('locale_changed', handleLocaleChanged);
    return () => {
      document.removeEventListener('locale_changed', handleLocaleChanged);
    };
  }, []);

  /**
   * Send data layer
   */
  useEffect(() => {
    const object: DataLayerObject = {
      type: pageType.HOMEPAGE,
      locale: i18n.language
    };
    updateGTMDataLayer(object);
  }, []);

  return (
    <>
      <Head>
        <meta name="description" content={t('pageDescription')} />
        <title>{t('pageTitle')}</title>
        <link rel="canonical" href={`${renderCanonicalHref}`} />
        {renderHrefLang.length > 0 && renderHrefLang}
      </Head>
      <SchemaMarkup data={props.schemaMarkups} />
      <BreadcrumbSchema breadcrumbs={breadcrumbs} />
      <Layout layoutData={buildingDatas}>
        <HomeHero statistics={props.statistics} />
        <HomePropertyScout />
        <HomeBestRentals deals={props.bestDeals} />
        <HomePopularAreas areas={props.popularAreas} />
        <HomeInDemandBuildings buildings={props.inDemandBuildings} />
        <HomePopularSearches />
        <HomeOurServices />
        <Observer offsetBottom="200px">{(visible: boolean) => visible && <HomeOurPartners />}</Observer>
        <Observer offsetBottom="200px">{(visible: boolean) => visible && <HomeChoosingPropertyScout />}</Observer>
        <Breadcrumb list={breadcrumbs} />
      </Layout>
    </>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  console.info(`src/pages/index.tsx@getStaticProps invoked`);
  const { locale = 'th' } = context;

  try {
    const [
      bestDealsResponse,
      popularAreasResponse,
      inDemandBuildingsResponse,
      schemaMarkupsResponse,
      rentalCountResponse
    ] = await Promise.all([
      getBestDeals(),
      getPopularAreas(),
      getInDemandBuildings(),
      getHomeSchema(),
      getRentalCount()
    ]);

    const statisticsInfo = [...statistics];
    const dspRentalCount = floor(rentalCountResponse.data / 1000) * 1000;
    statisticsInfo.forEach((element) => {
      if (element.i18nKey === 'propertiesForRent') {
        element.total = dspRentalCount;
      }
    });
    return {
      props: {
        bestDeals: bestDealsResponse.data.data,
        popularAreas: filterLanguage(popularAreasResponse.data.data, locale),
        inDemandBuildings: inDemandBuildingsResponse.data.data,
        schemaMarkups: schemaMarkupsResponse.data,
        statistics: statisticsInfo,
        ...(await serverSideTranslations(
          locale,
          ['common', 'home', 'footer', 'how-it-works', 'contact', 'listing', 'list-your-property'],
          nextI18NextConfig
        ))
      },
      revalidate: parseInt(process.env.NEXT_CACHE_TIME || '300', 10)
    };
  } catch (error) {
    console.info(`src/pages/index.tsx@getStaticProps error`, error);
    return {
      notFound: true
    };
  }
};

export default Home;
