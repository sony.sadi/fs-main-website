import { useCallback, useEffect, useMemo } from 'react';
import { GetStaticProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { TFunction, useTranslation } from 'next-i18next';
import Head from 'next/head';
import cntl from 'cntl';
import { useRouter } from 'next/router';

import Layout from '@components/layout';
import Header from '@components/list-your-property/header';
import OurService from '@components/list-your-property/our-service';
import ListYourPropertyOurPartners from '@components/list-your-property/our-partners';
import FAQ from '@components/list-your-property/faq';
import { Language, LocaleNamespace, Page } from '@constants/enum';
import ContactForm from '@forms/contact';
import Breadcrumb from '@components/breadcrumb';
import nextI18NextConfig from '@nextI18NextConfig';
import BreadcrumbSchema from '@components/breadcrumb/schema';
import { generateHrefLang, getAlternateLanguage } from '@utils/filter-language';
import { DataLayerObject, updateGTMDataLayer } from '@utils/gtm';
import { pageType } from '@constants';
import { BuildingObject, getBuildingDatas, getLayoutData } from '@utils/buildings';

interface ListYourPropertyProps {
  buildingData: BuildingObject;
}

const ListYourProperty = ({ buildingData }: ListYourPropertyProps) => {
  const { t, i18n } = useTranslation(LocaleNamespace.ListYourProperty);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);
  const router = useRouter();

  const breadcrumbs = useMemo(() => {
    return [
      { label: tCommon('home'), path: '/' },
      { label: tCommon('listProperty'), path: tCommon('listYourPropertyPath') }
    ];
  }, [tCommon]);

  const handleLocaleChanged = useCallback((e: CustomEventInit) => {
    const { locale, tLocale }: { locale: Language; tLocale: TFunction } = e.detail;
    router.replace(tLocale('listYourPropertyPath'), undefined, {
      locale
    });
  }, []);

  /**
   * Render hreflang for alternate language
   */
  const renderHrefLang = useMemo(() => {
    const alternateLanguage = getAlternateLanguage(i18n.language);
    if (alternateLanguage.length === 0) return [];

    const alternateLinks = alternateLanguage.map((value: string, index: number) => {
      const ti18n = i18n.getFixedT(value);
      return (
        <link
          key={`index_${index}`}
          rel="alternate"
          href={`${generateHrefLang(ti18n('listYourPropertyPath'), value)}`}
          hrefLang={`${value.toUpperCase()}`}
        />
      );
    });
    return alternateLinks;
  }, [i18n.language]);

  /**
   * Render canonical href
   */
  const renderCanonicalHref = useMemo(() => {
    const ti18n = i18n.getFixedT(i18n.language);

    return `${generateHrefLang(ti18n('listYourPropertyPath'), i18n.language)}`;
  }, [i18n.language]);

  useEffect(() => {
    document.addEventListener('locale_changed', handleLocaleChanged);
    return () => {
      document.removeEventListener('locale_changed', handleLocaleChanged);
    };
  }, []);

  /**
   * Send data layer
   */
  useEffect(() => {
    const object: DataLayerObject = {
      type: pageType.LIST_YOUR_PROPERTY,
      locale: i18n.language
    };
    updateGTMDataLayer(object);
  }, []);

  return (
    <>
      <Head>
        <meta name="description" content={t('pageDescription')} />
        <title>{t('pageTitle')}</title>
        <link rel="canonical" href={renderCanonicalHref} />
        {renderHrefLang.length > 0 && renderHrefLang}
      </Head>
      <BreadcrumbSchema breadcrumbs={breadcrumbs} />
      <Layout layoutData={getLayoutData({ buildingData })}>
        <main className={classes.container}>
          <div className={classes.containerHeader}>
            <Header />
            <div>
              <OurService />
              <ListYourPropertyOurPartners />
              <FAQ />
            </div>
          </div>
          <div className="hidden lg:block lg:pt-5">
            <div className="sticky top-24">
              <ContactForm page={Page.ListYourProperty} />
            </div>
          </div>
        </main>
        <Breadcrumb list={breadcrumbs} />
      </Layout>
    </>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  console.info(`src/pages/list-your-property/index.tsx@getStaticProps invoked`);
  const { locale = 'th' } = context;

  const buildingData = await getBuildingDatas(locale as Language);

  return {
    props: {
      buildingData,
      ...(await serverSideTranslations(
        locale,
        ['common', 'contact-form', 'footer', 'listing', 'home', 'list-your-property', 'how-it-works'],
        nextI18NextConfig
      ))
    }
  };
};

const classes = {
  container: cntl`
    flex w-full lg:max-w-container-list-your-property mx-auto lg:mt-5
  `,
  containerHeader: cntl`
    grid w-full lg:mr-7 relative pt-5
  `
};

export default ListYourProperty;
