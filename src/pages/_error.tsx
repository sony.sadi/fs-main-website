import Head from 'next/head';
import React, { useMemo } from 'react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Footer from '@components/layout/footer';
import nextI18NextConfig from '@nextI18NextConfig';
import { GetServerSideProps } from 'next';
import { BasicSearchTheme, Language, LocaleNamespace, Page } from '@constants/enum';
import BasicSearch from '@components/basic-search';
import NotFoundHeading from '@components/notfound/heading';
import cntl from 'cntl';
import { useTranslation } from 'react-i18next';
import Breadcrumb from '@components/breadcrumb';
import BreadcrumbSchema from '@components/breadcrumb/schema';
import NotFoundHeader from '@components/layout/header/notfound';
import { BuildingObject, getBuildingDatas, getLayoutData } from '@utils/buildings';

const ErrorPage = (props: { asPath: string; buildingData: BuildingObject }) => {
  const { t } = useTranslation(LocaleNamespace.NotFound);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);

  const breadcrumbs = useMemo(() => {
    return [
      { label: tCommon('home'), path: '/' },
      { label: t('notfoundLabel'), path: props.asPath }
    ];
  }, [tCommon]);

  return (
    <>
      <Head>
        <title>{t('pageTitle')}</title>
        <meta name="robots" content="noindex, nofollow" />
      </Head>
      <BreadcrumbSchema breadcrumbs={breadcrumbs} />
      <NotFoundHeader />
      <main className={classes.main}>
        <div className={classes.wrapper}>
          <NotFoundHeading />
          <BasicSearch theme={BasicSearchTheme.Dark} page={Page.Notfound} />
        </div>
        <Breadcrumb list={breadcrumbs} />
      </main>
      <Footer footerBuilding={getLayoutData({ buildingData: props.buildingData }).footerBuilding} />
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const locale = context.locale as Language;
  const buildingData = await getBuildingDatas(locale as Language);
  return {
    props: {
      asPath: context.req.url,
      buildingData,
      ...(await serverSideTranslations(
        locale,
        [LocaleNamespace.Common, LocaleNamespace.Footer, LocaleNamespace.NotFound],
        nextI18NextConfig
      ))
    }
  };
};

const classes = {
  main: cntl`pt-75px lg:pt-65px`,
  wrapper: cntl`flex-1 w-full py-20 flex-center flex-col px-4`
};

export default ErrorPage;
