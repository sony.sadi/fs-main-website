import { useCallback, useEffect, useMemo } from 'react';
import { GetStaticProps } from 'next';
import Head from 'next/head';
import { TFunction, useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';

import Layout from '@components/layout';
import AboutUsHero from '@components/about-us/hero';
import AboutUsMission from '@components/about-us/mission';
import AboutUsPropertyScout from '@components/about-us/property-scout';
import AboutUsTeam from '@components/about-us/team';
import AboutUsValues from '@components/about-us/values';
import { Language, LocaleNamespace } from '@constants/enum';
import Breadcrumb from '@components/breadcrumb';
import nextI18NextConfig from '@nextI18NextConfig';
import BreadcrumbSchema from '@components/breadcrumb/schema';
import { generateHrefLang, getAlternateLanguage } from '@utils/filter-language';
import { DataLayerObject, updateGTMDataLayer } from '@utils/gtm';
import { pageType } from '@constants';
import { BuildingObject, getBuildingDatas, getLayoutData } from '@utils/buildings';

interface Props {
  buildingData: BuildingObject;
}

function AboutUs({ buildingData }: Props) {
  const { t, i18n } = useTranslation(LocaleNamespace.About);
  const { t: tCommon } = useTranslation(LocaleNamespace.Common);
  const router = useRouter();

  const breadcrumbs = useMemo(() => {
    return [
      { label: tCommon('home'), path: '/' },
      { label: tCommon('aboutUs'), path: tCommon('aboutUsPath') }
    ];
  }, [tCommon]);

  const handleLocaleChanged = useCallback((e: CustomEventInit) => {
    const { locale, tLocale }: { locale: Language; tLocale: TFunction } = e.detail;
    router.replace(tLocale('aboutUsPath'), undefined, {
      locale
    });
  }, []);

  /**
   * Render hreflang for alternate language
   */
  const renderHrefLang = useMemo(() => {
    const alternateLanguage = getAlternateLanguage(i18n.language);
    if (alternateLanguage.length === 0) return [];

    const alternateLinks = alternateLanguage.map((value: string, index: number) => {
      const ti18n = i18n.getFixedT(value);
      return (
        <link
          key={`index_${index}`}
          rel="alternate"
          href={`${generateHrefLang(ti18n('aboutUsPath'), value)}`}
          hrefLang={`${value.toUpperCase()}`}
        />
      );
    });
    return alternateLinks;
  }, [i18n.language]);

  /**
   * Render canonical href
   */
  const renderCanonicalHref = useMemo(() => {
    const ti18n = i18n.getFixedT(i18n.language);

    return `${generateHrefLang(ti18n('aboutUsPath'), i18n.language)}`;
  }, [i18n.language]);

  useEffect(() => {
    document.addEventListener('locale_changed', handleLocaleChanged);
    return () => {
      document.removeEventListener('locale_changed', handleLocaleChanged);
    };
  }, []);

  /**
   * Send data layer
   */
  useEffect(() => {
    const object: DataLayerObject = {
      type: pageType.ABOUT_US,
      locale: i18n.language
    };
    updateGTMDataLayer(object);
  }, []);

  return (
    <>
      <Head>
        <meta name="description" content={t('pageDescription')} />
        <title>{t('pageTitle')}</title>
        <link rel="canonical" href={renderCanonicalHref} />
        {renderHrefLang.length > 0 && renderHrefLang}
      </Head>
      <BreadcrumbSchema breadcrumbs={breadcrumbs} />
      <Layout layoutData={getLayoutData({ buildingData })}>
        <AboutUsHero />
        <AboutUsMission />
        <AboutUsPropertyScout />
        <AboutUsTeam />
        <AboutUsValues />
        <Breadcrumb list={breadcrumbs} />
      </Layout>
    </>
  );
}

export const getStaticProps: GetStaticProps = async (context) => {
  console.info(`src/pages/about-us/index.tsx@getStaticProps invoked`);
  const { locale = 'th' } = context;

  const buildingData = await getBuildingDatas(locale as Language);

  return {
    props: {
      buildingData,
      ...(await serverSideTranslations(
        locale,
        ['common', 'about', 'footer', 'contact', 'home', 'listing'],
        nextI18NextConfig
      ))
    }
  };
};

export default AboutUs;
