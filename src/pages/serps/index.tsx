import { useCallback, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { GetStaticProps } from 'next';
import Head from 'next/head';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { TFunction, useTranslation } from 'next-i18next';
import omit from 'lodash/omit';

import ListingsSearch from '@components/listings/search';
import { getPublicRentals } from '@services/http-client/search';
import { useSSRTranslations } from '@utils';
import { searchActions, selectSearchAddress } from '@stores/slices/search';
import { getSearchRequest } from '@utils/search';
import { SearchRequest, SearchResponse } from '@interfaces/search';
import { Language, LocaleNamespace, Page } from '@constants/enum';
import { BreadcrumbItem } from '@interfaces/breadcrumb';
import { getBreadcrumbs } from '@services/http-client/breadcrumb';
import { useRouter } from 'next/router';
import { stringifyUrl, getDefaultParams } from '@flexstay.rentals/fs-url-stringify';
import nextI18NextConfig from '@nextI18NextConfig';
import { generateHrefLang, getAlternateLanguage } from '@utils/filter-language';
import { getSerpSeoMeta } from '@services/http-client/seo';
import { assembleSeo } from '@utils/seo';
import { SeoResponse } from '@interfaces/seo';
import { DataLayerObject, updateGTMDataLayer } from '@utils/gtm';
import { pageType } from '@constants';
import { BuildingObject, getBuildingDatas } from '@utils/buildings';

interface RentalsProps {
  rentals: SearchResponse;
  rentalsParams: SearchRequest;
  breadcrumbs: BreadcrumbItem[];
  seo: SeoResponse;
  buildingData: BuildingObject;
}

function Rentals(props: RentalsProps) {
  const dispatch = useDispatch();
  const router = useRouter();
  const { t, i18n } = useTranslation(LocaleNamespace.Common);
  const language = i18n.language as Language;
  const searchAddress = useSelector(selectSearchAddress);

  const seo = useMemo(() => {
    return assembleSeo({
      seo: props.seo,
      page: props.rentals.page,
      pageSize: props.rentals.perPage,
      total: props.rentals.total,
      lowestPrice: props.rentals.aggs?.lowest_price as number,
      address: searchAddress || '',
      coords: props.rentalsParams.coords,
      t
    });
  }, [props.seo, props.rentalsParams, props.rentals, searchAddress]);

  useEffect(() => {
    dispatch(searchActions.setRentalsParams(props.rentalsParams));
    dispatch(searchActions.setRentals(props.rentals));
    dispatch(searchActions.setSearchTerm('')); // Reset search term when access to default serps. Avoid get previous search term when filter on default Serp

    return () => {
      dispatch(searchActions.resetRentalsParams());
    };
  }, [props.rentalsParams, props.rentals]);

  const handleLocaleChanged = useCallback(
    (e: CustomEventInit) => {
      const { locale, tLocale }: { locale: Language; tLocale: TFunction } = e.detail;
      router.replace(
        stringifyUrl({ request: { ...props.rentalsParams, index_serp_types: ['rents'] }, t: tLocale }).path,
        undefined,
        {
          locale
        }
      );
    },
    [props.rentalsParams]
  );

  /**
   * Render hreflang for alternate language
   */
  const renderHrefLang = useMemo(() => {
    const alternateLanguage = getAlternateLanguage(language);
    if (alternateLanguage.length === 0) return [];

    const alternateLinks = alternateLanguage.map((value: string, idx) => {
      const urlPath = stringifyUrl({
        request: { ...getDefaultParams(), index_serp_types: ['rents'] },
        t: i18n.getFixedT(value)
      }).path;

      return (
        <link
          key={idx}
          rel="alternate"
          href={`${generateHrefLang(urlPath, value, true)}`}
          hrefLang={`${value.toUpperCase()}`}
        />
      );
    });
    return alternateLinks;
  }, [props.rentalsParams, language]);

  /**
   * Render canonical href
   */
  const renderCanonicalHref = useMemo(() => {
    const urlPath = stringifyUrl({
      request: { ...getDefaultParams(), index_serp_types: ['rents'] },
      t: i18n.getFixedT(language)
    }).path;

    return `${generateHrefLang(urlPath, language, true)}`;
  }, [props.rentalsParams, language]);

  useEffect(() => {
    document.addEventListener('locale_changed', handleLocaleChanged);
    return () => {
      document.removeEventListener('locale_changed', handleLocaleChanged);
    };
  }, [props.rentalsParams]);

  const noIndexSerp = useMemo(() => {
    // NOTE: FS-1463 Hide all SALES URLs for Google / noindex, nofollow
    return props.rentalsParams.serp_type === 'sales';
  }, [props.rentalsParams, props.rentals]);

  /**
   * Send data layer
   */
  useEffect(() => {
    const object: DataLayerObject = {
      type: pageType.SERPS,
      locale: language
    };
    updateGTMDataLayer(object);
  }, []);

  return (
    <>
      <Head>
        <meta name="description" content={seo.meta_description} />
        <title>{seo.meta_title}</title>
        <link rel="canonical" href={renderCanonicalHref} />
        {renderHrefLang.length > 0 && renderHrefLang}
        {noIndexSerp && <meta name="robots" content="noindex, nofollow" />}
      </Head>
      <ListingsSearch
        allowSchema
        seo={seo}
        page={Page.Listings}
        breadcrumbs={props.breadcrumbs}
        rentals={props.rentals}
        rentalsParams={props.rentalsParams}
        buildingData={props.buildingData}
      />
    </>
  );
}

export const getStaticProps: GetStaticProps = async (context) => {
  console.info(`src/pages/serps/index.tsx@getStaticProps invoked`);
  const locale = (context.locale as Language) || Language.TH;
  const ssrConfig = await serverSideTranslations(locale, ['common', 'listing', 'footer']);
  const { t } = useSSRTranslations(ssrConfig);

  const url = [t('bangkokProvince'), t('rentalsValue')].join('/');

  try {
    const searchRequest = await getSearchRequest(context, t);
    const [rentalResponse, breadcrumbResponse, seoResponse, buildingData] = await Promise.all([
      getPublicRentals(omit(searchRequest, 'where')),
      getBreadcrumbs(searchRequest, locale),
      getSerpSeoMeta({ ...searchRequest, locale, url }),
      getBuildingDatas(locale)
    ]);

    return {
      props: {
        rentalsParams: getDefaultParams(searchRequest.serp_type),
        rentals: rentalResponse.data,
        breadcrumbs: breadcrumbResponse.data,
        seo: seoResponse.data,
        buildingData,
        ...(await serverSideTranslations(locale, ['common', 'listing', 'footer'], nextI18NextConfig))
      },
      revalidate: parseInt(process.env.NEXT_CACHE_TIME || '300', 10)
    };
  } catch (error) {
    console.info('src/pages/serps/index.tsx@getStaticProps', error);

    return {
      notFound: true
    };
  }
};

export default Rentals;
