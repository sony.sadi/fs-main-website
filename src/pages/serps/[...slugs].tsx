import { useCallback, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { GetStaticPathsResult, GetStaticProps } from 'next';
import { useRouter } from 'next/router';
import { TFunction, useTranslation } from 'next-i18next';
import omit from 'lodash/omit';
import Head from 'next/head';
import { join } from 'path';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import ListingsSearch from '@components/listings/search';
import { getPublicRentals } from '@services/http-client/search';
import { searchActions, selectSearchAddress, selectSearchTerm } from '@stores/slices/search';
import { getSearchRequest, getWhereQuery, QueryResponse } from '@utils/search';
import { SearchRequest, SearchResponse } from '@interfaces/search';
import { useSSRTranslations } from '@utils';
import { Language, LocaleNamespace, Page } from '@constants/enum';
import { BreadcrumbItem } from '@interfaces/breadcrumb';
import nextI18NextConfig from '@nextI18NextConfig';
import { stringifyUrl, getShouldIndexSerp, getDefaultRadius, SerpType } from '@flexstay.rentals/fs-url-stringify';
import { generateHrefLang, getAlternateLanguage } from '@utils/filter-language';
import { SeoResponse } from '@interfaces/seo';
import { assembleSeo } from '@utils/seo';
import { DataLayerObject, updateGTMDataLayer } from '@utils/gtm';
import { DEFAULT_SEARCH_ADDRESS, pageType } from '@constants';
import { generateCombineDataSerps } from '@utils/data-serp';
import { BuildingObject, getBuildingDatas } from '@utils/buildings';

interface CatchAllRoutesProps {
  rentals: SearchResponse;
  rentalsParams: SearchRequest;
  breadcrumbs: BreadcrumbItem[];
  seo: SeoResponse;
  queryData: QueryResponse;
  buildingData: BuildingObject;
  serpType?: SerpType;
}

function CatchAllRoutes(props: CatchAllRoutesProps) {
  const router = useRouter();
  const dispatch = useDispatch();
  const { t, i18n } = useTranslation(LocaleNamespace.Common);
  const language = i18n.language as Language;
  const searchTerm = useSelector(selectSearchTerm);
  const searchAddress = useSelector(selectSearchAddress);

  if (router.isFallback) {
    return null;
  }

  const seo = useMemo(() => {
    return assembleSeo({
      seo: props.seo,
      page: props.rentals.page,
      pageSize: props.rentals.perPage,
      total: props.rentals.total,
      lowestPrice: props.rentals.aggs?.lowest_price as number,
      address: searchAddress || '',
      coords: props.rentalsParams.coords,
      t
    });
  }, [props.seo, props.rentalsParams, props.rentals, searchAddress]);
  useEffect(() => {
    dispatch(searchActions.setRentalsParams(props.rentalsParams));
    dispatch(searchActions.setSearchTerm(props.rentalsParams.where || ''));
    const address = props.rentalsParams.coords ? searchTerm : props.rentalsParams.where;
    dispatch(searchActions.setSearchAddress(address || ''));
    dispatch(searchActions.setRentals(props.rentals));

    return () => {
      dispatch(searchActions.resetRentalsParams());
    };
  }, [props.rentalsParams, props.rentals]);

  useEffect(() => {
    return () => {
      dispatch(searchActions.setSearchAddress(''));
    };
  }, []);

  /**
   * Send data layer
   */
  useEffect(() => {
    const object: DataLayerObject = {
      type: pageType.SERPS,
      locale: language
    };
    updateGTMDataLayer(object);
  }, []);

  const handleLocaleChanged = useCallback(
    async (e: CustomEventInit) => {
      const { locale, tLocale }: { locale: Language; tLocale: TFunction } = e.detail;
      const whereQuery = await getWhereQuery(locale, tLocale, props.rentalsParams);
      router.replace(
        stringifyUrl({
          request: { ...props.rentalsParams, where: whereQuery.queryValue, index_serp_types: ['rents'] },
          t: tLocale
        }).path,
        undefined,
        {
          locale
        }
      );
    },
    [props.rentalsParams]
  );

  /**
   * Render hreflang for alternate language
   */
  const renderHrefLang = useMemo(() => {
    const { queryData } = props;

    const alternateLanguage = getAlternateLanguage(language);

    if (alternateLanguage.length === 0) return [];
    const alternateLinks = alternateLanguage.map((value: Language, index: number) => {
      const urlPath = stringifyUrl({
        request: {
          ...props.rentalsParams,
          where: queryData.queryDatas ? queryData.queryDatas[value] : '',
          index_serp_types: ['rents']
        },
        t: i18n.getFixedT(value)
      }).path;
      return (
        <link
          key={index}
          rel="alternate"
          href={`${generateHrefLang(urlPath, value, true)}`}
          hrefLang={`${value.toUpperCase()}`}
        />
      );
    });

    return alternateLinks;
  }, [props.queryData, props.rentalsParams, language]);

  /**
   * Render canonical href
   */
  const renderCanonicalHref = useMemo(() => {
    const { where } = props.rentalsParams;
    const urlPath = stringifyUrl({
      request: { ...props.rentalsParams, where: where || searchTerm, index_serp_types: ['rents'] },
      t: i18n.getFixedT(language)
    }).path;

    return `${generateHrefLang(urlPath, language, true)}`;
  }, [props.rentalsParams, language]);

  const noIndexSerp = useMemo(() => {
    // NOTE: FS-1463 Hide all SALES URLs for Google / noindex, nofollow
    return (
      !getShouldIndexSerp({ params: props.rentalsParams, total: props.rentals.total }) ||
      props.rentalsParams.serp_type === 'sales'
    );
  }, [props.rentalsParams, props.rentals]);

  useEffect(() => {
    document.addEventListener('locale_changed', handleLocaleChanged);
    return () => {
      document.removeEventListener('locale_changed', handleLocaleChanged);
    };
  }, [props.rentalsParams]);

  return (
    <>
      <Head>
        <meta name="description" content={seo.meta_description} />
        <title>{seo.meta_title}</title>
        <link rel="canonical" href={renderCanonicalHref} />
        {renderHrefLang.length > 0 && renderHrefLang}
        {noIndexSerp && <meta name="robots" content="noindex, nofollow" />}
      </Head>
      <ListingsSearch
        allowSchema
        page={Page.Listings}
        seo={seo}
        serpType={props.serpType}
        breadcrumbs={props.breadcrumbs}
        rentals={props.rentals}
        rentalsParams={props.rentalsParams}
        buildingData={props.buildingData}
      />
    </>
  );
}

export const getStaticProps: GetStaticProps = async (context) => {
  console.info(`src/pages/serps/[...slugs].tsx@getStaticProps invoked`);
  const locale = (context.locale as Language) || Language.TH;

  const url = (context.params?.slugs as string[]).join('/');
  const ssrConfig = await serverSideTranslations(locale, ['common', 'listing', 'footer'], nextI18NextConfig);
  const { t } = useSSRTranslations(ssrConfig);
  try {
    const searchRequest = await getSearchRequest(context, t);

    const [rentalResponse, combineDataResponse, buildingData] = await Promise.all([
      getPublicRentals(omit(searchRequest, 'where')),
      generateCombineDataSerps(locale, t, searchRequest, url),
      getBuildingDatas(locale)
    ]);

    const maxTotalPage = Math.max(Math.ceil(rentalResponse.data.total / searchRequest.limit), 1);

    if (searchRequest.page > maxTotalPage) {
      throw new Error(`Page ${searchRequest.page} is not exist for this entries`);
    }

    const poiId = searchRequest.areaId || searchRequest.transportationId || searchRequest.buildingId;
    const isNonRecognizedSearch = !!searchRequest.where && !poiId;
    if (isNonRecognizedSearch) {
      const localePrefix = locale === Language.TH ? '' : locale;
      const defaultRadius = getDefaultRadius({ address: DEFAULT_SEARCH_ADDRESS });
      const { path } = stringifyUrl({
        t,
        request: {
          ...searchRequest,
          radius: searchRequest.radius === defaultRadius ? 0 : searchRequest.radius,
          index_serp_types: ['rents']
        }
      });

      return {
        redirect: {
          statusCode: 302,
          destination: encodeURI(join('/', localePrefix, path, '/'))
        }
      };
    }

    return {
      props: {
        rentalsParams: searchRequest,
        rentals: rentalResponse.data,
        breadcrumbs: combineDataResponse.breadcrumbs,
        seo: combineDataResponse.serp_seo,
        queryData: combineDataResponse.queryObject,
        buildingData,
        serpType: searchRequest.serp_type,
        ...ssrConfig
      },
      revalidate: parseInt(process.env.NEXT_CACHE_TIME || '300', 10)
    };
  } catch (error) {
    console.info('src/pages/serps/[...slugs].tsx@getStaticProps', error);
    return {
      notFound: true
    };
  }
};

export async function getStaticPaths(): Promise<GetStaticPathsResult> {
  return {
    paths: [],
    fallback: 'blocking'
  };
}

export default CatchAllRoutes;
