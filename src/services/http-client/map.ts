import { AxiosResponse } from 'axios';
import { Listing } from '@interfaces/listing';
import { RentalsCoordinateParams, RentalsCoordinateResponse } from '@interfaces/map';
import { BANGKOK_LAT_LNG, BANGKOK_PROVINCE } from '@constants';
import connectionInstance from './connection-instance';

export function getPublicRentalsCoordinates(
  params: RentalsCoordinateParams
): Promise<AxiosResponse<RentalsCoordinateResponse>> {
  if (!(params.buildingId || params.areaId || params.transportationId)) {
    params.coords = params.coords.replace('-', ',') || BANGKOK_LAT_LNG;
  }

  return connectionInstance.get('rentals/public-search-coordinate-v1', {
    params: { ...params, showLoading: false, province: BANGKOK_PROVINCE }
  });
}

export function getPropertyById(id: number): Promise<AxiosResponse<Listing>> {
  return connectionInstance.get(`rentals/frontend/${id}`, { params: { showLoading: false } });
}
