import Axios, { AxiosRequestConfig, AxiosResponse, AxiosError } from 'axios';
import omit from 'lodash/omit';

const __DEV__ = process.env.NODE_ENV === 'development';

const SERVER_LOGGER_ENABLE = process.env.NEXT_NEST_LOGGER_ENABLED === 'true';

function loader() {
  let requestCount = 0;

  return (isLoading: boolean) => {
    if (isLoading) {
      requestCount++;
    } else if (requestCount > 0) {
      requestCount--;
    }

    // store.dispatch(commonActions.setLoading(!!requestCount));
  };
}

const setLoading = loader();

const ConnectionInstance = Axios.create({
  timeout: 20000,
  baseURL: process.env.NEXT_PUBLIC_EGSWING_API_BASE_URL,
  paramsSerializer(params) {
    params = omit(params, 'showLoading');

    const searchParams = new URLSearchParams();
    // eslint-disable-next-line no-restricted-syntax
    for (const key of Object.keys(params)) {
      const param = params[key];
      if (param !== undefined) {
        if (Array.isArray(param)) {
          param.forEach((p, i) => {
            searchParams.append(`${key}[${i}]`, p);
          });
        } else {
          searchParams.append(key, param);
        }
      }
    }
    return searchParams.toString();
  }
});

ConnectionInstance.interceptors.request.use(
  (requestConfig: AxiosRequestConfig) => {
    if (requestConfig.params?.showLoading !== false) {
      setLoading(true);
    }

    return requestConfig;
  },
  (error: AxiosError) => {
    setLoading(false);
    if (__DEV__) {
      console.error('EGSwing API Request Error:', error);
    }
    if (SERVER_LOGGER_ENABLE) {
      console.error(`DEBUG: ${error.config.method} ${error.config.url} ${error.response?.status}`);
    }
    return Promise.reject(error);
  }
);

ConnectionInstance.interceptors.response.use(
  (response: AxiosResponse) => {
    if (response.config.params?.showLoading !== false) {
      setLoading(false);
    }

    // Try to find the access token from response
    if (response.data?.token?.accessToken) {
      ConnectionInstance.defaults.headers.common = {
        Authorization: `Bearer ${response.data?.token?.accessToken}`
      };
    }

    return response;
  },
  (error: AxiosError) => {
    setLoading(false);
    if (__DEV__) {
      console.error('EGSWING API Response Error:', error);
    }
    if (SERVER_LOGGER_ENABLE) {
      console.error(`DEBUG: ${error.config.method} ${error.config.url} ${error.response?.status}`);
    }
    const errorMessage = error?.response?.data?.message;
    if (errorMessage) {
      return Promise.reject(new Error(errorMessage));
    }
    return Promise.reject(error);
  }
);

export default ConnectionInstance;
