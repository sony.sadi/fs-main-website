import { AxiosResponse } from 'axios';

import {
  BuildingsRequest,
  SearchRequest,
  Building,
  SearchResponse,
  AreaResponse,
  InDemandBuildingResponse,
  SimilarListing,
  PoisSuggestionRequest,
  PoisSuggestionResponse,
  GenericPoisSuggestionRequest,
  TransportationResponse,
  PropertyPadResponse,
  PropertyPad
} from '@interfaces/search';
import {
  BANGKOK_LAT_LNG,
  MAX_FILTERING_PRICE,
  MAX_FILTERING_FLOOR_SIZE,
  BANGKOK_PROVINCE,
  MAX_FILTERING_SALE_PRICE
} from '@constants/index';
import { SortType, getDefaultParams, SerpType } from '@flexstay.rentals/fs-url-stringify';
import { Listing } from '@interfaces/listing';
import { Tenure } from '@constants/enum/listing';
import connectionInstance from './connection-instance';

export function getBuildings(params: BuildingsRequest): Promise<AxiosResponse<Building[]>> {
  return connectionInstance.get('buildings', { params });
}

export function getPoisSuggestion(params: PoisSuggestionRequest): Promise<AxiosResponse<PoisSuggestionResponse>> {
  return connectionInstance.get('frontend/suggestions', { params });
}

export function getAreasPoisSuggestion(params: GenericPoisSuggestionRequest): Promise<AxiosResponse<AreaResponse>> {
  return connectionInstance.get('poi-areas/search', { params });
}

export function getTransportationsPoisSuggestion(
  params: GenericPoisSuggestionRequest
): Promise<AxiosResponse<TransportationResponse>> {
  return connectionInstance.get('poi-transportations/search', { params });
}

export function getBuildingPoisSuggestion(
  params: GenericPoisSuggestionRequest
): Promise<AxiosResponse<InDemandBuildingResponse>> {
  return connectionInstance.get('poi-buildings/search', { params });
}

export function getPublicRentals(params: SearchRequest): Promise<AxiosResponse<SearchResponse>> {
  const requestParams = {
    ...params,
    minPrice: Math.min(params.minPrice, params.maxPrice),
    maxPrice: Math.max(params.minPrice, params.maxPrice),
    floorSize_lte: params.floorSize_lte || MAX_FILTERING_FLOOR_SIZE,
    province: BANGKOK_PROVINCE
  };

  if (requestParams.sortBy === SortType.Recommended && requestParams.buildingId) {
    requestParams.sortBy = SortType.Distance;
  }

  if (requestParams.sortBy === SortType.Recommended) {
    requestParams.sortBy = SortType.ListingSource;
  }

  requestParams.maxPrice =
    requestParams.maxPrice || (requestParams.serp_type === 'sales' ? MAX_FILTERING_SALE_PRICE : MAX_FILTERING_PRICE);

  if (!(requestParams.buildingId || requestParams.areaId || requestParams.transportationId)) {
    requestParams.coords = params.coords.replace('-', ',') || BANGKOK_LAT_LNG;
  }

  return connectionInstance.get('rentals/public-search-v1', { params: requestParams });
}

export function getRentNearby(props: {
  lat: number;
  lng: number;
  radius: number;
  limit: number;
}): Promise<AxiosResponse<SearchResponse>> {
  const params = {
    ...getDefaultParams(),
    maxPrice: MAX_FILTERING_PRICE,
    floorSize_lte: MAX_FILTERING_FLOOR_SIZE,
    province: BANGKOK_PROVINCE,
    coords: `${props.lat},${props.lng}`,
    radius: props.radius,
    limit: props.limit,
    serp_type: 'rents'
  };
  return connectionInstance.get('rentals/public-search-v1', { params });
}

export function getSaleNearby(props: {
  lat: number;
  lng: number;
  radius: number;
  limit: number;
}): Promise<AxiosResponse<SearchResponse>> {
  const params = {
    ...getDefaultParams(),
    maxPrice: MAX_FILTERING_SALE_PRICE,
    floorSize_lte: MAX_FILTERING_FLOOR_SIZE,
    province: BANGKOK_PROVINCE,
    coords: `${props.lat},${props.lng}`,
    radius: props.radius,
    limit: props.limit,
    serp_type: 'sales'
  };
  return connectionInstance.get('rentals/public-search-v1', { params });
}

export function getBestDeals(): Promise<AxiosResponse<SearchResponse>> {
  return connectionInstance.get('rentals/public-search-v1', {
    params: {
      highlightListing: 'bestdeal',
      sortBy: 'newest',
      limit: 10,
      province: BANGKOK_PROVINCE
    }
  });
}

export function getRecommendedUnits(buildingId: number, serp_type: SerpType): Promise<AxiosResponse<SearchResponse>> {
  return connectionInstance.get('rentals/public-search-v1', {
    params: {
      sortBy: 'listing_source',
      limit: 20,
      buildingQuery: 'match',
      buildingId,
      province: BANGKOK_PROVINCE,
      serp_type
    }
  });
}

export function getSimilarListingsNearby(params: SimilarListing): Promise<AxiosResponse<SearchResponse>> {
  return connectionInstance.get('rentals/public-search-v1', {
    params: {
      minPrice: params.price - params.price * 0.2,
      maxPrice: params.price + params.price * 0.2,
      limit: 10,
      coords: `${params.lat},${params.long}`,
      province: BANGKOK_PROVINCE,
      serp_type: params.serp_type
    }
  });
}

export function getPopularAreas(): Promise<AxiosResponse<AreaResponse>> {
  return connectionInstance.get('poi-areas/search', {
    params: {
      priority: 'AAA',
      sortBy: 'newest',
      _limit: 10
    }
  });
}

export function getInDemandBuildings(): Promise<AxiosResponse<InDemandBuildingResponse>> {
  return connectionInstance.get('poi-buildings/search', {
    params: {
      priority: 'AAA',
      _limit: 11
    }
  });
}

export function getPropertyPad(identifier: string): Promise<AxiosResponse<PropertyPadResponse[]>> {
  return connectionInstance.get('property-pads', {
    params: {
      identifier
    }
  });
}

export function sendPropertyPadToServer(propertyPad: PropertyPad): Promise<AxiosResponse<PropertyPadResponse>> {
  const { id, identifier, favouriteRentals, hubspotdealurl, name } = propertyPad;
  if (!id) {
    return connectionInstance.post(`/property-pads`, {
      identifier,
      user_id: 0,
      name,
      hubspotdealurl,
      rental_ids: favouriteRentals
    });
  }
  return connectionInstance.put(`/property-pads/${id}`, { rental_ids: favouriteRentals });
}

export function syncPropertyPadWithBackend(
  id: number,
  action: string,
  rentalId: number
): Promise<AxiosResponse<PropertyPadResponse>> {
  return connectionInstance.post(`/property-pads/sync/${id}?action=${action}&rental_id=${rentalId}`);
}

export function getPropertyPadRentals(params: number[]): Promise<AxiosResponse<Listing[]>> {
  const url = `rentals?${params.map((id) => `id_in=${id}`).join('&')}`;

  return connectionInstance.get(url);
}

export function getRentalCount(): Promise<AxiosResponse<number>> {
  const params = {
    ...getDefaultParams(),
    maxPrice: MAX_FILTERING_PRICE,
    floorSize_lte: MAX_FILTERING_FLOOR_SIZE,
    province: BANGKOK_PROVINCE,
    coords: BANGKOK_LAT_LNG,
    radius: 40
  };
  return connectionInstance.get(`rentals/public-search-v1/count`, {
    params
  });
}
