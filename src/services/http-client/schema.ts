import { AxiosResponse } from 'axios';
import connectionInstance from './connection-instance';

export function getHomeSchema(): Promise<AxiosResponse<Record<string, any>>> {
  return connectionInstance.get('schema-markups/home');
}
