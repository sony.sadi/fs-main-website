import { Language } from '@constants/enum';
import { BreadcrumbItem } from '@interfaces/breadcrumb';
import { SearchRequest } from '@interfaces/search';
import { SeoResponse } from '@interfaces/seo';
import { AxiosResponse } from 'axios';
import connectionInstance from './connection-instance';

export interface CombineSearchParam extends SearchRequest {
  radiusBreadcrumb: number;
  coordsBreadcrumb: string;
  radiusSeo: number;
  coordsSeo: string;
  lang: Language;
  province: string;
}

interface CombineSearchResponse<T> {
  serp_seo: SeoResponse;
  breadcrumbs: BreadcrumbItem[];
  poi_search: T;
}

/**
 * API get data combine serps
 * @param searchParams
 * @param _q
 * @param poiType
 * @returns
 */
export function getDataBuildSerp<T>(
  searchParams: CombineSearchParam,
  _q?: string,
  poiType?: string
): Promise<AxiosResponse<CombineSearchResponse<T>>> {
  const requestParams = {
    ...searchParams,
    query_key: _q || null,
    poi_type_key: poiType?.replace('poi-', '') || null
  };
  return connectionInstance.get('frontend/search', { params: requestParams });
}
