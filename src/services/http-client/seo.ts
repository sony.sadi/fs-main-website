import { BANGKOK_PROVINCE } from '@constants';
import { Language } from '@constants/enum';
import { SearchRequest } from '@interfaces/search';
import { SeoResponse } from '@interfaces/seo';
import { getSerpRequestRadius } from '@utils';
import { AxiosResponse } from 'axios';
import connectionInstance from './connection-instance';

export interface GetSerpSeoMetaProps extends SearchRequest {
  locale: Language;
  url: string;
}

export function getSerpSeoMeta(params: GetSerpSeoMetaProps): Promise<AxiosResponse<SeoResponse>> {
  const defaultRadius = getSerpRequestRadius({ ...params });
  const requestParams = {
    ...params,
    radius: params.radius === defaultRadius ? 0 : params.radius,
    coords: params.coords.replace('-', ','),
    province: BANGKOK_PROVINCE
  };
  return connectionInstance.get('serp', {
    params: requestParams
  });
}
