import { LandingTransportContent } from '@interfaces/landing-transport';
import { AxiosResponse } from 'axios';
import connectionInstance from './connection-instance';

export const getLandingTransportationContent = (id: number): Promise<AxiosResponse<LandingTransportContent>> => {
  return connectionInstance.get(`/frontend/landing-pages/transportation/${id}`);
};
