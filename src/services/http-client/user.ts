import ConnectionInstance from './connection-instance';

// eslint-disable-next-line import/prefer-default-export
export const getMe = () => ConnectionInstance.get('me');
