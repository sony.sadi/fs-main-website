import { DEFAULT_SEARCH_ADDRESS } from '@constants';
import { Language } from '@constants/enum';
import { PoiType, getDefaultRadius } from '@flexstay.rentals/fs-url-stringify';
import { SearchRequest } from '@interfaces/search';
import connectionInstance from './connection-instance';

export function getListingDetailBreadcrumbs(id: number, lang: Language) {
  return connectionInstance.get(`breadcrumbs/rentals/${id}`, {
    params: {
      lang
    }
  });
}

export function getBreadcrumbs(params: SearchRequest, lang: Language) {
  const requestParams = {
    ...params,
    lang
  };
  let poiType;
  let where = params.where || '';

  const poiId = requestParams.buildingId || requestParams.areaId || requestParams.transportationId;

  if (poiId) {
    switch (poiId) {
      case requestParams.buildingId:
        poiType = PoiType.BUILDING;
        break;
      case requestParams.areaId:
        poiType = PoiType.AREA;
        break;
      case requestParams.transportationId:
        poiType = PoiType.TRANSPORTATION;
        break;
      default:
        break;
    }
  } else {
    requestParams.coords = params.coords.replace('-', ',');
    where = params.coords ? params.where || '' : DEFAULT_SEARCH_ADDRESS;
  }
  const isDefaultRadius = getDefaultRadius({ address: where, type: poiType }) === params.radius;

  requestParams.radius = isDefaultRadius ? 0 : params.radius;

  return connectionInstance.get('breadcrumbs/rentals', { params: requestParams });
}
