import { Language } from '@constants/enum';

export const WATTHANA_COORDINATES = {
  LAT: 13.725882,
  LNG: 100.577048
};

export const CONTACT_US_COORDINATES = {
  LAT: 13.729432731070167,
  LNG: 100.58040590195748
};

export const BANGKOK_LAT_LNG = '13.7563309,100.5017651';

export const BANGKOK_PROVINCE = 'Bangkok';

export const DEFAULT_SEARCH_ADDRESS = 'Bangkok, Thailand';

export const DEFAULT_SEARCH_RADIUS = 40;

export const MAX_FILTERING_PRICE = 1e6;
export const MAX_FILTERING_SALE_PRICE = 1e9 - 1;
export const MAX_FILTERING_FLOOR_SIZE = 1e3;

export const MAX_CARD_RENTAL_IMAGES = 6;

export const GOOGLE_TAG_MANAGER_RENDER_INIT_IN_MS = 20000;

export const EMAILJS = {
  EMAIL_TEMPLATE: 'flexstay_testing',
  EMAIL_SERVICE_ID: 'infogmail',
  PAGE: {
    LISTING_DETAILS: 'listing details page',
    CONTACT_US: 'Contact Us page',
    HOW_IT_WORKS: 'How it works'
  }
};

export const LANGUAGES = {
  [Language.US]: {
    name: Language.US,
    icon: 'english-flag',
    text: 'English'
  },
  [Language.TH]: {
    name: Language.TH,
    icon: 'thai-flag',
    text: 'ภาษาไทย'
  }
};

export const KEYBOARD_CODE = {
  ENTER: 13
};

export const RADIUS_TO_SHOW_CLOSEST_TRANSPORT = 2000;

export const LOGO = {
  DATA_TEST_ID: 'flexstay-logo'
};

export const metricKeys = {
  TTFB: 'TTFB',
  FCP: 'FCP',
  LCP: 'LCP',
  CLS: 'CLS',
  FID: 'FID'
};

export const iconSizes = [16, 32, 48, 72, 96, 144, 192, 512];

export const defaultLanguage = 'th';

export const pageType = {
  SERPS: 'serps',
  HOMEPAGE: 'home-page',
  LISTING: 'listing',
  ABOUT_US: 'about-us',
  CONTACT_US: 'contact-us',
  HOW_DOES_IT_WORK: 'how-does-it-work',
  LIST_YOUR_PROPERTY: 'list-your-property',
  PRIVACY: 'privacy',
  PROPERTYPAD: 'propertypad'
};

export const fontWeightOption = [300, 400, 500, 700];

export const filterOptions: Record<string, any>[] = [
  {
    value: 'rents',
    filterLabel: 'rentFilter'
  },
  {
    value: 'sales',
    filterLabel: 'buyFilter'
  }
];
export const LISTINGS_PER_PAGE = 20;
