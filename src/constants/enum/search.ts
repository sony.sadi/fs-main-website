export enum Params {
  Where = 'where',
  BuildingId = 'buildingId',
  Coords = 'coords',
  MinPrice = 'minPrice',
  MaxPrice = 'maxPrice',
  FloorSizeGte = 'floorSize_gte',
  FloorSizeLte = 'floorSize_lte',
  NumberBedrooms = 'numberBedrooms',
  MinLease = 'minLease',
  Amenities = 'amenities',
  // Features = 'features',
  Radius = 'radius',
  PropertyType = 'propertyType',
  SortBy = 'sortBy',
  Page = 'page',
  Limit = 'limit',
  BuildingType = 'building_type'
}

export enum NumberOfBed {
  AnyType = 'any_type',
  OnePlus = '1_plus',
  TwoPlus = '2_plus',
  ThreePlus = '3_plus',
  FourPlus = '4_plus'
}

export enum MinLease {
  Monthly = 'one_month',
  TwoMonths = 'two_months',
  ThreeMonths = 'three_months',
  SixMonths = 'six_months',
  TwelveMonths = 'twelve_months'
}

export enum PropertyType {
  Loft = 'loft',
  Apartment = 'apartment',
  ServicedApartment = 'serviced_apartment',
  Condo = 'condo',
  House = 'house',
  Townhouse = 'townhouse',
  Commercial = 'commercial'
}

export enum Amenity {
  Balcony = 'amenityBalcony',
  WalkInWardrobe = 'amenityWalkInWardrobe',
  StoveCooktop = 'amenityStoveCooktop',
  Refrigerator = 'amenityRefrigerator',
  Tv = 'amenityTv',
  Microwave = 'amenityMicrowave',
  Oven = 'amenityOven',
  Playground = 'amenityPlayground',
  PetFriendly = 'petFriendly',
  Gym = 'amenityGym',
  Garden = 'amenityGarden',
  Parking = 'amenityParking',
  Pool = 'amenitySwimmingPool',
  Jacuzzi = 'amenityJacuzzi',
  Bathtub = 'amenityBathtub'
}

export enum SortType {
  Recommended = '',
  Newest = 'newest',
  PriceLow = 'price_low',
  PriceHigh = 'price_high',
  Distance = 'distance',
  ListingSource = 'listing_source'
}

export enum PoiType {
  AREA = 'poi-areas',
  TRANSPORTATION = 'poi-transportation',
  BUILDING = 'poi-building',
  GOOGLE = 'gpoi'
}

export enum TransportationSystem {
  BTS = 'BTS',
  MRT = 'MRT',
  ARL = 'ARL'
}

export enum BuildingType {
  Apartment = 'apartment',
  ServicedApartment = 'serviced_apartment',
  Condo = 'condo',
  House = 'house',
  Townhouse = 'townhouse',
  Commercial = 'commercial'
}
