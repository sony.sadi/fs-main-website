export enum NumberOfBedrooms {
  OneBedRoom = 'one_bedroom',
  TwoBedRooms = 'two_bedrooms',
  ThreeBedRooms = 'three_bedrooms',
  FourBedRooms = 'four_bedrooms',
  FiveBedRooms = 'five_bedrooms',
  SixBedRooms = 'six_bedrooms',
  SevenBedRooms = 'seven_bedrooms',
  EightBedRooms = 'eight_bedrooms',
  NineBedRooms = 'nine_bedrooms',
  TenBedRooms = 'ten_bedrooms'
}

export enum NumberOfBathroom {
  OneBathRoom = 'one_bathroom',
  TwoBathRooms = 'two_bathrooms',
  ThreeBathRooms = 'three_bathrooms',
  FourBathRooms = 'four_bathrooms',
  FiveBathRooms = 'five_bathrooms',
  SixBathRooms = 'six_bathrooms',
  SevenBathRooms = 'seven_bathrooms',
  EightBathRooms = 'eight_bathrooms',
  NineBathRooms = 'nine_bathrooms',
  TenBathRooms = 'ten_bathrooms'
}

export enum FunishedType {
  Unfurnished = 'unfurnished',
  PartlyFurnished = 'partly_furnished',
  FullyFurnished = 'fully_furnished',
  FullyFurnished_live = 'fully_furnished_live'
}

export enum Tenure {
  Rent = 'rent',
  Sell = 'sell',
  Rentsell = 'rentsell',
  Other = 'other',
  Unknown = 'unknown'
}
