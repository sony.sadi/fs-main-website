export enum LocaleNamespace {
  Common = 'common',
  Listing = 'listing',
  Home = 'home',
  Footer = 'footer',
  HowItWorks = 'how-it-works',
  Contact = 'contact',
  ListYourProperty = 'list-your-property',
  About = 'about',
  Privacy = 'privacy',
  ContactForm = 'contact-form',
  LandingTransportation = 'landing-transportation',
  NotFound = '404'
}

export enum Language {
  US = 'en',
  TH = 'th'
}

export enum FsSelectTheme {
  Dark = 'dark',
  Light = 'light'
}

export enum IconContact {
  Facebook = 'FACEBOOK',
  Line = 'LINE',
  Phone = 'PHONE',
  PhoneOutLine = 'PHONE_OUTLINE',
  MailBox = 'MAIL_BOX'
}

export enum IconSocial {
  Instagram = 'INSTAGRAM',
  Twitter = 'TWITTER',
  Linkedin = 'LINKEDIN'
}

export enum ListingTabs {
  Default = 'DEFAULT',
  Price = 'PRICE',
  Bed = 'BED',
  MoreFilter = 'MORE_FILTERS'
}

export enum Page {
  Home = 'HOME',
  Listings = 'LISTINGS',
  Listing = 'LISTING',
  HowItWorks = 'HOW_IT_WORKS',
  Contact = 'CONTACT',
  ListYourProperty = 'LIST_YOUR_PROPERTY',
  Propertypad = 'PROPERTYPAD',
  Notfound = 'NOTFOUND',
  LandingTransportList = 'LANDING_TRANSPORT_LIST',
  LandingTransportDetail = 'LANDING_TRANSPORT_DETAIL'
}

export enum BasicSearchTheme {
  Light,
  Dark
}
