# syntax=docker/dockerfile:1.2

# Build stage
FROM node:12.16.0-alpine3.11 As development
RUN apk add --no-cache git

WORKDIR /usr/src/build

COPY . .

RUN yarn cache clean
RUN --mount=type=secret,id=npmrc,target=/usr/src/build/.npmrc,required=true yarn

RUN --mount=type=secret,id=env,target=/usr/src/build/.env.cmd.production,required=true yarn build

ENV NODE_ENV production

RUN --mount=type=secret,id=npmrc,target=/usr/src/build/.npmrc,required=true yarn

# Pack Image stage
FROM node:12.16.0-alpine3.11 As production

RUN apk add --no-cache git
# Add dump-init
RUN apk add dumb-init

WORKDIR /usr/src/app


COPY --from=development --chown=node:node /usr/src/build/node_modules ./node_modules
COPY --from=development --chown=node:node /usr/src/build/build ./build
COPY --from=development --chown=node:node /usr/src/build/.next ./.next
COPY --from=development --chown=node:node /usr/src/build/nest* ./
COPY --from=development --chown=node:node /usr/src/build/next* ./
COPY --from=development --chown=node:node /usr/src/build/sentry* ./

USER node

# dumb-int as PID 1
ENTRYPOINT ["dumb-init", "--"]

CMD ["node", "build/main.js"]
