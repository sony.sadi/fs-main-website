# How we setup CI/CD process

## Gitlab pipeline

### Environment variables

```
SSH_PRIVATE_KEY // private ssh key to access to staging server
SSH_USER // ssh user to access to staging server
HOST // staging server
```

We should separate to difference deployment environments.


### Gitlab pipeline

Read more at: https://docs.gitlab.com/ee/ci/pipelines/

The idea is use Gitlab pipeline connecting to server via SSH, run `yarn build && yarn pm2:start:staging` to build and run project with PM2 tool.

Please look at for more detail `.gitlab-ci.yml`

## PM2

For example

```
{
  "apps": [
      {
          "name": "fs-main-website:3900",
          "script": "next",
          "args": "start -p 3900", --> change 3900 to other port if it existed already
          "instances": 1,
          "exec_mode": "fork",
          "listen_timeout": 5000,
          "wait_ready": true,
          "update-env": true,
					"time": true
      }
  ]
}
```