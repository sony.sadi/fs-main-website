// eslint-disable-next-line @typescript-eslint/no-var-requires
const HttpApi = require('i18next-http-backend');

module.exports = {
  i18n: {
    lng: 'en',
    defaultLocale: 'th',
    locales: ['en', 'th'],
    partialBundledLanguages: true,
    defaultNs: 'common',
    ns: [
      'about',
      'common',
      'contact-form',
      'contact',
      'footer',
      'home',
      'how-it-works',
      'list-your-property',
      'listing',
      'privacy',
      'landing-transportation',
      '404'
    ]
  },
  serializeConfig: false,
  use: [HttpApi],
  backend: {
    loadPath: `${process.env.NEXT_PUBLIC_EGSWING_API_BASE_URL}website-locales/{{lng}}/{{ns}}`
  },
  // https://github.com/vercel/next.js/issues/22508#issuecomment-791205161
  react: {
    useSuspense: false
  }
};
